FROM php:7.2-apache

RUN docker-php-ext-install pdo_mysql

RUN echo "extension=apcu.so" > /usr/local/etc/php/conf.d/ext-acpu.ini
RUN pecl install apcu

# enables Apache's rewrite module
RUN a2enmod rewrite

COPY . /var/www/html/

RUN chown www-data:www-data -R /var/www/html

USER www-data
WORKDIR /var/www/html
RUN cp var/env.template.yml var/env.yml

USER root

LABEL traefik.docker.network=traefik_proxy traefik.enable=true traefik.port=80 traefik.frontend.entryPoints=http,https
