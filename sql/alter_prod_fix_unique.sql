ALTER TABLE departments DROP FOREIGN KEY departments_region_code_foreign;
DROP INDEX regions_code_unique ON regions;
CREATE UNIQUE INDEX regions_code_unique ON regions (code);
ALTER TABLE departments ADD CONSTRAINT departments_region_code_foreign FOREIGN KEY (region_code) REFERENCES regions (code) ON DELETE CASCADE;