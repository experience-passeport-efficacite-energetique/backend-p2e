CREATE TABLE `price_ranges` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `unit` ENUM(
    'FIXED',
    'UNIT',
    'SURFACE'
  ) NULL DEFAULT NULL,
  `min` DECIMAL(10,2) NULL DEFAULT NULL,
  `max` DECIMAL(10,2) NULL DEFAULT NULL,
  PRIMARY KEY(`id`)
);

CREATE TABLE `addresses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(255) NULL DEFAULT NULL,
  `zipcode` VARCHAR(10) NULL DEFAULT NULL,
  `city` VARCHAR(100) NULL DEFAULT NULL,
  `department_num` VARCHAR(3) NULL DEFAULT NULL,
  `latitude` FLOAT NULL DEFAULT NULL,
  `longitude` FLOAT NULL DEFAULT NULL,
  `x` DECIMAL(10,2) NULL DEFAULT NULL,
  `y` DECIMAL(10,2) NULL DEFAULT NULL,
  INDEX `addresses__city_idx` ( `city` ASC ),
  INDEX `addresses__department_num_idx` ( `department_num` ASC ),
  PRIMARY KEY (`id`)
);

CREATE TABLE `users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `firstname` VARCHAR(45) NULL DEFAULT NULL,
  `lastname` VARCHAR(45) NULL DEFAULT NULL,
  `phone` VARCHAR(45) NULL DEFAULT NULL,
  INDEX `users__email_idx` ( `email` ASC ),
  INDEX `users__lastname_idx` ( `lastname` ASC ),
  INDEX `users__firstname_idx` ( `firstname` ASC ),
  PRIMARY KEY (`id`)
);

CREATE TABLE `auditors` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY REFERENCES `users` (`id`),
  `address_id` INT NULL,
  `company` VARCHAR(45) NULL,
  `logo` VARCHAR(255) NULL,
  INDEX `auditors__address_idx` (`address_id` ASC),
  CONSTRAINT `fk_auditor__address`
    FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `heatings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rts_index` CHAR NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `ventilations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rts_index` CHAR NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `domestic_hot_waters` (
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
);

CREATE TABLE `passport_walls_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_floor_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_roof_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_vents_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_ventilation_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_airtightness_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'EXCELLENT',
    'GOOD',
    'FAIR',
    'POOR'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_heating_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_thermostat_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER',
    'IRRELEVANT'
  ) NULL DEFAULT NULL
);

CREATE TABLE `passport_dhw_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER'
  ) NULL DEFAULT NULL
);

# DDT : PROBLEME ! LES OPTIONS SONT TROP SPECIFIQUES POUR UN COMPORTEMENT GENERIQUE ENTIEREMENT ADMINISTRABLE (à moins de tomber dans un espèce de wordpress du pauvre...)
# NEANMOINS, les prix doivent être administrables !
# => ON VA OPTER POUR UN typage en ENUM unique dans la table `technical_diagnostics`, pour n'administrer que les prix !
# L'api ne prévoira donc pas de CREATE/DELETE sur les diagnostics, seulement un update sur le champ price.
# A noter que cette solution permettra à des auditeurs de faire appel à leur propre grille de tarif s'ils le souhaitent (cf table `technical_diagnostics_price_ranges`)

CREATE TABLE `passport_tdf_data` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `electrical_wiring_state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `electrical_wiring_last_control` ENUM(
    'NONE',
    '<= 3 YEARS',
    '> 3 YEARS',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `gas_system_state` ENUM(
    'NONE',
    'NEW',
    'USED',
    'FIXER_UPPER',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `gas_system_last_control` ENUM(
    'NONE',
    '<= 3 YEARS',
    '> 3 YEARS',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `plumbing_state` ENUM(
    'NEW',
    'USED',
    'FIXER_UPPER',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `plumbing_last_control` ENUM(
    'NONE',
    '<= 3 YEARS',
    '> 3 YEARS',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `lead_presence` ENUM(
    'YES',
    'NO',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `lead_last_control` ENUM(
    'NONE',
    '<= 3 YEARS',
    '> 3 YEARS',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `asbestos_presence` ENUM(
    'YES',
    'NO',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `asbestos_last_control` ENUM(
    'NONE',
    '<= 3 YEARS',
    '> 3 YEARS',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `termites_presence` ENUM(
    'YES',
    'NO',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `termites_last_control` ENUM(
    'NONE',
    '<= 3 YEARS',
    '> 3 YEARS',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `epcs` ENUM(
    'NONE',
    '<= 10 YEARS',
    '> 10 YEARS',
    'UNKNOWN'
  ) NULL DEFAULT NULL,
  `septic_tank_individual` TINYINT NULL DEFAULT NULL,
  `septic_tank_last_control_year` YEAR NULL DEFAULT NULL,
  `comments` TEXT NULL DEFAULT NULL
);

CREATE TABLE `passports` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `address_id` INT NULL DEFAULT NULL,
  `owner_id` INT NULL DEFAULT NULL,
  `auditor_id` INT NULL DEFAULT NULL,
  `type` ENUM(
    'HOUSE',
    'APARTMENT'
  ) NULL DEFAULT NULL,
  `project` TEXT NULL DEFAULT NULL,
  `climate_zone` ENUM (
    'H1',
    'H2',
    'H3'
  ) NULL DEFAULT NULL,
  `altitude` INT NULL DEFAULT NULL,
  `heated_surface` INT NULL DEFAULT NULL,
  `construction_year` YEAR NULL DEFAULT NULL,
  `surface_type` ENUM(
    'DEVELOPED',
    'RECTANGULAR',
    'ELONGATED'
  ) NULL DEFAULT NULL,
  `adjacency` ENUM(
    'X_LARGE',
    'LARGE',
    'MEDIUM',
    'SMALL',
    'NONE'
  ) NULL DEFAULT NULL,
  `floors` ENUM(
    '1_LEVEL',
    '2_LEVELS_WITH_ATTIC',
    '2_LEVELS',
    '3_LEVELS_WITH_ATTIC',
    '3_LEVELS'
  ) NULL DEFAULT NULL,
  `south_exposure` ENUM(
    'SUFFICIENT',
    'INSUFFICIENT'
  ) NULL DEFAULT NULL,
  `general_comments` TEXT NULL DEFAULT NULL,
  `creation_date` DATETIME NULL DEFAULT NULL,
  `modification_date` DATETIME NULL DEFAULT NULL,
  `walls_data_id` INTEGER NULL DEFAULT NULL,
  `floor_data_id` INTEGER NULL DEFAULT NULL,
  `roof_data_id` INTEGER NULL DEFAULT NULL,
  `vents_data_id` INTEGER NULL DEFAULT NULL,
  `ventilation_data_id` INTEGER NULL DEFAULT NULL,
  `airtightness_data_id` INTEGER NULL DEFAULT NULL,
  `heating_data_id` INTEGER NULL DEFAULT NULL,
  `thermostat_data_id` INTEGER NULL DEFAULT NULL,
  `dhw_data_id` INTEGER NULL DEFAULT NULL,
  `tdf_data_id` INTEGER NULL DEFAULT NULL,
  INDEX `passport__address_idx` (`address_id` ASC),
  INDEX `passport__owner_idx` (`owner_id` ASC),
  INDEX `passport__auditor_idx` (`auditor_id` ASC),
  INDEX `passport__walls_data_idx` (`walls_data_id` ASC),
  INDEX `passport__floor_data_idx` (`floor_data_id` ASC),
  INDEX `passport__roof_data_idx` (`roof_data_id` ASC),
  INDEX `passport__vents_data_idx` (`vents_data_id` ASC),
  INDEX `passport__ventilation_data_idx` (`ventilation_data_id` ASC),
  INDEX `passport__airtightness_data_idx` (`airtightness_data_id` ASC),
  INDEX `passport__heating_data_idx` (`heating_data_id` ASC),
  INDEX `passport__thermostat_data_idx` (`thermostat_data_id` ASC),
  INDEX `passport__dhw_data_idx` (`dhw_data_id` ASC),
  INDEX `passport__tdf_data_idx` (`tdf_data_id` ASC),
  CONSTRAINT `fk_passport__address`
    FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__owner`
    FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__auditor`
    FOREIGN KEY (`auditor_id`) REFERENCES `auditors` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__walls_data`
    FOREIGN KEY (`walls_data_id`) REFERENCES `passport_walls_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__floor_data`
    FOREIGN KEY (`floor_data_id`) REFERENCES `passport_floor_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__roof_data`
    FOREIGN KEY (`roof_data_id`) REFERENCES `passport_roof_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__vents_data`
    FOREIGN KEY (`vents_data_id`) REFERENCES `passport_vents_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__ventilation_data`
    FOREIGN KEY (`ventilation_data_id`) REFERENCES `passport_ventilation_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__airtightness_data`
    FOREIGN KEY (`airtightness_data_id`) REFERENCES `passport_airtightness_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__heating_data`
    FOREIGN KEY (`heating_data_id`) REFERENCES `passport_heating_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__thermostat_data`
    FOREIGN KEY (`thermostat_data_id`) REFERENCES `passport_thermostat_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__dhw_data`
    FOREIGN KEY (`dhw_data_id`) REFERENCES `passport_dhw_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_passport__tdf_data`
    FOREIGN KEY (`tdf_data_id`) REFERENCES `passport_tdf_data` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY (`id`)
);

CREATE TABLE `technical_diagnostics_price_ranges` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` ENUM(
    'ELECTRICAL_WIRING',
    'GAS_SYSTEM',
    'PLUMBING',
    'LEAD',
    'ASBESTOS',
    'TERMITES',
    'EPCS',
    'SEPTIC_TANK'
  ) NULL DEFAULT NULL,
  `price_range_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `leads` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL DEFAULT NULL,
  `passport_id` INT NULL DEFAULT NULL,
  `address_id` INT NULL DEFAULT NULL,
  `lead_origin` VARCHAR(255) NULL DEFAULT NULL,
  `state` ENUM(
    'EN_ATTENTE',
    'ASSIGNE',
    'CONTACTE'
  ) NULL DEFAULT NULL,
  `creation_date` DATETIME NULL DEFAULT NULL,
  `legals_1` TINYINT NULL DEFAULT NULL,
  `legals_2` TINYINT NULL DEFAULT NULL,
  `comments` TEXT NULL DEFAULT NULL,
  INDEX `leads__user_idx` (`user_id` ASC),
  INDEX `leads__passport_idx` (`passport_id` ASC),
  INDEX `leads__address_idx` (`address_id` ASC),
  CONSTRAINT `fk_lead__user`
    FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_lead__passport`
    FOREIGN KEY (`passport_id`) REFERENCES `passports` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_lead__address`
    FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY (`id`)
);

CREATE TABLE `assignments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lead_id` INT NULL DEFAULT NULL,
  `auditor_id` INT NULL DEFAULT NULL,
  `creation_date` DATETIME NULL DEFAULT NULL,
  INDEX `assignments__lead_idx` (`lead_id` ASC),
  INDEX `assignments__auditor_idx` (`auditor_id` ASC),
  CONSTRAINT `fk_assignment__lead`
    FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignment__auditor`
    FOREIGN KEY (`auditor_id`) REFERENCES `auditors` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY(`id`)
);

CREATE TABLE `renovation_technical_scenarios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(10) NULL DEFAULT NULL,
  INDEX `renovation_technical_scenarios__code_idx` (`code` ASC),
  PRIMARY KEY(`id`)
);

CREATE TABLE `renovation_plan_improvements`
(
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `price_range_id` INT NULL DEFAULT NULL,
  `floor_id` INT NULL DEFAULT NULL,
  `floor_state` INT NULL DEFAULT NULL,
  `floor_intervention_year` YEAR NULL DEFAULT NULL,
  `floor_comment` TEXT NULL DEFAULT NULL,
  `floor_selected` TINYINT NULL DEFAULT NULL,
  `roof_id` INT NULL DEFAULT NULL,
  `roof_state` INT NULL DEFAULT NULL,
  `roof_intervention_year` YEAR NULL DEFAULT NULL,
  `roof_comment` TEXT NULL DEFAULT NULL,
  `roof_selected` TINYINT NULL DEFAULT NULL,
  `vents_id` INT NULL DEFAULT NULL,
  `vents_state` INT NULL DEFAULT NULL,
  `vents_intervention_year` YEAR NULL DEFAULT NULL,
  `vents_comment` TEXT NULL DEFAULT NULL,
  `vents_selected` TINYINT NULL DEFAULT NULL,
  `ventilation_id` INT NULL DEFAULT NULL,
  `ventilation_state` INT NULL DEFAULT NULL,
  `ventilation_intervention_year` YEAR NULL DEFAULT NULL,
  `ventilation_comment` TEXT NULL DEFAULT NULL,
  `ventilation_selected` TINYINT NULL DEFAULT NULL,
  `airtightness_id` INT NULL DEFAULT NULL,
  `airtightness_state` INT NULL DEFAULT NULL,
  `airtightness_intervention_year` YEAR NULL DEFAULT NULL,
  `airtightness_comment` TEXT NULL DEFAULT NULL,
  `airtightness_selected` TINYINT NULL DEFAULT NULL,
  `heating_id` INT NULL DEFAULT NULL,
  `heating_state` INT NULL DEFAULT NULL,
  `heating_intervention_year` YEAR NULL DEFAULT NULL,
  `heating_comment` TEXT NULL DEFAULT NULL,
  `heating_selected` TINYINT NULL DEFAULT NULL,
  `thermostat_id` INT NULL DEFAULT NULL,
  `thermostat_state` INT NULL DEFAULT NULL,
  `thermostat_intervention_year` YEAR NULL DEFAULT NULL,
  `thermostat_comment` TEXT NULL DEFAULT NULL,
  `thermostat_selected` TINYINT NULL DEFAULT NULL,
  `dhw_id` INT NULL DEFAULT NULL,
  `dhw_state` INT NULL DEFAULT NULL,
  `dhw_intervention_year` YEAR NULL DEFAULT NULL,
  `dhw_comment` TEXT NULL DEFAULT NULL,
  `dhw_selected` TINYINT NULL DEFAULT NULL
);

CREATE TABLE `renovation_plan_tdf`
(
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `electrical_wiring_state` INT NULL DEFAULT NULL,
  `electrical_wiring_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL,
  `gas_system_state` INT NULL DEFAULT NULL,
  `gas_system_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL,
  `plumbing_state` INT NULL DEFAULT NULL,
  `plumbing_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL,
  `lead_state` INT NULL DEFAULT NULL,
  `lead_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL,
  `asbestos_state` INT NULL DEFAULT NULL,
  `asbestos_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL,
  `termites_state` INT NULL DEFAULT NULL,
  `termites_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL,
  `epcs_state` INT NULL DEFAULT NULL,
  `epcs_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL,
  `septic_tank_state` INT NULL DEFAULT NULL,
  `septic_tank_control` ENUM(
    'UP_TO_DATE',
    'OUTDATED',
    'IRRELEVANT'
  ) NULL DEFAULT NULL
);

CREATE TABLE `renovation_plans` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `passport_id` INT NULL DEFAULT NULL,
  `tdf_price_range_id` INT NULL DEFAULT NULL,
  `improvement_id` INT NULL DEFAULT NULL,
  `tdf_id` INT NULL DEFAULT NULL,
  CONSTRAINT `fk_renovation_plan__passport`
    FOREIGN KEY (`passport_id`) REFERENCES `passports` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_renovation_plan__tdf_price_range`
    FOREIGN KEY (`tdf_price_range_id`) REFERENCES `price_ranges` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_renovation_plan__improvement`
    FOREIGN KEY (`improvement_id`) REFERENCES `renovation_plan_improvements` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_renovation_plan__tdf`
    FOREIGN KEY (`tdf_id`) REFERENCES `renovation_plan_tdf` (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY(`id`)
);
