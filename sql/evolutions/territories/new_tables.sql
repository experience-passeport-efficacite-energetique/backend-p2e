create table territories
(
  id    int PRIMARY KEY AUTO_INCREMENT,
  label varchar(255) null
)
  engine = InnoDB
  collate = utf8_unicode_ci;

CREATE TABLE territories_cities
(
  id int PRIMARY KEY AUTO_INCREMENT,
  territory_id int,
  city_id int(10) unsigned
)
  engine = InnoDB
  collate = utf8_unicode_ci;

CREATE INDEX territories_cities__territory_id_idx ON territories_cities (territory_id);
CREATE INDEX territories_cities__city_id_idx ON territories_cities (city_id);

ALTER TABLE territories_cities ADD CONSTRAINT fk_territories_cities__territories FOREIGN KEY (territory_id) REFERENCES territories (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE territories_cities ADD CONSTRAINT fk_territories_cities__cities FOREIGN KEY (city_id) REFERENCES cities (id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE territories_users
(
  id int PRIMARY KEY AUTO_INCREMENT,
  territory_id int,
  user_id int
)
  engine = InnoDB
  collate = utf8_unicode_ci;

CREATE INDEX territories_users__territory_id_idx ON territories_users (territory_id);
CREATE INDEX territories_users__user_id_idx ON territories_users (user_id);

ALTER TABLE territories_users ADD CONSTRAINT fk_territories_users__territories FOREIGN KEY (territory_id) REFERENCES territories (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE territories_users ADD CONSTRAINT fk_territories_users__users FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE addresses ADD COLUMN citycode varchar(5) AFTER city;
ALTER TABLE addresses ADD COLUMN city_id int(10) UNSIGNED AFTER citycode;

CREATE INDEX addresses__city_id_idx ON addresses (city_id);
ALTER TABLE addresses ADD CONSTRAINT fk_addresses__cities FOREIGN KEY (city_id) REFERENCES cities (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE territories_addresses
(
  id int PRIMARY KEY AUTO_INCREMENT,
  territory_id int,
  address_id int
)
  engine = InnoDB
  collate = utf8_unicode_ci;

CREATE INDEX territories_addresses__territory_id_idx ON territories_addresses (territory_id);
CREATE INDEX territories_addresses__address_id_idx ON territories_addresses (address_id);

ALTER TABLE territories_addresses ADD CONSTRAINT fk_territories_addresses__territories FOREIGN KEY (territory_id) REFERENCES territories (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE territories_addresses ADD CONSTRAINT fk_territories_addresses__addresses FOREIGN KEY (address_id) REFERENCES addresses (id) ON DELETE CASCADE ON UPDATE CASCADE;