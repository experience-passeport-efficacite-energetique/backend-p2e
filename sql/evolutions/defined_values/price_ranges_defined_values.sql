ALTER TABLE price_ranges ADD defined_min double NULL;
ALTER TABLE price_ranges ADD defined_max double NULL;
UPDATE price_ranges SET defined_min = min;
UPDATE price_ranges SET defined_max = max;