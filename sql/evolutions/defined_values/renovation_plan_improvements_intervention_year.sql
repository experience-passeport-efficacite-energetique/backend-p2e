ALTER TABLE renovation_plan_improvements CHANGE walls_intervention_year walls_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE floor_intervention_year floor_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE roof_intervention_year roof_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE vents_intervention_year vents_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE ventilation_intervention_year ventilation_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE airtightness_intervention_year airtightness_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE heating_intervention_year heating_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE thermostat_intervention_year thermostat_proposed_intervention_year int(11);
ALTER TABLE renovation_plan_improvements CHANGE dhw_intervention_year dhw_proposed_intervention_year int(11);

ALTER TABLE renovation_plan_improvements ADD dhw_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD thermostat_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD heating_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD airtightness_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD ventilation_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD vents_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD roof_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD floor_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements ADD walls_defined_intervention_year int(11) NULL;
ALTER TABLE renovation_plan_improvements
  MODIFY COLUMN walls_defined_intervention_year int(11) AFTER walls_proposed_intervention_year,
  MODIFY COLUMN floor_defined_intervention_year int(11) AFTER floor_proposed_intervention_year,
  MODIFY COLUMN roof_defined_intervention_year int(11) AFTER roof_proposed_intervention_year,
  MODIFY COLUMN vents_defined_intervention_year int(11) AFTER vents_proposed_intervention_year,
  MODIFY COLUMN ventilation_defined_intervention_year int(11) AFTER ventilation_proposed_intervention_year,
  MODIFY COLUMN airtightness_defined_intervention_year int(11) AFTER airtightness_proposed_intervention_year,
  MODIFY COLUMN heating_defined_intervention_year int(11) AFTER heating_proposed_intervention_year,
  MODIFY COLUMN thermostat_defined_intervention_year int(11) AFTER thermostat_proposed_intervention_year,
  MODIFY COLUMN dhw_defined_intervention_year int(11) AFTER dhw_proposed_intervention_year;

ALTER TABLE renovation_plan_improvements ADD dhw_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD thermostat_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD heating_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD airtightness_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD ventilation_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD vents_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD roof_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD floor_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements ADD walls_force_intervention_year tinyint(1) DEFAULT 0;
ALTER TABLE renovation_plan_improvements
  MODIFY COLUMN walls_force_intervention_year tinyint(1) AFTER walls_proposed_intervention_year,
  MODIFY COLUMN floor_force_intervention_year tinyint(1) AFTER floor_proposed_intervention_year,
  MODIFY COLUMN roof_force_intervention_year tinyint(1) AFTER roof_proposed_intervention_year,
  MODIFY COLUMN vents_force_intervention_year tinyint(1) AFTER vents_proposed_intervention_year,
  MODIFY COLUMN ventilation_force_intervention_year tinyint(1) AFTER ventilation_proposed_intervention_year,
  MODIFY COLUMN airtightness_force_intervention_year tinyint(1) AFTER airtightness_proposed_intervention_year,
  MODIFY COLUMN heating_force_intervention_year tinyint(1) AFTER heating_proposed_intervention_year,
  MODIFY COLUMN thermostat_force_intervention_year tinyint(1) AFTER thermostat_proposed_intervention_year,
  MODIFY COLUMN dhw_force_intervention_year tinyint(1) AFTER dhw_proposed_intervention_year;

UPDATE renovation_plan_improvements SET walls_defined_intervention_year = walls_proposed_intervention_year;
UPDATE renovation_plan_improvements SET floor_defined_intervention_year = floor_proposed_intervention_year;
UPDATE renovation_plan_improvements SET roof_defined_intervention_year = roof_proposed_intervention_year;
UPDATE renovation_plan_improvements SET vents_defined_intervention_year = vents_proposed_intervention_year;
UPDATE renovation_plan_improvements SET ventilation_defined_intervention_year = ventilation_proposed_intervention_year;
UPDATE renovation_plan_improvements SET airtightness_defined_intervention_year = airtightness_proposed_intervention_year;
UPDATE renovation_plan_improvements SET heating_defined_intervention_year = heating_proposed_intervention_year;
UPDATE renovation_plan_improvements SET thermostat_defined_intervention_year = thermostat_proposed_intervention_year;
UPDATE renovation_plan_improvements SET dhw_defined_intervention_year = dhw_proposed_intervention_year;