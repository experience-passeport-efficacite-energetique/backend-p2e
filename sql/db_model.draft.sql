-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `firstname` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  PRIMARY KEY (`id`)
);


-- -----------------------------------------------------
-- Table `address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `address`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(255) NULL,
  `zipcode` VARCHAR(10) NULL,
  `city` VARCHAR(100) NULL,
  `department_num` VARCHAR(3) NULL,
  `latitude` FLOAT NULL,
  `longitude` FLOAT NULL,
  `x` DECIMAL(10,2) NULL,
  `y` DECIMAL(10,2) NULL,
  PRIMARY KEY (`id`)
);


-- -----------------------------------------------------
-- Table `auditor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auditor`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `company` VARCHAR(45) NULL,
  `logo` VARCHAR(255) NULL,
  `address_id` INT NULL,
  `users_id` INT NOT NULL,
  INDEX `id_address_idx` (`address_id` ASC),
  INDEX `id_idx` (`users_id` ASC),
  CONSTRAINT `fk_auditor_address_id`
    FOREIGN KEY (`address_id`)
    REFERENCES `address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_auditor_user_id`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `abstract_passport_block`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `abstract_passport_block`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `comments` TEXT NULL,
  `state` TINYINT(1) NULL,
  `planned_work_year` INT NULL,
  PRIMARY KEY (`id`)
);


-- -----------------------------------------------------
-- Table `cost_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cost_data`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `cost_unit` INT NOT NULL COMMENT 'ENUM: HT/U, HT/m2, etc',
  `min_cost` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  `max_cost` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`id`)
);


-- -----------------------------------------------------
-- Table `abstract_installation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `abstract_installation`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `before_work` TINYINT NOT NULL DEFAULT 0,
  `after_work` TINYINT NOT NULL DEFAULT 0,
  `cost_data_id` INT NULL,
  `label` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `cost_data_id_idx` (`cost_data_id` ASC),
  CONSTRAINT `cost_data_id`
    FOREIGN KEY (`cost_data_id`)
    REFERENCES `cost_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `heating_installation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heating_installation` (
  `id` INT NOT NULL,
  CONSTRAINT `fk_heating_id`
    FOREIGN KEY (`id`)
    REFERENCES `abstract_installation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  PRIMARY KEY (`id`)
);


-- -----------------------------------------------------
-- Table `heating_passport_block`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heating_passport_block` (
  `id` INT NOT NULL,
  `actual_heating_installation_id` INT NOT NULL,
  `heating_renewal` INT NOT NULL COMMENT 'ENUM',
  `renewal_year` INT NULL,
  `maintenance_frequency` INT NOT NULL COMMENT 'ENUM',
  INDEX `fh_heating_installation_id_idx` (`actual_heating_installation_id` ASC),
  CONSTRAINT `fk_heating_passport_block_id`
    FOREIGN KEY (`id`)
    REFERENCES `abstract_passport_block` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fh_heating_installation_id`
    FOREIGN KEY (`actual_heating_installation_id`)
    REFERENCES `heating_installation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  PRIMARY KEY (`id`)
);


-- -----------------------------------------------------
-- Table `passport`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `passport`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `auditor_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `address_id` INT NOT NULL,
  `creation_date` DATETIME NOT NULL,
  `update_date` DATETIME NOT NULL,
  `heating_block_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`auditor_id` ASC),
  INDEX `id_idx1` (`users_id` ASC),
  INDEX `id_idx2` (`address_id` ASC),
  INDEX `heating°block_id_idx` (`heating_block_id` ASC),
  CONSTRAINT `auditor_id`
    FOREIGN KEY (`auditor_id`)
    REFERENCES `auditor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `users_id`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `address_id`
    FOREIGN KEY (`address_id`)
    REFERENCES `address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `heating_block_id`
    FOREIGN KEY (`heating_block_id`)
    REFERENCES `heating_passport_block` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `lead`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lead` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `address_id` INT NOT NULL,
  `lead_origin` VARCHAR(255) NULL DEFAULT 'Voir valeurs dans le formulaire',
  `lead_origin_other` VARCHAR(255) NULL,
  `state` VARCHAR(45) NOT NULL DEFAULT 'EN_ATTENTE, ASSIGNE, CONTACTE',
  `creation_date` DATETIME NOT NULL,
  `passport_id` INT NULL,
  `comments` TEXT NULL,
  `legals_1` TINYINT NOT NULL,
  `legals_2` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`address_id` ASC),
  INDEX `id_idx1` (`users_id` ASC),
  CONSTRAINT `id`
    FOREIGN KEY (`address_id`)
    REFERENCES `address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id`
    FOREIGN KEY (`id`)
    REFERENCES `passport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `assignement`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `assignement`
(
  `id` INT NOT NULL,
  `lead_id` INT NOT NULL,
  `auditor_id` INT NOT NULL,
  `assignement_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_lead_has_auditor_auditor1_idx` (`auditor_id` ASC),
  INDEX `fk_lead_has_auditor_lead1_idx` (`lead_id` ASC),
  CONSTRAINT `fk_lead_has_auditor_lead1`
    FOREIGN KEY (`lead_id`)
    REFERENCES `lead` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lead_has_auditor_auditor1`
    FOREIGN KEY (`auditor_id`)
    REFERENCES `auditor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `ddt_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ddt_type`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(255) NOT NULL,
  `cost_data_id` INT NOT NULL,
  `order` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_cost_data_id_idx` (`cost_data_id` ASC),
  CONSTRAINT `fk_cost_data_id`
    FOREIGN KEY (`cost_data_id`)
    REFERENCES `cost_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `ddt_entry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ddt_entry`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ddt_type_id` INT NOT NULL,
  `state` INT NULL DEFAULT NULL,
  `control_date` INT NOT NULL DEFAULT 0,
  `result` VARCHAR(45) NULL,
  `passport_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`passport_id` ASC),
  CONSTRAINT `fk_passport_id`
    FOREIGN KEY (`passport_id`)
    REFERENCES `passport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ddt_type`
    FOREIGN KEY (`ddt_type_id`)
    REFERENCES `ddt_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `abstract_audit_result_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `abstract_audit_result_item`
(
  `id` INT NOT NULL,
  `abstract_installation_id` INT NOT NULL,
  `is_selected` TINYINT NOT NULL DEFAULT 0,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_abstract_installation_id_idx` (`abstract_installation_id` ASC),
  CONSTRAINT `fk_abstract_installation_id`
    FOREIGN KEY (`abstract_installation_id`)
    REFERENCES `abstract_installation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `audit_result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `audit_result`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `passport_id` INT NOT NULL,
  `creation_date` DATETIME NOT NULL,
  `update_date` DATETIME NOT NULL,
  `state` VARCHAR(45) NULL,
  `title` VARCHAR(255) NOT NULL COMMENT 'Pas en V1',
  `heating_installation_id` INT NOT NULL,
  `energy_consumption_actual` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  `energy_consumption_after` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  `energy_consumption_ideal` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  `improvements_costs_min` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  `improvements_costs_max` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  `ddt_costs_min` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  `ddt_costs_max` DECIMAL(10,2) NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`id`),
  INDEX `passport_id_UNIQUE` (`passport_id` ASC),
  INDEX `fk_heating_installation_id_idx` (`heating_installation_id` ASC),
  CONSTRAINT `passport_id`
    FOREIGN KEY (`passport_id`)
    REFERENCES `passport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_heating_installation_id`
    FOREIGN KEY (`heating_installation_id`)
    REFERENCES `abstract_audit_result_item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


-- -----------------------------------------------------
-- Table `audit_result_ddt_entries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `audit_result_ddt_entries`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `audit_result_id` INT NOT NULL,
  `ddt_entry_id` INT NOT NULL,
  `selected` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_audit_result_id_idx` (`audit_result_id` ASC),
  INDEX `fk_ddt_entry_id_idx` (`ddt_entry_id` ASC),
  CONSTRAINT `fk_audit_result_id`
    FOREIGN KEY (`audit_result_id`)
    REFERENCES `audit_result` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ddt_type_id`
    FOREIGN KEY (`ddt_entry_id`)
    REFERENCES `ddt_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
