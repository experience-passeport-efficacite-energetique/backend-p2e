<?php
require_once '../vendor/autoload.php';
use SebastianBergmann\CodeCoverage\CodeCoverage;

$coverage = new CodeCoverage;
$coverage->filter()->addDirectoryToWhitelist('../src');
$coverage->filter()->removeDirectoryFromWhitelist('../src/Command');
$coverage->filter()->removeDirectoryFromWhitelist('../src/Enum');
$coverage->filter()->removeFileFromWhitelist('../src/console.php');
$coverage->filter()->removeFileFromWhitelist('../src/Service/StaticDataService.php');
$coverage->filter()->removeFileFromWhitelist('../src/Controller/RESTfulAPIController.php');
$coverage->filter()->removeFileFromWhitelist('../src/Service/Provider/SchemaVersionControlServiceProvider.php');
$coverage->start('API coverage');
