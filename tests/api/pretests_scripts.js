if (pm.environment.get("is_initialized") !== "true") {
    pm.sendRequest({
        url: 'https://api-adresse.data.gouv.fr/search/?q=10%20rue%20Gabrielle%20Josserand%20Pantin&type=housenumber&autocomplete=0',
        method: 'GET'
    }, function (err, res) {
        if (res.code !== 200) return;
        let addr = res.json().features[0];
        userAddr = {
            latitude: addr.geometry.coordinates[0],
            longitude: addr.geometry.coordinates[1],
            address: addr.properties.name,
            zipcode: addr.properties.postcode,
            city: addr.properties.city,
            citycode: addr.properties.citycode,
            departmentNumber: addr.properties.citycode.substr(0, 2),
            legalX: addr.properties.x,
            legalY: addr.properties.y
        };
        pm.environment.set("user_address", JSON.stringify(userAddr));
    });

    priceRangeSchema = {
        "type": "object",
        "properties": {
            "unit": {
                "$id": "/properties/priceRange/properties/unit",
                "type": "string",
                "enum": ["FIXED", "UNIT", "SURFACE"],
                "title": "The Unit Schema ",
                "default": ""
            },
            "min": {
                "$id": "/properties/priceRange/properties/min",
                "type": "integer",
                "title": "The Min Schema ",
                "default": 0
            },
            "max": {
                "$id": "/properties/priceRange/properties/max",
                "type": "integer",
                "title": "The Max Schema ",
                "default": 0
            },
            "definedMin": {
                "$id": "/properties/priceRange/properties/definedMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The Defined Min Schema ",
                "default": 0
            },
            "definedMax": {
                "$id": "/properties/priceRange/properties/definedMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The Defined Max Schema ",
                "default": 0
            }
        },
        "required": ["unit", "min", "max", "definedMin", "definedMax"]
    };
    pm.environment.set("priceRangeSchema", priceRangeSchema);

    dhwAndHeatingSchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema ",
                "default": 0
            },
            "label": {
                "$id": "/properties/label",
                "type": "string",
                "title": "The Label Schema ",
                "default": ""
            },
            "priceRange": {
                "$id": "/properties/priceRange",
                "type": ["null", "object"],
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "available": {
                "$id": "/properties/available",
                "type": "boolean",
                "title": "The Available Schema ",
                "default": false
            },
            "resistance": {
                "$id": "/properties/resistance",
                "type": "number",
                "title": "The Resistance Schema ",
                "default": 0
            },
            "apci": {
                "$id": "/properties/apci",
                "type": "number",
                "title": "The Apci Schema ",
                "default": 0
            },
            "longevity": {
                "$id": "/properties/longevity",
                "type": ["null", "integer"],
                "title": "The Longevity Schema ",
                "default": 0
            },
            "emissionFactor": {
                "$id": "/properties/emissionFactor",
                "type": "number",
                "title": "The emissionFactor Schema ",
                "default": 0
            },
            "energyNeeds": {
                "$id": "/properties/energyNeeds",
                "type": "number",
                "title": "The energyNeeds Schema ",
                "default": 0
            },
            "cep": {
                "$id": "/properties/cep",
                "type": "number",
                "title": "The cep Schema ",
                "default": 0
            },
            "cef": {
                "$id": "/properties/cef",
                "type": "number",
                "title": "The cef Schema ",
                "default": 0
            },
            "ges": {
                "$id": "/properties/ges",
                "type": "number",
                "title": "The Ges Schema ",
                "default": 0
            },
            "primaryEnergy": {
                "$id": "/properties/primaryEnergy",
                "type": "number",
                "title": "The Primaryenergy Schema ",
                "default": 0
            }
        },
        "required": ["id", "label", "available"]
    };
    pm.environment.set("domesticHotWatersSchema", dhwAndHeatingSchema);
    pm.environment.set("heatingSchema", dhwAndHeatingSchema);

    ventilationSchema = {
        "type": "object",
        "additionalProperties": true,
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema ",
                "default": 0
            },
            "label": {
                "$id": "/properties/label",
                "type": "string",
                "title": "The Label Schema ",
                "default": ""
            },
            "priceRange": {
                "$id": "/properties/priceRange",
                "type": ["null", "object"],
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "available": {
                "$id": "/properties/available",
                "type": "boolean",
                "title": "The Available Schema ",
                "default": false
            },
            "resistance": {
                "$id": "/properties/resistance",
                "type": "number",
                "title": "The Resistance Schema ",
                "default": 0
            },
            "longevity": {
                "$id": "/properties/longevity",
                "type": ["null", "integer"],
                "title": "The Longevity Schema ",
                "default": 0
            }
        },
        "required": ["id", "label", "available"]
    };
    pm.environment.set("ventilationSchema", ventilationSchema);

    citySchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema ",
                "default": 0
            },
            "departmentCode": {
                "$id": "/properties/departmentCode",
                "type": "string",
                "title": "The departmentCode Schema ",
                "default": ""
            },
            "inseeCode": {
                "$id": "/properties/inseeCode",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The inseeCode Schema",
                "default": null
            },
            "zipCode": {
                "$id": "/properties/zipCode",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The zipCode Schema",
                "default": null
            },
            "name": {
                "$id": "/properties/name",
                "type": "string",
                "title": "The name Schema ",
                "default": ""
            },
            "slug": {
                "$id": "/properties/slug",
                "type": "string",
                "title": "The slug Schema ",
                "default": ""
            },
            "gpsLat": {
                "$id": "/properties/gpsLat",
                "type": "number",
                "title": "The gpsLat Schema ",
                "default": ""
            },
            "gpsLng": {
                "$id": "/properties/gpsLng",
                "type": "number",
                "title": "The gpsLng Schema ",
                "default": ""
            }
        },
        "required": [
            "id",
            "inseeCode",
            "name"
        ]
    };
    pm.environment.set("citySchema", citySchema);

    addressSchema = {
        "type": "object",
        "additionalProperties": true,
        "properties": {
            "id": {
                "$id": "/properties/address/properties/id",
                "type": "integer",
                "title": "The Id Schema"
            },
            "address": {
                "$id": "/properties/address/properties/address",
                "type": "string",
                "title": "The Address Schema"
            },
            "zipcode": {
                "$id": "/properties/address/properties/zipcode",
                "type": "string",
                "title": "The Zipcode Schema"
            },
            "city": {
                "$id": "/properties/address/properties/city",
                "type": "string",
                "title": "The City Schema"
            },
            "cityObject": {
                "$id": "/properties/cityObject",
                "type": ["null", "object"],
                "additionalProperties": true,
                "title": "The City Schema",
                "properties": citySchema.properties,
                "required": citySchema.required
            },
            "citycode": {
                "$id": "/properties/address/properties/citycode",
                "type": "string",
                "title": "The Citycode (INSEE code) Schema"
            },
            "departmentNumber": {
                "$id": "/properties/address/properties/departmentNumber",
                "type": "string",
                "title": "The Departmentnumber Schema"
            },
            "latitude": {
                "$id": "/properties/address/properties/latitude",
                "type": "number",
                "title": "The Latitude Schema"
            },
            "longitude": {
                "$id": "/properties/address/properties/longitude",
                "type": "number",
                "title": "The Longitude Schema"
            },
            "legalX": {
                "$id": "/properties/address/properties/legalX",
                "type": "number",
                "title": "The Legalx Schema"
            },
            "legalY": {
                "$id": "/properties/address/properties/legalY",
                "type": "number",
                "title": "The Legaly Schema"
            }
        },
        "required": [
            "address",
            "zipcode",
            "city",
            "departmentNumber",
            "latitude",
            "longitude",
            "legalX",
            "legalY"
        ]
    };
    pm.environment.set("addressSchema", addressSchema);

    userSchema = {
        "type": "object",
        "additionalProperties": true,
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema"
            },
            "address": {
                "$id": "/properties/address",
                "type": ["null", "object"],
                "additionalProperties": true,
                "properties": addressSchema.properties,
                "required": addressSchema.required
            },
            "email": {
                "$id": "/properties/email",
                "type": "string",
                "title": "The Email Schema"
            },
            "firstname": {
                "$id": "/properties/firstname",
                "type": ["null", "string"],
                "title": "The Firstname Schema"
            },
            "lastname": {
                "$id": "/properties/lastname",
                "type": ["null", "string"],
                "title": "The Lastname Schema"
            },
            "company": {
                "$id": "/properties/company",
                "type": ["null", "string"],
                "title": "The Company Schema"
            },
            "phone": {
                "$id": "/properties/phone",
                "type": ["null", "string"],
                "title": "The Phone Schema"
            },
            "roles": {
                "$id": "/properties/roles",
                "type": "array",
                "items": {
                    "$id": "/properties/roles/items",
                    "type": "string",
                    "title": "The role Schema",
                    "enum": [
                        "Admin", "User", "Auditor", "Admin territory", "User territory"
                    ]
                }
            }
        },
        "required": [
            "id",
            "email",
            "roles"
        ]
    };
    pm.environment.set("userSchema", userSchema);

    var leadSchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema ",
                "default": 0
            },
            "owner": {
                "$id": "/properties/owner",
                "type": "object",
                "properties": userSchema.properties,
                "required": userSchema.required
            },
            "address": {
                "$id": "/properties/address",
                "type": "object",
                "properties": addressSchema.properties,
                "required": addressSchema.required
            },
            "leadOrigin": {
                "$id": "/properties/leadOrigin",
                "type": "string",
                "title": "The LeadOrigin Schema "
            },
            "state": {
                "$id": "/properties/state",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The state Schema "
            },
            "creationDate": {
                "$id": "/properties/creationDate",
                "type": "string",
                "format": "date-time",
                "title": "The creationDate Schema "
            },
            "assigned": {
                "$id": "/properties/assigned",
                "type": "boolean",
                "title": "The Assigned Schema ",
                "default": false
            },
            "legals1": {
                "$id": "/properties/legals1",
                "type": "boolean",
                "title": "The Legals1 Schema ",
                "default": true
            },
            "legals2": {
                "$id": "/properties/legals2",
                "type": "boolean",
                "title": "The Legals2 Schema ",
                "default": true
            },
            "comments": {
                "$id": "/properties/comments",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The Comments Schema "
            },
            "passport": {
                "$id": "/properties/passport",
                "type": [
                    "null",
                    "object"
                ],
                "properties": {},
                "required": []
            }
        },
        "required": [
            "id",
            "owner",
            "address",
            "creationDate",
            "legals1",
            "legals2",
            "assigned"
        ]
    };
    pm.environment.set("leadSchema", leadSchema);

    var combinatorySchema = {
        "$id": "https://api.app.passeport-efficacite-energetique.org/combinatories",
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema ",
                "default": 0,
                "examples": [
                    406
                ]
            },
            "number": {
                "$id": "/properties/number",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The number Schema ",
                "default": 0,
                "examples": [
                    1
                ]
            },
            "code": {
                "$id": "/properties/code",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The code Schema ",
                "default": 0,
                "examples": [
                    1
                ]
            },
            "level": {
                "$id": "/properties/level",
                "type": "string",
                "title": "The level Schema ",
                "default": "",
                "enum": [
                    "A",
                    "B",
                    "Seuil Eco-PTZ"
                ],
                "examples": [
                    "A",
                    "B",
                    "Seuil Eco-PTZ"
                ]
            },
            "protected": {
                "$id": "/properties/protected",
                "type": "boolean",
                "title": "The Protected Schema ",
                "default": false,
                "examples": [
                    true, false
                ]
            },
            "zone": {
                "$id": "/properties/zone",
                "type": [
                    "null",
                    "string"
                ],
                "enum": [
                    null,
                    "H1",
                    "H2",
                    "H3"
                ],
                "title": "The Climatezone Schema ",
                "default": "H1",
                "examples": [
                    "H1",
                    "H2",
                    "H3"
                ]
            },
            "insulation": {
                "$id": "/properties/insulation",
                "type": [
                    "null",
                    "string"
                ],
                "enum": [
                    null,
                    "ETI",
                    "ITI"
                ],
                "title": "The Insulation Schema ",
                "default": "ITI",
                "examples": [
                    "ETI",
                    "ITI"
                ]
            },
            "airtightness": {
                "$id": "/properties/airtightness",
                "type": [
                    "null",
                    "string"
                ],
                "enum": [
                    null,
                    "GOOD",
                    "VERY_GOOD"
                ],
                "title": "The Airtightness Schema ",
                "default": "GOOD",
                "examples": [
                    "GOOD",
                    "VERY_GOOD"
                ]
            },
            "ventilation": {
                "$id": "/properties/ventilation",
                "type": [
                    "null",
                    "object"
                ],
                "properties": {
                    "id": {
                        "$id": "/properties/ventilation/properties/id",
                        "type": "integer",
                        "title": "The Id Schema ",
                        "default": 0
                    }
                }
            },
            "heating": {
                "$id": "/properties/heating",
                "type": [
                    "null",
                    "object"
                ],
                "properties": {
                    "id": {
                        "$id": "/properties/heating/properties/id",
                        "type": "integer",
                        "title": "The Id Schema ",
                        "default": 0
                    }
                }
            },
            "dhw": {
                "$id": "/properties/dhw",
                "type": [
                    "null",
                    "object"
                ],
                "properties": {
                    "id": {
                        "$id": "/properties/dhw/properties/id",
                        "type": "integer",
                        "title": "The Id Schema ",
                        "default": 0
                    }
                }
            },
            "wallsValue": {
                "$id": "/properties/wallsValue",
                "type": "number",
                "title": "The Wallsvalue Schema ",
                "default": 0,
                "examples": [
                    4.5
                ]
            },
            "wallsPriceRange": {
                "$id": "/properties/wallsPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "ventsValue": {
                "$id": "/properties/ventsValue",
                "type": "number",
                "title": "The Ventsvalue Schema ",
                "default": 0,
                "examples": [
                    1.39
                ]
            },
            "ventsPriceRange": {
                "$id": "/properties/ventsPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "floorValue": {
                "$id": "/properties/floorValue",
                "type": "number",
                "title": "The Floorvalue Schema ",
                "default": 0,
                "examples": [
                    3.1
                ]
            },
            "floorPriceRange": {
                "$id": "/properties/floorPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "roofValue": {
                "$id": "/properties/roofValue",
                "type": "number",
                "title": "The Roofvalue Schema ",
                "default": 0,
                "examples": [
                    7.5
                ]
            },
            "roofPriceRange": {
                "$id": "/properties/roofPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            }
        },
        "required": [
            "id",
            "number",
            "level",
            "code",
            "protected",
            "zone",
            "insulation",
            "airtightness",
            "ventilation",
            "heating",
            "wallsValue",
            "wallsPriceRange",
            "ventsValue",
            "ventsPriceRange",
            "floorValue",
            "floorPriceRange",
            "roofValue",
            "roofPriceRange"
        ]
    };

    pm.environment.set("combinatorySchema", combinatorySchema);

    var combinatoryEcoPTZSchema = {
        "$id": "https://api.app.passeport-efficacite-energetique.org/combinatories",
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema ",
                "default": 0,
                "examples": [
                    406
                ]
            },
            "number": {
                "$id": "/properties/number",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The number Schema ",
                "default": 0,
                "examples": [
                    1
                ]
            },
            "code": {
                "$id": "/properties/code",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The code Schema ",
                "default": 0,
                "examples": [
                    1
                ]
            },
            "level": {
                "$id": "/properties/level",
                "type": "string",
                "title": "The level Schema ",
                "default": "",
                "enum": [
                    "A",
                    "B",
                    "Seuil Eco-PTZ"
                ],
                "examples": [
                    "A",
                    "B",
                    "Seuil Eco-PTZ"
                ]
            },
            "protected": {
                "$id": "/properties/protected",
                "type": "boolean",
                "title": "The Protected Schema ",
                "default": false,
                "examples": [
                    true
                ]
            },
            "zone": {
                "$id": "/properties/zone",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The Climatezone Schema ",
                "maxLength": 0
            },
            "insulation": {
                "$id": "/properties/insulation",
                "type": [
                    "null",
                    "string"
                ],
                "maxLength": 0,
                "title": "The Insulation Schema "
            },
            "airtightness": {
                "$id": "/properties/airtightness",
                "type": [
                    "string",
                    "null"
                ],
                "maxLength": 0,
                "title": "The Airtightness Schema "
            },
            "ventilation": {
                "$id": "/properties/ventilation",
                "type": "null"
            },
            "heating": {
                "$id": "/properties/heating",
                "type": "null"
            },
            "wallsValue": {
                "$id": "/properties/wallsValue",
                "type": "number",
                "title": "The Wallsvalue Schema ",
                "default": 0,
                "examples": [
                    4.5
                ]
            },
            "wallsPriceRange": {
                "$id": "/properties/wallsPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "ventsValue": {
                "$id": "/properties/ventsValue",
                "type": "number",
                "title": "The Ventsvalue Schema ",
                "default": 0,
                "examples": [
                    1.39
                ]
            },
            "ventsPriceRange": {
                "$id": "/properties/ventsPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "floorValue": {
                "$id": "/properties/floorValue",
                "type": "number",
                "title": "The Floorvalue Schema ",
                "default": 0,
                "examples": [
                    3.1
                ]
            },
            "floorPriceRange": {
                "$id": "/properties/floorPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            },
            "roofValue": {
                "$id": "/properties/roofValue",
                "type": "number",
                "title": "The Roofvalue Schema ",
                "default": 0,
                "examples": [
                    7.5
                ]
            },
            "roofPriceRange": {
                "$id": "/properties/roofPriceRange",
                "type": "object",
                "properties": priceRangeSchema.properties,
                "required": priceRangeSchema.required
            }
        },
        "required": [
            "id",
            "level",
            "protected",
            "zone",
            "insulation",
            "airtightness",
            "ventilation",
            "heating",
            "wallsValue",
            "wallsPriceRange",
            "ventsValue",
            "ventsPriceRange",
            "floorValue",
            "floorPriceRange",
            "roofValue",
            "roofPriceRange"
        ]
    };
    pm.environment.set("combinatoryEcoPTZSchema", combinatoryEcoPTZSchema);

    var passportSchema = {
        "$id": "https://api.app.passeport-efficacite-energetique.org/passports",
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema "
            },
            "lead": {
                "$id": "/properties/lead",
                "type": "object",
                "properties": leadSchema.properties,
                "required": ["id", "creationDate", "legals1", "legals2", "assigned"]
            },
            "owner": {
                "$id": "/properties/owner",
                "type": ["object"],
                "additionalProperties": true,
                "properties": userSchema.properties,
                "required": userSchema.required
            },
            "auditor": {
                "$id": "/properties/auditor",
                "type": ["null", "object"],
                "additionalProperties": true,
                "properties": userSchema.properties,
                "required": userSchema.required
            },
            "address": {
                "$id": "/properties/address",
                "type": ["null", "object"],
                "additionalProperties": true,
                "properties": addressSchema.properties,
                "required": addressSchema.required
            },
            "type": {
                "$id": "/properties/type",
                "type": "string",
                "enum": [
                    "HOUSE",
                    "APARTMENT"
                ],
                "title": "The Type Schema ",
                "default": "HOUSE",
                "examples": [
                    "HOUSE",
                    "APARTMENT"
                ]
            },
            "project": {
                "$id": "/properties/project",
                "type": "string",
                "title": "The Project Schema ",
                "default": "",
                "examples": [
                    "Rénovation d'une maison de 1990"
                ]
            },
            "climateZone": {
                "$id": "/properties/climateZone",
                "type": "string",
                "enum": [
                    "H1",
                    "H2",
                    "H3"
                ],
                "title": "The Climatezone Schema ",
                "default": "H1",
                "examples": [
                    "H1",
                    "H2",
                    "H3"
                ]
            },
            "altitude": {
                "$id": "/properties/altitude",
                "type": "integer",
                "title": "The Altitude Schema ",
                "default": 0,
                "examples": [
                    1400
                ]
            },
            "emission": {
                "$id": "/properties/emission",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The Emission Schema ",
                "default": null,
                "examples": [
                    82.9
                ]
            },
            "pec": {
                "$id": "/properties/pec",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The PEC (Primary Energy Consumption) Schema",
                "default": null,
                "examples": [
                    82.9
                ]
            },
            "heatedSurface": {
                "$id": "/properties/heatedSurface",
                "type": "integer",
                "title": "The Heatedsurface Schema ",
                "default": 0,
                "examples": [
                    120
                ]
            },
            "constructionYear": {
                "$id": "/properties/constructionYear",
                "type": "integer",
                "title": "The Constructionyear Schema ",
                "default": 0,
                "examples": [
                    1990,
                    2013
                ]
            },
            "surfaceType": {
                "$id": "/properties/surfaceType",
                "type": [
                    "null",
                    "string"
                ],
                "enum": [
                    null,
                    "DEVELOPED",
                    "RECTANGULAR",
                    "ELONGATED"
                ],
                "title": "The Surfacetype Schema ",
                "default": null,
                "examples": [
                    "DEVELOPED",
                    "RECTANGULAR",
                    "ELONGATED"
                ]
            },
            "adjacency": {
                "$id": "/properties/adjacency",
                "type": [
                    "null",
                    "string"
                ],
                "enum": [
                    null,
                    "X_LARGE",
                    "LARGE",
                    "MEDIUM",
                    "SMALL",
                    "NONE"
                ],
                "title": "The Adjacency Schema ",
                "default": null,
                "examples": [
                    "X_LARGE",
                    "LARGE",
                    "MEDIUM",
                    "SMALL",
                    "NONE"
                ]
            },
            "floors": {
                "$id": "/properties/floors",
                "type": [
                    "null",
                    "string"
                ],
                "enum": [
                    null,
                    "ONE_LEVEL",
                    "TWO_LEVELS_WITH_ATTIC",
                    "TWO_LEVELS",
                    "THREE_LEVELS_WITH_ATTIC",
                    "THREE_LEVELS"
                ],
                "title": "The Floors Schema ",
                "default": null,
                "examples": [
                    "ONE_LEVEL",
                    "TWO_LEVELS_WITH_ATTIC",
                    "TWO_LEVELS",
                    "THREE_LEVELS_WITH_ATTIC",
                    "THREE_LEVELS"
                ]
            },
            "southExposure": {
                "$id": "/properties/southExposure",
                "type": [
                    "null",
                    "string"
                ],
                "enum": [
                    null,
                    "SUFFICIENT",
                    "INSUFFICIENT"
                ],
                "title": "The Southexposure Schema ",
                "default": null,
                "examples": [
                    "SUFFICIENT",
                    "INSUFFICIENT"
                ]
            },
            "generalComments": {
                "$id": "/properties/generalComments",
                "type": "string",
                "title": "The Generalcomments Schema ",
                "default": "",
                "examples": [
                    "Rien à dire"
                ]
            },
            "wallsData": {
                "$id": "/properties/wallsData",
                "type": ["null", "object"],
                "properties": {
                    "state": {
                        "$id": "/properties/wallsData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ],
                        "title": "The State Schema ",
                        "default": null,
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ]
                    },
                    "rehabilitation": {
                        "$id": "/properties/wallsData/properties/rehabilitation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "ORIGINAL",
                            "REPLACED"
                        ],
                        "title": "The Rehabilitation Schema ",
                        "default": null,
                        "examples": [
                            "NONE",
                            "ORIGINAL",
                            "REPLACED"
                        ]
                    },
                    "surface": {
                        "$id": "/properties/wallsData/properties/surface",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The Surface Schema ",
                        "default": null,
                        "examples": [
                            160
                        ]
                    },
                    "currentInsulation": {
                        "$id": "/properties/wallsData/properties/currentInsulation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "ETI",
                            "ITI",
                            "NONE"
                        ],
                        "title": "The Currentinsulation Schema ",
                        "default": "NONE",
                        "examples": [
                            "ETI",
                            "ITI",
                            "NONE"
                        ]
                    },
                    "plannedInsulation": {
                        "$id": "/properties/wallsData/properties/plannedInsulation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "ETI",
                            "ITI"
                        ],
                        "title": "The Plannedinsulation Schema ",
                        "default": null,
                        "examples": [
                            "ETI",
                            "ITI"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/wallsData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "comment": {
                        "$id": "/properties/wallsData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "Murs en bon état"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "Murs à refaire"
                        ]
                    }
                }
            },
            "floorData": {
                "$id": "/properties/floorData",
                "type": ["null", "object"],
                "properties": {
                    "state": {
                        "$id": "/properties/floorData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ],
                        "title": "The State Schema ",
                        "default": null,
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ]
                    },
                    "floorType": {
                        "$id": "/properties/floorData/properties/floorType",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "OTHER_UNHEATED_SPACE",
                            "CRAWL_SPACE",
                            "TERREPLEIN"
                        ],
                        "title": "The Floortype Schema ",
                        "default": null,
                        "examples": [
                            "OTHER_UNHEATED_SPACE",
                            "CRAWL_SPACE",
                            "TERREPLEIN"
                        ]
                    },
                    "rehabilitation": {
                        "$id": "/properties/floorData/properties/rehabilitation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "ORIGINAL",
                            "REPLACED"
                        ],
                        "title": "The Rehabilitation Schema ",
                        "default": null,
                        "examples": [
                            "NONE",
                            "ORIGINAL",
                            "REPLACED"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/floorData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "surface": {
                        "$id": "/properties/floorData/properties/surface",
                        "type": "integer",
                        "title": "The Surface Schema ",
                        "default": 0,
                        "examples": [
                            80
                        ]
                    },
                    "comment": {
                        "$id": "/properties/floorData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "Plancher en bon état"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "Plancher à refaire"
                        ]
                    }
                }
            },
            "roofData": {
                "$id": "/properties/roofData",
                "type": ["null", "object"],
                "properties": {
                    "state": {
                        "$id": "/properties/roofData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ],
                        "title": "The State Schema ",
                        "default": null,
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ]
                    },
                    "atticType": {
                        "$id": "/properties/roofData/properties/atticType",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "CONVERTED",
                            "CONVERTIBLE",
                            "NON_CONVERTIBLE"
                        ],
                        "title": "The Attictype Schema ",
                        "default": null,
                        "examples": [
                            "CONVERTED",
                            "CONVERTIBLE",
                            "NON_CONVERTIBLE"
                        ]
                    },
                    "rehabilitation": {
                        "$id": "/properties/roofData/properties/rehabilitation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "ORIGINAL",
                            "REPLACED"
                        ],
                        "title": "The Rehabilitation Schema ",
                        "default": null,
                        "examples": [
                            "NONE",
                            "ORIGINAL",
                            "REPLACED"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/roofData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "surface": {
                        "$id": "/properties/roofData/properties/surface",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The Surface Schema ",
                        "default": null,
                        "examples": [
                            40
                        ]
                    },
                    "comment": {
                        "$id": "/properties/roofData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "En bon état"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "à refaire"
                        ]
                    }
                }
            },
            "ventsData": {
                "$id": "/properties/ventsData",
                "type": ["null", "object"],
                "properties": {
                    "state": {
                        "$id": "/properties/ventsData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ],
                        "title": "The State Schema ",
                        "default": null,
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ]
                    },
                    "glazingType": {
                        "$id": "/properties/ventsData/properties/glazingType",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "SIMPLE_GLAZING",
                            "OLD_DOUBLE_GLAZING",
                            "DOUBLE_GLAZING",
                            "HIGH_PERFORMANCE_DOUBLE_GLAZING",
                            "TRIPLE_GLAZING",
                            "DOUBLE_WINDOW"
                        ],
                        "title": "The Glazingtype Schema ",
                        "default": null,
                        "examples": [
                            "SIMPLE_GLAZING",
                            "OLD_DOUBLE_GLAZING",
                            "DOUBLE_GLAZING",
                            "HIGH_PERFORMANCE_DOUBLE_GLAZING",
                            "TRIPLE_GLAZING",
                            "DOUBLE_WINDOW"
                        ]
                    },
                    "rehabilitation": {
                        "$id": "/properties/ventsData/properties/rehabilitation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "ORIGINAL",
                            "REPLACED"
                        ],
                        "title": "The Rehabilitation Schema ",
                        "default": null,
                        "examples": [
                            "ORIGINAL",
                            "REPLACED"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/ventsData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "surface": {
                        "$id": "/properties/ventsData/properties/surface",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The Surface Schema ",
                        "default": null,
                        "examples": [
                            14
                        ]
                    },
                    "comment": {
                        "$id": "/properties/ventsData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "en bon état"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "à refaire"
                        ]
                    }
                }
            },
            "ventilationData": {
                "$id": "/properties/ventilationData",
                "type": ["null", "object"],
                "properties": {
                    "state": {
                        "$id": "/properties/ventilationData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ],
                        "title": "The State Schema ",
                        "default": "",
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ]
                    },
                    "currentVentilation": {
                        "$id": "/properties/ventilationData/properties/currentVentilation",
                        "type": [
                            "null",
                            "object"
                        ],
                        "properties": ventilationSchema.properties,
                        "required": ventilationSchema.required
                    },
                    "rehabilitation": {
                        "$id": "/properties/ventilationData/properties/rehabilitation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "ORIGINAL",
                            "REPLACED"
                        ],
                        "title": "The Rehabilitation Schema ",
                        "default": null,
                        "examples": [
                            "ORIGINAL",
                            "REPLACED"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/ventilationData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "maintenance": {
                        "$id": "/properties/ventilationData/properties/maintenance",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "EVERY_YEAR",
                            "EVERY_TWO_YEAR_OR_MORE"
                        ],
                        "title": "The Maintenance Schema ",
                        "default": null,
                        "examples": [
                            "NONE",
                            "EVERY_YEAR",
                            "EVERY_TWO_YEAR_OR_MORE"
                        ]
                    },
                    "plannedVentilation": {
                        "$id": "/properties/ventilationData/properties/plannedVentilation",
                        "type": [
                            "null",
                            "object"
                        ],
                        "properties": ventilationSchema.properties,
                        "required": ventilationSchema.required
                    },
                    "comment": {
                        "$id": "/properties/ventilationData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "en bon état"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "à refaire"
                        ]
                    }
                }
            },
            "airtightnessData": {
                "$id": "/properties/airtightnessData",
                "type": ["null", "object"],
                "properties": {
                    "currentAirtightness": {
                        "$id": "/properties/airtightnessData/properties/currentAirtightness",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "BAD",
                            "AVERAGE",
                            "GOOD",
                            "VERY_GOOD"
                        ],
                        "title": "The Currentairtightness Schema ",
                        "default": null,
                        "examples": [
                            "BAD",
                            "AVERAGE",
                            "GOOD",
                            "VERY_GOOD"
                        ]
                    },
                    "plannedAirtightness": {
                        "$id": "/properties/airtightnessData/properties/plannedAirtightness",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "GOOD",
                            "VERY_GOOD"
                        ],
                        "title": "The Plannedairtightness Schema ",
                        "default": null,
                        "examples": [
                            "GOOD",
                            "VERY_GOOD"
                        ]
                    },
                    "comment": {
                        "$id": "/properties/airtightnessData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "en bon état"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "à refaire"
                        ]
                    }
                }
            },
            "heatingData": {
                "$id": "/properties/heatingData",
                "type": ["null", "object"],
                "properties": {
                    "state": {
                        "$id": "/properties/heatingData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ],
                        "title": "The State Schema ",
                        "default": null,
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ]
                    },
                    "currentHeating": {
                        "$id": "/properties/heatingData/properties/currentHeating",
                        "type": "object",
                        "properties": dhwAndHeatingSchema.properties,
                        "required": dhwAndHeatingSchema.required
                    },
                    "rehabilitation": {
                        "$id": "/properties/heatingData/properties/rehabilitation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "ORIGINAL",
                            "REPLACED"
                        ],
                        "title": "The Rehabilitation Schema ",
                        "default": null,
                        "examples": [
                            "ORIGINAL",
                            "REPLACED"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/heatingData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "maintenance": {
                        "$id": "/properties/heatingData/properties/maintenance",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "EVERY_YEAR",
                            "EVERY_TWO_YEAR_OR_MORE"
                        ],
                        "title": "The Maintenance Schema ",
                        "default": null,
                        "examples": [
                            "NONE",
                            "EVERY_YEAR",
                            "EVERY_TWO_YEAR_OR_MORE"
                        ]
                    },
                    "plannedHeating": {
                        "$id": "/properties/heatingData/properties/plannedHeating",
                        "type": "object",
                        "properties": dhwAndHeatingSchema.properties,
                        "required": dhwAndHeatingSchema.required
                    },
                    "comment": {
                        "$id": "/properties/heatingData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "en bon état"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "à refaire"
                        ]
                    }
                }
            },
            "thermostatData": {
                "$id": "/properties/thermostatData",
                "type": ["null", "object"],
                "properties": {
                    "presence": {
                        "$id": "/properties/thermostatData/properties/presence",
                        "type": "boolean",
                        "title": "The Presence Schema ",
                        "default": false,
                        "examples": [
                            false
                        ]
                    },
                    "state": {
                        "$id": "/properties/thermostatData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NOT_APPLICABLE"
                        ],
                        "title": "The State Schema ",
                        "default": null,
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NOT_APPLICABLE"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/thermostatData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "comment": {
                        "$id": "/properties/thermostatData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "à rénover"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "à refaire"
                        ]
                    }
                }
            },
            "dhwData": {
                "$id": "/properties/dhwData",
                "type": ["null", "object"],
                "properties": {
                    "state": {
                        "$id": "/properties/dhwData/properties/state",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ],
                        "title": "The State Schema ",
                        "default": null,
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER"
                        ]
                    },
                    "currentDhw": {
                        "$id": "/properties/dhwData/properties/currentDhw",
                        "type": "object",
                        "properties": dhwAndHeatingSchema.properties,
                        "required": dhwAndHeatingSchema.required
                    },
                    "isCurrentSolar": {
                        "$id": "/properties/dhwData/properties/isCurrentSolar",
                        "type": "boolean",
                        "title": "The Iscurrentsolar Schema ",
                        "default": false,
                        "examples": [
                            false
                        ]
                    },
                    "rehabilitation": {
                        "$id": "/properties/dhwData/properties/rehabilitation",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "DEPENDS_ON_HEATING",
                            "ORIGINAL",
                            "REPLACED"
                        ],
                        "title": "The Rehabilitation Schema ",
                        "default": null,
                        "examples": [
                            "DEPENDS_ON_HEATING",
                            "ORIGINAL",
                            "REPLACED"
                        ]
                    },
                    "rehabilitationYear": {
                        "$id": "/properties/dhwData/properties/rehabilitationYear",
                        "type": [
                            "null",
                            "integer"
                        ],
                        "title": "The rehabilitationYear Schema ",
                        "default": null,
                        "examples": [
                            null,
                            1999
                        ]
                    },
                    "maintenance": {
                        "$id": "/properties/dhwData/properties/maintenance",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "EVERY_YEAR",
                            "EVERY_TWO_YEAR_OR_MORE"
                        ],
                        "title": "The Maintenance Schema ",
                        "default": null,
                        "examples": [
                            "NONE",
                            "EVERY_YEAR",
                            "EVERY_TWO_YEAR_OR_MORE"
                        ]
                    },
                    "plannedDhw": {
                        "$id": "/properties/dhwData/properties/plannedDhw",
                        "type": "object",
                        "properties": dhwAndHeatingSchema.properties,
                        "required": dhwAndHeatingSchema.required
                    },
                    "isPlannedSolar": {
                        "$id": "/properties/dhwData/properties/isPlannedSolar",
                        "type": "boolean",
                        "title": "The Isplannedsolar Schema ",
                        "default": false,
                        "examples": [
                            true
                        ]
                    },
                    "comment": {
                        "$id": "/properties/dhwData/properties/comment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "à rénover"
                        ]
                    },
                    "projectComment": {
                        "$id": "/properties/wallsData/properties/projectComment",
                        "type": [
                            "null",
                            "string"
                        ],
                        "title": "The Project Comment Schema ",
                        "default": "",
                        "examples": [
                            "à refaire"
                        ]
                    }
                }
            },
            "tdfData": {
                "$id": "/properties/tdfData",
                "type": ["null", "object"],
                "properties": {
                    "electricalWiringState": {
                        "$id": "/properties/tdfData/properties/electricalWiringState",
                        "title": "The Electricalwiringstate Schema ",
                        "type": "string",
                        "enum": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NO_IDEA"
                        ],
                        "default": "NO_IDEA",
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NO_IDEA"
                        ]
                    },
                    "electricalWiringLastControl": {
                        "$id": "/properties/tdfData/properties/electricalWiringLastControl",
                        "title": "The Electricalwiringlastcontrol Schema ",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ],
                        "default": null,
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "gasSystemState": {
                        "$id": "/properties/tdfData/properties/gasSystemState",
                        "title": "The Gassystemstate Schema ",
                        "type": "string",
                        "enum": [
                            "NONE",
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NO_IDEA"
                        ],
                        "default": "NONE",
                        "examples": [
                            "NONE",
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NO_IDEA"
                        ]
                    },
                    "gasSystemLastControl": {
                        "$id": "/properties/tdfData/properties/gasSystemLastControl",
                        "title": "The Gassystemlastcontrol Schema ",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ],
                        "default": null,
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "plumbingState": {
                        "$id": "/properties/tdfData/properties/plumbingState",
                        "title": "The Plumbingstate Schema ",
                        "type": "string",
                        "enum": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NO_IDEA"
                        ],
                        "default": "NO_IDEA",
                        "examples": [
                            "NEW",
                            "USED",
                            "FIXER_UPPER",
                            "NO_IDEA"
                        ]
                    },
                    "plumbingLastControl": {
                        "$id": "/properties/tdfData/properties/plumbingLastControl",
                        "title": "The Plumbinglastcontrol Schema ",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ],
                        "default": null,
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "leadPresence": {
                        "$id": "/properties/tdfData/properties/leadPresence",
                        "title": "The Leadpresence Schema ",
                        "type": "string",
                        "enum": [
                            "PRESENT",
                            "ABSENT",
                            "NO_IDEA"
                        ],
                        "default": "NO_IDEA",
                        "examples": [
                            "PRESENT",
                            "ABSENT",
                            "NO_IDEA"
                        ]
                    },
                    "leadLastControl": {
                        "$id": "/properties/tdfData/properties/leadLastControl",
                        "title": "The Leadlastcontrol Schema ",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ],
                        "default": null,
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "asbestosPresence": {
                        "$id": "/properties/tdfData/properties/asbestosPresence",
                        "title": "The Asbestospresence Schema ",
                        "type": "string",
                        "enum": [
                            "PRESENT",
                            "ABSENT",
                            "NO_IDEA"
                        ],
                        "default": "NO_IDEA",
                        "examples": [
                            "PRESENT",
                            "ABSENT",
                            "NO_IDEA"
                        ]
                    },
                    "asbestosLastControl": {
                        "$id": "/properties/tdfData/properties/asbestosLastControl",
                        "title": "The Asbestoslastcontrol Schema ",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ],
                        "default": null,
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "termitesPresence": {
                        "$id": "/properties/tdfData/properties/termitesPresence",
                        "title": "The Termitespresence Schema ",
                        "type": "string",
                        "enum": [
                            "PRESENT",
                            "ABSENT",
                            "NO_IDEA"
                        ],
                        "default": "NO_IDEA",
                        "examples": [
                            "PRESENT",
                            "ABSENT",
                            "NO_IDEA"
                        ]
                    },
                    "termitesLastControl": {
                        "$id": "/properties/tdfData/properties/termitesLastControl",
                        "title": "The Termiteslastcontrol Schema ",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ],
                        "default": null,
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "epcs": {
                        "$id": "/properties/tdfData/properties/epcs",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_10_YEARS",
                            "MORE_THAN_10_YEARS",
                            "NO_IDEA"
                        ],
                        "title": "The Epcs Schema ",
                        "default": "NO_IDEA",
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_10_YEARS",
                            "MORE_THAN_10_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "septicTankIndividual": {
                        "$id": "/properties/tdfData/properties/septicTankIndividual",
                        "type": "boolean",
                        "title": "The Septictankindividual Schema ",
                        "default": false,
                        "examples": [
                            false
                        ]
                    },
                    "septicTankLastControl": {
                        "$id": "/properties/tdfData/properties/septicTankLastControl",
                        "type": [
                            "null",
                            "string"
                        ],
                        "enum": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ],
                        "title": "The Septictanklastcontrol Schema ",
                        "default": null,
                        "examples": [
                            null,
                            "NONE",
                            "LESS_THAN_3_YEARS",
                            "MORE_THAN_3_YEARS",
                            "NO_IDEA"
                        ]
                    },
                    "comment": {
                        "$id": "/properties/tdfData/properties/comment",
                        "type": "string",
                        "title": "The Comment Schema ",
                        "default": "",
                        "examples": [
                            "Rien à redire"
                        ]
                    }
                }
            }
        }
    }
    pm.environment.set("passportSchema", passportSchema);

    territorySchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema",
                "default": 0
            },
            "label": {
                "$id": "/properties/label",
                "type": "string",
                "title": "The Label Schema",
                "default": ""
            },
            "users": {
                "$id": "/properties/users",
                "type": "array",
                "items": {
                    "$id": "/properties/users/items",
                    "type": ["null", "object"],
                    "properties": userSchema.properties,
                    "required": userSchema.required
                }
            },
            "cities": {
                "$id": "/properties/cities",
                "type": "array",
                "items": {
                    "$id": "/properties/cities/items",
                    "type": ["null", "object"],
                    "properties": citySchema.properties,
                    "required": citySchema.required
                }
            }
        },
        "required": [
            "id",
            "label"
        ]
    };
    pm.environment.set("territorySchema", territorySchema);

    renovationPlanImprovementSchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema",
                "default": 0
            },
            "combinatory": {
                "$id": "/properties/combinatory",
                "type": "object",
                "properties": combinatorySchema.properties,
                "required": combinatorySchema.required
            },
            "ventilation": {
                "$id": "/properties/ventilation",
                "type": "object",
                "properties": ventilationSchema.properties,
                "required": ventilationSchema.required
            },
            "heating": {
                "$id": "/properties/heating",
                "type": "object",
                "properties": dhwAndHeatingSchema.properties,
                "required": dhwAndHeatingSchema.required
            },
            "dhw": {
                "$id": "/properties/dhw",
                "type": "object",
                "properties": dhwAndHeatingSchema.properties,
                "required": dhwAndHeatingSchema.required
            },
            "wallsState": {
                "$id": "/properties/wallsState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The wallsState Schema",
                "default": null
            },
            "wallsProposedInterventionYear": {
                "$id": "/properties/wallsProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The wallsProposedInterventionYear Schema",
                "default": null
            },
            "wallsForceInterventionYear": {
                "$id": "/properties/wallsForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The wallsForceInterventionYear Schema",
                "default": false
            },
            "wallsDefinedInterventionYear": {
                "$id": "/properties/wallsDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The wallsDefinedInterventionYear Schema",
                "default": null
            },
            "wallsDefinedPriceMin": {
                "$id": "/properties/wallsDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The wallsDefinedPriceMin Schema",
                "default": null
            },
            "wallsDefinedPriceMax": {
                "$id": "/properties/wallsDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The wallsDefinedPriceMax Schema",
                "default": null
            },
            "wallsSelected": {
                "$id": "/properties/wallsSelected",
                "type": "boolean",
                "title": "The wallsSelected Schema",
                "default": false
            },
            "floorState": {
                "$id": "/properties/floorState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The floorState Schema",
                "default": null
            },
            "floorProposedInterventionYear": {
                "$id": "/properties/floorProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The floorProposedInterventionYear Schema",
                "default": null
            },
            "floorForceInterventionYear": {
                "$id": "/properties/floorForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The floorForceInterventionYear Schema",
                "default": false
            },
            "floorDefinedInterventionYear": {
                "$id": "/properties/floorDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The floorDefinedInterventionYear Schema",
                "default": null
            },
            "floorDefinedPriceMin": {
                "$id": "/properties/floorDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The floorDefinedPriceMin Schema",
                "default": null
            },
            "floorDefinedPriceMax": {
                "$id": "/properties/floorDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The floorDefinedPriceMax Schema",
                "default": null
            },
            "floorSelected": {
                "$id": "/properties/floorSelected",
                "type": "boolean",
                "title": "The floorSelected Schema",
                "default": false
            },
            "roofState": {
                "$id": "/properties/roofState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The roofState Schema",
                "default": null
            },
            "roofProposedInterventionYear": {
                "$id": "/properties/roofProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The roofProposedInterventionYear Schema",
                "default": null
            },
            "roofForceInterventionYear": {
                "$id": "/properties/roofForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The roofForceInterventionYear Schema",
                "default": false
            },
            "roofDefinedInterventionYear": {
                "$id": "/properties/roofDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The roofDefinedInterventionYear Schema",
                "default": null
            },
            "roofDefinedPriceMin": {
                "$id": "/properties/roofDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The roofDefinedPriceMin Schema",
                "default": null
            },
            "roofDefinedPriceMax": {
                "$id": "/properties/roofDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The roofDefinedPriceMax Schema",
                "default": null
            },
            "roofSelected": {
                "$id": "/properties/roofSelected",
                "type": "boolean",
                "title": "The roofSelected Schema",
                "default": false
            },
            "ventsState": {
                "$id": "/properties/ventsState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventsState Schema",
                "default": null
            },
            "ventsProposedInterventionYear": {
                "$id": "/properties/ventsProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventsProposedInterventionYear Schema",
                "default": null
            },
            "ventsForceInterventionYear": {
                "$id": "/properties/ventsForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The ventsForceInterventionYear Schema",
                "default": false
            },
            "ventsDefinedInterventionYear": {
                "$id": "/properties/ventsDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventsDefinedInterventionYear Schema",
                "default": null
            },
            "ventsDefinedPriceMin": {
                "$id": "/properties/ventsDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventsDefinedPriceMin Schema",
                "default": null
            },
            "ventsDefinedPriceMax": {
                "$id": "/properties/ventsDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventsDefinedPriceMax Schema",
                "default": null
            },
            "ventsSelected": {
                "$id": "/properties/ventsSelected",
                "type": "boolean",
                "title": "The ventsSelected Schema",
                "default": false
            },
            "ventilationState": {
                "$id": "/properties/ventilationState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventilationState Schema",
                "default": null
            },
            "ventilationProposedInterventionYear": {
                "$id": "/properties/ventilationProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventilationProposedInterventionYear Schema",
                "default": null
            },
            "ventilationForceInterventionYear": {
                "$id": "/properties/ventilationForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The ventilationForceInterventionYear Schema",
                "default": false
            },
            "ventilationDefinedInterventionYear": {
                "$id": "/properties/ventilationDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventilationDefinedInterventionYear Schema",
                "default": null
            },
            "ventilationDefinedPriceMin": {
                "$id": "/properties/ventilationDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventilationDefinedPriceMin Schema",
                "default": null
            },
            "ventilationDefinedPriceMax": {
                "$id": "/properties/ventilationDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The ventilationDefinedPriceMax Schema",
                "default": null
            },
            "ventilationSelected": {
                "$id": "/properties/ventilationSelected",
                "type": "boolean",
                "title": "The ventilationSelected Schema",
                "default": false
            },
            "airtightnessValue": {
                "$id": "/properties/airtightnessValue",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The airtightnessValue Schema",
                "default": null
            },
            "airtightnessState": {
                "$id": "/properties/airtightnessState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The airtightnessState Schema",
                "default": null
            },
            "airtightnessProposedInterventionYear": {
                "$id": "/properties/airtightnessProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The airtightnessProposedInterventionYear Schema",
                "default": null
            },
            "airtightnessForceInterventionYear": {
                "$id": "/properties/airtightnessForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The airtightnessForceInterventionYear Schema",
                "default": false
            },
            "airtightnessDefinedInterventionYear": {
                "$id": "/properties/airtightnessDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The airtightnessDefinedInterventionYear Schema",
                "default": null
            },
            "airtightnessDefinedPriceMin": {
                "$id": "/properties/airtightnessDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The airtightnessDefinedPriceMin Schema",
                "default": null
            },
            "airtightnessDefinedPriceMax": {
                "$id": "/properties/airtightnessDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The airtightnessDefinedPriceMax Schema",
                "default": null
            },
            "airtightnessSelected": {
                "$id": "/properties/airtightnessSelected",
                "type": "boolean",
                "title": "The airtightnessSelected Schema",
                "default": false
            },
            "heatingState": {
                "$id": "/properties/heatingState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The heatingState Schema",
                "default": null
            },
            "heatingProposedInterventionYear": {
                "$id": "/properties/heatingProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The heatingProposedInterventionYear Schema",
                "default": null
            },
            "heatingForceInterventionYear": {
                "$id": "/properties/heatingForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The heatingForceInterventionYear Schema",
                "default": false
            },
            "heatingDefinedInterventionYear": {
                "$id": "/properties/heatingDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The heatingDefinedInterventionYear Schema",
                "default": null
            },
            "heatingDefinedPriceMin": {
                "$id": "/properties/heatingDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The heatingDefinedPriceMin Schema",
                "default": null
            },
            "heatingDefinedPriceMax": {
                "$id": "/properties/heatingDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The heatingDefinedPriceMax Schema",
                "default": null
            },
            "heatingSelected": {
                "$id": "/properties/heatingSelected",
                "type": "boolean",
                "title": "The heatingSelected Schema",
                "default": false
            },
            "thermostatState": {
                "$id": "/properties/thermostatState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The thermostatState Schema",
                "default": null
            },
            "thermostatProposedInterventionYear": {
                "$id": "/properties/thermostatProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The thermostatProposedInterventionYear Schema",
                "default": null
            },
            "thermostatForceInterventionYear": {
                "$id": "/properties/thermostatForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The thermostatForceInterventionYear Schema",
                "default": false
            },
            "thermostatDefinedInterventionYear": {
                "$id": "/properties/thermostatDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The thermostatDefinedInterventionYear Schema",
                "default": null
            },
            "thermostatDefinedPriceMin": {
                "$id": "/properties/thermostatDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The thermostatDefinedPriceMin Schema",
                "default": null
            },
            "thermostatDefinedPriceMax": {
                "$id": "/properties/thermostatDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The thermostatDefinedPriceMax Schema",
                "default": null
            },
            "thermostatSelected": {
                "$id": "/properties/thermostatSelected",
                "type": "boolean",
                "title": "The thermostatSelected Schema",
                "default": false
            },
            "dhwState": {
                "$id": "/properties/dhwState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The dhwState Schema",
                "default": null
            },
            "dhwProposedInterventionYear": {
                "$id": "/properties/dhwProposedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The dhwProposedInterventionYear Schema",
                "default": null
            },
            "dhwForceInterventionYear": {
                "$id": "/properties/dhwForceInterventionYear",
                "type": [
                    "null",
                    "boolean"
                ],
                "title": "The dhwForceInterventionYear Schema",
                "default": false
            },
            "dhwDefinedInterventionYear": {
                "$id": "/properties/dhwDefinedInterventionYear",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The dhwDefinedInterventionYear Schema",
                "default": null
            },
            "dhwDefinedPriceMin": {
                "$id": "/properties/dhwDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The dhwDefinedPriceMin Schema",
                "default": null
            },
            "dhwDefinedPriceMax": {
                "$id": "/properties/dhwDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The dhwDefinedPriceMax Schema",
                "default": null
            },
            "dhwSelected": {
                "$id": "/properties/dhwSelected",
                "type": "boolean",
                "title": "The dhwSelected Schema",
                "default": false
            }
        },
        "required": [
            "combinatory",
            "ventilation",
            "heating",
            "dhw"
        ]
    };
    pm.environment.set("renovationPlanImprovementSchema", renovationPlanImprovementSchema);

    renovationPlanTdfSchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema",
                "default": 0
            },
            "electricalWiringState": {
                "$id": "/properties/electricalWiringState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The electricalWiringState Schema",
                "default": null
            },
            "electricalWiringControl": {
                "$id": "/properties/electricalWiringControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The electricalWiringControl Schema",
                "default": null
            },
            "electricalWiringDefinedPriceMin": {
                "$id": "/properties/electricalWiringDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The electricalWiringDefinedPriceMin Schema",
                "default": null
            },
            "electricalWiringDefinedPriceMax": {
                "$id": "/properties/electricalWiringDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The electricalWiringDefinedPriceMax Schema",
                "default": null
            },
            "electricalWiringSelected": {
                "$id": "/properties/electricalWiringSelected",
                "type": "boolean",
                "title": "The electricalWiringSelected Schema",
                "default": false
            },
            "gasSystemState": {
                "$id": "/properties/gasSystemState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The gasSystemState Schema",
                "default": null
            },
            "gasSystemDefinedPriceMin": {
                "$id": "/properties/gasSystemDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The gasSystemDefinedPriceMin Schema",
                "default": null
            },
            "gasSystemDefinedPriceMax": {
                "$id": "/properties/gasSystemDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The gasSystemDefinedPriceMax Schema",
                "default": null
            },
            "gasSystemControl": {
                "$id": "/properties/gasSystemControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The gasSystemControl Schema",
                "default": null
            },
            "gasSystemSelected": {
                "$id": "/properties/gasSystemSelected",
                "type": "boolean",
                "title": "The gasSystemSelected Schema",
                "default": false
            },
            "plumbingState": {
                "$id": "/properties/plumbingState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The plumbingState Schema",
                "default": null
            },
            "plumbingDefinedPriceMin": {
                "$id": "/properties/plumbingDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The plumbingDefinedPriceMin Schema",
                "default": null
            },
            "plumbingDefinedPriceMax": {
                "$id": "/properties/plumbingDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The plumbingDefinedPriceMax Schema",
                "default": null
            },
            "plumbingControl": {
                "$id": "/properties/plumbingControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The plumbingControl Schema",
                "default": null
            },
            "plumbingSelected": {
                "$id": "/properties/plumbingSelected",
                "type": "boolean",
                "title": "The plumbingSelected Schema",
                "default": false
            },
            "leadState": {
                "$id": "/properties/leadState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The leadState Schema",
                "default": null
            },
            "leadDefinedPriceMin": {
                "$id": "/properties/leadDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The leadDefinedPriceMin Schema",
                "default": null
            },
            "leadDefinedPriceMax": {
                "$id": "/properties/leadDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The leadDefinedPriceMax Schema",
                "default": null
            },
            "leadControl": {
                "$id": "/properties/leadControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The leadControl Schema",
                "default": null
            },
            "leadSelected": {
                "$id": "/properties/leadSelected",
                "type": "boolean",
                "title": "The leadSelected Schema",
                "default": false
            },
            "asbestosState": {
                "$id": "/properties/asbestosState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The asbestosState Schema",
                "default": null
            },
            "asbestosDefinedPriceMin": {
                "$id": "/properties/asbestosDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The asbestosDefinedPriceMin Schema",
                "default": null
            },
            "asbestosDefinedPriceMax": {
                "$id": "/properties/asbestosDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The asbestosDefinedPriceMax Schema",
                "default": null
            },
            "asbestosControl": {
                "$id": "/properties/asbestosControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The asbestosControl Schema",
                "default": null
            },
            "asbestosSelected": {
                "$id": "/properties/asbestosSelected",
                "type": "boolean",
                "title": "The asbestosSelected Schema",
                "default": false
            },
            "termitesState": {
                "$id": "/properties/termitesState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The termitesState Schema",
                "default": null
            },
            "termitesControl": {
                "$id": "/properties/termitesControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The termitesControl Schema",
                "default": null
            },
            "termitesDefinedPriceMin": {
                "$id": "/properties/termitesDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The termitesDefinedPriceMin Schema",
                "default": null
            },
            "termitesDefinedPriceMax": {
                "$id": "/properties/termitesDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The termitesDefinedPriceMax Schema",
                "default": null
            },
            "termitesSelected": {
                "$id": "/properties/termitesSelected",
                "type": "boolean",
                "title": "The termitesSelected Schema",
                "default": false
            },
            "epcsState": {
                "$id": "/properties/epcsState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The epcsState Schema",
                "default": null
            },
            "epcsDefinedPriceMin": {
                "$id": "/properties/epcsDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The epcsDefinedPriceMin Schema",
                "default": null
            },
            "epcsDefinedPriceMax": {
                "$id": "/properties/epcsDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The epcsDefinedPriceMax Schema",
                "default": null
            },
            "epcsControl": {
                "$id": "/properties/epcsControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The epcsControl Schema",
                "default": null
            },
            "epcsSelected": {
                "$id": "/properties/epcsSelected",
                "type": "boolean",
                "title": "The epcsSelected Schema",
                "default": false
            },
            "septicTankState": {
                "$id": "/properties/septicTankState",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The septicTankState Schema",
                "default": null
            },
            "septicTankDefinedPriceMin": {
                "$id": "/properties/septicTankDefinedPriceMin",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The septicTankDefinedPriceMin Schema",
                "default": null
            },
            "septicTankDefinedPriceMax": {
                "$id": "/properties/septicTankDefinedPriceMax",
                "type": [
                    "null",
                    "integer"
                ],
                "title": "The septicTankDefinedPriceMax Schema",
                "default": null
            },
            "septicTankControl": {
                "$id": "/properties/septicTankControl",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The septicTankControl Schema",
                "default": null
            },
            "septicTankSelected": {
                "$id": "/properties/septicTankSelected",
                "type": "boolean",
                "title": "The septicTankSelected Schema",
                "default": false
            },
            "comment": {
                "$id": "/properties/comment",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The comment Schema",
                "default": null
            },
        }
    };
    pm.environment.set("renovationPlanTdfSchema", renovationPlanTdfSchema);

    renovationPlanSchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema",
                "default": 0
            },
            "passport": {
                "$id": "/properties/passport",
                "type": "object",
                "properties": passportSchema.properties,
                "required": passportSchema.required
            },
            "improvement": {
                "$id": "/properties/improvement",
                "type": "object",
                "properties": renovationPlanImprovementSchema.properties,
                "required": renovationPlanImprovementSchema.required
            },
            "tdf": {
                "$id": "/properties/tdf",
                "type": "object",
                "properties": renovationPlanTdfSchema.properties,
                "required": renovationPlanTdfSchema.required
            },
            "budgetComment": {
                "$id": "/properties/budgetComment",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The budgetComment Schema",
                "default": null
            },
            "currentConsumption": {
                "$id": "/properties/currentConsumption",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The currentConsumption Schema",
                "default": null
            },
            "plannedConsumption": {
                "$id": "/properties/plannedConsumption",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The plannedConsumption Schema",
                "default": null
            },
            "economy": {
                "$id": "/properties/economy",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The economy Schema",
                "default": null
            },
            "combinatoryConsumption": {
                "$id": "/properties/combinatoryConsumption",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The combinatoryConsumption Schema",
                "default": null
            },
            "combinatoryEconomy": {
                "$id": "/properties/combinatoryEconomy",
                "type": [
                    "null",
                    "number"
                ],
                "title": "The combinatoryEconomy Schema",
                "default": null
            },
            "log": {
                "$id": "/properties/log",
                "type": [
                    "null",
                    "string"
                ],
                "title": "The log Schema",
                "default": null
            },
        },
        "required": [
            "id",
            "passport",
            "improvement",
            "tdf"
        ]
    };
    pm.environment.set("renovationPlanSchema", renovationPlanSchema);

    vigilancePointConditionSchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema",
                "default": 0
            },
            "precedingLot": {
                "$id": "/properties/precedingLot",
                "type": ["null", "string"],
                "title": "The PrecedingLot Schema ",
                "default": null
            },
            "precedingLotValueBefore": {
                "$id": "/properties/precedingLotValueBefore",
                "type": ["null", "string"],
                "title": "The PrecedingLotValueBefore Schema",
                "default": null
            },
            "precedingLotValueAfter": {
                "$id": "/properties/precedingLotValueAfter",
                "type": ["null", "string"],
                "title": "The PrecedingLotValueAfter Schema",
                "default": null
            },
            "followingLot": {
                "$id": "/properties/followingLot",
                "type": ["null", "string"],
                "title": "The FollowingLot Schema ",
                "default": null
            },
            "followingLotValueBefore": {
                "$id": "/properties/followingLotValueBefore",
                "type": ["null", "string"],
                "title": "The FollowingLotValueBefore Schema",
                "default": null
            },
            "followingLotValueAfter": {
                "$id": "/properties/followingLotValueAfter",
                "type": ["null", "string"],
                "title": "The FollowingLotValueAfter Schema",
                "default": null
            },
            "creationDate": {
                "$id": "/properties/creationDate",
                "type": "string",
                "format": "date-time",
                "title": "The CreationDate Schema "
            },
        },
        "required": [
            "id",
            "creationDate"
        ]
    };
    pm.environment.set("vigilancePointConditionSchema", vigilancePointConditionSchema);

    vigilancePointSchema = {
        "type": "object",
        "properties": {
            "id": {
                "$id": "/properties/id",
                "type": "integer",
                "title": "The Id Schema",
                "default": 0
            },
            "complexity": {
                "$id": "/properties/complexity",
                "type": "integer",
                "title": "The Complexity Schema",
                "default": 0
            },
            "priority": {
                "$id": "/properties/priority",
                "type": "integer",
                "title": "The Priority Schema",
                "default": 0
            },
            "title": {
                "$id": "/properties/title",
                "type": "string",
                "title": "The Title Schema ",
                "default": ""
            },
            "description": {
                "$id": "/properties/description",
                "type": "string",
                "title": "The Description Schema ",
                "default": ""
            },
            "creationDate": {
                "$id": "/properties/creationDate",
                "type": "string",
                "format": "date-time",
                "title": "The CreationDate Schema "
            },
            "modificationDate": {
                "$id": "/properties/modificationDate",
                "type": ["null", "string"],
                "format": "date-time",
                "title": "The ModificationDate Schema ",
                "default": null
            },
            "conditions": {
                "$id": "/properties/conditions",
                "type": "array",
                "items": {
                    "$id": "/properties/conditions/items",
                    "type": ["null", "object"],
                    "properties": vigilancePointConditionSchema.properties,
                    "required": vigilancePointConditionSchema.required
                }
            },
        },
        "required": [
            "id",
            "complexity",
            "priority",
            "description",
            "creationDate"
        ]
    };
    pm.environment.set("vigilancePointSchema", vigilancePointSchema);

    /* FIXTURES */

    /* User fixtures */
    let userLeadAssign1 = {
        "email": "user-lead-assig-1@mail.com",
        "firstname": "User 1",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "24 rue charles maher rochefort",
        "role": "User",
        "password": "pass"
    };

    let userLeadAssign2 = {
        "email": "user-lead-assig-2@mail.com",
        "firstname": "User 2",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "2 rue charles maher rochefort",
        "role": "User",
        "password": "pass"
    };

    let userLeadAssign3 = {
        "email": "user-lead-assig-3@mail.com",
        "firstname": "User 3",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "4 rue charles maher rochefort",
        "role": "User",
        "password": "pass"
    };

    let userLeadAssign4 = {
        "email": "user-lead-assig-4@mail.com",
        "firstname": "User 4",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "2-8 Rue des Noyers 69005 Lyon",
        "role": "User",
        "password": "pass"
    };

    let auditorLeadAssign1 = {
        "email": "auditor-lead-assig-1@mail.com",
        "firstname": "Auditor 1",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "22 rue charles maher rochefort",
        "role": "Auditor",
        "password": "pass"
    };

    let auditorLeadAssign2 = {
        "email": "auditor-lead-assig-2@mail.com",
        "firstname": "Auditor 2",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "20 rue charles maher rochefort",
        "role": "Auditor",
        "password": "pass"
    };

    let auditorLeadAssign3 = {
        "email": "auditor-lead-assig-3@mail.com",
        "firstname": "Auditor 3",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "18 rue charles maher rochefort",
        "role": "Auditor",
        "password": "pass"
    };

    let auditorLeadAssign4 = {
        "email": "auditor-lead-assig-4@mail.com",
        "firstname": "Auditor 4",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "16 rue charles maher rochefort",
        "role": "Auditor",
        "password": "pass"
    };

    let auditorLeadAssign5 = {
        "email": "auditor-lead-assig-5@mail.com",
        "firstname": "Auditor 5",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "14 rue charles maher rochefort",
        "role": "Auditor",
        "password": "pass"
    };

    let auditorLeadAssign6 = {
        "email": "auditor-lead-assig-6@mail.com",
        "firstname": "Auditor 6",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "12 rue charles maher rochefort",
        "role": "Auditor",
        "password": "pass"
    };

    let auditorLeadAssign7 = {
        "email": "auditor-lead-assig-7@mail.com",
        "firstname": "Auditor 7",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "3 allée de la mule gujan-mestras",
        "role": "Auditor",
        "password": "pass"
    };

    let userTerritory1 = {
        "email": "userTerritory1@mail.com",
        "firstname": "userTerritory1",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "3 allée de la mule gujan-mestras",
        "role": "User territory",
        "password": "pass"
    };

    let userTerritory2 = {
        "email": "userTerritory2@mail.com",
        "firstname": "userTerritory2",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "3 allée de la mule gujan-mestras",
        "role": "User territory",
        "password": "pass"
    };

    let adminTerritory1 = {
        "email": "adminTerritory1@mail.com",
        "firstname": "adminTerritory1",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "3 allée de la mule gujan-mestras",
        "role": "Admin territory",
        "password": "pass"
    };

    let adminTerritory2 = {
        "email": "adminTerritory2@mail.com",
        "firstname": "adminTerritory2",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "3 allée de la mule gujan-mestras",
        "role": "Admin territory",
        "password": "pass"
    };

    let superAdmin1 = {
        "email": "superAdmin1@mail.com",
        "firstname": "superAdmin1",
        "lastname": "Lead assignations",
        "phone": "0666666666",
        "company": "whatever",
        "address": "3 allée de la mule gujan-mestras",
        "role": "Admin",
        "password": "pass"
    };

    pm.environment.set("userFixtures", {
        [userLeadAssign1.email]: userLeadAssign1,
        [userLeadAssign2.email]: userLeadAssign2,
        [userLeadAssign3.email]: userLeadAssign3,
        [userLeadAssign4.email]: userLeadAssign4,
        [auditorLeadAssign1.email]: auditorLeadAssign1,
        [auditorLeadAssign2.email]: auditorLeadAssign2,
        [auditorLeadAssign3.email]: auditorLeadAssign3,
        [auditorLeadAssign4.email]: auditorLeadAssign4,
        [auditorLeadAssign5.email]: auditorLeadAssign5,
        [auditorLeadAssign6.email]: auditorLeadAssign6,
        [auditorLeadAssign7.email]: auditorLeadAssign7,
        [userTerritory1.email]: userTerritory1,
        [userTerritory2.email]: userTerritory2,
        [adminTerritory1.email]: adminTerritory1,
        [adminTerritory2.email]: adminTerritory2,
        [superAdmin1.email]: superAdmin1,
    });


    /* Lead fixtures */
    let leadAssign1 = {
        "leadOrigin": "Test Postman - lead assign",
        "legals1": true,
        "legals2": true,
        "comments": "lead fixture 1",
        "linkToLead": "http://postman.test/{leadId}"
    }

    let leadAssign2 = {
        "leadOrigin": "Test Postman - lead assign",
        "legals1": true,
        "legals2": true,
        "comments": "lead fixture 2",
        "linkToLead": "http://postman.test/{leadId}"
    }

    let leadAssign3 = {
        "leadOrigin": "Test Postman - lead assign",
        "legals1": true,
        "legals2": true,
        "comments": "lead fixture 3",
        "linkToLead": "http://postman.test/{leadId}"
    }

    let leadAssign4 = {
        "leadOrigin": "Test Postman - lead assign",
        "legals1": true,
        "legals2": true,
        "comments": "lead fixture 4",
        "linkToLead": "http://postman.test/{leadId}"
    }

    pm.environment.set("leadFixtures", {
        lead1: leadAssign1,
        lead2: leadAssign2,
        lead3: leadAssign3,
        lead4: leadAssign4
    });

    /* Passport fixture */
    let passport1 = {
        "type": "HOUSE",
        "project": "Rénovation d'une maison de 1990",
        "climateZone": "H1",
        "altitude": 1400,
        "heatedSurface": 120,
        "constructionYear": 1990,
        "surfaceType": "RECTANGULAR",
        "adjacency": "LARGE",
        "floors": "TWO_LEVELS_WITH_ATTIC",
        "southExposure": "SUFFICIENT",
        "generalComments": "Rien à dire",
        "wallsData": {
            "state": "USED",
            "rehabilitation": "REPLACED",
            "rehabilitationYear": 1990,
            "surface": 160,
            "currentInsulation": "ETI",
            "plannedInsulation": "ETI",
            "comment": "Murs en bon état",
            "projectComment": "WADDUUUUUP"
        },
        "floorData": {
            "state": "USED",
            "floorType": "CRAWL_SPACE",
            "rehabilitation": "ORIGINAL",
            "surface": 80,
            "comment": "Plancher en bon état"
        },
        "roofData": {
            "state": "FIXER_UPPER",
            "atticType": "CONVERTIBLE",
            "atticTypePlanned": false,
            "rehabilitation": "REPLACED",
            "rehabilitationYear": 1990,
            "surface": 40,
            "comment": "En bon état"
        },
        "ventsData": {
            "state": "NEW",
            "glazingType": "OLD_DOUBLE_GLAZING",
            "poseType": "TUNNEL",
            "rehabilitation": "ORIGINAL",
            "surface": 14,
            "comment": "en bon état"
        },
        "ventilationData": {
            "state": "USED",
            "currentVentilation": {
                "id": 1
            },
            "rehabilitation": "REPLACED",
            "rehabilitationYear": 1990,
            "maintenance": "NONE",
            "plannedVentilation": {
                "id": 4
            },
            "comment": "en bon état"
        },
        "airtightnessData": {
            "currentAirtightness": "AVERAGE",
            "plannedAirtightness": "GOOD",
            "comment": "en bon état"
        },
        "heatingData": {
            "state": "NEW",
            "currentHeating": {
                "id": 8
            },
            "rehabilitation": "ORIGINAL",
            "maintenance": "EVERY_YEAR",
            "plannedHeating": {
                "id": 1
            },
            "comment": "en bon état"
        },
        "thermostatData": {
            "presence": false,
            "state": "USED",
            "comment": "à rénover"
        },
        "dhwData": {
            "state": "USED",
            "currentDhw": {
                "id": 5
            },
            "isCurrentSolar": false,
            "rehabilitation": "DEPENDS_ON_HEATING",
            "maintenance": "EVERY_YEAR",
            "plannedDhw": {
                "id": 25
            },
            "isPlannedSolar": true,
            "comment": "à rénover"
        },
        "tdfData": {
            "electricalWiringState": "NEW",
            "electricalWiringLastControl": "NONE",
            "gasSystemState": "NONE",
            "gasSystemLastControl": null,
            "plumbingState": "USED",
            "plumbingLastControl": "LESS_THAN_3_YEARS",
            "leadPresence": "PRESENT",
            "leadLastControl": "MORE_THAN_3_YEARS",
            "asbestosPresence": "PRESENT",
            "asbestosLastControl": "NO_IDEA",
            "termitesPresence": "ABSENT",
            "termitesLastControl": "NO_IDEA",
            "epcs": "MORE_THAN_10_YEARS",
            "septicTankIndividual": false,
            "septicTankLastControl": null,
            "comment": "Rien à redire"
        }
    }

    pm.environment.set("passportFixtures", {
        passport1
    });

    let renovationPlan1 = {
        "improvement": {
            "roofSelected": true,
            "floorSelected": true,
            "heatingSelected": false,
            "ventsSelected": false,
            "ventilationSelected": false,
            "combinatory": {
                "id": 1239,
            }
        },
        "tdf": {}
    }

    pm.environment.set("renovationPlanFixtures", {
        renovationPlan1
    });

    let combinatoryNoProtected1 = {
        "number": 15,
        "level": "B",
        "code": 31137,
        "protected": false,
        "zone": "H3",
        "insulation": "ITI",
        "airtightness": "VERY_GOOD",
        "ventilation": {
            "id": 4
        },
        "heating": {
            "id": 7
        },
        "wallsValue": 7.5,
        "wallsPriceRange": {
            "unit": "UNIT",
            "min": 3500,
            "max": 6500,
            "definedMin": 3500,
            "definedMax": 6500
        },
        "ventsValue": 1.1,
        "ventsPriceRange": {
            "unit": "SURFACE",
            "min": 25,
            "max": 50,
            "definedMin": 25,
            "definedMax": 50
        },
        "floorValue": 4.5,
        "floorPriceRange": {
            "unit": "SURFACE",
            "min": 130,
            "max": 180,
            "definedMin": 130,
            "definedMax": 180
        },
        "roofValue": 10,
        "roofPriceRange": {
            "unit": "SURFACE",
            "min": 40,
            "max": 60,
            "definedMin": 40,
            "definedMax": 60
        }
    }

    pm.environment.set("combinatoriesFixtures", {
        combinatoryNoProtected1
    });

    /* Vigilance point fixtures */
    let vigilancePoint1 = {
        "complexity": 1,
        "priority": 1,
        "title": "Vigilance Point 1",
        "description": "Vigilance Point 1"
    }
    let vigilancePoint2 = {
        "complexity": 2,
        "priority": 3,
        "title": "Vigilance Point 2",
        "description": "Vigilance Point 2"
    }
    let vigilancePoint3 = {
        "complexity": 3,
        "priority": 2,
        "title": "Vigilance Point 3",
        "description": "Vigilance Point 3"
    }
    let vigilancePoint4 = {
        "complexity": 3,
        "priority": 1,
        "title": "Vigilance Point 4",
        "description": "Vigilance Point 4"
    }
    let vigilancePoint5 = {
        "complexity": 3,
        "priority": 2,
        "title": "Vigilance Point 5",
        "description": "Vigilance Point 5"
    }

    let vpCondition1 = {
        "precedingLot": "floor.floorType",
        "precedingLotValueBefore": "CRAWL_SPACE",
        "followingLot": "heating.id",
        "followingLotValueAfter": "1"
    }
    let vpCondition2 = {
        "precedingLot": "floor.floorType",
        "precedingLotValueBefore": "CRAWL_SPACE",
        "followingLot": "vents.poseType",
        "followingLotValueAfter": "TUNNEL"
    }
    let vpCondition3 = {
        "precedingLot": "heating.id",
        "precedingLotValueBefore": "8",
        "followingLot": "ventilation.id",
        "followingLotValueAfter": "2",
    }
    let vpCondition4 = {
        "precedingLot": "floor.floorType",
        "precedingLotValueBefore": "CRAWL_SPACE",
        "followingLot": "ventilation.id",
        "followingLotValueAfter": "4",
    }
    let vpCondition5 = {
        "precedingLot": "floor.floorType",
        "precedingLotValueBefore": "CRAWL_SPACE",
        "followingLot": "vents.poseType",
        "followingLotValueAfter": "whatever"
    }

    pm.environment.set("vpFixtures", {
        vp1: vigilancePoint1,
        vp2: vigilancePoint2,
        vp3: vigilancePoint3,
        vp4: vigilancePoint4,
        vp5: vigilancePoint5,
        condition1: vpCondition1,
        condition2: vpCondition2,
        condition3: vpCondition3,
        condition4: vpCondition4,
        condition5: vpCondition5
    });

    /* ENVIRONMENT METHODS*/
    postman.setEnvironmentVariable("utils", () => {
        const DEFAULT_NB_RETRY = 5;
        const RETRY_DELAY = 2000;
        const SEPARATOR = "***********************";

        var getNow = () => {
            var currentdate = new Date();
            return currentdate.getDate() + "/"
                + (currentdate.getMonth() + 1) + "/"
                + currentdate.getFullYear() + " @ "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds() + " ";
        };

        var getAdminToken = (callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Fetching admin token...');
            return pm.sendRequest({
                url: pm.environment.get("url") + '/auth',
                method: 'POST',
                header: {
                    'content-type': 'application/json',
                    'Accept': 'application/json'
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify({ username: pm.environment.get("admin_user"), password: pm.environment.get("admin_password") })
                }
            }, function (err, res) {
                if (res.json().token) {
                    console.log(getNow() + 'Success');
                } else {
                    console.error(getNow() + "ERROR :");
                    console.error(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().token);
                }
            });
        };

        var getToken = (user, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Fetching token for user: ' + user.email + '...');
            return pm.sendRequest({
                url: pm.environment.get("url") + '/auth',
                method: 'POST',
                header: {
                    'content-type': 'application/json',
                    'Accept': 'application/json'
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify({ username: user.email, password: user.password })
                }
            }, function (err, res) {
                if (res.json().token) {
                    console.log(getNow() + 'token for user ' + user.email + ': Success');
                } else {
                    console.error(getNow() + "ERROR :");
                    console.error(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().token);
                }
            });
        };

        var getAddressFromDataGouv = (search, callback, retry = DEFAULT_NB_RETRY) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Fetching address from search: ' + search + '...');
            return pm.sendRequest({
                url: 'https://api-adresse.data.gouv.fr/search/?q=' + encodeURI(search) + '&type=housenumber&autocomplete=0',
                method: 'GET'
            }, function (err, res) {
                if (res.code == 200) {
                    let addr = res.json().features[0];
                    let addressResult = {
                        latitude: addr.geometry.coordinates[1],
                        longitude: addr.geometry.coordinates[0],
                        address: addr.properties.name,
                        zipcode: addr.properties.postcode,
                        city: addr.properties.city,
                        citycode: addr.properties.citycode,
                        departmentNumber: addr.properties.citycode.substr(0, 2),
                        legalX: addr.properties.x,
                        legalY: addr.properties.y
                    };
                    console.log(getNow() + 'address found for search ' + search + ': Success');
                    console.log(SEPARATOR);
                    if (callback) {
                        callback(addressResult);
                    }
                } else {
                    if (retry > 0) {
                        console.warn(getNow() + "WARNING - failed to retrieve address for search '" + search + "' - retrying after " + RETRY_DELAY + "ms (" + retry + " attempt(s) remaining)");
                        setTimeout(function () {
                            getAddressFromDataGouv(search, callback, retry - 1);
                        }, RETRY_DELAY);
                    } else {
                        console.error(getNow() + "ERROR - Failed to retrieve address for search '" + search + "'");
                    }
                }
            });
        };

        var formatFullAddressToBasicAddress = (addr) => {
            return {
                latitude: addr.latitude,
                longitude: addr.longitude,
                address: addr.address,
                zipcode: addr.zipcode,
                city: addr.city,
                citycode: addr.citycode,
                departmentNumber: addr.departmentNumber,
                legalX: addr.legalX,
                legalY: addr.legalY
            };
        }

        var mailtrapClean = (callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + "Cleaning mailtrap inbox...");
            return pm.sendRequest({
                url: 'https://mailtrap.io/api/v1/inboxes/' + pm.environment.get('mailtrap_inbox_id') + '/clean',
                method: 'PATCH',
                header: {
                    'Api-Token': pm.environment.get("mailtrap_api_token")
                }
            }, function (err, res) {
                console.log(getNow() + "...result:");
                console.log(res);
                console.log(SEPARATOR);
                if (callback) {
                    callback();
                }
            });
        };

        var mailtrapFindMessages = (params, callback, retry = DEFAULT_NB_RETRY, nbExpectedResults = -1) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Searching mail in mailtrap. Params:');
            console.log(params);

            pm.sendRequest({
                url: 'https://mailtrap.io/api/v1/inboxes/' + pm.environment.get('mailtrap_inbox_id') + '/messages',
                method: 'GET',
                header: {
                    'Api-Token': pm.environment.get("mailtrap_api_token")
                }
            }, function (err, res) {
                let messages = res.json();
                console.log("All messages from mailtrap :")
                console.log(messages)
                let foundMessages = [];
                for (let messageIndex in messages) {
                    let messageMatch = true;

                    if (params.to) {
                        messageMatch &= messages[messageIndex].to_email === params.to;
                    }
                    if (params.from) {
                        messageMatch &= messages[messageIndex].from_email === params.from;
                    }
                    if (params.subject) {
                        if (params.strictSubject) {
                            messageMatch &= messages[messageIndex].subject === params.subject;
                        } else {
                            messageMatch &= messages[messageIndex].subject.includes(params.subject);
                        }
                    }
                    if (messageMatch) {
                        foundMessages.push(messages[messageIndex]);
                        if (params.onlyFirst) {
                            break;
                        }
                    }
                }
                console.log(getNow() + 'Messages found in mailtrap for params ' + JSON.stringify(params) + ':');
                console.log(foundMessages);
                if (nbExpectedResults !== -1 && foundMessages.length !== nbExpectedResults) {
                    if (retry > 0) {
                        console.warn(getNow() + "WARNING - failed expected number of emails (found " + foundMessages.length + " intead of " + nbExpectedResults + " expected) - retrying after " + RETRY_DELAY + "ms (" + retry + " attempt(s) remaining)");
                        setTimeout(function () {
                            mailtrapFindMessages(params, callback, retry - 1, nbExpectedResults);
                        }, RETRY_DELAY);
                    } else {
                        console.warn(getNow() + "WARNING - failed to retrieve any email");
                        if (callback) {
                            callback(foundMessages);
                        }
                    }

                } else {
                    if (callback) {
                        callback(foundMessages);
                    }
                }
            });
        }

        var mailtrapGetActivationToken = (user, callback, retry = DEFAULT_NB_RETRY) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Fetching activation token for user ' + user.email + '...');
            pm.sendRequest({
                url: 'https://mailtrap.io/api/v1/inboxes/' + pm.environment.get('mailtrap_inbox_id') + '/messages',
                method: 'GET',
                header: {
                    'Api-Token': pm.environment.get("mailtrap_api_token")
                }
            }, function (err, res) {
                let messages = res.json();
                let activationMessage;
                for (let messageIndex in messages) {
                    if (messages[messageIndex].to_email === user.email && messages[messageIndex].id > 0) {
                        activationMessage = messages[messageIndex];
                        break;
                    }
                }
                if (activationMessage) {
                    pm.sendRequest({
                        url: 'https://mailtrap.io' + activationMessage.html_path,
                        method: 'GET',
                        header: {
                            'Api-Token': pm.environment.get("mailtrap_api_token")
                        }
                    }, function (err, res) {
                        let tokenUrl = pm.environment.get("ACTIVATION_URL").replace("{token}", "([a-f0-9]+)");
                        tokenUrl = tokenUrl.replace(new RegExp(/\?/, 'g'), '\\?');
                        var regex = new RegExp(tokenUrl, "g");
                        var token = regex.exec(res.text())[1];
                        console.log(getNow() + 'token for user ' + user.email + ': Success');
                        console.log(SEPARATOR);
                        if (callback) {
                            callback(token);
                        }
                    });
                } else {
                    if (retry > 0) {
                        console.warn(getNow() + "WARNING - failed to retrieve activation email for user '" + user.email + "'- retrying after " + RETRY_DELAY + "ms (" + retry + " attempt(s) remaining)");
                        setTimeout(function () {
                            mailtrapGetActivationToken(user, callback, retry - 1);
                        }, RETRY_DELAY);
                    } else {
                        console.error(getNow() + "ERROR - Failed to retrieve activation email for user '" + user.email + "'");
                        if (callback) {
                            callback();
                        }
                    }
                }
            });
        };

        var activateAccount = (token, password, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Activating account with token ' + token + '...');
            return pm.sendRequest({
                url: pm.environment.get("url") + '/account/activate/' + token,
                method: 'POST',
                header: {
                    'content-type': 'application/json',
                    'Accept': 'application/json'
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify({ password: password })
                }
            }, function (err, res) {
                console.log(getNow() + '...result :');
                console.log(res.json());
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().token);
                }
            });
        };

        var getUserFixture = (email) => {
            return pm.environment.get("userFixtures")[email];
        };

        var createUser = (user, adminToken, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Creating user ' + user.email + '...');
            getAddressFromDataGouv(user.address, function (address) {
                var userData = {
                    "activationUrl": pm.environment.get("ACTIVATION_URL"),
                    "email": user.email,
                    "firstname": user.firstname,
                    "lastname": user.lastname,
                    "phone": user.phone,
                    "company": user.company,
                    "address": address,
                    "roles": [user.role]
                }
                pm.sendRequest({
                    url: pm.environment.get("url") + '/users/',
                    method: 'POST',
                    header: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + adminToken
                    },
                    body: {
                        mode: 'raw',
                        raw: JSON.stringify(userData)
                    }
                }, function (err, res) {
                    if (res.json().user) {
                        console.log(getNow() + '...user ' + user.email + ' created: Success');
                    } else {
                        console.log(getNow() + "ERROR - API answer:");
                        console.log(res.json());
                    }
                    console.log(SEPARATOR);
                    mailtrapGetActivationToken(user, function (activationToken) {
                        activateAccount(activationToken, user.password, function () {
                            if (callback) {
                                callback(res.json().user);
                            }
                        })
                    })
                });
            });
        };

        var deleteUser = (user, token, callback, nextUsersToDelete) => {
            console.log(SEPARATOR);

            if (user.email === pm.environment.get("admin_user")) {
                console.log(getNow() + 'No no no, no deleting the admin (' + user.email + '), you little sh...');
                if (nextUsersToDelete && nextUsersToDelete.length > 0) {
                    user = nextUsersToDelete.pop();
                } else if (callback) {
                    callback();
                    return undefined;
                }
            }
            console.log(getNow() + 'Deleting user ' + user.email + '...');
            pm.sendRequest({
                url: pm.environment.get("url") + '/users/' + user.id,
                method: 'DELETE',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...result for deletion of user with id ' + user.id + ':');
                console.log(res.json());
                console.log(SEPARATOR);
                if (nextUsersToDelete && nextUsersToDelete.length > 0) {
                    let nextUser = nextUsersToDelete.pop();
                    deleteUser(nextUser, token, callback, nextUsersToDelete);
                } else if (callback) {
                    callback();
                }
            });
        };

        var deleteAllUsers = (token, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting all users...');

            pm.sendRequest({
                url: pm.environment.get("url") + '/users/?perpage=10000',
                method: 'GET',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                let users = res.json().users;
                if (users.length > 0) {
                    firstUser = users.pop();
                    deleteUser(firstUser, token, callback, users);
                    console.log(SEPARATOR);
                } else if (callback) {
                    callback()
                }
            });
        }

        var getLeadFixture = (name) => {
            return pm.environment.get("leadFixtures")[name];
        };

        var deleteLead = (lead, token, callback, nextLeadsToDelete) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting lead with id ' + lead.id + '...');

            pm.sendRequest({
                url: pm.environment.get("url") + '/leads/' + lead.id,
                method: 'DELETE',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(SEPARATOR);
                if (nextLeadsToDelete && nextLeadsToDelete.length > 0) {
                    let nextLead = nextLeadsToDelete.pop();
                    deleteLead(nextLead, token, callback, nextLeadsToDelete);
                } else if (callback) {
                    callback();
                }
            });
        }

        var createLead = (lead, creatorUser, adminToken, callback) => {
            console.log(SEPARATOR);

            var leadData = {
                leadOrigin: lead.leadOrigin,
                legals1: lead.legals1,
                legals2: lead.legals2,
                linkToLead: lead.linkToLead,
                comments: lead.comment,
                owner: {
                    id: creatorUser.id
                },
                address: formatFullAddressToBasicAddress(creatorUser.address)
            }

            console.log(getNow() + 'Creating lead from data');

            pm.sendRequest({
                url: pm.environment.get("url") + '/leads/',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + adminToken
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify(leadData)
                }
            }, function (err, res) {
                if (res.json().lead) {
                    console.log(getNow() + '...lead created: Success');
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().lead);
                }
            });
        };

        var assignLead = (lead, auditorToken, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Trying to assign lead ' + lead.id + ' to auditor');
            pm.sendRequest({
                url: pm.environment.get("url") + '/my/assignments',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + auditorToken
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify({ lead: { id: lead.id } })
                }
            }, function (err, res) {
                if (res.json().lead) {
                    console.log(getNow() + '...lead ' + lead.id + ' assigned:');
                    console.log(res.json().user);
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().lead);
                }
            });
        }

        var deleteAllLeads = (token, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting all leads...');
            pm.sendRequest({
                url: pm.environment.get("url") + '/leads/?perpage=10000',
                method: 'GET',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...leads found :');
                console.log(res.json());
                let leads = res.json().leads;
                if (leads.length > 0) {
                    firstLead = leads.pop();
                    deleteLead(firstLead, token, callback, leads);
                } else if (callback) {
                    callback();
                }
                console.log(SEPARATOR);
            });
        }

        var getPassportFixture = (name) => {
            return pm.environment.get("passportFixtures")[name];
        };

        var createPassport = (passport, creatorUser, auditor, lead, adminToken, callback) => {
            console.log(SEPARATOR);

            var passportData = formatPassportRequest(lead, auditor, creatorUser.address, passport)

            console.log(getNow() + 'Creating passport from data :');
            console.log(passportData)

            pm.sendRequest({
                url: pm.environment.get("url") + '/passports/',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + adminToken
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify(passportData)
                }
            }, function (err, res) {
                if (res.json().passport) {
                    console.log(getNow() + '...passport created: Success');
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().passport);
                }
            });
        };

        var deletePassport = (passport, token, callback, nextPassportsToDelete) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting passport with id ' + passport.id + '...');

            pm.sendRequest({
                url: pm.environment.get("url") + '/passports/' + passport.id,
                method: 'DELETE',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...result for deletion of passport with id ' + passport.id + ':');
                console.log(res.json());
                console.log(SEPARATOR);
                if (nextPassportsToDelete && nextPassportsToDelete.length > 0) {
                    let nextPassport = nextPassportsToDelete.pop();
                    deletePassport(nextPassport, token, callback, nextPassportsToDelete);
                } else if (callback) {
                    callback();
                }
            });
        }

        var deleteAllPassports = (token, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting all passports...');
            pm.sendRequest({
                url: pm.environment.get("url") + '/passports/?perpage=10000',
                method: 'GET',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                let passports = res.json().passports;
                if (passports.length > 0) {
                    firstPassport = passports.pop();
                    deletePassport(firstPassport, token, callback, passports);
                } else if (callback) {
                    callback();
                }
                console.log(SEPARATOR);
            });
        }

        var formatPassportRequest = (lead, auditor, userAddr, passportData) => {
            return {
                lead: {
                    id: lead.id
                },
                auditor: auditor,
                address: formatFullAddressToBasicAddress(userAddr),
                ...passportData
            };
        }

        var getRenovationPlanFixture = (name) => {
            return pm.environment.get("renovationPlanFixtures")[name];
        };

        var createRenovationPlan = (renovationPlan, passport, adminToken, callback) => {
            console.log(SEPARATOR);

            renovationPlanData = {
                ...renovationPlan,
                passport: {
                    id: passport.id
                }
            }

            console.log(getNow() + 'Creating renovation plan from data :');
            console.log(renovationPlanData)

            pm.sendRequest({
                url: pm.environment.get("url") + '/renovationPlans/',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + adminToken
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify(renovationPlanData)
                }
            }, function (err, res) {
                if (res.json().renovationPlan) {
                    console.log(getNow() + '...renovation plan created: Success');
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().renovationPlan);
                }
            });
        };

        var deleteRenovationPlan = (renovationPlan, token, callback, nextRenovationPlansToDelete) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting renovation plan with id ' + renovationPlan.id + '...');

            pm.sendRequest({
                url: pm.environment.get("url") + '/renovationPlans/' + renovationPlan.id,
                method: 'DELETE',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...result for deletion of renovation plan with id ' + renovationPlan.id + ':');
                console.log(res.json());
                console.log(SEPARATOR);
                if (nextRenovationPlansToDelete && nextRenovationPlansToDelete.length > 0) {
                    let nextRenovationPlan = nextRenovationPlansToDelete.pop();
                    deleteRenovationPlan(nextRenovationPlan, token, callback, nextRenovationPlansToDelete);
                } else if (callback) {
                    callback();
                }
            });
        }

        var deleteAllRenovationPlans = (token, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting all renovation plans...');
            pm.sendRequest({
                url: pm.environment.get("url") + '/renovationPlans/?perpage=10000',
                method: 'GET',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...renovation plans found :');
                console.log(res.json());
                let renovationPlans = res.json().renovationPlans;
                if (renovationPlans.length > 0) {
                    firstRenovationPlan = renovationPlans.pop();
                    deleteRenovationPlan(firstRenovationPlan, token, callback, renovationPlans);
                } else if (callback) {
                    callback();
                }
                console.log(SEPARATOR);
            });
        }

        var computeRPVPs = (renovationPlan, token, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Computing VPs for renovation plan with id ' + renovationPlan.id + '...');

            pm.sendRequest({
                url: pm.environment.get("url") + '/renovationPlans/' + renovationPlan.id + '/computeVps',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                if (res.json().renovationPlan) {
                    console.log(getNow() + '... Renovation plan with id ' + renovationPlan.id + ' once VPs are computed:');
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().renovationPlan);
                }
            });
        }

        var getCombinatoriesFixture = (name) => {
            return pm.environment.get("combinatoriesFixtures")[name];
        };

        var createCombinatory = (combinatory, adminToken, callback) => {
            console.log(SEPARATOR);

            console.log(getNow() + 'Creating combinatory from data :');
            console.log(combinatory)

            pm.sendRequest({
                url: pm.environment.get("url") + '/combinatories/',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + adminToken
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify(combinatory)
                }
            }, function (err, res) {
                if (res.json().combinatory) {
                    console.log(getNow() + '...combinatory created: Success');
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().combinatory);
                }
            });
        };

        var getVPFixture = (name) => {
            return pm.environment.get("vpFixtures")[name];
        };

        var deleteVP = (vp, token, callback, nextVPsToDelete) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting vp with id ' + vp.id + '...');

            pm.sendRequest({
                url: pm.environment.get("url") + '/vigilancePoints/' + vp.id,
                method: 'DELETE',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...result for deletion of vp with id ' + vp.id + ':');
                console.log(res.json());
                console.log(SEPARATOR);
                if (nextVPsToDelete && nextVPsToDelete.length > 0) {
                    let nextVP = nextVPsToDelete.pop();
                    deleteVP(nextVP, token, callback, nextVPsToDelete);
                } else if (callback) {
                    callback();
                }
            });
        }

        var createVP = (vp, adminToken, callback) => {
            console.log(SEPARATOR);

            console.log(getNow() + 'Creating VP from data :');
            console.log(vp)

            pm.sendRequest({
                url: pm.environment.get("url") + '/vigilancePoints/',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + adminToken
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify(vp)
                }
            }, function (err, res) {
                if (res.json().vigilancePoint) {
                    console.log(getNow() + '...vp created: Success');
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().vigilancePoint);
                }
            });
        };

        var deleteAllVPs = (token, callback) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting all VPs...');
            pm.sendRequest({
                url: pm.environment.get("url") + '/vigilancePoints/?perpage=10000',
                method: 'GET',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...VPs found :');
                console.log(res.json());
                let vps = res.json().vigilancePoints;
                if (vps.length > 0) {
                    firstVP = vps.pop();
                    deleteVP(firstVP, token, callback, vps);
                    console.log(SEPARATOR);
                } else if (callback) {
                    callback();
                }

            });
        }

        var deleteCondition = (condition, vp, token, callback, nextConditionsToDelete) => {
            console.log(SEPARATOR);
            console.log(getNow() + 'Deleting condition with id ' + condition.id + 'from vp with id ' + vp.id + '...');

            pm.sendRequest({
                url: pm.environment.get("url") + '/vigilancePoints/' + vp.id + '/removeCondition/' + condition.id,
                method: 'DELETE',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            }, function (err, res) {
                console.log(getNow() + '...result for deletion of condition with id ' + condition.id + ':');
                console.log(res.json());
                console.log(SEPARATOR);
                if (nextConditionsToDelete && nextConditionsToDelete.length > 0) {
                    let nextCondition = nextConditionsToDelete.pop();
                    deleteCondition(nextCondition, token, callback, nextConditionsToDelete);
                } else if (callback) {
                    callback();
                }
            });
        }

        var createCondition = (condition, vp, adminToken, callback) => {
            console.log(SEPARATOR);

            console.log(getNow() + 'Creating condition from data :');
            console.log(condition);
            console.log(getNow() + 'For VP :');
            console.log(vp);

            pm.sendRequest({
                url: pm.environment.get("url") + '/vigilancePoints/' + vp.id + '/addCondition',
                method: 'POST',
                header: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + adminToken
                },
                body: {
                    mode: 'raw',
                    raw: JSON.stringify(condition)
                }
            }, function (err, res) {
                if (res.json().vigilancePoint) {
                    console.log(getNow() + '...condition created: Success');
                } else {
                    console.log(getNow() + "ERROR - API answer:");
                    console.log(res.json());
                }
                console.log(SEPARATOR);
                if (callback) {
                    callback(res.json().vigilancePoint);
                }
            });
        };

        var cleanEverything = (callback) => {
            mailtrapClean(function () {
                getAdminToken(function (adminToken) {
                    deleteAllRenovationPlans(adminToken, function () {
                        deleteAllPassports(adminToken, function () {
                            deleteAllLeads(adminToken, function () {
                                deleteAllUsers(adminToken, function () {
                                    deleteAllVPs(adminToken, function () {
                                        if (callback) {
                                            callback(adminToken);
                                        }
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }

        return {
            mailtrap: {
                cleanInbox: mailtrapClean,
                getActivationToken: mailtrapGetActivationToken,
                findMessages: mailtrapFindMessages
            },
            dataGouv: {
                findAddress: getAddressFromDataGouv
            },
            users: {
                fixtures: {
                    get: getUserFixture
                },
                create: createUser,
                delete: deleteUser,
                deleteAll: deleteAllUsers,
                activate: activateAccount
            },
            leads: {
                fixtures: {
                    get: getLeadFixture
                },
                create: createLead,
                delete: deleteLead,
                deleteAll: deleteAllLeads,
                assign: assignLead
            },
            passports: {
                fixtures: {
                    get: getPassportFixture
                },
                create: createPassport,
                delete: deletePassport,
                deleteAll: deleteAllPassports,
                format: formatPassportRequest
            },
            renovationPlans: {
                fixtures: {
                    get: getRenovationPlanFixture
                },
                create: createRenovationPlan,
                delete: deleteRenovationPlan,
                deleteAll: deleteAllRenovationPlans,
                computeVPs: computeRPVPs
            },
            combinatories: {
                fixtures: {
                    get: getCombinatoriesFixture
                },
                create: createCombinatory
            },
            vps: {
                fixtures: {
                    get: getVPFixture
                },
                condition: {
                    create: createCondition,
                    delete: deleteCondition
                },
                create: createVP,
                delete: deleteVP
            },
            auth: {
                getToken,
                getAdminToken
            },
            format: {
                address: {
                    fullToBasic: formatFullAddressToBasicAddress
                }
            },
            cleanAll: cleanEverything
        };
    });

    pm.environment.set("is_initialized", "true");
}