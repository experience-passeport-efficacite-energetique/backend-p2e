<?php

require_once __DIR__.'/../../vendor/autoload.php';

require_once __DIR__.'/../../src/env.php';
require_once __DIR__.'/../../src/app.php';
require_once __DIR__.'/../../src/security.php';
require_once __DIR__.'/../../src/controllers.php';
require_once __DIR__.'/../../src/errors.php';
require_once __DIR__.'/../../src/console.php';
