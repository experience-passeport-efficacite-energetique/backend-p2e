<?php

namespace Agilap\Tests;

use Agilap\Model\Bean\User;
use Agilap\Model\Dao\UserDao;
use Agilap\Service\JWTService;
use Lcobucci\JWT\Parser;
use PHPUnit\Framework\TestCase;
use TheCodingMachine\TDBM\NoBeanFoundException;
use TheCodingMachine\TDBM\TDBMException;

class JWTServiceTest extends TestCase
{
    private const USERNAME = 'test@phpunit';
    private const PASSWORD = 'Sesame::open()';

    /** @var JWTService */
    private $jwtService;
    /** @var UserDao */
    private $userDao;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        global $app;// = require __DIR__.'/../../src/app.php';
        $this->jwtService = $app['jwt.service'];
        $this->userDao = new UserDao($app['tdbmService']);
    }

    public function testGenerateJWT()
    {
        $user = $this->getTestUser();
        self::assertNotNull($user);
        $jwt = $this->jwtService->generateJWT($user);
        $parser = new Parser();
        $token = $parser->parse($jwt);
        $issuer = strval($token->getClaim('iss'));
        self::assertEquals(self::USERNAME, $issuer);
    }


    private function getTestUser()
    {
        $username = self::USERNAME;
        try {
            $user =  $this->userDao->getByEmail($username);
            if ($user !== null) {
                return $user;
            } else {
                $user = new User();
                $user->setEmail($username);
                $user->setPassword(self::PASSWORD);
                $user->setActive(true);
                $this->userDao->save($user);
                return $user;
            }
        } catch (TDBMException $exception) {
            self::assertFalse(true, $exception->getMessage());
            return null;
        }
    }

    protected function tearDown()
    {
        $user = $this->userDao->getByEmail(self::USERNAME);
        if ($user !== null) {
            $this->userDao->delete($user);
        }
    }
}
