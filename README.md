[![pipeline status](https://gitlab.com/experience-passeport-efficacite-energetique/backend-p2e/badges/dev/pipeline.svg)](https://gitlab.com/experience-passeport-efficacite-energetique/backend-p2e/commits/dev) [![coverage report](https://gitlab.com/experience-passeport-efficacite-energetique/backend-p2e/badges/dev/coverage.svg)](https://experience-passeport-efficacite-energetique.gitlab.io/backend-p2e/)

- [Installation](#installation)
  - [Environment](#environment)
  - [Database management](#database-management)
  - [PHP Extensions](#php-extensions)
  - [Local developpment with Docker (preferred way)](#local-developpment-with-docker-preferred-way)
    - [Dev tips](#dev-tips)
- [CI Workflow (to be improve!)](#ci-workflow-to-be-improve)
  - [Dev](#dev)
  - [Staging](#staging)
  - [Prod](#prod)
- [Tests](#tests)
- [API P2E](#api-p2e)
  - [Generate doc](#generate-doc)

This is the Backend part of Passeport Efficacité Energétique application. This was developed in Php / Silex framework.
To view frontend application repo -> https://gitlab.com/experience-passeport-efficacite-energetique/frontend-p2e

# Installation

## Environment

All environment-specific values are provided through, following priority:

- untracked file **var/env.yml** (you could init it from `cp var/env.default.yml var/env.yml`)
- environment variables set from the execution context

By defaut an admin user is created first, see in `var/env.default.yml`

```
ADMIN_EMAIL: admin@localhost
ADMIN_PASSWORD: password
```

Therefore, if you run this project on a platform that does not allow you to have untracked files, you have to be able at least to set its environment variables.

Doc for the TDBM framework is here:
https://thecodingmachine.github.io/tdbm/index.html

Doc for the TCM Fluid Hydrator:
https://github.com/thecodingmachine/fluid-hydrator

## Database management

**Mysql 5.7** is preferable, make sure engine **InnoDB** is used (to support foreign keys).

Once your database connection is ready, you will need to initialize your schema. Run command:

```sh
bin/console schema:apply -y
```

To update DB, you need to modify `etc/db_schema.yml`

Whenever updating the project on server, use this command if modifications are safe, otherwise, if you need to review
the SQL for the migration, you can run:

```sh
bin/console schema:apply --dry-run
```

Then, you can edit the SQL before manually executing it.

When developing, more precisely when you need to modify your database schema, make sure to respect **STRICTLY** the order
of the columns in tables; if you do not, you may unexpectedly switch lines in YAML file, ending up in a very unpleasant
yo-yo game with co-developers. Make sure to use option `--strict` with `apply` command to avoid these problems.

Once the DB is updateed, you need to regenerate the PHP beans :
no docker:

```
bin/console tdbm:generate
```

with docker:

```
docker-compose exec web bin/console tdbm:generate
```

## PHP Extensions

Make sure you have following extensions installed and enabled:

- php_yaml

## Local developpment with Docker (preferred way)

```sh
docker-compose build
docker-compose up -d
```

`-d` option is for detached mode (you could check logs for a container with `docker-compose logs -f <name of service>`)
By default db files ares persisted in `.docker/mysql/volume` folder.
_**NB: at first launch you could have race condition between DB startup and web.** Safe way consists in launching the DB service first with `docker-compose up -d db` and then `docker-compose up -d` to start both services._

After correct launch execute these data imports:

```sh
docker-compose exec web php bin/console db:load etc/db_static_regions.yml
docker-compose exec web php bin/console db:load etc/db_static_departments.yml
docker-compose exec web php bin/console db:load etc/db_static_cities.yml
```

### Dev tips

log in file:

```
error_log("\n" . "SomeString" . print_r($someVar, true), 3, "/var/tmp/debug.log");
```

get a terminal in docker:

```
docker-compose exec web bash
```

and then you can watch the log file inside docker :

```
cd /var/tmp/
tail -f ./debug.log
```

# CI Workflow (to be improve!)

## Dev

During dev commit to whatever branch you want (prefer feature-_ named branch for each feature, then ask for a merge request into dev), except `master` or with tagged ref like `release-_`. The minimal pipeline is to run test and coverage

## Staging

If you want to deploy to staging env, tag with ref like `release-*`. It will run tests like in dev but additionaly deploy to the server see CI/CD > Environments

## Prod

On commit on master, it will run all the pipe and deploy for production on the server

# Tests

Few Phpunit tests in `tests/php` folder. But very complete API tests with Postman (as E2E tests).
Postman tests usage:

- `tests/api/pretests_scripts.js` sumup the collection property defining the prequest scripts.
- `tests/api/tests.postman_collection.json`: the Postman collection
- `tests/api/tests.postman_environment.json`: the Postman environment to execute the tests' collection. Remember to adapt values to your configuration.
- `tests/api/coverage_utils`: PHP scripts to append and prepend via .htaccess modification (see `etc/docker.htaccess` for eg).

# API P2E

## Generate doc

To generate the api doc to the openAPI format :

```
./vendor/bin/openapi -o ./docs/api/OAS-p2e-api.json ./src
```

docker:

```
docker-compose exec web ./vendor/bin/openapi -o ./docs/api/OAS-p2e-api.json ./src
```

API doc is available at https://api.app.passeport-efficacite-energetique.org/doc
