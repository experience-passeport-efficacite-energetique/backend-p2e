<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

require_once __DIR__.'/../vendor/autoload.php';

require __DIR__.'/../src/env.php';

if (DEV) {
    Debug::enable();
    ErrorHandler::register();
    ExceptionHandler::register();
    ini_set('error_reporting', E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    ini_set('display_errors', 1);
}

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../src/security.php';
require __DIR__.'/../src/controllers.php';
require __DIR__.'/../src/errors.php';
$app->run();
