<?php

use Agilap\Controller\AccountController;
use Agilap\Controller\APIController\CombinatoryAPIController;
use Agilap\Controller\APIController\DomesticHotWaterAPIController;
use Agilap\Controller\APIController\HeatingAPIController;
use Agilap\Controller\APIController\LeadApiController;
use Agilap\Controller\APIController\OptionsAPIController;
use Agilap\Controller\APIController\PassportAPIController;
use Agilap\Controller\APIController\PlaceSearchAPIController;
use Agilap\Controller\APIController\ProfileAPIController;
use Agilap\Controller\APIController\RenovationPlanAPIController;
use Agilap\Controller\APIController\StaticPricesAPIController;
use Agilap\Controller\APIController\TerritoryAPIController;
use Agilap\Controller\APIController\UserAPIController;
use Agilap\Controller\APIController\VentilationAPIController;
use Agilap\Controller\APIController\VigilancePointAPIController;
use Agilap\Controller\AuthenticationController;
use Agilap\Controller\SwaggerUIController;
use Agilap\Enum\Role;
use Agilap\Security\CheckRole;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/** @var Application $app */

/**
 *  @OA\Info(title="P2E API", version="1.0")
 *
 *  @OA\SecurityScheme(
 *    securityScheme="JWTtoken",
 *    name="JWTtoken",
 *    type="http",
 *    bearerFormat="JWT",
 *    scheme="bearer"
 *  )
 *
 *  @OA\Response(
 *     response=400,
 *     description="Bad request",
 *  )
 *  @OA\Response(
 *     response=401,
 *     description="Unauthorized",
 *  )
 *  @OA\Response(
 *     response=403,
 *     description="Forbidden",
 *  )
 *  @OA\Response(
 *     response=404,
 *     description="Not found",
 *  )
 *  @OA\Response(
 *     response=422,
 *     description="Unprocessable entity",
 *  )
 *
 *  @OA\Schema(
 *      schema="idParam",
 *      type="integer"
 *  )
 *
 *  @OA\Schema(
 *      schema="genericBoolean",
 *      type="boolean"
 *  )
 *
 *  @OA\Schema(
 *      schema="genericToken",
 *      type="string"
 *  )
 *
 *  @OA\Schema(
 *     schema="zoneEnum",
 *     type="string",
 *     enum={"H1","H2","H3"}
 *  )
 *
 *  @OA\Schema(
 *     schema="airtightnessEnum",
 *     type="string",
 *     enum={"BAD","AVERAGE","GOOD","VERY_GOOD"}
 *  )
 *
 *  @OA\Schema(
 *     schema="insulationEnum",
 *     type="string",
 *     enum={"ITI","ETI","NONE"}
 *  )
 */


$app['controller.options'] = function ($app) {
    return new OptionsAPIController($app['routes'], $app['request_context']);
}; {
    $app->options('{anything}', 'controller.options:options')->assert('anything', '.*');
}

$app['controller.auth'] = function ($app) {
    return new AuthenticationController($app['users'], $app['jwt.service'], $app['security.token_storage']);
}; { // Authentication
    $app->get('/auth', 'controller.auth:authenticate');
    $app->post('/auth', 'controller.auth:authenticate');

    $app->get('/auth/refresh', 'controller.auth:refresh');
    $app->post('/auth/refresh', 'controller.auth:refresh');
}

$app['controller.swagger'] = new SwaggerUIController(); { // API Doc display
    $app->get('/doc', 'controller.swagger:getApiDoc');
}

$app['controller.account'] = function ($app) {
    return new AccountController($app['tdbmService'], $app['mail.helper']);
}; { // Account management
    $app->post('/account/create', 'controller.account:create');

    $app->post('/account/recover', 'controller.account:recover');

    $app->post('/account/reset-password/{recoveryToken}', 'controller.account:resetPassword');

    $app->post('/account/activate/{activationToken}', 'controller.account:activate');
}

$app['controller.profile'] = function ($app) {
    return new ProfileAPIController($app['tdbmService'], $app['security.token_storage'], $app['mail.helper']);
}; { // Personal information
    $app->get('/my/profile', 'controller.profile:get');
    $app->patch('/my/profile', 'controller.profile:update');

    $app->get('/my/assignments', 'controller.profile:getAssignments');
    $app->post('/my/assignments', 'controller.profile:addAssignment');
    $app->delete('/my/assignments/{id}', 'controller.profile:deleteAssignment')->assert('id', '\d+');

    $app->get('/my/leads', 'controller.profile:getLeads');
    $app->get('/app/maxFlyingDistances', 'controller.profile:getMaxFlyingDistances')->before(CheckRole::required(Role::AUDITOR, Role::ADMIN));
}

$app['controller.territories'] = new TerritoryAPIController($app['tdbmService'], $app['security.token_storage']); { // territories management
    $app->post('/territories/{id}/user-assignment', 'controller.territories:addUserAssignment')->before(CheckRole::required(Role::ADMIN, Role::ADMIN_TERRITORY));
    $app->get('/territories/{id}/user-assignment', 'controller.territories:getUserAssignments')->before(CheckRole::required(Role::ADMIN, Role::ADMIN_TERRITORY));
    $app->delete('/territories/{id}/user-assignment', 'controller.territories:deleteUserAssignment')->assert('id', '\d+')->before(CheckRole::required(Role::ADMIN, Role::ADMIN_TERRITORY));
    $app->post('/territories/{id}/city-assignment', 'controller.territories:addCityAssignment')->before(CheckRole::required(Role::ADMIN));
    $app->get('/territories/{id}/city-assignment', 'controller.territories:getCityAssignments')->before(CheckRole::required(Role::ADMIN, Role::ADMIN_TERRITORY));
    $app->delete('/territories/{id}/city-assignment', 'controller.territories:deleteCityAssignment')->assert('id', '\d+')->before(CheckRole::required(Role::ADMIN));
    $app->mount('/territories', $app['controller.territories']);
}

$app['controller.places'] = new PlaceSearchAPIController($app['tdbmService'], $app['security.token_storage']); { // territories management
    $app->get('/places', 'controller.places:findFromString');
    $app->mount('/places', $app['controller.places']);
}

$app['controller.users'] = new UserAPIController($app['tdbmService'], $app['security.token_storage'], $app['mail.helper']);
$app->post('/users/{id}/invite', 'controller.users:invite')->before(CheckRole::required(Role::AUDITOR, Role::ADMIN_TERRITORY, Role::ADMIN));
$app->mount('/users', $app['controller.users']);

$app['controller.leads'] =  new LeadApiController($app['tdbmService'], $app['security.token_storage'], $app['mail.helper']); {
    $app->get('/leads/dashboard', 'controller.leads:getDashboardData')->before(CheckRole::required(Role::AUDITOR, Role::ADMIN_TERRITORY, Role::USER_TERRITORY, Role::ADMIN));
    $app->mount('/leads', $app['controller.leads']);
}

$app['controller.passports'] = new PassportAPIController($app[('tdbmService')], $app['security.token_storage']); { // renovation plan management
    $app->get('/passports/emissionTarget', 'controller.passports:getEmissionTarget');
    $app->mount('/passports', $app['controller.passports']);
}

$app->mount('/heatings', new HeatingAPIcontroller($app['tdbmService'], $app['security.token_storage']));
$app->mount('/domesticHotWaters', new DomesticHotWaterAPIController($app['tdbmService'], $app['security.token_storage']));
$app->mount('/ventilations', new VentilationAPIController($app['tdbmService']));
$app->mount('/combinatories', new CombinatoryAPIController($app['tdbmService'], $app['security.token_storage']));

$app['controller.renovationPlans'] = new RenovationPlanAPIController($app[('tdbmService')], $app['security.token_storage'], $app['middleware.service'], $app['mail.helper']); { // renovation plan management
    $app->post('/renovationPlans/{id}/computeVps', 'controller.renovationPlans:computeVPs');
    $app->get('/renovationPlans/{id}/priceRanges', 'controller.renovationPlans:getPriceRanges');
    $app->get('/renovationPlans/{id}/send_report', 'controller.renovationPlans:sendReport');
    $app->get('/renovationPlans/{id}/vigilancePoint', 'controller.renovationPlans:getVigilancePoints');
    $app->mount('/renovationPlans', $app['controller.renovationPlans']);
}

$app['controller.vigilancePoint'] = new VigilancePointAPIController($app[('tdbmService')], $app['security.token_storage']); {
    $app->post('/vigilancePoints/{vpId}/addCondition', 'controller.vigilancePoint:addCondition');
    $app->patch('/vigilancePoints/{vpId}/updateCondition/{conditionId}', 'controller.vigilancePoint:updateCondition');
    $app->delete('/vigilancePoints/{vpId}/removeCondition/{conditionId}', 'controller.vigilancePoint:removeCondition');
    $app->mount('/vigilancePoints', $app['controller.vigilancePoint']);
}

$app->mount('/staticPrices', new StaticPricesAPIController($app['tdbmService']));

if (DEV) {
    // TODO: find a proper way to clear cache when needed (ie updating project)
    $app->get('clear_cache', function () {
        (new \Doctrine\Common\Cache\ApcuCache())->deleteAll();
        return new JsonResponse(['message' => 'cache cleared']);
    });
}

$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', ALLOW_ORIGIN);
});
