<?php

use Agilap\Exception\APIException;
use Agilap\Exception\ApiSchemaException;
use Agilap\Exception\DataNotFoundException;
use MetaHydrator\Exception\HydratingException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

$app->error(function(NotFoundHttpException $e, Request $request, $code) use ($app) {
    return new JsonResponse(['message' => $e->getMessage() ?: 'This route does not exist'], Response::HTTP_NOT_FOUND);
});
$app->error(function (AuthenticationException $e, Request $request, $code) {
    return new JsonResponse(['message' => $e->getMessage() ?: 'Authentication failed'], Response::HTTP_FORBIDDEN);
});
$app->error(function (UnauthorizedHttpException $e, Request $request, $code) {
    $authUrl = $request->getSchemeAndHttpHost().$request->getBaseUrl().'/auth';
    return new JsonResponse(['message' => $e->getMessage() ?: 'Unauthorized access'], Response::HTTP_UNAUTHORIZED,
                            ['WWW-Authenticate' => "Bearer realm=\"$authUrl\""]);
});
$app->error(function (AccessDeniedHttpException $e, Request $request, $code) {
    return new JsonResponse(['message' => $e->getMessage() ?: 'Access forbidden'], Response::HTTP_FORBIDDEN);
});
$app->error(function (ApiSchemaException $e, Request $request, $code) {
    return new JsonResponse([
        'message' => $e->getMessage() ?: 'Invalid data provided',
        'errors' => $e->getErrors()
    ], Response::HTTP_UNPROCESSABLE_ENTITY);
});
$app->error(function (HydratingException $e, Request $request, $code) {
    return new JsonResponse([
        'message' => $e->getMessage() ?: 'Invalid data provided',
        'errors' => $e->getErrorsMap()
    ], Response::HTTP_UNPROCESSABLE_ENTITY);
});
$app->error(function (DataNotFoundException $e, Request $request, $code) {
    return new JsonResponse([
        'message' => $e->getMessage() ?: 'Resource not found'
    ], Response::HTTP_NOT_FOUND);
});
$app->error(function (MethodNotAllowedHttpException $e, Request $request, $code) {
    if ($e->getHeaders()['Allow'] === 'OPTIONS') {
        return new JsonResponse(['message' => 'This route does not exist'], Response::HTTP_NOT_FOUND);
    } else {
        return new JsonResponse(['message' => $e->getMessage()], Response::HTTP_METHOD_NOT_ALLOWED, $e->getHeaders());
    }
});
$app->error(function (APIException $e, Request $request, $code) {
    return new JsonResponse($e->getBody(), $e->getStatusCode(), $e->getHeaders());
});

// Fallback
if (DEV) {
    $app->error(function(Throwable $t, Request $request, $code) {
        return new JsonResponse([
            'message' => $t->getMessage() ?: 'Internal server error',
            'file' => $t->getFile(),
            'line' => $t->getLine(),
            'trace' => $t->getTrace()
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    });
} else {
    $app->error(function (Throwable $t, Request $request, $code) use ($app) {
        return new JsonResponse(['message' => 'Internal server error'], Response::HTTP_INTERNAL_SERVER_ERROR);
    });
}
