<?php
namespace Agilap\Hydration\Hydrator;

use MetaHydrator\Handler\SimpleHydratingHandler;
use MetaHydrator\MetaHydrator;
use MetaHydrator\Parser\FloatParser;
use MetaHydrator\Parser\StringParser;
use MetaHydrator\Validator\MaxLengthValidator;
use MetaHydrator\Validator\NotEmptyValidator;

class AddressHydrator extends MetaHydrator
{
    public function __construct(string $errorMessage = 'Invalid value')
    {
        $handlers = [];
        $handlers[] = new SimpleHydratingHandler('address', new StringParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('zipcode', new StringParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('city', new StringParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('citycode', new StringParser('Invalid value'));
        $handlers[] = new SimpleHydratingHandler('departmentNumber', new StringParser('Invalid value'), [
            new NotEmptyValidator('This field is required'),
            new MaxLengthValidator(3, 'Longueur max: 3')
        ]);

        $handlers[] = new SimpleHydratingHandler('latitude', new FloatParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('longitude', new FloatParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('legalX', new FloatParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('legalY', new FloatParser('Invalid value'), [new NotEmptyValidator('This field is required')]);

        parent::__construct($handlers);
    }
}
