<?php

namespace Agilap\Hydration\Hydrator;

use Agilap\Enum\PriceUnit;
use Agilap\Model\Bean\PriceRange;
use MetaHydrator\Handler\SimpleHydratingHandler;
use MetaHydrator\MetaHydrator;
use MetaHydrator\Parser\FloatParser;
use MetaHydrator\Parser\StringParser;
use MetaHydrator\Validator\AbstractValidator;
use MetaHydrator\Validator\EnumValidator;
use MetaHydrator\Validator\NotEmptyValidator;

class PriceRangeHydrator extends MetaHydrator
{
    public function __construct(string $errorMessage = 'invalid range')
    {
        $handlers = [];
        $handlers[] = new SimpleHydratingHandler('unit', new StringParser('Invalid value'), [new NotEmptyValidator('This field is required'), new EnumValidator(PriceUnit::Enum, "Allowed values: " . implode(', ', PriceUnit::Enum))]);
        $handlers[] = new SimpleHydratingHandler('min', new FloatParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('max', new FloatParser('Invalid value'), [new NotEmptyValidator('This field is required')]);
        $handlers[] = new SimpleHydratingHandler('definedMin', new FloatParser('Invalid value'));
        $handlers[] = new SimpleHydratingHandler('definedMax', new FloatParser('Invalid value'));
        $validators = [];
        $validators['range'] = new class ($errorMessage) extends AbstractValidator
        {
            /**
             * @param mixed $value
             * @param PriceRange|null $contextObject
             */
            public function validate($value, $contextObject = null)
            {
                if ((!key_exists('min', $value) || !key_exists('max', $value)
                        || !key_exists('definedMin', $value) || !key_exists('definedMax', $value))
                    && $contextObject === null
                ) {
                    return;
                }
                $min = isset($value['min']) ? $value['min'] : $contextObject->getMin();
                $max = isset($value['max']) ? $value['max'] : $contextObject->getMax();
                if ($min > $max) {
                    $this->throw();
                }
                $definedMin = isset($value['definedMin']) ? $value['definedMin'] : 0;
                $definedMax = isset($value['definedMax']) ? $value['definedMax'] : 0;
                if ($definedMin > $definedMax) {
                    $this->throw();
                }
            }
        };
        parent::__construct($handlers, $validators);
    }
}
