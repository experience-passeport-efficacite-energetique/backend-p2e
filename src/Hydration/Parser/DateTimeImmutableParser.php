<?php
namespace Agilap\Hydration\Parser;


use MetaHydrator\Parser\DateTimeParser;

class DateTimeImmutableParser extends DateTimeParser
{
    public function parse($rawValue)
    {
        /** @var null|\DateTime $dateTime */
        $dateTime = parent::parse($rawValue);
        if ($dateTime !== null) {
            return \DateTimeImmutable::createFromMutable($dateTime);
        } else {
            return null;
        }
    }
}