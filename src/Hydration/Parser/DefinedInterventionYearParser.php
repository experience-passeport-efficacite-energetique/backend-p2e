<?php

namespace Agilap\Hydration\Parser;

use MetaHydrator\Exception\ParsingException;
use MetaHydrator\Parser\IntParser;

class DefinedInterventionYearParser extends IntParser
{
    private $forceInterventionYearPointer;
    private $proposedInterventionYearPointer;

    public function __construct($forceInterventionYearPointer, $proposedInterventionYearPointer)
    {
        parent::__construct('');
        $this->forceInterventionYearPointer = $forceInterventionYearPointer;
        $this->proposedInterventionYearPointer = $proposedInterventionYearPointer;
    }

    /**
     * @param $rawValue
     * @return mixed
     *
     * @throws ParsingException
     */
    public function parse($rawValue)
    {
        if ($rawValue === null || $rawValue === '') {
            return null;
        }

        if (!$this->forceInterventionYearPointer->value) {
            return parent::parse($this->proposedInterventionYearPointer->value);
        }

        return parent::parse($rawValue);
    }
}
