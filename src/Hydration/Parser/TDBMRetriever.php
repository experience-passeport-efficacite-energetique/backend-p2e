<?php
namespace Agilap\Hydration\Parser;

use MetaHydrator\Exception\ParsingException;
use MetaHydrator\Parser\AbstractParser;
use TheCodingMachine\TDBM\TDBMService;

class TDBMRetriever extends AbstractParser
{
    /** @var TDBMService */
    protected $tdbmService;

    /** @var string */
    protected $table;

    /** @var array */
    protected $aliases;

    public function __construct(TDBMService $tdbmService, string $table, array $aliases = [], $errorMessage = 'Database error')
    {
        parent::__construct($errorMessage);
        $this->tdbmService = $tdbmService;
        $this->table = $table;
        $this->aliases = $aliases;
    }

    /**
     * @param $rawValue
     * @return mixed
     *
     * @throws ParsingException
     */
    public function parse($rawValue)
    {
        if (empty($rawValue) || !is_array($rawValue)) {
            return null;
        }

        $params = [];
        $filters = [];
        foreach ($this->tdbmService->getPrimaryKeyColumns($this->table) as $pk) {
            $key = key_exists($pk, $this->aliases) ? $this->aliases[$pk] : $pk;
            if (!key_exists($key, $rawValue) || !is_scalar($rawValue[$key])) {
                return null;
            }
            $params[$key] = $rawValue[$key];
            $filters[] = "$pk = :$key";
        }

        try {
            return $this->tdbmService->findObjectOrFail($this->table, implode(' AND ', $filters), $params);
        } catch (\Exception $e) {
            $this->throw();
            return null;
        }
    }
}
