<?php

namespace Agilap\Hydration\Validator;

use Agilap\Model\Bean\Lead;
use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\AbstractValidator;

class UnassignedLeadValidator extends AbstractValidator
{
    public function __construct(string $errorMessage = "Demande d'audit déjà assignée")
    {
        parent::__construct($errorMessage);
    }

    /**
     * @param Lead $value
     * @param $contextObject
     *
     * @throws ValidationException
     */
    public function validate($value, $contextObject = null)
    {
        if ($value !== null && $value->getAuditor() !== null) {
            $this->throw();
        }
    }
}
