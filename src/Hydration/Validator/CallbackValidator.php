<?php

namespace Agilap\Hydration\Validator;

use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\ValidatorInterface;

class CallbackValidator implements ValidatorInterface
{
    /** @var callable */
    private $callback;

    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * @param mixed $value
     * @param $contextObject
     *
     * @throws ValidationException
     */
    public function validate($value, $contextObject = null)
    {
        call_user_func($this->callback, $value, $contextObject);
    }
}