<?php
namespace Agilap\Hydration\Validator;

use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\AbstractValidator;
use TheCodingMachine\TDBM\TDBMService;

class IntegerHigherThanValidator extends AbstractValidator
{

  /** @var integer */
  private $valuePointer;

  public function __construct($valuePointer, $valueName)
  {
    $errorMessage = 'Doit être supérieur ' . $valueName;
    parent::__construct($errorMessage);
    $this->valuePointer = $valuePointer;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, $contextObject = null)
  {
    if ($value && $value < $this->valuePointer->value) {
      $this->throw();
    }
  }
}
