<?php

namespace Agilap\Hydration\Validator;

use Agilap\Misc\Pointer;
use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\ValidatorInterface;

class CacheResult implements ValidatorInterface
{
    private $pointer;

    public function __construct(Pointer $pointer)
    {
        $this->pointer = $pointer;
    }

    /**
     * @param mixed $value
     * @param $contextObject
     *
     * @throws ValidationException
     */
    public function validate($value, $contextObject = null)
    {
        $this->pointer->value = $value;
    }
}