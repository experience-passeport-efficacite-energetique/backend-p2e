<?php

namespace Agilap\Hydration\Validator;

use Agilap\Model\Bean\Lead;
use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\AbstractValidator;

class UndoneLeadValidator extends AbstractValidator
{
    public function __construct(string $errorMessage = "un passeport existe déjà pour cette demande")
    {
        parent::__construct($errorMessage);
    }

    /**
     * @param Lead $value
     * @param $contextObject
     *
     * @throws ValidationException
     */
    public function validate($value, $contextObject = null)
    {
        if ($value !== null) {
            return;
        }
        $passports = $value->getPassports();
        if (count($passports) === 0) {
            return;
        }
        if (count($passports) === 1 && $passports[0] === $contextObject) {
            return;
        }
        $this->throw();
    }
}
