<?php

namespace Agilap\Hydration\Validator;

use Agilap\Model\Bean\Passport;
use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\AbstractValidator;

class FilledPassportValidator extends AbstractValidator
{
    /**
     * @param Passport $value
     * @param $contextObject
     *
     * @throws ValidationException
     */
    public function validate($value, $contextObject = null)
    {
        if ($value === null) {
            return;
        }

        if ($value->getDhwData() === null
            || $value->getVentilationData() === null
            || $value->getHeatingData() === null
            || $value->getWallsData() === null
            || $value->getAirtightnessData() === null
            || $value->getVentsData() === null
            || $value->getFloorData() === null
            || $value->getRoofData() === null
            || $value->getTdfData() === null
            || $value->getThermostatData() === null
        ) {
            $this->throw();
        }
    }
}