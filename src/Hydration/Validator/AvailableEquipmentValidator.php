<?php

namespace Agilap\Hydration\Validator;

use Agilap\Model\Bean\DomesticHotWater;
use Agilap\Model\Bean\Heating;
use Agilap\Model\Bean\Ventilation;
use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\AbstractValidator;

class AvailableEquipmentValidator extends AbstractValidator
{
    /** @var bool */
    private $availability;

    public function __construct(bool $availability, string $errorMessage = 'Invalid value')
    {
        parent::__construct($errorMessage);
        $this->availability = $availability;
    }

    /**
     * @param DomesticHotWater|Heating|Ventilation $value
     * @param $contextObject
     *
     * @throws ValidationException
     */
    public function validate($value, $contextObject = null)
    {
        if ($value !== null && $value->getAvailable() != $this->availability) {
            $this->throw();
        }
    }
}
