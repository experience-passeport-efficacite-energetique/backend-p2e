<?php
namespace Agilap\Hydration\Validator;

use MetaHydrator\Exception\ValidationException;
use MetaHydrator\Validator\AbstractValidator;
use TheCodingMachine\TDBM\TDBMService;

class TDBMUniqueValidator extends AbstractValidator
{
    /** @var TDBMService */
    private $tdbmService;

    /** @var string */
    private $table;

    /** @var string */
    private $column;

    public function __construct(TDBMService $tdbmService, string $table, string $column, $errorMessage = null)
    {
        parent::__construct($errorMessage ?? "$column déjà utilisé");
        $this->tdbmService = $tdbmService;
        $this->table = $table;
        $this->column = $column;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, $contextObject = null)
    {
        $beans = $this->tdbmService->findObjects($this->table, "$this->column = :value", ['value' => $value]);
        if ($beans->count() > 0 && $beans[0] !== $contextObject) {
            $this->throw();
        }
    }
}
