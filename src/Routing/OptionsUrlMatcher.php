<?php

namespace Agilap\Routing;

class OptionsUrlMatcher extends \Symfony\Component\Routing\Matcher\UrlMatcher
{
    public function getAllowedMethods($pathInfo): array
    {
        $this->context->setMethod(null);
        $this->allow = [];
        $this->matchCollection(rawurldecode($pathInfo), $this->routes);
        return $this->allow;
    }
}
