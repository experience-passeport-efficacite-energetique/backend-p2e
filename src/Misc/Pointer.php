<?php
namespace Agilap\Misc;

class Pointer
{
    public $value;

    public function __construct($value = null)
    {
        $this->value = $value;
    }
}
