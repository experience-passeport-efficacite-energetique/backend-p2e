<?php

namespace Agilap\Command;

use Agilap\Service\StaticDataService;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class LoadDataCommand extends Command
{
    /** @var StaticDataService */
    private $dataService;

    public function __construct(StaticDataService $dataService)
    {
        parent::__construct();
        $this->dataService = $dataService;
    }

    protected function configure()
    {
        $this->setName('db:load');
        $this->addArgument('datafile', InputArgument::REQUIRED, 'YAML data file to load');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dataFile = $input->getArgument('datafile');
        $output->writeln("Loading file  $dataFile:");
        $res = $this->dataService->load($dataFile);
        $output->writeln("$res rows affected.");
    }
}
