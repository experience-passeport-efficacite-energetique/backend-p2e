<?php
namespace Agilap\Command;

use Agilap\Enum\Role;
use Agilap\Hydration\Validator\TDBMUniqueValidator;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\UserDao;
use MetaHydrator\Exception\HydratingException;
use Mouf\Hydrator\Hydrator;
use Silex\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

class CreateUserCommand extends Command
{
    /** @var UserDao */
    private $dao;
    /** @var TDBMService */
    private $tdbmService;
    /** @var Application */
    private $app;

    public function __construct(Application $app)
    {
        parent::__construct('user:create');
        $this->app = $app;
    }

    protected function configure()
    {
        $this->setDescription('Create a new user');
        $this->addOption('email', null, InputOption::VALUE_REQUIRED, 'email of the user');
        $this->addOption('password', null, InputOption::VALUE_REQUIRED, 'password of the user');
        $this->addOption('roles', null, InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED, 'role (User, Auditor of Admin', ['User']);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->tdbmService = $this->app['tdbmService'];
        $this->dao = new UserDao($this->tdbmService);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = $input->getOptions();
        $hydrator = $this->hydrator();
        try {
            /** @var User $user */
            $user = $hydrator->hydrateNewObject($data, User::class);
        } catch (HydratingException $e) {
            throw new \InvalidArgumentException(Yaml::dump($e->getErrorsMap()));
        }
        $user->setActive(true);
        $this->dao->save($user);
        $id = $user->getId();
        $output->writeln("user($id) created");
    }

    private function hydrator(): Hydrator
    {
        return FluidHydrator::new()
            ->field('email')->string()->required()->email()->validator(new TDBMUniqueValidator($this->tdbmService, 'users', 'email'))
            ->field('password')->string()->required()
            ->field('roles')->string()->enum(Role::Enum)->array()
        ;
    }
}
