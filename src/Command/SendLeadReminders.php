<?php

namespace Agilap\Command;

use Agilap\Enum\Role;
use Agilap\Model\Dao\LeadDao;
use Agilap\Model\Dao\UserDao;
use Agilap\Service\MailHelper;
use Silex\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendLeadReminders extends Command
{

    /** @var TDBMService */
    private $tdbmService;
    /** @var Application */
    private $app;
    /** @var LeadDao */
    private $leadDao;
    /** @var UserDao */
    private $userDao;
    /** @var MailHelper */
    private $mailHelper;

    public function __construct(Application $app)
    {
        parent::__construct('mails:leads:reminders');
        $this->app = $app;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->tdbmService = $this->app['tdbmService'];
        $this->leadDao = new LeadDao($this->tdbmService);
        $this->userDao = new UserDao($this->tdbmService);
        $this->mailHelper = new MailHelper();
    }

    protected function configure()
    {
        $this->setDescription('Send reminders for unassigned leads');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sendFirstReminders();
        $this->sendSecondReminders();
    }

    private function sendFirstReminders()
    {
        $leads = $this->leadDao->getLeadsNeedingReminder(true);

        foreach ($leads as $lead) {
            $auditors = $this->userDao->getWithinDistanceFromLead($lead);
            $this->mailHelper->sendNewLeadReminder($lead, $auditors);
        }
    }

    private function sendSecondReminders()
    {
        $leads = $this->leadDao->getLeadsNeedingReminder(false);
        $admins = $this->userDao->findSuperAdmins();

        foreach ($leads as $lead) {
            $auditors = $this->userDao->getWithinDistanceFromLead($lead);
            $adminsTerritories = $this->userDao->searchByTerritories(
                $lead->getOwner()->getTerritories(),
                ['role' => Role::ADMIN_TERRITORY]
            );

            $this->mailHelper->sendLeadAdminNotification($lead, $auditors, $adminsTerritories, $admins);
        }
    }
}
