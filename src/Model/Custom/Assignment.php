<?php

namespace Agilap\Model\Custom;

use Agilap\Model\Bean\Lead;

/**
 * @OA\Schema(
 *   description="Add Assignement Object",
 *   schema="AddAssignment",
 *   @OA\Property(
 *      property="lead",
 *      ref="#/components/schemas/OnlyIdLead"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Get Assignement Object",
 *   @OA\Property(
 *      property="lead",
 *      ref="#/components/schemas/Lead",
 *   )
 * )
 */
class Assignment
{
    /** @var Lead */
    private $lead;
    public function setLead(Lead $lead) { $this->lead = $lead; }
    public function getLead(): ?Lead { return $this->lead; }
}