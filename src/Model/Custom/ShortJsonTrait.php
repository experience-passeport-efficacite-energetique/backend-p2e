<?php

namespace Agilap\Model\Custom;

trait ShortJsonTrait
{
    abstract function getId(): ?int;

    public function shortJson()
    {
        return ['id' => $this->getId()];
    }
}
