<?php
namespace Agilap\Model\Custom;

trait TDBMDependentRefDaoTrait
{
    /**
     * @param TDBMDependentRefBeanTrait $bean
     */
    public function saveDependent($bean)
    {
        foreach ($bean->getDependent() as $item) {
            if (method_exists($item, 'getDependent')) {
                $this->saveDependent($item);
            }
            $this->tdbmService->save($item);
            if (method_exists($item, 'getDebranched')) {
                $this->deleteDebranched($item);
            }
        }
    }

    /**
     * @param TDBMDependentRefBeanTrait $bean
     */
    public function deleteDependent($bean)
    {
        foreach ($bean->getDependent() as $item) {
            $this->tdbmService->delete($item);
            if (method_exists($item, 'getDependent')) {
                $this->deleteDependent($item);
            }
        }
    }

    /**
     * @param TDBMDependentRefBeanTrait $bean
     */
    public function deleteDebranched($bean)
    {
        foreach ($bean->getDebranched() as $item) {
            $this->tdbmService->delete($item);
            if (method_exists($item, 'getDependent')) {
                $this->deleteDependent($item);
            }
        }
    }
}