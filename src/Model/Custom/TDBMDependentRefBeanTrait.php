<?php
namespace Agilap\Model\Custom;

use TheCodingMachine\TDBM\AbstractTDBMObject;

/**
 * This trait helps handling owned sub-beans, and more precisely:
 * - saving them right before the owner bean.
 * - deleting them when they become unused, in order to avoid pollution in database.
 *
 * You may only use this trait in a Bean class which parent implements methods getRef/setRef. You will also need to
 * implement method getDependentFkNames
 */
trait TDBMDependentRefBeanTrait
{
    private $_debranchedData = [];
    private $_dependentData = [];

    public function getDebranched(): array
    {
        return array_filter($this->_debranchedData);
    }

    public function getDependent(): array
    {
        $dependent = [];
        foreach ($this->getDependentFkNames() as $fkName) {
            $ref = $this->getRef($fkName);
            if ($ref !== null) {
                $dependent[] = $ref;
            }
        }
        return $dependent;

        return array_filter($this->_dependentData);
    }

    public function legalizeDependencies(): void
    {
        $this->_debranchedData = [];
        $this->_dependentData = [];
    }

    public function setDependent(string $key, $newValue, $oldValue)
    {
        if ($oldValue !== $newValue) {
            if (!key_exists($key, $this->_debranchedData)) {
                $this->_debranchedData[$key] = $oldValue;
            } else if ($newValue === $this->_debranchedData[$key]) {
                unset($this->_debranchedData[$key]);
            }
        }
        $this->_dependentData[$key] = $newValue;
    }


    abstract function getDependentFkNames(): array;
    abstract function getRef(string $foreignKeyName, ?string $tableName = null): ?AbstractTDBMObject;
    protected function setRef(string $foreignKeyName, AbstractTDBMObject $bean = null, ?string $tableName = null): void
    {
        if (in_array($foreignKeyName, $this->getDependentFkNames())) {
            $current = $this->getRef($foreignKeyName, $tableName);
            if ($current !== $bean) {
                if (!key_exists($foreignKeyName, $this->_debranchedData)) {
                    $this->_debranchedData[$foreignKeyName] = $current;
                } else if ($bean === $this->_debranchedData[$foreignKeyName]) {
                    unset($this->_debranchedData[$foreignKeyName]);
                }
            }
        }
        parent::setRef($foreignKeyName, $bean, $tableName);
    }
}
