<?php

namespace Agilap\Model\Custom;

trait TDBMHiddenIdTrait
{
    public function jsonSerialize($stopRecursion = false)
    {
        $array = parent::jsonSerialize($stopRecursion);
        unset($array['id']);
        return $array;
    }
}
