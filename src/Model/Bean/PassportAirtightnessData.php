<?php
/*
 * This file has been automatically generated by TDBM.
 * You can edit this file as it will not be overwritten.
 */

namespace Agilap\Model\Bean;

use Agilap\Model\Bean\Generated\AbstractPassportAirtightnessData;
use Agilap\Model\Custom\TDBMHiddenIdTrait;

/**
 * The PassportAirtightnessData class maps the 'passport_airtightness_data' table in database.
 *
 * @OA\Schema(
 *   description="Passport Wall data Object",
 *   allOf= {
 *      @OA\Schema(ref="#/components/schemas/CommentPassportEquipment"),
 *      @OA\Schema(
 *          @OA\Property(
 *             property="currentAirtightness",
 *             type="string",
 *             enum={"BAD","AVERAGE","GOOD","VERY_GOOD"},
 *             description="current type of insulation"
 *          ),
 *          @OA\Property(
 *             property="plannedAirtightness",
 *             type="string",
 *             enum={"GOOD","VERY_GOOD"},
 *             description="planned type of insulation"
 *          )
 *      )
 *   }
 * )
 */
class PassportAirtightnessData extends AbstractPassportAirtightnessData
{
    use TDBMHiddenIdTrait;
}
