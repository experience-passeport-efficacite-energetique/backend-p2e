<?php
/*
 * This file has been automatically generated by TDBM.
 * You can edit this file as it will not be overwritten.
 */

namespace Agilap\Model\Bean;

use Agilap\Model\Bean\Generated\AbstractTerritory;
use Agilap\Model\Dao\AddressDao;
use TheCodingMachine\TDBM\TDBMService;

/**
 * The Territory class maps the 'territories' table in database.
 *
 * @OA\Schema(
 *   schema="OnlyIdTerritory",
 *   description="Territory object with only the ID property",
 *   @OA\Property(
 *      property="id",
 *      type="integer",
 *      description="id of the territory"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Unassigned user Object",
 *   schema="UserUnassignedTerritory",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      description="Confirmation that user with id `id` has been unassigned from territory",
 *      example="User(`{user_id}`) has been unassigned"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Unassigned city Object",
 *   schema="CityUnassignedTerritory",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      description="Confirmation that city with id `id` has been unassigned from territory",
 *      example="City(`{city_id}`) has been unassigned"
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="OnlyLabelTerritory",
 *   description="Territory object with only the Label property",
 *      @OA\Property(
 *        property="label",
 *        type="string",
 *        description="Name of the territory"
 *      )
 * )
 *
 * @OA\Schema(
 *   schema="MinimalGetTerritory",
 *   description="Territory object",
 *   allOf= {
 *      @OA\Schema(ref="#/components/schemas/OnlyIdTerritory"),
 *      @OA\Schema(ref="#/components/schemas/OnlyLabelTerritory")
 *   }
 * )
 *
 * @OA\Schema(
 *   description="Deleted territory Object",
 *   schema="DeletedTerritory",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      description="Confirmation that territory with id `id` has been deleted",
 *      example="territory(`{territory_id}`) has been deleted"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Territory object",
 *   allOf= {
 *      @OA\Schema(ref="#/components/schemas/MinimalGetTerritory"),
 *      @OA\Schema(
 *          @OA\Property(
 *             property="cities",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/MinimalGetCity"),
 *         ),
 *          @OA\Property(
 *             property="users",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/MinimalGetUser"),
 *         ),
 *      )
 *   }
 * )
 */
class Territory extends AbstractTerritory
{
	public function addCity(City $city) : void
	{
		parent::addCity($city);
		$addressDao = new AddressDao($this->tdbmService);
		$addresses = $addressDao->findByCityObject($city);
		foreach($addresses as $address){
			$this->addAddress($address);
		}
	}

	public function removeCity(City $city) : void
	{
		parent::removeCity($city);
		$addressDao = new AddressDao($this->tdbmService);
		$addresses = $addressDao->findByCityObject($city);
		foreach($addresses as $address){
			$this->removeAddress($address);
		}
	}

	/**
     * Serializes the object for JSON encoding.
     *
     * @param bool $stopRecursion Parameter used internally by TDBM to stop embedded objects from embedding other objects.
     * @return array
     */
    public function jsonSerialize($stopRecursion = false)
    {
        $array = [];
        $array['id'] = $this->getId();
        $array['label'] = $this->getLabel();

        if (!$stopRecursion) {
            $array['cities'] = array_map(function (City $city) {
                return $city->jsonSerializeLight();
            }, $this->getCities());
        }
        if (!$stopRecursion) {
            $array['users'] = array_map(function (User $user) {
                return $user->jsonSerialize(true);
            }, $this->getUsers());
        }

        return $array;
    }
}
