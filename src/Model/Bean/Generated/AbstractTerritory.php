<?php
declare(strict_types=1);

namespace Agilap\Model\Bean\Generated;

use TheCodingMachine\TDBM\ResultIterator;
use TheCodingMachine\TDBM\AlterableResultIterator;
use Ramsey\Uuid\Uuid;
use Agilap\Model\Bean\Address;
use Agilap\Model\Bean\City;
use Agilap\Model\Bean\User;
use TheCodingMachine\TDBM\AbstractTDBMObject;

/*
 * This file has been automatically generated by TDBM.
 * DO NOT edit this file, as it might be overwritten.
 * If you need to perform changes, edit the Territory class instead!
 */

/**
 * The AbstractTerritory class maps the 'territories' table in database.
 */
abstract class AbstractTerritory extends AbstractTDBMObject implements \JsonSerializable
{
    /**
     * The constructor takes all compulsory arguments.
     *

     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The getter for the "id" column.
     *
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->get('id', 'territories');
    }

    /**
     * The setter for the "id" column.
     *
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->set('id', $id, 'territories');
    }

    /**
     * The getter for the "label" column.
     *
     * @return string|null
     */
    public function getLabel() : ?string
    {
        return $this->get('label', 'territories');
    }

    /**
     * The setter for the "label" column.
     *
     * @param string|null $label
     */
    public function setLabel(?string $label) : void
    {
        $this->set('label', $label, 'territories');
    }

    /**
     * Returns the list of Address associated to this bean via the territories_addresses pivot table.
     *
     * @return Address[]
     */
    public function getAddresses() : array
    {
        return $this->_getRelationships('territories_addresses');
    }
    /**
     * Adds a relationship with Address associated to this bean via the territories_addresses pivot table.
     *
     * @param Address $address
     */
    public function addAddress(Address $address) : void
    {
        $this->addRelationship('territories_addresses', $address);
    }
    /**
     * Deletes the relationship with Address associated to this bean via the territories_addresses pivot table.
     *
     * @param Address $address
     */
    public function removeAddress(Address $address) : void
    {
        $this->_removeRelationship('territories_addresses', $address);
    }
    /**
     * Returns whether this bean is associated with Address via the territories_addresses pivot table.
     *
     * @param Address $address
     * @return bool
     */
    public function hasAddress(Address $address) : bool
    {
        return $this->hasRelationship('territories_addresses', $address);
    }
    /**
     * Sets all relationships with Address associated to this bean via the territories_addresses pivot table.
     * Exiting relationships will be removed and replaced by the provided relationships.
     *
     * @param Address[] $addresss
     */
    public function setAddresses(array $addresss) : void
    {
        $this->setRelationships('territories_addresses', $addresss);
    }
    /**
     * Returns the list of City associated to this bean via the territories_cities pivot table.
     *
     * @return City[]
     */
    public function getCities() : array
    {
        return $this->_getRelationships('territories_cities');
    }
    /**
     * Adds a relationship with City associated to this bean via the territories_cities pivot table.
     *
     * @param City $city
     */
    public function addCity(City $city) : void
    {
        $this->addRelationship('territories_cities', $city);
    }
    /**
     * Deletes the relationship with City associated to this bean via the territories_cities pivot table.
     *
     * @param City $city
     */
    public function removeCity(City $city) : void
    {
        $this->_removeRelationship('territories_cities', $city);
    }
    /**
     * Returns whether this bean is associated with City via the territories_cities pivot table.
     *
     * @param City $city
     * @return bool
     */
    public function hasCity(City $city) : bool
    {
        return $this->hasRelationship('territories_cities', $city);
    }
    /**
     * Sets all relationships with City associated to this bean via the territories_cities pivot table.
     * Exiting relationships will be removed and replaced by the provided relationships.
     *
     * @param City[] $citys
     */
    public function setCities(array $citys) : void
    {
        $this->setRelationships('territories_cities', $citys);
    }
    /**
     * Returns the list of User associated to this bean via the territories_users pivot table.
     *
     * @return User[]
     */
    public function getUsers() : array
    {
        return $this->_getRelationships('territories_users');
    }
    /**
     * Adds a relationship with User associated to this bean via the territories_users pivot table.
     *
     * @param User $user
     */
    public function addUser(User $user) : void
    {
        $this->addRelationship('territories_users', $user);
    }
    /**
     * Deletes the relationship with User associated to this bean via the territories_users pivot table.
     *
     * @param User $user
     */
    public function removeUser(User $user) : void
    {
        $this->_removeRelationship('territories_users', $user);
    }
    /**
     * Returns whether this bean is associated with User via the territories_users pivot table.
     *
     * @param User $user
     * @return bool
     */
    public function hasUser(User $user) : bool
    {
        return $this->hasRelationship('territories_users', $user);
    }
    /**
     * Sets all relationships with User associated to this bean via the territories_users pivot table.
     * Exiting relationships will be removed and replaced by the provided relationships.
     *
     * @param User[] $users
     */
    public function setUsers(array $users) : void
    {
        $this->setRelationships('territories_users', $users);
    }

    /**
     * Serializes the object for JSON encoding.
     *
     * @param bool $stopRecursion Parameter used internally by TDBM to stop embedded objects from embedding other objects.
     * @return array
     */
    public function jsonSerialize($stopRecursion = false)
    {
        $array = [];
        $array['id'] = $this->getId();
        $array['label'] = $this->getLabel();

        if (!$stopRecursion) {
            $array['addresses'] = array_map(function (Address $address) {
                return $address->jsonSerialize(true);
            }, $this->getAddresses());
        }
        if (!$stopRecursion) {
            $array['cities'] = array_map(function (City $city) {
                return $city->jsonSerialize(true);
            }, $this->getCities());
        }
        if (!$stopRecursion) {
            $array['users'] = array_map(function (User $user) {
                return $user->jsonSerialize(true);
            }, $this->getUsers());
        }

        return $array;
    }

    /**
     * Returns an array of used tables by this bean (from parent to child relationship).
     *
     * @return string[]
     */
    protected function getUsedTables() : array
    {
        return [ 'territories' ];
    }
}
