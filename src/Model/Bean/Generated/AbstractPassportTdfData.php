<?php
declare(strict_types=1);

namespace Agilap\Model\Bean\Generated;

use TheCodingMachine\TDBM\ResultIterator;
use TheCodingMachine\TDBM\AlterableResultIterator;
use Ramsey\Uuid\Uuid;
use Agilap\Model\Bean\Passport;
use TheCodingMachine\TDBM\AbstractTDBMObject;

/*
 * This file has been automatically generated by TDBM.
 * DO NOT edit this file, as it might be overwritten.
 * If you need to perform changes, edit the PassportTdfData class instead!
 */

/**
 * The AbstractPassportTdfData class maps the 'passport_tdf_data' table in database.
 */
abstract class AbstractPassportTdfData extends AbstractTDBMObject implements \JsonSerializable
{
    /**
     * The constructor takes all compulsory arguments.
     *

     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The getter for the "id" column.
     *
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->get('id', 'passport_tdf_data');
    }

    /**
     * The setter for the "id" column.
     *
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->set('id', $id, 'passport_tdf_data');
    }

    /**
     * The getter for the "electrical_wiring_state" column.
     *
     * @return string|null
     */
    public function getElectricalWiringState() : ?string
    {
        return $this->get('electrical_wiring_state', 'passport_tdf_data');
    }

    /**
     * The setter for the "electrical_wiring_state" column.
     *
     * @param string|null $electrical_wiring_state
     */
    public function setElectricalWiringState(?string $electrical_wiring_state) : void
    {
        $this->set('electrical_wiring_state', $electrical_wiring_state, 'passport_tdf_data');
    }

    /**
     * The getter for the "electrical_wiring_last_control" column.
     *
     * @return string|null
     */
    public function getElectricalWiringLastControl() : ?string
    {
        return $this->get('electrical_wiring_last_control', 'passport_tdf_data');
    }

    /**
     * The setter for the "electrical_wiring_last_control" column.
     *
     * @param string|null $electrical_wiring_last_control
     */
    public function setElectricalWiringLastControl(?string $electrical_wiring_last_control) : void
    {
        $this->set('electrical_wiring_last_control', $electrical_wiring_last_control, 'passport_tdf_data');
    }

    /**
     * The getter for the "gas_system_state" column.
     *
     * @return string|null
     */
    public function getGasSystemState() : ?string
    {
        return $this->get('gas_system_state', 'passport_tdf_data');
    }

    /**
     * The setter for the "gas_system_state" column.
     *
     * @param string|null $gas_system_state
     */
    public function setGasSystemState(?string $gas_system_state) : void
    {
        $this->set('gas_system_state', $gas_system_state, 'passport_tdf_data');
    }

    /**
     * The getter for the "gas_system_last_control" column.
     *
     * @return string|null
     */
    public function getGasSystemLastControl() : ?string
    {
        return $this->get('gas_system_last_control', 'passport_tdf_data');
    }

    /**
     * The setter for the "gas_system_last_control" column.
     *
     * @param string|null $gas_system_last_control
     */
    public function setGasSystemLastControl(?string $gas_system_last_control) : void
    {
        $this->set('gas_system_last_control', $gas_system_last_control, 'passport_tdf_data');
    }

    /**
     * The getter for the "plumbing_state" column.
     *
     * @return string|null
     */
    public function getPlumbingState() : ?string
    {
        return $this->get('plumbing_state', 'passport_tdf_data');
    }

    /**
     * The setter for the "plumbing_state" column.
     *
     * @param string|null $plumbing_state
     */
    public function setPlumbingState(?string $plumbing_state) : void
    {
        $this->set('plumbing_state', $plumbing_state, 'passport_tdf_data');
    }

    /**
     * The getter for the "plumbing_last_control" column.
     *
     * @return string|null
     */
    public function getPlumbingLastControl() : ?string
    {
        return $this->get('plumbing_last_control', 'passport_tdf_data');
    }

    /**
     * The setter for the "plumbing_last_control" column.
     *
     * @param string|null $plumbing_last_control
     */
    public function setPlumbingLastControl(?string $plumbing_last_control) : void
    {
        $this->set('plumbing_last_control', $plumbing_last_control, 'passport_tdf_data');
    }

    /**
     * The getter for the "lead_presence" column.
     *
     * @return string|null
     */
    public function getLeadPresence() : ?string
    {
        return $this->get('lead_presence', 'passport_tdf_data');
    }

    /**
     * The setter for the "lead_presence" column.
     *
     * @param string|null $lead_presence
     */
    public function setLeadPresence(?string $lead_presence) : void
    {
        $this->set('lead_presence', $lead_presence, 'passport_tdf_data');
    }

    /**
     * The getter for the "lead_last_control" column.
     *
     * @return string|null
     */
    public function getLeadLastControl() : ?string
    {
        return $this->get('lead_last_control', 'passport_tdf_data');
    }

    /**
     * The setter for the "lead_last_control" column.
     *
     * @param string|null $lead_last_control
     */
    public function setLeadLastControl(?string $lead_last_control) : void
    {
        $this->set('lead_last_control', $lead_last_control, 'passport_tdf_data');
    }

    /**
     * The getter for the "asbestos_presence" column.
     *
     * @return string|null
     */
    public function getAsbestosPresence() : ?string
    {
        return $this->get('asbestos_presence', 'passport_tdf_data');
    }

    /**
     * The setter for the "asbestos_presence" column.
     *
     * @param string|null $asbestos_presence
     */
    public function setAsbestosPresence(?string $asbestos_presence) : void
    {
        $this->set('asbestos_presence', $asbestos_presence, 'passport_tdf_data');
    }

    /**
     * The getter for the "asbestos_last_control" column.
     *
     * @return string|null
     */
    public function getAsbestosLastControl() : ?string
    {
        return $this->get('asbestos_last_control', 'passport_tdf_data');
    }

    /**
     * The setter for the "asbestos_last_control" column.
     *
     * @param string|null $asbestos_last_control
     */
    public function setAsbestosLastControl(?string $asbestos_last_control) : void
    {
        $this->set('asbestos_last_control', $asbestos_last_control, 'passport_tdf_data');
    }

    /**
     * The getter for the "termites_presence" column.
     *
     * @return string|null
     */
    public function getTermitesPresence() : ?string
    {
        return $this->get('termites_presence', 'passport_tdf_data');
    }

    /**
     * The setter for the "termites_presence" column.
     *
     * @param string|null $termites_presence
     */
    public function setTermitesPresence(?string $termites_presence) : void
    {
        $this->set('termites_presence', $termites_presence, 'passport_tdf_data');
    }

    /**
     * The getter for the "termites_last_control" column.
     *
     * @return string|null
     */
    public function getTermitesLastControl() : ?string
    {
        return $this->get('termites_last_control', 'passport_tdf_data');
    }

    /**
     * The setter for the "termites_last_control" column.
     *
     * @param string|null $termites_last_control
     */
    public function setTermitesLastControl(?string $termites_last_control) : void
    {
        $this->set('termites_last_control', $termites_last_control, 'passport_tdf_data');
    }

    /**
     * The getter for the "epcs" column.
     *
     * @return string|null
     */
    public function getEpcs() : ?string
    {
        return $this->get('epcs', 'passport_tdf_data');
    }

    /**
     * The setter for the "epcs" column.
     *
     * @param string|null $epcs
     */
    public function setEpcs(?string $epcs) : void
    {
        $this->set('epcs', $epcs, 'passport_tdf_data');
    }

    /**
     * The getter for the "septic_tank_individual" column.
     *
     * @return bool|null
     */
    public function getSepticTankIndividual() : ?bool
    {
        return $this->get('septic_tank_individual', 'passport_tdf_data');
    }

    /**
     * The setter for the "septic_tank_individual" column.
     *
     * @param bool|null $septic_tank_individual
     */
    public function setSepticTankIndividual(?bool $septic_tank_individual) : void
    {
        $this->set('septic_tank_individual', $septic_tank_individual, 'passport_tdf_data');
    }

    /**
     * The getter for the "septic_tank_last_control" column.
     *
     * @return string|null
     */
    public function getSepticTankLastControl() : ?string
    {
        return $this->get('septic_tank_last_control', 'passport_tdf_data');
    }

    /**
     * The setter for the "septic_tank_last_control" column.
     *
     * @param string|null $septic_tank_last_control
     */
    public function setSepticTankLastControl(?string $septic_tank_last_control) : void
    {
        $this->set('septic_tank_last_control', $septic_tank_last_control, 'passport_tdf_data');
    }

    /**
     * The getter for the "comment" column.
     *
     * @return string|null
     */
    public function getComment() : ?string
    {
        return $this->get('comment', 'passport_tdf_data');
    }

    /**
     * The setter for the "comment" column.
     *
     * @param string|null $comment
     */
    public function setComment(?string $comment) : void
    {
        $this->set('comment', $comment, 'passport_tdf_data');
    }

    /**
     * Returns the list of Passport pointing to this bean via the tdf_data_id column.
     *
     * @return Passport[]|AlterableResultIterator
     */
    public function getPassports() : AlterableResultIterator
    {
        return $this->retrieveManyToOneRelationshipsStorage('passports', 'fk_passport__tdf_data', 'passports', ['passports.tdf_data_id' => $this->get('id', 'passport_tdf_data')]);
    }


    /**
     * Serializes the object for JSON encoding.
     *
     * @param bool $stopRecursion Parameter used internally by TDBM to stop embedded objects from embedding other objects.
     * @return array
     */
    public function jsonSerialize($stopRecursion = false)
    {
        $array = [];
        $array['id'] = $this->getId();
        $array['electricalWiringState'] = $this->getElectricalWiringState();
        $array['electricalWiringLastControl'] = $this->getElectricalWiringLastControl();
        $array['gasSystemState'] = $this->getGasSystemState();
        $array['gasSystemLastControl'] = $this->getGasSystemLastControl();
        $array['plumbingState'] = $this->getPlumbingState();
        $array['plumbingLastControl'] = $this->getPlumbingLastControl();
        $array['leadPresence'] = $this->getLeadPresence();
        $array['leadLastControl'] = $this->getLeadLastControl();
        $array['asbestosPresence'] = $this->getAsbestosPresence();
        $array['asbestosLastControl'] = $this->getAsbestosLastControl();
        $array['termitesPresence'] = $this->getTermitesPresence();
        $array['termitesLastControl'] = $this->getTermitesLastControl();
        $array['epcs'] = $this->getEpcs();
        $array['septicTankIndividual'] = $this->getSepticTankIndividual();
        $array['septicTankLastControl'] = $this->getSepticTankLastControl();
        $array['comment'] = $this->getComment();


        return $array;
    }

    /**
     * Returns an array of used tables by this bean (from parent to child relationship).
     *
     * @return string[]
     */
    protected function getUsedTables() : array
    {
        return [ 'passport_tdf_data' ];
    }
}
