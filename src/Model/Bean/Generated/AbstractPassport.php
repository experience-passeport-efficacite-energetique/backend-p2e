<?php
declare(strict_types=1);

namespace Agilap\Model\Bean\Generated;

use TheCodingMachine\TDBM\ResultIterator;
use TheCodingMachine\TDBM\AlterableResultIterator;
use Ramsey\Uuid\Uuid;
use Agilap\Model\Bean\Address;
use Agilap\Model\Bean\Lead;
use Agilap\Model\Bean\User;
use Agilap\Model\Bean\PassportWallData;
use Agilap\Model\Bean\PassportFloorData;
use Agilap\Model\Bean\PassportRoofData;
use Agilap\Model\Bean\PassportVentData;
use Agilap\Model\Bean\PassportVentilationData;
use Agilap\Model\Bean\PassportAirtightnessData;
use Agilap\Model\Bean\PassportHeatingData;
use Agilap\Model\Bean\PassportThermostatData;
use Agilap\Model\Bean\PassportDhwData;
use Agilap\Model\Bean\PassportTdfData;
use Agilap\Model\Bean\RenovationPlan;
use TheCodingMachine\TDBM\AbstractTDBMObject;

/*
 * This file has been automatically generated by TDBM.
 * DO NOT edit this file, as it might be overwritten.
 * If you need to perform changes, edit the Passport class instead!
 */

/**
 * The AbstractPassport class maps the 'passports' table in database.
 */
abstract class AbstractPassport extends AbstractTDBMObject implements \JsonSerializable
{
    /**
     * The constructor takes all compulsory arguments.
     *

     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The getter for the "id" column.
     *
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->get('id', 'passports');
    }

    /**
     * The setter for the "id" column.
     *
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->set('id', $id, 'passports');
    }

    /**
     * Returns the Address object bound to this object via the address_id column.
     *
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->getRef('fk_passport__address', 'passports');
    }

    /**
     * The setter for the Address object bound to this object via the address_id column.
     *
     * @param Address|null $object
     */
    public function setAddress(?Address $object) : void
    {
        $this->setRef('fk_passport__address', $object, 'passports');
    }

    /**
     * Returns the Lead object bound to this object via the lead_id column.
     *
     * @return Lead|null
     */
    public function getLead(): ?Lead
    {
        return $this->getRef('fk_passport__lead', 'passports');
    }

    /**
     * The setter for the Lead object bound to this object via the lead_id column.
     *
     * @param Lead|null $object
     */
    public function setLead(?Lead $object) : void
    {
        $this->setRef('fk_passport__lead', $object, 'passports');
    }

    /**
     * Returns the User object bound to this object via the owner_id column.
     *
     * @return User|null
     */
    public function getOwner(): ?User
    {
        return $this->getRef('fk_passport__owner', 'passports');
    }

    /**
     * The setter for the User object bound to this object via the owner_id column.
     *
     * @param User|null $object
     */
    public function setOwner(?User $object) : void
    {
        $this->setRef('fk_passport__owner', $object, 'passports');
    }

    /**
     * Returns the User object bound to this object via the auditor_id column.
     *
     * @return User|null
     */
    public function getAuditor(): ?User
    {
        return $this->getRef('fk_passport__auditor', 'passports');
    }

    /**
     * The setter for the User object bound to this object via the auditor_id column.
     *
     * @param User|null $object
     */
    public function setAuditor(?User $object) : void
    {
        $this->setRef('fk_passport__auditor', $object, 'passports');
    }

    /**
     * Returns the PassportWallData object bound to this object via the walls_data_id column.
     *
     * @return PassportWallData|null
     */
    public function getWallsData(): ?PassportWallData
    {
        return $this->getRef('fk_passport__walls_data', 'passports');
    }

    /**
     * The setter for the PassportWallData object bound to this object via the walls_data_id column.
     *
     * @param PassportWallData|null $object
     */
    public function setWallsData(?PassportWallData $object) : void
    {
        $this->setRef('fk_passport__walls_data', $object, 'passports');
    }

    /**
     * Returns the PassportFloorData object bound to this object via the floor_data_id column.
     *
     * @return PassportFloorData|null
     */
    public function getFloorData(): ?PassportFloorData
    {
        return $this->getRef('fk_passport__floor_data', 'passports');
    }

    /**
     * The setter for the PassportFloorData object bound to this object via the floor_data_id column.
     *
     * @param PassportFloorData|null $object
     */
    public function setFloorData(?PassportFloorData $object) : void
    {
        $this->setRef('fk_passport__floor_data', $object, 'passports');
    }

    /**
     * Returns the PassportRoofData object bound to this object via the roof_data_id column.
     *
     * @return PassportRoofData|null
     */
    public function getRoofData(): ?PassportRoofData
    {
        return $this->getRef('fk_passport__roof_data', 'passports');
    }

    /**
     * The setter for the PassportRoofData object bound to this object via the roof_data_id column.
     *
     * @param PassportRoofData|null $object
     */
    public function setRoofData(?PassportRoofData $object) : void
    {
        $this->setRef('fk_passport__roof_data', $object, 'passports');
    }

    /**
     * Returns the PassportVentData object bound to this object via the vents_data_id column.
     *
     * @return PassportVentData|null
     */
    public function getVentsData(): ?PassportVentData
    {
        return $this->getRef('fk_passport__vents_data', 'passports');
    }

    /**
     * The setter for the PassportVentData object bound to this object via the vents_data_id column.
     *
     * @param PassportVentData|null $object
     */
    public function setVentsData(?PassportVentData $object) : void
    {
        $this->setRef('fk_passport__vents_data', $object, 'passports');
    }

    /**
     * Returns the PassportVentilationData object bound to this object via the ventilation_data_id column.
     *
     * @return PassportVentilationData|null
     */
    public function getVentilationData(): ?PassportVentilationData
    {
        return $this->getRef('fk_passport__ventilation_data', 'passports');
    }

    /**
     * The setter for the PassportVentilationData object bound to this object via the ventilation_data_id column.
     *
     * @param PassportVentilationData|null $object
     */
    public function setVentilationData(?PassportVentilationData $object) : void
    {
        $this->setRef('fk_passport__ventilation_data', $object, 'passports');
    }

    /**
     * Returns the PassportAirtightnessData object bound to this object via the airtightness_data_id column.
     *
     * @return PassportAirtightnessData|null
     */
    public function getAirtightnessData(): ?PassportAirtightnessData
    {
        return $this->getRef('fk_passport__airtightness_data', 'passports');
    }

    /**
     * The setter for the PassportAirtightnessData object bound to this object via the airtightness_data_id column.
     *
     * @param PassportAirtightnessData|null $object
     */
    public function setAirtightnessData(?PassportAirtightnessData $object) : void
    {
        $this->setRef('fk_passport__airtightness_data', $object, 'passports');
    }

    /**
     * Returns the PassportHeatingData object bound to this object via the heating_data_id column.
     *
     * @return PassportHeatingData|null
     */
    public function getHeatingData(): ?PassportHeatingData
    {
        return $this->getRef('fk_passport__heating_data', 'passports');
    }

    /**
     * The setter for the PassportHeatingData object bound to this object via the heating_data_id column.
     *
     * @param PassportHeatingData|null $object
     */
    public function setHeatingData(?PassportHeatingData $object) : void
    {
        $this->setRef('fk_passport__heating_data', $object, 'passports');
    }

    /**
     * Returns the PassportThermostatData object bound to this object via the thermostat_data_id column.
     *
     * @return PassportThermostatData|null
     */
    public function getThermostatData(): ?PassportThermostatData
    {
        return $this->getRef('fk_passport__thermostat_data', 'passports');
    }

    /**
     * The setter for the PassportThermostatData object bound to this object via the thermostat_data_id column.
     *
     * @param PassportThermostatData|null $object
     */
    public function setThermostatData(?PassportThermostatData $object) : void
    {
        $this->setRef('fk_passport__thermostat_data', $object, 'passports');
    }

    /**
     * Returns the PassportDhwData object bound to this object via the dhw_data_id column.
     *
     * @return PassportDhwData|null
     */
    public function getDhwData(): ?PassportDhwData
    {
        return $this->getRef('fk_passport__dhw_data', 'passports');
    }

    /**
     * The setter for the PassportDhwData object bound to this object via the dhw_data_id column.
     *
     * @param PassportDhwData|null $object
     */
    public function setDhwData(?PassportDhwData $object) : void
    {
        $this->setRef('fk_passport__dhw_data', $object, 'passports');
    }

    /**
     * Returns the PassportTdfData object bound to this object via the tdf_data_id column.
     *
     * @return PassportTdfData|null
     */
    public function getTdfData(): ?PassportTdfData
    {
        return $this->getRef('fk_passport__tdf_data', 'passports');
    }

    /**
     * The setter for the PassportTdfData object bound to this object via the tdf_data_id column.
     *
     * @param PassportTdfData|null $object
     */
    public function setTdfData(?PassportTdfData $object) : void
    {
        $this->setRef('fk_passport__tdf_data', $object, 'passports');
    }

    /**
     * The getter for the "type" column.
     *
     * @return string|null
     */
    public function getType() : ?string
    {
        return $this->get('type', 'passports');
    }

    /**
     * The setter for the "type" column.
     *
     * @param string|null $type
     */
    public function setType(?string $type) : void
    {
        $this->set('type', $type, 'passports');
    }

    /**
     * The getter for the "project" column.
     *
     * @return string|null
     */
    public function getProject() : ?string
    {
        return $this->get('project', 'passports');
    }

    /**
     * The setter for the "project" column.
     *
     * @param string|null $project
     */
    public function setProject(?string $project) : void
    {
        $this->set('project', $project, 'passports');
    }

    /**
     * The getter for the "climate_zone" column.
     *
     * @return string|null
     */
    public function getClimateZone() : ?string
    {
        return $this->get('climate_zone', 'passports');
    }

    /**
     * The setter for the "climate_zone" column.
     *
     * @param string|null $climate_zone
     */
    public function setClimateZone(?string $climate_zone) : void
    {
        $this->set('climate_zone', $climate_zone, 'passports');
    }

    /**
     * The getter for the "altitude" column.
     *
     * @return int|null
     */
    public function getAltitude() : ?int
    {
        return $this->get('altitude', 'passports');
    }

    /**
     * The setter for the "altitude" column.
     *
     * @param int|null $altitude
     */
    public function setAltitude(?int $altitude) : void
    {
        $this->set('altitude', $altitude, 'passports');
    }

    /**
     * The getter for the "heated_surface" column.
     *
     * @return int|null
     */
    public function getHeatedSurface() : ?int
    {
        return $this->get('heated_surface', 'passports');
    }

    /**
     * The setter for the "heated_surface" column.
     *
     * @param int|null $heated_surface
     */
    public function setHeatedSurface(?int $heated_surface) : void
    {
        $this->set('heated_surface', $heated_surface, 'passports');
    }

    /**
     * The getter for the "construction_year" column.
     *
     * @return int|null
     */
    public function getConstructionYear() : ?int
    {
        return $this->get('construction_year', 'passports');
    }

    /**
     * The setter for the "construction_year" column.
     *
     * @param int|null $construction_year
     */
    public function setConstructionYear(?int $construction_year) : void
    {
        $this->set('construction_year', $construction_year, 'passports');
    }

    /**
     * The getter for the "surface_type" column.
     *
     * @return string|null
     */
    public function getSurfaceType() : ?string
    {
        return $this->get('surface_type', 'passports');
    }

    /**
     * The setter for the "surface_type" column.
     *
     * @param string|null $surface_type
     */
    public function setSurfaceType(?string $surface_type) : void
    {
        $this->set('surface_type', $surface_type, 'passports');
    }

    /**
     * The getter for the "adjacency" column.
     *
     * @return string|null
     */
    public function getAdjacency() : ?string
    {
        return $this->get('adjacency', 'passports');
    }

    /**
     * The setter for the "adjacency" column.
     *
     * @param string|null $adjacency
     */
    public function setAdjacency(?string $adjacency) : void
    {
        $this->set('adjacency', $adjacency, 'passports');
    }

    /**
     * The getter for the "floors" column.
     *
     * @return string|null
     */
    public function getFloors() : ?string
    {
        return $this->get('floors', 'passports');
    }

    /**
     * The setter for the "floors" column.
     *
     * @param string|null $floors
     */
    public function setFloors(?string $floors) : void
    {
        $this->set('floors', $floors, 'passports');
    }

    /**
     * The getter for the "south_exposure" column.
     *
     * @return string|null
     */
    public function getSouthExposure() : ?string
    {
        return $this->get('south_exposure', 'passports');
    }

    /**
     * The setter for the "south_exposure" column.
     *
     * @param string|null $south_exposure
     */
    public function setSouthExposure(?string $south_exposure) : void
    {
        $this->set('south_exposure', $south_exposure, 'passports');
    }

    /**
     * The getter for the "general_comments" column.
     *
     * @return string|null
     */
    public function getGeneralComments() : ?string
    {
        return $this->get('general_comments', 'passports');
    }

    /**
     * The setter for the "general_comments" column.
     *
     * @param string|null $general_comments
     */
    public function setGeneralComments(?string $general_comments) : void
    {
        $this->set('general_comments', $general_comments, 'passports');
    }

    /**
     * The getter for the "creation_date" column.
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreationDate() : ?\DateTimeImmutable
    {
        return $this->get('creation_date', 'passports');
    }

    /**
     * The setter for the "creation_date" column.
     *
     * @param \DateTimeImmutable|null $creation_date
     */
    public function setCreationDate(?\DateTimeImmutable $creation_date) : void
    {
        $this->set('creation_date', $creation_date, 'passports');
    }

    /**
     * The getter for the "modification_date" column.
     *
     * @return \DateTimeImmutable|null
     */
    public function getModificationDate() : ?\DateTimeImmutable
    {
        return $this->get('modification_date', 'passports');
    }

    /**
     * The setter for the "modification_date" column.
     *
     * @param \DateTimeImmutable|null $modification_date
     */
    public function setModificationDate(?\DateTimeImmutable $modification_date) : void
    {
        $this->set('modification_date', $modification_date, 'passports');
    }

    /**
     * The getter for the "pec" column.
     *
     * @return float|null
     */
    public function getPec() : ?float
    {
        return $this->get('pec', 'passports');
    }

    /**
     * The setter for the "pec" column.
     *
     * @param float|null $pec
     */
    public function setPec(?float $pec) : void
    {
        $this->set('pec', $pec, 'passports');
    }

    /**
     * The getter for the "emissions" column.
     *
     * @return float|null
     */
    public function getEmissions() : ?float
    {
        return $this->get('emissions', 'passports');
    }

    /**
     * The setter for the "emissions" column.
     *
     * @param float|null $emissions
     */
    public function setEmissions(?float $emissions) : void
    {
        $this->set('emissions', $emissions, 'passports');
    }

    /**
     * Returns the list of RenovationPlan pointing to this bean via the passport_id column.
     *
     * @return RenovationPlan[]|AlterableResultIterator
     */
    public function getRenovationPlans() : AlterableResultIterator
    {
        return $this->retrieveManyToOneRelationshipsStorage('renovation_plans', 'fk_renovation_plan__passport', 'renovation_plans', ['renovation_plans.passport_id' => $this->get('id', 'passports')]);
    }


    /**
     * Serializes the object for JSON encoding.
     *
     * @param bool $stopRecursion Parameter used internally by TDBM to stop embedded objects from embedding other objects.
     * @return array
     */
    public function jsonSerialize($stopRecursion = false)
    {
        $array = [];
        $array['id'] = $this->getId();
        if (!$stopRecursion) {
            $object = $this->getAddress();
            $array['address'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getLead();
            $array['lead'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getOwner();
            $array['owner'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getAuditor();
            $array['auditor'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getWallsData();
            $array['wallsData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getFloorData();
            $array['floorData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getRoofData();
            $array['roofData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getVentsData();
            $array['ventsData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getVentilationData();
            $array['ventilationData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getAirtightnessData();
            $array['airtightnessData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getHeatingData();
            $array['heatingData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getThermostatData();
            $array['thermostatData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getDhwData();
            $array['dhwData'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getTdfData();
            $array['tdfData'] = $object ? $object->jsonSerialize(true) : null;
        }
        $array['type'] = $this->getType();
        $array['project'] = $this->getProject();
        $array['climateZone'] = $this->getClimateZone();
        $array['altitude'] = $this->getAltitude();
        $array['heatedSurface'] = $this->getHeatedSurface();
        $array['constructionYear'] = $this->getConstructionYear();
        $array['surfaceType'] = $this->getSurfaceType();
        $array['adjacency'] = $this->getAdjacency();
        $array['floors'] = $this->getFloors();
        $array['southExposure'] = $this->getSouthExposure();
        $array['generalComments'] = $this->getGeneralComments();
        $array['creationDate'] = ($this->getCreationDate() === null) ? null : $this->getCreationDate()->format('c');
        $array['modificationDate'] = ($this->getModificationDate() === null) ? null : $this->getModificationDate()->format('c');
        $array['pec'] = $this->getPec();
        $array['emissions'] = $this->getEmissions();


        return $array;
    }

    /**
     * Returns an array of used tables by this bean (from parent to child relationship).
     *
     * @return string[]
     */
    protected function getUsedTables() : array
    {
        return [ 'passports' ];
    }

    /**
     * Method called when the bean is removed from database.
     *
     */
    protected function onDelete() : void
    {
        parent::onDelete();
        $this->setRef('fk_passport__address', null, 'passports');
        $this->setRef('fk_passport__lead', null, 'passports');
        $this->setRef('fk_passport__owner', null, 'passports');
        $this->setRef('fk_passport__auditor', null, 'passports');
        $this->setRef('fk_passport__walls_data', null, 'passports');
        $this->setRef('fk_passport__floor_data', null, 'passports');
        $this->setRef('fk_passport__roof_data', null, 'passports');
        $this->setRef('fk_passport__vents_data', null, 'passports');
        $this->setRef('fk_passport__ventilation_data', null, 'passports');
        $this->setRef('fk_passport__airtightness_data', null, 'passports');
        $this->setRef('fk_passport__heating_data', null, 'passports');
        $this->setRef('fk_passport__thermostat_data', null, 'passports');
        $this->setRef('fk_passport__dhw_data', null, 'passports');
        $this->setRef('fk_passport__tdf_data', null, 'passports');
    }
}
