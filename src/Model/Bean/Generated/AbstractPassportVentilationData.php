<?php
declare(strict_types=1);

namespace Agilap\Model\Bean\Generated;

use TheCodingMachine\TDBM\ResultIterator;
use TheCodingMachine\TDBM\AlterableResultIterator;
use Ramsey\Uuid\Uuid;
use Agilap\Model\Bean\Ventilation;
use Agilap\Model\Bean\Passport;
use TheCodingMachine\TDBM\AbstractTDBMObject;

/*
 * This file has been automatically generated by TDBM.
 * DO NOT edit this file, as it might be overwritten.
 * If you need to perform changes, edit the PassportVentilationData class instead!
 */

/**
 * The AbstractPassportVentilationData class maps the 'passport_ventilation_data' table in database.
 */
abstract class AbstractPassportVentilationData extends AbstractTDBMObject implements \JsonSerializable
{
    /**
     * The constructor takes all compulsory arguments.
     *

     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The getter for the "id" column.
     *
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->get('id', 'passport_ventilation_data');
    }

    /**
     * The setter for the "id" column.
     *
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->set('id', $id, 'passport_ventilation_data');
    }

    /**
     * Returns the Ventilation object bound to this object via the current_ventilation_id column.
     *
     * @return Ventilation|null
     */
    public function getCurrentVentilation(): ?Ventilation
    {
        return $this->getRef('fk_passport_ventilation_data__current_ventilation', 'passport_ventilation_data');
    }

    /**
     * The setter for the Ventilation object bound to this object via the current_ventilation_id column.
     *
     * @param Ventilation|null $object
     */
    public function setCurrentVentilation(?Ventilation $object) : void
    {
        $this->setRef('fk_passport_ventilation_data__current_ventilation', $object, 'passport_ventilation_data');
    }

    /**
     * Returns the Ventilation object bound to this object via the planned_ventilation_id column.
     *
     * @return Ventilation|null
     */
    public function getPlannedVentilation(): ?Ventilation
    {
        return $this->getRef('fk_passport_ventilation_data__planned_ventilation', 'passport_ventilation_data');
    }

    /**
     * The setter for the Ventilation object bound to this object via the planned_ventilation_id column.
     *
     * @param Ventilation|null $object
     */
    public function setPlannedVentilation(?Ventilation $object) : void
    {
        $this->setRef('fk_passport_ventilation_data__planned_ventilation', $object, 'passport_ventilation_data');
    }

    /**
     * The getter for the "state" column.
     *
     * @return string|null
     */
    public function getState() : ?string
    {
        return $this->get('state', 'passport_ventilation_data');
    }

    /**
     * The setter for the "state" column.
     *
     * @param string|null $state
     */
    public function setState(?string $state) : void
    {
        $this->set('state', $state, 'passport_ventilation_data');
    }

    /**
     * The getter for the "rehabilitation" column.
     *
     * @return string|null
     */
    public function getRehabilitation() : ?string
    {
        return $this->get('rehabilitation', 'passport_ventilation_data');
    }

    /**
     * The setter for the "rehabilitation" column.
     *
     * @param string|null $rehabilitation
     */
    public function setRehabilitation(?string $rehabilitation) : void
    {
        $this->set('rehabilitation', $rehabilitation, 'passport_ventilation_data');
    }

    /**
     * The getter for the "rehabilitation_year" column.
     *
     * @return int|null
     */
    public function getRehabilitationYear() : ?int
    {
        return $this->get('rehabilitation_year', 'passport_ventilation_data');
    }

    /**
     * The setter for the "rehabilitation_year" column.
     *
     * @param int|null $rehabilitation_year
     */
    public function setRehabilitationYear(?int $rehabilitation_year) : void
    {
        $this->set('rehabilitation_year', $rehabilitation_year, 'passport_ventilation_data');
    }

    /**
     * The getter for the "maintenance" column.
     *
     * @return string|null
     */
    public function getMaintenance() : ?string
    {
        return $this->get('maintenance', 'passport_ventilation_data');
    }

    /**
     * The setter for the "maintenance" column.
     *
     * @param string|null $maintenance
     */
    public function setMaintenance(?string $maintenance) : void
    {
        $this->set('maintenance', $maintenance, 'passport_ventilation_data');
    }

    /**
     * The getter for the "comment" column.
     *
     * @return string|null
     */
    public function getComment() : ?string
    {
        return $this->get('comment', 'passport_ventilation_data');
    }

    /**
     * The setter for the "comment" column.
     *
     * @param string|null $comment
     */
    public function setComment(?string $comment) : void
    {
        $this->set('comment', $comment, 'passport_ventilation_data');
    }

    /**
     * The getter for the "project_comment" column.
     *
     * @return string|null
     */
    public function getProjectComment() : ?string
    {
        return $this->get('project_comment', 'passport_ventilation_data');
    }

    /**
     * The setter for the "project_comment" column.
     *
     * @param string|null $project_comment
     */
    public function setProjectComment(?string $project_comment) : void
    {
        $this->set('project_comment', $project_comment, 'passport_ventilation_data');
    }

    /**
     * Returns the list of Passport pointing to this bean via the ventilation_data_id column.
     *
     * @return Passport[]|AlterableResultIterator
     */
    public function getPassports() : AlterableResultIterator
    {
        return $this->retrieveManyToOneRelationshipsStorage('passports', 'fk_passport__ventilation_data', 'passports', ['passports.ventilation_data_id' => $this->get('id', 'passport_ventilation_data')]);
    }


    /**
     * Serializes the object for JSON encoding.
     *
     * @param bool $stopRecursion Parameter used internally by TDBM to stop embedded objects from embedding other objects.
     * @return array
     */
    public function jsonSerialize($stopRecursion = false)
    {
        $array = [];
        $array['id'] = $this->getId();
        if (!$stopRecursion) {
            $object = $this->getCurrentVentilation();
            $array['currentVentilation'] = $object ? $object->jsonSerialize(true) : null;
        }
        if (!$stopRecursion) {
            $object = $this->getPlannedVentilation();
            $array['plannedVentilation'] = $object ? $object->jsonSerialize(true) : null;
        }
        $array['state'] = $this->getState();
        $array['rehabilitation'] = $this->getRehabilitation();
        $array['rehabilitationYear'] = $this->getRehabilitationYear();
        $array['maintenance'] = $this->getMaintenance();
        $array['comment'] = $this->getComment();
        $array['projectComment'] = $this->getProjectComment();


        return $array;
    }

    /**
     * Returns an array of used tables by this bean (from parent to child relationship).
     *
     * @return string[]
     */
    protected function getUsedTables() : array
    {
        return [ 'passport_ventilation_data' ];
    }

    /**
     * Method called when the bean is removed from database.
     *
     */
    protected function onDelete() : void
    {
        parent::onDelete();
        $this->setRef('fk_passport_ventilation_data__current_ventilation', null, 'passport_ventilation_data');
        $this->setRef('fk_passport_ventilation_data__planned_ventilation', null, 'passport_ventilation_data');
    }
}
