<?php

namespace Agilap\Model\Bean;

interface IDashboardable
{
    public function dashboardDataJson(): array;
}
