<?php
/*
 * This file has been automatically generated by TDBM.
 * You can edit this file as it will not be overwritten.
 */

namespace Agilap\Model\Bean;

use Agilap\Model\Bean\Generated\AbstractPassport;
use Agilap\Model\Custom\TDBMDependentRefBeanTrait;

/**
 * The Passport class maps the 'passports' table in database.
 *
 *
 * @OA\Schema(
 *   schema="MaintenanceStateEnum",
 *   description="Maintenance state",
 *   @OA\Property(
 *      property="maintenance",
 *      type="string",
 *      enum={"NONE","EVERY_YEAR","EVERY_TWO_YEAR_OR_MORE"},
 *      description="current type of insulation"
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="CommentPassportEquipment",
 *   description="Comment field found in most passports sub categories",
 *   @OA\Property(
 *      property="comment",
 *      type="string",
 *      description="comment on a passport sub category"
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="StatePassportEquipment",
 *   description="State field found in most passports sub categories",
 *   @OA\Property(
 *      property="state",
 *      type="string",
 *      enum={"NEW","USED","FIXER_UPPER","NOT_APPLICABLE"},
 *      description="state of the equipment in this sub category"
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="SurfacePassportEquipment",
 *   description="Surface field found in some passports sub categories",
 *   @OA\Property(
 *      property="surface",
 *      type="integer",
 *      description="surface in square meters of a passport sub category"
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="RehabilitationPassportEquipment",
 *   description="Rehabilitation field found in some passports sub categories",
 *   @OA\Property(
 *      property="rehabilitation",
 *      type="string",
 *      enum={"NONE","ORIGINAL","REPLACED","DEPENDS_ON_HEATING"},
 *      description="Rehabilitation state of the equipment in this category"
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="RehabilitationYearPassportEquipment",
 *   description="Rehabilitation Year field found in some passports sub categories",
 *   @OA\Property(
 *      property="rehabilitationYear",
 *      type="integer",
 *      description="year of rehabilitation"
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="OnlyIdPassport",
 *   description="Passport object with only the Id property",
 *   @OA\Property(
 *      property="id",
 *      type="integer",
 *      description="id of the passport"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Deleted passport Object",
 *   schema="DeletedPassport",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      description="Confirmation that passport with id `id` has been deleted",
 *      example="passport(`{passport_id}`) has been deleted"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Passport Object",
 *   schema="BaseGetPassport",
 *   allOf= {
 *      @OA\Schema(ref="#/components/schemas/OnlyIdPassport"),
 *      @OA\Schema(
 *          @OA\Property(
 *              property="creationDate",
 *              type="string",
 *              description="Date at which the passport was created",
 *              example="2018-02-22T12:57:37+00:00"
 *          ),
 *          @OA\Property(
 *              property="modificationDate",
 *              type="string",
 *              description="Date at which the passport was last modified",
 *              example="2019-02-22T12:57:37+00:00"
 *          ),
 *          @OA\Property(
 *             property="renovationPlans",
 *             type="array",
 *             @OA\Items(ref="#/components/schemas/OnlyIdRenovationPlan"),
 *         )
 *      )
 *   }
 * )
 *
 * @OA\Schema(
 *   description="Passport Object",
 *   schema="MinimalGetPassport",
 *   allOf= {
 *      @OA\Schema(ref="#/components/schemas/BaseGetPassport"),
 *      @OA\Schema(
 *          @OA\Property(
 *              property="auditor",
 *              ref="#/components/schemas/OnlyNamesUser"
 *          )
 *      )
 *   }
 * )
 *
 * @OA\Schema(
 *   description="Passport Object",
 *   allOf= {
 *      @OA\Schema(ref="#/components/schemas/BaseGetPassport"),
 *      @OA\Schema(
 *          @OA\Property(
 *              property="address",
 *              ref="#/components/schemas/Address"
 *          ),
 *          @OA\Property(
 *              property="lead",
 *              ref="#/components/schemas/PassportLead"
 *          ),
 *          @OA\Property(
 *              property="owner",
 *              ref="#/components/schemas/MinimalGetUser"
 *          ),
 *          @OA\Property(
 *              property="auditor",
 *              ref="#/components/schemas/MinimalGetUser"
 *          ),
 *          @OA\Property(
 *             property="wallsData",
 *             ref="#/components/schemas/PassportWallData"
 *         ),
 *          @OA\Property(
 *             property="floorData",
 *             ref="#/components/schemas/PassportFloorData"
 *         ),
 *          @OA\Property(
 *             property="roofData",
 *             ref="#/components/schemas/PassportRoofData"
 *         ),
 *          @OA\Property(
 *             property="ventsData",
 *             ref="#/components/schemas/PassportVentData"
 *         ),
 *          @OA\Property(
 *             property="ventilationData",
 *             ref="#/components/schemas/PassportVentilationData"
 *         ),
 *          @OA\Property(
 *             property="airtightnessData",
 *             ref="#/components/schemas/PassportAirtightnessData"
 *         ),
 *          @OA\Property(
 *             property="heatingData",
 *             ref="#/components/schemas/PassportHeatingData"
 *         ),
 *          @OA\Property(
 *             property="thermostatData",
 *             ref="#/components/schemas/PassportThermostatData"
 *         ),
 *          @OA\Property(
 *             property="dhwData",
 *             ref="#/components/schemas/PassportDhwData"
 *         ),
 *          @OA\Property(
 *             property="tdfData",
 *             ref="#/components/schemas/PassportTdfData"
 *         ),
 *         @OA\Property(
 *             property="type",
 *             type="string",
 *             enum={"HOUSE","APARTMENT"},
 *             description="Type of building for this passport"
 *         ),
 *         @OA\Property(
 *             property="project",
 *             type="string",
 *             description="Description of the project"
 *         ),
 *         @OA\Property(
 *             property="climateZone",
 *             type="string",
 *             enum={"H1","H2","H3"},
 *             description="Type of building for this passport"
 *         ),
 *         @OA\Property(
 *             property="altitude",
 *             type="integer",
 *             description="Altitude of the building for this passport"
 *         ),
 *         @OA\Property(
 *             property="heatedSurface",
 *             type="integer",
 *             description="Heated surface for this project"
 *         ),
 *         @OA\Property(
 *             property="constructionYear",
 *             type="integer",
 *             description="Construction year of the building"
 *         ),
 *         @OA\Property(
 *             property="surfaceType",
 *             type="string",
 *             enum={"DEVELOPED","RECTANGULAR","ELONGATED"},
 *             description="Type of surface for the project"
 *         ),
 *         @OA\Property(
 *             property="adjacency",
 *             type="string",
 *             enum={"X_LARGE","LARGE","MEDIUM","SMALL","NONE"}
 *         ),
 *         @OA\Property(
 *             property="floors",
 *             type="string",
 *             enum={"ONE_LEVEL","TWO_LEVELS_WITH_ATTIC","TWO_LEVELS","THREE_LEVELS_WITH_ATTIC","THREE_LEVELS"},
 *             description="Number of floors"
 *         ),
 *         @OA\Property(
 *             property="southExposure",
 *             type="string",
 *             enum={"SUFFICIENT","INSUFFICIENT"}
 *         ),
 *         @OA\Property(
 *             property="generalComments",
 *             type="string",
 *         ),
 *         @OA\Property(
 *             property="creationDate",
 *             type="string",
 *             description="Date of creation",
 *             example="2018-02-22T12:57:37+00:00"
 *         ),
 *         @OA\Property(
 *             property="modificationDate",
 *             type="string",
 *             description="Date of modification",
 *             example="2019-02-22T12:57:37+00:00"
 *         ),
 *         @OA\Property(
 *             property="pec",
 *             type="number",
 *             description="Primary Energy Coefficient"
 *         ),
 *         @OA\Property(
 *             property="emissions",
 *             type="number",
 *             description="CO2 Emissions"
 *         )
 *      )
 *   }
 * )
 */
class Passport extends AbstractPassport implements IDashboardable
{
    use TDBMDependentRefBeanTrait;
    function getDependentFkNames(): array
    {
        return [
            'fk_passport__walls_data',
            'fk_passport__floor_data',
            'fk_passport__roof_data',
            'fk_passport__vents_data',
            'fk_passport__ventilation_data',
            'fk_passport__airtightness_data',
            'fk_passport__heating_data',
            'fk_passport__thermostat_data',
            'fk_passport__dhw_data',
            'fk_passport__tdf_data',
            'fk_passport__address'
        ];
    }

    public function setLead(?Lead $object): void
    {
        parent::setLead($object);
        if ($object) {
            $this->setOwner($object->getOwner());
        }
    }

    public function jsonSerialize($stopRecursion = false)
    {
        $array = parent::jsonSerialize($stopRecursion);

        $heatingData = $this->getHeatingData();
        $array['heatingData'] = $heatingData ? $heatingData->jsonSerialize($stopRecursion) : null;

        $dhwData = $this->getDhwData();
        $array['dhwData'] = $dhwData ? $dhwData->jsonSerialize($stopRecursion) : null;

        $ventilationData = $this->getVentilationData();
        $array['ventilationData'] = $ventilationData ? $ventilationData->jsonSerialize($stopRecursion) : null;

        $address = $this->getAddress();
        $array['address'] = $address ? $address->jsonSerialize($stopRecursion) : null;

        $array['renovationPlans'] = array_map(function (RenovationPlan $plan) {
            return $plan->shortJson();
        }, $this->getRenovationPlans()->toArray());

        return $array;
    }

    public function shortJson(): array
    {
        $array = [];
        $array['id'] = $this->getId();
        $array['creationDate'] = $this->getCreationDate() ? $this->getCreationDate()->format('c') : null;
        $array['modificationDate'] = $this->getModificationDate() ? $this->getModificationDate()->format('c') : null;
        $auditor = $this->getAuditor();
        $array['auditor'] = $auditor ? [
            'firstname' => $auditor->getFirstname(),
            'lastname' => $auditor->getLastname()
        ] : null;
        $array['renovationPlans'] = array_map(function (RenovationPlan $plan) {
            return $plan->shortJson();
        }, $this->getRenovationPlans()->toArray());
        return $array;
    }

    public function dashboardDataJson(): array
    {
        $array = [];
        $array['id'] = $this->getId();
        $array['creationDate'] = $this->getCreationDate() ? $this->getCreationDate()->format('c') : null;
        $array['modificationDate'] = $this->getModificationDate() ? $this->getModificationDate()->format('c') : null;
        $array['renovationPlans'] = array_map(function (RenovationPlan $plan) {
            return $plan->dashboardDataJson();
        }, $this->getRenovationPlans()->toArray());
        return $array;
    }
}
