<?php

/*
 * This file has been automatically generated by TDBM.
 * You can edit this file as it will not be overwritten.
 */

namespace Agilap\Model\Dao;

use Agilap\Model\Dao\Generated\AbstractRegionDao;

/**
 * The RegionDao class will maintain the persistence of Region class into the regions table.
 */
class RegionDao extends AbstractRegionDao
{
    public function findByNameContains(string $search)
    {
        return $this->find(
            "name LIKE :searchContains",
            ["searchContains" => '%' . $search . '%']
        );
    }
}
