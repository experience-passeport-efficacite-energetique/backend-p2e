<?php

namespace Agilap\Model\Dao;

trait AndOrWhereTrait
{
    public function andOrWhere($hasWhere): string
    {
        if (!$hasWhere) {
            return <<<SQL
 where
SQL;
        } else {
            return <<<SQL
 and
SQL;
        }
    }
}
