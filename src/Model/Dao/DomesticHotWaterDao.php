<?php

/*
 * This file has been automatically generated by TDBM.
 * You can edit this file as it will not be overwritten.
 */

namespace Agilap\Model\Dao;

use Agilap\Model\Bean\DomesticHotWater;
use Agilap\Model\Custom\TDBMDependentRefDaoTrait;
use Agilap\Model\Dao\Generated\AbstractDomesticHotWaterDao;
use Porpaginas\Result;

/**
 * The DomesticHotWaterDao class will maintain the persistence of DomesticHotWater class into the domestic_hot_waters table.
 */
class DomesticHotWaterDao extends AbstractDomesticHotWaterDao
{
    use TDBMDependentRefDaoTrait;

    public function save(DomesticHotWater $obj): void
    {
        $obj->setCef(round($obj->getEnergyNeeds() * $obj->getResistance(), 1));
        $obj->setCep(round($obj->getCef() * $obj->getPrimaryEnergy(), 1));
        $obj->setGes(round($obj->getCef() * $obj->getEmissionFactor(), 1));
        $this->saveDependent($obj);
        parent::save($obj);
        $this->deleteDebranched($obj);
    }

    /**
     * @return iterable|Result
     */
    public function search(array $params): iterable
    {
        $orderBy = null;
        $clauses = [];
        $sqlParams = [];
        $additionalTables = [];

        foreach ($params as $key => $value) {
            switch ($key) {
                case 'available':
                    $clauses[] = 'domestic_hot_waters.available = :available';
                    $sqlParams['available'] = intval($value);
                    break;
            }
        }

        $filter = implode(' AND ', array_map(function ($clause) {
            return "($clause)";
        }, $clauses));
        return $this->find($filter, $sqlParams, $orderBy, array_unique($additionalTables));
    }
}
