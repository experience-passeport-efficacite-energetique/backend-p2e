<?php
namespace Agilap\Response;

use Porpaginas\Page;
use Symfony\Component\HttpFoundation\JsonResponse;

class PageResponse extends JsonResponse
{
    public function __construct(Page $page, string $types)
    {
        parent::__construct([
            $types => iterator_to_array($page->getIterator()),
            'perpage' => intval($page->getCurrentLimit()),
            'count' => intval($page->count()),
            'total' => intval($page->totalCount()),
            'index' => intval($page->getCurrentPage())
        ]);
    }
}