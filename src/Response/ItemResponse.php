<?php
namespace Agilap\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class ItemResponse extends JsonResponse
{
    public function __construct($item, $type, $status = 200, array $headers = array())
    {
        parent::__construct([$type => $item], $status, $headers);
    }
}
