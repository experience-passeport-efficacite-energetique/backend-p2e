<?php

namespace Agilap\Error;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface ErrorHandlerInterface
{
    function handle(\Exception $e, Request $request, $code): Response;
}