<?php
namespace Agilap\Error;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiErrorHandler implements ErrorHandlerInterface
{
    function handle(\Exception $e, Request $request, $code): Response
    {
        return new JsonResponse(['message' => $e->getMessage()]);
    }
}