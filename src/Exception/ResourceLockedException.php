<?php

namespace Agilap\Exception;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ResourceLockedException extends \Exception
{
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, Response::HTTP_LOCKED, $previous);
    }
}
