<?php

namespace Agilap\Exception;

use Throwable;

class APIException extends \Exception
{
    /** @var int */
    private $statusCode;
    /** @var array */
    private $body;
    /** @var array */
    private $headers;

    public function __construct(array $body, int $statusCode, array $headers = [])
    {
        parent::__construct();
        $this->body = $body;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function getStatusCode(): int { return $this->statusCode; }
    public function getBody(): array { return $this->body; }
    public function getHeaders(): array { return $this->headers; }
}
