<?php
namespace Agilap\Exception;

class ApiSchemaException extends \Exception
{
    /** @var array */
    private $errors;
    /** @return array */
    public function getErrors() { return $this->errors; }

    /**
     * HydratingException constructor.
     * All the leaves of errors map should describe a specific error.
     *
     * @param array $errors
     * @param string $message
     * @param int $code
     */
    public function __construct($errors, $message = "", $code = 422)
    {
        parent::__construct($message, $code, null);
        $this->errors = $errors;
    }
}