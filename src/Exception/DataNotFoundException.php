<?php
namespace Agilap\Exception;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class DataNotFoundException extends \Exception
{
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, Response::HTTP_NOT_FOUND, $previous);
    }
}
