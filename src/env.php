<?php

use Symfony\Component\Yaml\Yaml;

/** @var array $env */
$env = getenv();
$enfFile = __DIR__ . '/../var/env.yml';
if (file_exists($enfFile) && is_readable($enfFile)) {
    $env = array_merge($env, Yaml::parse(file_get_contents($enfFile)));
}

$def = function ($name, $default = null) use ($env) {
    return key_exists($name, $env) ? $env[$name] : $default;
};

define('DEV', $def('DEV', false));

define('DB_HOST', $def('DB_HOST'));
define('DB_DRIVER', $def('DB_DRIVER'));
define('DB_PORT', $def('DB_PORT'));
define('DB_NAME', $def('DB_NAME'));
define('DB_USER', $def('DB_USER'));
define('DB_PASSWORD', $def('DB_PASSWORD'));
define('DB_CHARSET', $def('DB_CHARSET'));
define('DB_URL', $def('DB_URL'));

define('API_AUDIENCE', $def('API_AUDIENCE'));
define('API_SECRET_KEY', $def('API_SECRET_KEY'));
define('API_KEY_LIFETIME', $def('API_KEY_LIFETIME'));

define('ALLOW_MULTIPLE_DEVICES', $def('ALLOW_MULTIPLE_DEVICES', true));
define('ALLOW_BASIC_AUTH', $def('ALLOW_BASIC_AUTH', false));
define('ALLOW_ORIGIN', $def('ALLOW_ORIGIN'));

define('ADMIN_EMAIL', $def('ADMIN_EMAIL'));
define('ADMIN_PASSWORD', $def('ADMIN_PASSWORD'));

define('MAILER_HOST', $def('MAILER_HOST'));
define('MAILER_PORT', $def('MAILER_PORT'));
define('MAILER_USER', $def('MAILER_USER'));
define('MAILER_PASSWORD', $def('MAILER_PASSWORD'));
define('MAILER_ENCRYPTION', $def('MAILER_ENCRYPTION'));
define('MAILER_AUTH_MODE', $def('MAILER_AUTH_MODE'));

define('NO_REPLY', $def('NO_REPLY'));

define('MIDDLEWARE_URL', $def('MIDDLEWARE_URL'));
define('MIDDLEWARE_USERNAME', $def('MIDDLEWARE_USERNAME'));
define('MIDDLEWARE_PASSWORD', $def('MIDDLEWARE_PASSWORD'));

define('MAX_FLYING_DISTANCE', $def('MAX_FLYING_DISTANCE'));
define('MIN_FLYING_DISTANCE', $def('MIN_FLYING_DISTANCE'));
define('MAX_NOTIFIED_AUDITOR_FOR_NEW_LEAD', $def('MAX_NOTIFIED_AUDITOR_FOR_NEW_LEAD'));

define('FRONT_ADMIN_URL', $def('FRONT_ADMIN_URL'));

