<?php
namespace Agilap\Enum;

final class AirtightnessType
{
    public const BAD = 'BAD';
    public const AVERAGE = 'AVERAGE';
    public const GOOD = 'GOOD';
    public const VERY_GOOD = 'VERY_GOOD';

    public const CurrentEnum = [self::BAD, self::AVERAGE, self::GOOD, self::VERY_GOOD];
    public const PlannedEnum = [self::GOOD, self::VERY_GOOD];

    private function __construct() { }
}
