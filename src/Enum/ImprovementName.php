<?php

namespace Agilap\Enum;

final class ImprovementName
{
    public const AIRTIGHTNESS = 'airtightness';
    public const DHW = 'dhw';
    public const FLOOR = 'floor';
    public const HEATING = 'heating';
    public const ROOF = 'roof';
    public const THERMOSTAT = 'thermostat';
    public const VENTILATION = 'ventilation';
    public const VENTS = 'vents';
    public const WALLS = 'walls';

    public const Enum = [
        self::AIRTIGHTNESS,
        self::DHW,
        self::FLOOR,
        self::HEATING,
        self::ROOF,
        self::THERMOSTAT,
        self::VENTILATION,
        self::VENTS,
        self::WALLS
    ];

    private function __construct()
    { }
}
