<?php
namespace Agilap\Enum;

final class Role
{
    public const USER = 'User';
    public const AUDITOR = 'Auditor';
    public const ADMIN_TERRITORY = 'Admin territory';
    public const USER_TERRITORY = 'User territory';
    public const ADMIN = 'Admin';

    public const Enum = [self::USER, self::AUDITOR, self::ADMIN_TERRITORY, self::USER_TERRITORY, self::ADMIN];

    private function __construct() { }
}
