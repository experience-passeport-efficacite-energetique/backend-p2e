<?php
namespace Agilap\Enum;

final class RenovationPlanStatus
{
    public const DRAFT = 'DRAFT';
    public const VALIDATED = 'VALIDATED';
    public const ARCHIVED = 'ARCHIVED';

    public const Enum = [self::DRAFT, self::VALIDATED, self::ARCHIVED];

    private function __construct()
    { }
}
