<?php
namespace Agilap\Enum;

final class FloorType
{
    public const OTHER_UNHEATED_SPACE = 'OTHER_UNHEATED_SPACE';
    public const CRAWL_SPACE = 'CRAWL_SPACE';
    public const TERREPLEIN = 'TERREPLEIN';

    public const Enum = [self::OTHER_UNHEATED_SPACE, self::CRAWL_SPACE, self::TERREPLEIN];

    private function __construct() { }
}
