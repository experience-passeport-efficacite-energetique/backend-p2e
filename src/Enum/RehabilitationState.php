<?php
namespace Agilap\Enum;

final class RehabilitationState
{
    public const NONE = 'NONE';
    public const ORIGINAL = 'ORIGINAL';
    public const REPLACED = 'REPLACED';
    public const DEPENDS_ON_HEATING = 'DEPENDS_ON_HEATING';

    public const Enum = [self::NONE, self::ORIGINAL, self::REPLACED];
    public const RequiredEnum = [self::ORIGINAL, self::REPLACED];
    public const DHWEnum = [self::DEPENDS_ON_HEATING, self::ORIGINAL, self::REPLACED];

    private function __construct() { }
}
