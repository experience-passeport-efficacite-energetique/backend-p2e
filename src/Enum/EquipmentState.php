<?php
namespace Agilap\Enum;

final class EquipmentState
{
    public const BRAND_NEW = 'NEW';
    public const USED = 'USED';
    public const FIXER_UPPER = 'FIXER_UPPER';
    public const NOT_APPLICABLE = 'NOT_APPLICABLE';

    public const Enum = [self::BRAND_NEW, self::USED, self::FIXER_UPPER];
    public const EnumWithNA = [self::BRAND_NEW, self::USED, self::FIXER_UPPER, self::NOT_APPLICABLE];

    private function __construct()
    { }
}
