<?php
namespace Agilap\Enum;

final class EnergyLevel
{
    public const A = 'A';
    public const B = 'B';
    public const ECO_PTZ = 'Seuil Eco-PTZ';

    public const Enum = [self::A, self::B, self::ECO_PTZ];

    private function __construct() { }
}
