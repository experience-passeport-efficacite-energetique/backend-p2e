<?php
namespace Agilap\Enum;

final class PriceUnit
{
    public const FIXED = 'FIXED';
    public const UNIT = 'UNIT';
    public const SURFACE = 'SURFACE';

    public const Enum = [self::FIXED, self::UNIT, self::SURFACE];

    private function __construct() { }
}
