<?php
namespace Agilap\Enum;

final class MaintenanceFrequency
{
    public const NONE = 'NONE';
    public const EVERY_YEAR = 'EVERY_YEAR';
    public const EVERY_TWO_YEAR_OR_MORE = 'EVERY_TWO_YEAR_OR_MORE';

    public const Enum = [self::NONE, self::EVERY_YEAR, self::EVERY_TWO_YEAR_OR_MORE];

    private function __construct() { }
}
