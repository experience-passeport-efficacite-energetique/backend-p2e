<?php
namespace Agilap\Enum;

final class AtticType
{
    public const NONE = 'NONE';
    public const CONVERTED = 'CONVERTED';
    public const CONVERTIBLE = 'CONVERTIBLE';
    public const NON_CONVERTIBLE = 'NON_CONVERTIBLE';

    public const Enum = [self::NONE, self::CONVERTED, self::CONVERTIBLE, self::NON_CONVERTIBLE];

    private function __construct() { }
}
