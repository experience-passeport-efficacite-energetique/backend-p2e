<?php
namespace Agilap\Enum;

final class TDFState
{
    public const NONE = 'NONE';
    public const NEW = 'NEW';
    public const USED = 'USED';
    public const FIXER_UPPER = 'FIXER_UPPER';
    public const NO_IDEA = 'NO_IDEA';

    public const Enum = [self::NEW, self::USED, self::FIXER_UPPER, self::NO_IDEA];
    public const Gaz_Specific_Enum = [self::NONE, self::NEW, self::USED, self::FIXER_UPPER, self::NO_IDEA];

    private function __construct() { }
}
