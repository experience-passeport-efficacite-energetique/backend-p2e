<?php
namespace Agilap\Enum;

final class Floors
{
    public const ONE_LEVEL = 'ONE_LEVEL';
    public const TWO_LEVELS_WITH_ATTIC = 'TWO_LEVELS_WITH_ATTIC';
    public const TWO_LEVELS = 'TWO_LEVELS';
    public const THREE_LEVELS_WITH_ATTIC = 'THREE_LEVELS_WITH_ATTIC';
    public const THREE_LEVELS = 'THREE_LEVELS';

    public const Enum = [self::ONE_LEVEL, self::TWO_LEVELS_WITH_ATTIC, self::TWO_LEVELS, self::THREE_LEVELS_WITH_ATTIC, self::THREE_LEVELS];

    private function __construct() { }
}
