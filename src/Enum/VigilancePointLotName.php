<?php

namespace Agilap\Enum;

final class VigilancePointLotName
{
    public const AIRTIGHTNESS = 'airtightness';
    public const DHW_SOLAR = 'dwh.solar';
    public const FLOOR_TYPE = 'floor.floorType';
    public const HEATING = 'heating.id';
    public const ROOF_ATTIC = 'roof.atticType';
    public const THERMOSTAT = 'thermostat';
    public const VENTILATION = 'ventilation.id';
    public const VENTS_POSE = 'vents.poseType';
    public const WALLS_INSULATION = 'walls.insulationType';

    public const Enum = [
        self::AIRTIGHTNESS,
        self::DHW_SOLAR,
        self::FLOOR_TYPE,
        self::HEATING,
        self::ROOF_ATTIC,
        self::THERMOSTAT,
        self::VENTILATION,
        self::VENTS_POSE,
        self::WALLS_INSULATION
    ];

    private function __construct()
    {
    }
}
