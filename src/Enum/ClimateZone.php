<?php
namespace Agilap\Enum;

final class ClimateZone
{
    public const H1 = 'H1';
    public const H2 = 'H2';
    public const H3 = 'H3';

    public const Enum = [self::H1, self::H2, self::H3];

    private function __construct() { }
}
