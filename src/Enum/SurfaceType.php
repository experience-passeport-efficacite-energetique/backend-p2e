<?php
namespace Agilap\Enum;

final class SurfaceType
{
    public const DEVELOPED = 'DEVELOPED';
    public const RECTANGULAR = 'RECTANGULAR';
    public const ELONGATED = 'ELONGATED';

    public const Enum = [self::DEVELOPED, self::RECTANGULAR, self::ELONGATED];

    private function __construct() { }
}
