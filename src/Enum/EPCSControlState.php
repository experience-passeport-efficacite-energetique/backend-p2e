<?php
namespace Agilap\Enum;

final class EPCSControlState
{
    public const NONE = 'NONE';
    public const LESS_THAN_10_YEARS = 'LESS_THAN_10_YEARS';
    public const MORE_THAN_10_YEARS = 'MORE_THAN_10_YEARS';
    public const NO_IDEA = 'NO_IDEA';

    public const Enum = [self::NONE, self::LESS_THAN_10_YEARS, self::MORE_THAN_10_YEARS, self::NO_IDEA];

    private function __construct() { }
}
