<?php

namespace Agilap\Enum;

final class VigilancePointStage
{
    public const ALL = 'all';
    public const CURRENT = 'current';
    public const PREVIOUS = 'previous';

    public const Enum = [
        self::ALL,
        self::CURRENT,
        self::PREVIOUS,
    ];

    private function __construct()
    { }
}
