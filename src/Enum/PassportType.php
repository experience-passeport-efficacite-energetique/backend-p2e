<?php
namespace Agilap\Enum;

final class PassportType
{
    public const HOUSE = 'HOUSE';
    public const APARTMENT = 'APARTMENT';

    public const Enum = [self::HOUSE, self::APARTMENT];

    private function __construct() { }
}
