<?php

namespace Agilap\Enum;

final class RoofType
{
    public const LOST_ATTIC = 'LOST_ATTIC'; // combles perdues
    public const RAKE = 'RAKE'; //rampant
    public const PATIO = 'PATIO'; //terrasse

    public const Enum = [self::LOST_ATTIC, self::RAKE, self::PATIO];

    private function __construct()
    {
    }
}
