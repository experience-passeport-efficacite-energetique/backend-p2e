<?php
namespace Agilap\Enum;

final class Adjacency
{
    public const X_LARGE = 'X_LARGE';
    public const LARGE = 'LARGE';
    public const MEDIUM = 'MEDIUM';
    public const SMALL = 'SMALL';
    public const NONE = 'NONE';

    public const Enum = [self::X_LARGE, self::LARGE, self::MEDIUM, self::SMALL, self::NONE];

    private function __construct() { }
}
