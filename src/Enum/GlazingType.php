<?php
namespace Agilap\Enum;

final class GlazingType
{
    public const SIMPLE_GLAZING = 'SIMPLE_GLAZING';
    public const OLD_DOUBLE_GLAZING = 'OLD_DOUBLE_GLAZING';
    public const DOUBLE_GLAZING = 'DOUBLE_GLAZING';
    public const HIGH_PERFORMANCE_DOUBLE_GLAZING = 'HIGH_PERFORMANCE_DOUBLE_GLAZING';
    public const TRIPLE_GLAZING = 'TRIPLE_GLAZING';
    public const DOUBLE_WINDOW = 'DOUBLE_WINDOW';

    public const Enum = [
        self::SIMPLE_GLAZING,
        self::OLD_DOUBLE_GLAZING,
        self::DOUBLE_GLAZING,
        self::HIGH_PERFORMANCE_DOUBLE_GLAZING,
        self::TRIPLE_GLAZING,
        self::DOUBLE_WINDOW
    ];

    private function __construct() { }
}
