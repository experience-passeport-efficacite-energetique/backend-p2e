<?php
namespace Agilap\Enum;

final class TDFPresenceType
{
    public const PRESENT = 'PRESENT';
    public const ABSENT = 'ABSENT';
    public const NO_IDEA = 'NO_IDEA';

    public const Enum = [self::PRESENT, self::ABSENT, self::NO_IDEA];

    private function __construct() { }
}
