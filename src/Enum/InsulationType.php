<?php
namespace Agilap\Enum;

final class InsulationType
{
    public const ITI = 'ITI'; // Internal Thermal Insulation
    public const ETI = 'ETI'; // External Thermal Insulation
    public const NONE = 'NONE'; // NO Insulation

    public const CurrentEnum = [self::ITI, self::ETI, self::NONE];
    public const PlannedEnum = [self::ITI, self::ETI];

    private function __construct() { }
}
