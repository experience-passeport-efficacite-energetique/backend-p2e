<?php
namespace Agilap\Enum;

final class TDFControlState
{
    public const NONE = 'NONE';
    public const LESS_THAN_3_YEARS = 'LESS_THAN_3_YEARS';
    public const MORE_THAN_3_YEARS = 'MORE_THAN_3_YEARS';
    public const NO_IDEA = 'NO_IDEA';

    public const Enum = [self::NONE, self::LESS_THAN_3_YEARS, self::MORE_THAN_3_YEARS, self::NO_IDEA];

    private function __construct() { }
}
