<?php
namespace Agilap\Enum;

final class PoseType
{
    public const RABBET = 'RABBET';
    public const INDOOR_WALL_MOUNTED = 'INDOOR_WALL_MOUNTED';
    public const TUNNEL = 'TUNNEL';
    public const OUTDOOR_WALL_MOUNTED = 'OUTDOOR_WALL_MOUNTED';

    public const Enum = [
        self::RABBET,
        self::INDOOR_WALL_MOUNTED,
        self::TUNNEL,
        self::OUTDOOR_WALL_MOUNTED
    ];

    private function __construct() { }
}
