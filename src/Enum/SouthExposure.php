<?php
namespace Agilap\Enum;

final class SouthExposure
{
    public const SUFFICIENT = 'SUFFICIENT';
    public const INSUFFICIENT = 'INSUFFICIENT';

    public const Enum = [self::SUFFICIENT, self::INSUFFICIENT];

    private function __construct() { }
}
