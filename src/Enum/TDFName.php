<?php

namespace Agilap\Enum;

final class TDFName
{
    public const ASBESTOS = 'asbestos';
    public const ELECTRICAL_WIRING = 'electricalWiring';
    public const EPCS = 'epcs';
    public const GAS_SYSTEM = 'gasSystem';
    public const LEAD = 'lead';
    public const PLUMBING = 'plumbing';
    public const SEPTIC_TANK = 'septicTank';
    public const TERMITES = 'termites';

    public const Enum = [
        self::ASBESTOS,
        self::ELECTRICAL_WIRING,
        self::EPCS,
        self::GAS_SYSTEM,
        self::LEAD,
        self::PLUMBING,
        self::SEPTIC_TANK,
        self::TERMITES
    ];

    private function __construct()
    { }
}
