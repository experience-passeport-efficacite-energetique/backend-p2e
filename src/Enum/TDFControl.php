<?php
namespace Agilap\Enum;

final class TDFControl
{
    public const NOT_CONCERNED = 'NOT_CONCERNED';
    public const UP_TO_DATE = 'UP_TO_DATE';
    public const TO_DO = 'TO_DO';

    public const Enum = [self::NOT_CONCERNED, self::UP_TO_DATE, self::TO_DO];

    private function __construct() { }
}
