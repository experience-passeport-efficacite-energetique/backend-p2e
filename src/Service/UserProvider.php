<?php
namespace Agilap\Service;

use Agilap\Model\Bean\Generated\AbstractUser;
use Agilap\Model\Dao\UserDao;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use TheCodingMachine\TDBM\TDBMService;

class UserProvider implements UserProviderInterface
{
    /** @var TDBMService */
    private $tdbmService;

    /** @var UserDao */
    private $userDao;

    function __construct(TDBMService $tdbmService)
    {
        $this->tdbmService = $tdbmService;
        $this->userDao = new UserDao($tdbmService);
    }

    /**
     * @param string $username The username
     * @return UserInterface
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        return $this->userDao->getByEmail($username);
    }

    /**
     * Refreshes the user.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the user is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof AbstractUser) {
            throw new UnsupportedUserException();
        }
        return $user;
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return is_a($class, AbstractUser::class, true);
    }
}
