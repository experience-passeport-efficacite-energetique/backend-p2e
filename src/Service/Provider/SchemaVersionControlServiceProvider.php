<?php
namespace Agilap\Service\Provider;

use BrainDiminished\SchemaVersionControl\SchemaVersionControlService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class SchemaVersionControlServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['schemaversion'] = function() use ($pimple) {
            return new SchemaVersionControlService($pimple['db'], $pimple['schemaversion.file']);
        };
    }
}