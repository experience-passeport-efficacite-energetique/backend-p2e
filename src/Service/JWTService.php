<?php

namespace Agilap\Service;

use Agilap\Model\Bean\User;
use Agilap\Model\Dao\UserDao;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use TheCodingMachine\TDBM\TDBMService;

class JWTService
{
    /** @var UserDao */
    private $userDao;

    public function __construct(TDBMService $tdbmService)
    {
        $this->userDao = new UserDao($tdbmService);
    }

    public function generateJWT(User $user): string
    {
        $apiKey = $this->generateAPIKey($user);
        $builder = new Builder();
        $builder
            ->setIssuer($user->getEmail())
            ->setSubject(implode('&', $user->getRoles()))
            ->setAudience(API_AUDIENCE)
            ->setId($apiKey)
            ->setIssuedAt(time())
            ->setExpiration(time() + API_KEY_LIFETIME);
        $builder->sign(new Sha512(), API_SECRET_KEY);

        $token = $builder->getToken();
        $jwt = $token->__toString();
        return $jwt;
    }

    private function generateAPIKey(User $user): string
    {
        if (ALLOW_MULTIPLE_DEVICES && !empty($user->getApiKey())) {
            return $user->getApiKey();
        } else {
            $apiKey = sha1($user->getEmail().time().rand());
            $user->setApiKey($apiKey);
            $this->userDao->save($user);
            return $apiKey;
        }
    }
}
