<?php

namespace Agilap\Service;

use Agilap\Enum\Role;
use Agilap\Model\Bean\Lead;
use Agilap\Model\Bean\RenovationPlan;

class MailHelper
{
    const EMAIL_ACCOUNT_CREATED = 'email-account-created';
    const EMAIL_INVITATION_AUDITOR = 'email-invitation-auditor';
    const EMAIL_INVITATION_USER_TERRITORY = 'email-invitation-user-territory';
    const EMAIL_ACCOUNT_RESET_PASSWORD = 'email-account-reset-password';
    const EMAIL_CONFIRMATION_LEAD_CREATED = 'email-lead-confirmation-created';
    const EMAIL_NEW_LEAD = 'email-lead-created-notification-auditor';
    const EMAIL_LEAD_1WEEK_REMINDER = 'email-lead-first-reminder';
    const EMAIL_LEAD_2WEEK_REMINDER = 'email-lead-second-reminder';
    const EMAIL_LEAD_ASSIGN = 'email-lead-assigned';
    const EMAIL_RENOVATION_PLAN_READY = 'email-renovation-plan-ready';

    public function __construct()
    {
        $this->dispatcher = new MiddlewareService();
    }

    /*
     * USER EMAIL
     */

    public function sendAccountCreated($user, $activationLink)
    {
        $to = $user->getEmail();
        $firstname = $user->getFirstname();
        $lastname = $user->getLastname();

        $mail_data = array("firstname" => $firstname, "lastname" => $lastname, "activationLink" => $activationLink);

        return $this->dispatcher->sendMail(array('to' => $to), self::EMAIL_ACCOUNT_CREATED, $mail_data);
    }

    public function sendInvitationFromAuditor($user, $activationLink, $creator)
    {
      return $this->sendInvitation($user, $activationLink, $creator, self::EMAIL_INVITATION_AUDITOR);
    }

    public function sendInvitationFromUserTerritory($user, $activationLink, $creator)
    {
      return $this->sendInvitation($user, $activationLink, $creator, self::EMAIL_INVITATION_USER_TERRITORY);
    }

    private function sendInvitation($user, $activationLink, $creator, $mailTemplate)
    {
        $to = $user->getEmail();
        $firstname = $user->getFirstname();
        $lastname = $user->getLastname();

        $mailData = [
            "firstname" => $firstname,
            "lastname" => $lastname,
            "activationLink" => $activationLink,
            "sender" => [
                "firstname" => $creator->getFirstname(),
                "lastname" => $creator->getLastname(),
                "company" => $creator->getCompany(),
                "phone" => $creator->getPhone(),
                "email" => $creator->getEmail(),
            ]
        ];

        return $this->dispatcher->sendMail(array('to' => $to), $mailTemplate, $mailData);
    }

    public function sendResetPassword($user, $resetPasswordLink)
    {
        $to = $user->getEmail();
        $firstname = $user->getFirstname();
        $lastname = $user->getLastname();

        $mail_data = array("firstname" => $firstname, "lastname" => $lastname, "resetPasswordLink" => $resetPasswordLink);
        return $this->dispatcher->sendMail(array('to' => $to), self::EMAIL_ACCOUNT_RESET_PASSWORD, $mail_data);
    }

    public function sendConfirmationLeadCreated(Lead $lead)
    {
        $owner = $lead->getOwner();

        $mailData = [
            "firstname" => $owner->getFirstname(),
            "lastname" => $owner->getLastname()
        ];

        return $this->dispatcher->sendMail(
            ['to' => $owner->getEmail()],
            self::EMAIL_CONFIRMATION_LEAD_CREATED,
            $mailData
        );
    }

    public function sendLeadAssign(Lead $lead)
    {
        $owner = $lead->getOwner();
        $auditor = $lead->getAuditor();

        $mailData = [
            "owner" => [
                "firstname" => $owner->getFirstname(),
                "lastname" => $owner->getLastname()
            ],
            "auditor" => [
                "firstname" => $auditor->getFirstname(),
                "lastname" => $auditor->getLastname(),
                "company" => $auditor->getCompany(),
                "phone" => $auditor->getPhone(),
                "email" => $auditor->getEmail()
            ],
        ];

        return $this->dispatcher->sendMail(
            ['to' => $owner->getEmail()],
            self::EMAIL_LEAD_ASSIGN,
            $mailData
        );
    }


    /*
     * AUDITOR EMAIL
     */

    public function sendNewLead($lead, $auditors)
    {
        $this->sendLeadNotification($lead, $auditors, false);
    }

    public function sendNewLeadReminder($lead, $auditors)
    {
        $this->sendLeadNotification($lead, $auditors, true);
    }

    private function sendLeadNotification(Lead $lead, $auditors, $isReminder = false)
    {
        $owner = $lead->getOwner();
        $address = $lead->getAddress();

        $mailData = [
            "owner" => [
                "firstname" => $owner->getFirstname(),
                "lastname" => $owner->getLastname()
            ],
            "address" => [
                "city" => $address->getCity(),
                "zipcode" => $address->getCitycode(),
            ],
            "leadsLink" => FRONT_ADMIN_URL . "/leads"
        ];

        foreach ($auditors as $auditor) {
            $mailData["auditor"] = [
                "firstname" => $auditor->getFirstname(),
                "lastname" => $auditor->getLastname(),
                "max_dist" => $auditor->getMaxFlyingDistance() ?? MAX_FLYING_DISTANCE
            ];

            $this->dispatcher->sendMail(
                ['to' => $auditor->getEmail()],
                $isReminder ? self::EMAIL_LEAD_1WEEK_REMINDER : self::EMAIL_NEW_LEAD,
                $mailData
            );
        }
    }

    public function sendLeadReport(RenovationPlan $renovationPlan, $attachment)
    {
        $passport = $renovationPlan->getPassport();
        $owner = $passport->getOwner();
        $auditor = $passport->getAuditor();

        $mailData = [
            'owner' => [
                "firstname" => $owner->getFirstname(),
                "lastname" => $owner->getLastname(),
            ],
            "auditor" => [
                "firstname" => $auditor->getFirstname(),
                "lastname" => $auditor->getLastname(),
            ],
            "profilLink" => FRONT_ADMIN_URL . "/profile"
        ];

        return $this->dispatcher->sendMail(
            ['to' => $owner->getEmail()],
            self::EMAIL_RENOVATION_PLAN_READY,
            $mailData,
            $attachment
        );
    }

    /*
     * ADMIN TERRITORY EMAIL
     */

    public function sendLeadAdminNotification(Lead $lead, $auditors, $adminsTerritories, $admins)
    {
        $owner = $lead->getOwner();
        $address = $lead->getAddress();

        if (!$address) {
            return;
        }

        $mailData = [
            "owner" => [
                "firstname" => $owner->getFirstname(),
                "lastname" => $owner->getLastname()
            ],
            "address" => [
                "city" => $address->getCity(),
                "zipcode" => $address->getCitycode(),
            ],
            "leadsLink" => FRONT_ADMIN_URL . "/leads",
            "auditors" => []
        ];

        foreach ($auditors as $auditor) {
            $mailData["auditors"][] = [
                "firstname" => $auditor->getFirstname(),
                "lastname" => $auditor->getLastname(),
                "company" => $auditor->getCompany(),
                "email" => $auditor->getEmail(),
                "phone" => $auditor->getPhone(),
            ];
        }

        $adminsEmail = [];
        foreach ($admins as $admin) {
            $adminsEmail[] = $admin->getEmail();
        }

        foreach ($adminsTerritories as $adminTerritory) {
          $mailData['recipient'] = [
            'firstname' => $adminTerritory->getFirstname(),
            'lastname' => $adminTerritory->getLastname()
          ];
          $this->dispatcher->sendMail(
              [
                'to' => $adminTerritory->getEmail(),
                'bcc' => $adminsEmail
              ],
              self::EMAIL_LEAD_2WEEK_REMINDER,
              $mailData
          );
        }
    }
}
