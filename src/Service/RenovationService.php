<?php

namespace Agilap\Service;

use Agilap\Enum\AirtightnessType;
use Agilap\Enum\EquipmentState;
use Agilap\Enum\PriceUnit;
use Agilap\Enum\TDFControl;
use Agilap\Model\Bean\PriceRange;
use Agilap\Model\Bean\RenovationPlan;
use Agilap\Model\Dao\StaticPriceDao;
use TheCodingMachine\TDBM\TDBMService;

class RenovationService
{
  /** @var TDBMService */
  private $tdbmService;
  /** @var StaticPriceDao */
  private $staticPriceDao;

  public function __construct(TDBMService $tdbmService)
  {
    $this->tdbmService = $tdbmService;
    $this->staticPriceDao = new StaticPriceDao($tdbmService);
  }

  public function updateFromPassport(RenovationPlan $plan)
  {
    // TODO: ALL longevities should be in database! (cf walls, floor, roof, vents, thermostat, airtightness)
    $improvement = $plan->getImprovement();
    if ($improvement === null) return;
    $passport = $plan->getPassport();

    if ($data = $passport->getWallsData()) {
      $improvement->setWallsState($state = $this->getState($data->getState()));
      $improvement->setWallsProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), $data->getRehabilitationYear(), $state, 30));
      $improvement->setWallsDefinedInterventionYear($improvement->getWallsProposedInterventionYear());
    }

    if ($data = $passport->getFloorData()) {
      $improvement->setFloorState($state = $this->getState($data->getState()));
      $improvement->setFloorProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), $data->getRehabilitationYear(), $state, 50));
      $improvement->setFloorDefinedInterventionYear($improvement->getFloorProposedInterventionYear());
    }

    if ($data = $passport->getRoofData()) {
      $improvement->setRoofState($state = $this->getState($data->getState()));
      $improvement->setRoofProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), $data->getRehabilitationYear(), $state, 30));
      $improvement->setRoofDefinedInterventionYear($improvement->getRoofProposedInterventionYear());
    }

    if ($data = $passport->getVentsData()) {
      $improvement->setVentsState($state = $this->getState($data->getState()));
      $improvement->setVentsProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), $data->getRehabilitationYear(), $state, 25));
      $improvement->setVentsDefinedInterventionYear($improvement->getVentsProposedInterventionYear());
    }

    if ($data = $passport->getHeatingData()) {
      $improvement->setHeating($data->getPlannedHeating());
      $improvement->setHeatingState($state = $this->getState($data->getState()));
      $improvement->setHeatingProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), $data->getRehabilitationYear(), $state, $data->getCurrentHeating()->getLongevity()));
      $improvement->setHeatingDefinedInterventionYear($improvement->getHeatingProposedInterventionYear());
    }

    if ($data = $passport->getVentilationData()) {
      $improvement->setVentilation($data->getPlannedVentilation());
      $improvement->setVentilationState($state = $this->getState($data->getState()));
      $improvement->setVentilationProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), $data->getRehabilitationYear(), $state, $data->getCurrentVentilation()->getLongevity()));
      $improvement->setVentilationDefinedInterventionYear($improvement->getVentilationProposedInterventionYear());
    }

    if ($data = $passport->getThermostatData()) {
      $improvement->setThermostatState($state = $this->getState($data->getState()));
      $improvement->setThermostatProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), 0, $state, 20));
      $improvement->setThermostatDefinedInterventionYear($improvement->getThermostatProposedInterventionYear());
    }

    if ($data = $passport->getDhwData()) {
      $improvement->setDhw($data->getPlannedDhw());
      $improvement->setDhwState($state = $this->getState($data->getState()));
      $improvement->setDhwProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), $data->getRehabilitationYear(), $state, $data->getCurrentDhw()->getLongevity()));
      $improvement->setDhwDefinedInterventionYear($improvement->getDhwProposedInterventionYear());
    }

    if ($data = $passport->getAirtightnessData()) {
      switch ($data->getPlannedAirtightness()) {
        case AirtightnessType::VERY_GOOD:
          $improvement->setAirtightnessValue(0.6);
          break;
        default:
          $improvement->setAirtightnessValue(0.8);
          break;
      }
      $improvement->setAirtightnessState($state = $this->getState($data->getCurrentAirtightness()));
      $improvement->setAirtightnessProposedInterventionYear($this->getInterventionYear($passport->getConstructionYear(), 0, $state, 0));
      $improvement->setAirtightnessDefinedInterventionYear($improvement->getAirtightnessProposedInterventionYear());
    }
  }

  public function updateTdfFromPassport(RenovationPlan $plan)
  {
    $tdfData = $plan->getPassport()->getTdfData();
    $plannedTdf = $plan->getTdf();
    $baseTdf = $plan->getPassport()->getTdfData();
    if ($plannedTdf === null) return;

    $plannedTdf->setElectricalWiringState($state = $this->getState($baseTdf->getElectricalWiringState()));
    $plannedTdf->setElectricalWiringControl($this->getTdfControl($state));

    $plannedTdf->setGasSystemState($state = $this->getState($baseTdf->getGasSystemState()));
    $plannedTdf->setGasSystemControl($this->getTdfControl($state));

    $plannedTdf->setPlumbingState($state = $this->getState($baseTdf->getPlumbingState()));
    $plannedTdf->setPlumbingControl($this->getTdfControl($state));

    $plannedTdf->setLeadState($state = $this->getState($baseTdf->getLeadPresence()));
    $plannedTdf->setLeadControl($this->getTdfControl($state));

    $plannedTdf->setGasSystemState($state = $this->getState($baseTdf->getAsbestosPresence()));
    $plannedTdf->setAsbestosControl($this->getTdfControl($state));

    $plannedTdf->setGasSystemState($state = $this->getState($baseTdf->getTermitesPresence()));
    $plannedTdf->setTermitesControl($this->getTdfControl($state));

    $plannedTdf->setEpcsState($state = $this->getState($baseTdf->getEpcs()));
    $plannedTdf->setEpcsControl($this->getTdfControl($state));

    $plannedTdf->setSepticTankState($state = $this->getState($baseTdf->getSepticTankIndividual()));
    $plannedTdf->setSepticTankControl($this->getTdfControl($state));
  }

  private function getState($state)
  {
    switch ($state) {
      case EquipmentState::FIXER_UPPER:
      case AirtightnessType::BAD:
        return 3;
      case EquipmentState::USED:
      case AirtightnessType::AVERAGE:
        return 2;
      case EquipmentState::BRAND_NEW:
      case AirtightnessType::GOOD:
      case AirtightnessType::VERY_GOOD:
        return 1;
      default:
        return 0;
    }
  }

  private function getInterventionYear(int $constructionYear, ?int $rehabilitationYear, int $state, ?int $longevity)
  {
    $year = intval(date('Y'));
    switch ($state) {
      case 1:
      case 2:
        return max($year, max($constructionYear, $rehabilitationYear) + $longevity);
      default:
        return $year;
    }
  }

  private function getTdfControl(int $state)
  {
    switch ($state) {
      case 1:
        return TDFControl::UP_TO_DATE;
      case 2:
      case 3:
        return TDFControl::TO_DO;
      default:
        return TDFControl::NOT_CONCERNED;
    }
  }

  public function getImprovementPriceRanges(RenovationPlan $plan)
  {
    $passport = $plan->getPassport();
    $improvement = $plan->getImprovement();

    $priceRanges = [];

    $priceRanges['walls'] = $improvement->getCombinatory()->getWallsPriceRange() ? $improvement->getCombinatory()->getWallsPriceRange()->apply(1, $passport->getWallsData()->getSurface()) : null;
    $priceRanges['floor'] = $improvement->getCombinatory()->getFloorPriceRange() ? $improvement->getCombinatory()->getFloorPriceRange()->apply(1, $passport->getFloorData()->getSurface()) : null;
    $priceRanges['roof'] = $improvement->getCombinatory()->getRoofPriceRange() ? $improvement->getCombinatory()->getRoofPriceRange()->apply(1, $passport->getRoofData()->getSurface()) : null;
    $priceRanges['vents'] = $improvement->getCombinatory()->getVentsPriceRange() ? $improvement->getCombinatory()->getVentsPriceRange()->apply(1, $passport->getVentsData()->getSurface()) : null;
    $priceRanges['heating'] = $improvement->getHeating()->getPriceRange() ? $improvement->getHeating()->getPriceRange()->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['ventilation'] = $improvement->getVentilation()->getPriceRange() ? $improvement->getVentilation()->getPriceRange()->apply(1, 1) : null;
    $priceRanges['thermostat'] = $this->staticPriceDao->get('THERMOSTAT') ? $this->staticPriceDao->get('THERMOSTAT')->apply(1, 1) : null;
    $priceRanges['dhw'] = $improvement->getDhw()->getPriceRange() ? $improvement->getDhw()->getPriceRange()->apply(1, 1) : null;
    $priceRanges['airtightness'] = $this->staticPriceDao->get('AIRTIGHTNESS') ? $this->staticPriceDao->get('AIRTIGHTNESS')->apply(1, 1) : null;

    return $priceRanges;
  }

  public function getTdfPriceRanges(RenovationPlan $plan)
  {
    $passport = $plan->getPassport();
    $improvement = $plan->getImprovement();

    $priceRanges = [];

    $priceRanges['asbestos'] = $this->staticPriceDao->get('TDF_ASBESTOS') ? $this->staticPriceDao->get('TDF_ASBESTOS')->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['epcs'] = $this->staticPriceDao->get('TDF_EPCS') ? $this->staticPriceDao->get('TDF_EPCS')->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['lead'] = $this->staticPriceDao->get('TDF_LEAD') ? $this->staticPriceDao->get('TDF_LEAD')->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['plumbing'] = $this->staticPriceDao->get('TDF_PLUMBING') ? $this->staticPriceDao->get('TDF_PLUMBING')->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['termites'] = $this->staticPriceDao->get('TDF_TERMITES') ? $this->staticPriceDao->get('TDF_TERMITES')->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['electricalWiring'] = $this->staticPriceDao->get('TDF_ELECTRICALWIRING') ? $this->staticPriceDao->get('TDF_ELECTRICALWIRING')->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['gasSystem'] = $this->staticPriceDao->get('TDF_GASSYSTEM') ? $this->staticPriceDao->get('TDF_GASSYSTEM')->apply(1, $passport->getHeatedSurface()) : null;
    $priceRanges['septicTank'] = $this->staticPriceDao->get('TDF_SEPTICTANK') ? $this->staticPriceDao->get('TDF_SEPTICTANK')->apply(1, $passport->getHeatedSurface()) : null;

    return $priceRanges;
  }

  public function getPriceRanges(RenovationPlan $plan)
  {
    return [
      'improvement' => $this->getImprovementPriceRanges($plan),
      'tdf' => $this->getTdfPriceRanges($plan)
    ];
  }
}
