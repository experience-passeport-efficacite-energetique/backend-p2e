<?php

namespace Agilap\Service;

use DocsDispatcherIo\Sdk\Argument\Attachment;
use DocsDispatcherIo\Sdk\Service\FileService;
use DocsDispatcherIo\Sdk\Authentication\BasicAuthAuthentication;
use DocsDispatcherIo\Sdk\Client;
use DocsDispatcherIo\Sdk\ServiceMediator;
use DocsDispatcherIo\Sdk\Service\EmailService;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class MiddlewareService
{
    /** @var Client $client */
    private $client;

    /**
     * MiddlewareService constructor.
     */
    public function __construct()
    {
        $authenticator = new BasicAuthAuthentication(MIDDLEWARE_USERNAME, MIDDLEWARE_PASSWORD);
        $this->client = new Client($authenticator);
        $this->client->setBaseUrl(MIDDLEWARE_URL);
    }

    /**
     * Send mail with template name
     *
     * @param array  $recipients
     * @param string $templateName
     * @param array  $data
     * @param array  $attachments
     *
     * @return int
     *
     * @throws \Throwable
     */
    public function sendMail(array $recipients, string $templateName, array $data, array $attachments = [])
    {
        $from = ['mail' => ADMIN_EMAIL, 'name' => ADMIN_EMAIL];
        $to = $recipients['to'];

        $emailService =  new EmailService($from, $to);

        if (array_key_exists('cc', $recipients)) {
            $emailService->setCc($recipients['cc']);
        }

        if (array_key_exists('bcc', $recipients)) {
            $emailService->setBcc($recipients['bcc']);
        }

        $emailService->setTemplateName($templateName);
        $emailService->setData($data);

        foreach ($attachments as $attachment) {
            $emailService->addAttachment(
                new Attachment(
                    $attachment['templateName'],
                    $attachment['resultFileName'],
                    $attachment['data'],
                    null
                )
            );
        }

        $requestFactory = (new ServiceMediator())->addService($emailService);

        try {
            $acceptHeader = empty($attachments) ? 'application/json' : 'application/pdf';

            return $this->client->executeRequest($requestFactory, $acceptHeader)->getStatusCode();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Generate renovation plan synthesis
     *
     * @param array  $data
     * @param string $templateName
     * @param string $fileName
     * @param array  $headers
     *
     * @return Response
     *
     * @throws \Throwable
     */
    public function generateSynthesis(array $data, string $templateName, string $fileName, array $headers = []): Response
    {
        $acceptHeader = $headers['Accept'] ?? 'application/pdf';
        $response = $this->generateFile($templateName, $data, $fileName, $acceptHeader);

        return new Response(
            $response->getBody()->getContents(),
            $response->getStatusCode(),
            ['content-Type' => $acceptHeader]
        );
    }

    /**
     * Generate file from template
     *
     * @param string $templateName
     * @param array  $data
     * @param string $outputFilename
     * @param string $acceptHeader
     *
     * @return ResponseInterface
     *
     * @throws \Throwable
     */
    private function generateFile(string $templateName, array $data, string $outputFilename, string $acceptHeader): ResponseInterface
    {
        $fileService = new FileService($templateName, $data, $outputFilename);
        $requestFactory = (new ServiceMediator())->addService($fileService);

        try {
            return $this->client->executeRequest($requestFactory, $acceptHeader);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
