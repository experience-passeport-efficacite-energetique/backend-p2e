<?php
namespace Agilap\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\Table;
use Symfony\Component\Yaml\Yaml;

class StaticDataService
{
    /** @var Connection */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function load(string $dataFile): int
    {
        $schema = $this->getSchema();
        $data = Yaml::parse(file_get_contents($dataFile));
        $res = 0;
        foreach ($data as $table => $rows) {
            $table = $schema->getTable($table);
            $res += $this->loadTable($table, $rows);
        }
        return $res;
    }

    private function loadTable(Table $table, array $data): int
    {
        $res = 0;
        foreach ($data as $row) {
            $res += $this->loadRow($table, $row);
        }
        return $res;
    }

    private function loadRow(Table $table, array $row): int
    {
        if ($table->hasPrimaryKey()) {
            $pk = [];
            foreach ($table->getPrimaryKeyColumns() as $column) {
                if (!isset($row[$column])) {
                    return $this->createRow($table, $row);
                }
                $pk[$column] = $row[$column];
            }
            if ($this->findRow($table->getName(), $pk)) {
                return $this->updateRow($table, $pk, array_diff_key($row, $pk));
            } else {
                return $this->createRow($table, $row);
            }
        } else {
            return $this->createRow($table, $row);
        }

    }

    private function findRow(string $table, array $primaryKey): int
    {
        $clauses = [];
        foreach ($primaryKey as $column => $value) {
            $clauses[] = "$column = :$column";
        }
        $clause = implode(' AND ', $clauses);

        $sql = "SELECT * FROM $table WHERE $clause;";
        return count($this->connection->fetchAll($sql, $primaryKey));
    }

    private function createRow(Table $table, array $row): int
    {
        return $this->connection->insert($table->getName(), $row);
    }

    private function updateRow(Table $table, array $primaryKey, array $row): int
    {
        return $this->connection->update($table->getName(), $row, $primaryKey);
    }

    private function getSchema(): Schema
    {
        return $this->connection->getSchemaManager()->createSchema();
    }
}
