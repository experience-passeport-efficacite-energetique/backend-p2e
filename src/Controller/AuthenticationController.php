<?php
namespace Agilap\Controller;

use Agilap\Exception\ApiSchemaException;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\UserDao;
use Agilap\Service\JWTService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use TheCodingMachine\TDBM\TDBMService;

/**
 * @OA\Schema(
 *   description="Object to send to request a JWT Token",
 *   schema="AuthRequest",
 *   @OA\Property(
 *      property="username",
 *      type="string",
 *   ),
 *   @OA\Property(
 *      property="password",
 *      type="string",
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Object received when auth successful",
 *   schema="AuthSuccessful",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      example="Authentication successful"
 *   ),
 *   @OA\Property(
 *      property="token",
 *      type="string",
 *   )
 * )
 */
class AuthenticationController extends AbstractController
{
    /** @var UserProviderInterface */
    private $userProvider;
    /** @var JWTService */
    private $jwtService;
    /** @var TokenStorage */
    private $tokenStorage;

    function __construct(UserProviderInterface $userProvider, JWTService $jwtService, TokenStorage $tokenStorage)
    {
        $this->userProvider = $userProvider;
        $this->jwtService = $jwtService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @OA\Post(
     *     path="/auth",
     *     tags={"Auth", "@POST"},
     *     description="Get a JWT token for future requests",
     *     @OA\RequestBody(
     *         description="credentials",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/AuthRequest")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="JWT token",
     *          @OA\JsonContent(ref="#/components/schemas/AuthSuccessful"),
     *     ),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function authenticate(Request $request): JsonResponse
    {
        try {
            $params = $this->extractAll($request);
            $errors = [];
            foreach (['username', 'password'] as $field) {
                if (!isset($params[$field]) || empty($params[$field])) {
                    $errors[$field] = 'This field is required';
                } else if (!is_scalar($params[$field])) {
                    $errors[$field] = 'Invalid value';
                }
            }
            if (!empty($errors)) {
                throw new ApiSchemaException($errors, 'Failed to connect');
            }

            $username = $params['username'];
            $password = $params['password'];
            /** @var User $user */
            $user = $this->userProvider->loadUserByUsername($username);
            if ($user !== null && $user->getActive() && password_verify($password, $user->getPassword())) {
                $jwt = $this->jwtService->generateJWT($user);
                return new JsonResponse([
                    'message' => 'Authentication successful',
                    'token' => $jwt
                ]);
            } else {
                throw new AuthenticationException();
            }
        } catch (AuthenticationException $exception) {
            return new JsonResponse([
                'message' => 'Authentication failed',
                'errors' => [
                    'form' => 'Identifiants invalides'
                ]
            ], Response::HTTP_FORBIDDEN);
        } catch (ApiSchemaException $exception) {
            return new JsonResponse([
                'message' => 'Authentication failed',
                'errors' => $exception->getErrors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @OA\Get(
     *     path="/auth/refresh",
     *     tags={"Auth", "@GET"},
     *     description="Get a new JWT token for future requests",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="JWT token",
     *          @OA\JsonContent(ref="#/components/schemas/AuthSuccessful"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     * )
     */
    public function refresh(): JsonResponse
    {
        if (
            $this->tokenStorage->getToken() === null
            || $this->tokenStorage->getToken()->getUser() === null
        ) {
            throw new AuthenticationCredentialsNotFoundException();
        }
        $user = $this->tokenStorage->getToken()->getUser();
        $newJwt = $this->jwtService->generateJWT($user);
        return new JsonResponse([
            'message' => 'Credentials successfully refreshed',
            'token' => $newJwt
        ]);
    }
}
