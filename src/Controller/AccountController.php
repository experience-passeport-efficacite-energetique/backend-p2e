<?php

namespace Agilap\Controller;

use Agilap\Enum\Role;
use Agilap\Exception\ApiSchemaException;
use Agilap\Hydration\Hydrator\AddressHydrator;
use Agilap\Hydration\Validator\TDBMUniqueValidator;
use Agilap\Model\Bean\Address;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\UserDao;
use Agilap\Service\MailHelper;
use MetaHydrator\Exception\HydratingException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

/**
 *  @OA\Schema(
 *   schema="CreateAccount",
 *   description="Create account object",
 *   allOf= {
 *      @OA\Schema(ref="#/components/schemas/AccountBaseUser"),
 *      @OA\Schema(
 *          @OA\Property(
 *            property="activationUrl",
 *            type="string",
 *            description="URL to follow to activate the account"
 *          ),
 *          @OA\Property(
 *             property="password",
 *             type="string",
 *             description="password of the created user"
 *         ),
 *         @OA\Property(
 *             property="address",
 *             ref="#/components/schemas/MinimalCreateAddress"
 *         ),
 *      )
 *   }
 * )
 *
 * @OA\Schema(
 *   description="Object received when account successfuly created",
 *   schema="CreationSuccess",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      example="Account created but waiting for activation"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Object received when account successfuly activated",
 *   schema="ActivationSuccess",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      example="Account activated"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Object received when account successfuly recovered",
 *   schema="RecoverAccount",
 *   @OA\Property(
 *      property="recoveryUrl",
 *      type="string"
 *   ),
 *   @OA\Property(
 *      property="username",
 *      type="string"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Object received when account successfuly recovered",
 *   schema="RecoverSuccess",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      example="An email has been sent to user user@email.com"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Object received when account successfuly recovered",
 *   schema="NewPassword",
 *   @OA\Property(
 *      property="password",
 *      type="string"
 *   )
 * )
 *
 * @OA\Schema(
 *   description="Object received when account successfuly recovered",
 *   schema="NewPasswordSuccess",
 *   @OA\Property(
 *      property="message",
 *      type="string",
 *      example="Password successfully reset"
 *   )
 * )
 *
 */
class AccountController extends AbstractController
{
    /** @var TDBMService */
    private $tdbmService;
    /** @var UserDao */
    private $userDao;

    private $mailHelper;

    function __construct(TDBMService $tdbmService, MailHelper $mailHelper)
    {
        $this->tdbmService = $tdbmService;
        $this->userDao = new UserDao($tdbmService);
        $this->mailHelper = $mailHelper;
    }

    /**
     * @OA\Post(
     *     path="/account/create",
     *     tags={"Account", "@POST"},
     *     description="Create an account",
     *     @OA\RequestBody(
     *         description="Account to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CreateAccount")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Account created",
     *          @OA\JsonContent(ref="#/components/schemas/CreationSuccess"),
     *     ),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(Request $request): JsonResponse
    {
        $hydrator = FluidHydrator::new()
            ->field('firstname')->string()->required()
            ->field('lastname')->string()->required()
            ->field('email')->string()->email()->required()->validator(new TDBMUniqueValidator($this->tdbmService, 'users', 'email'))
            ->field('password')->string()->required()
            ->field('address')->subobject(Address::class)->hydrator(new AddressHydrator())
            ->field('company')->string()
            ->field('phone')->string()->phone()
            ->field('activationUrl')->string()->required()->regex('(\{token\})', 'Url should contain injection `{token}`');

        $data = $this->extractRequest($request);
        try {
            /** @var User $user */
            $user = $hydrator->hydrateNewObject($data, User::class);
        } catch (HydratingException $e) {
            return new JsonResponse(['errors' => $e->getErrorsMap()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user->setActive(false);
        $user->setRoles([Role::USER]);
        $user->setActivationToken(sha1($user->getEmail() . time() . rand()));

        $activationToken = $user->getActivationToken();
        $activationLink = str_replace('{token}', $activationToken, $data['activationUrl']);

        if (!$this->mailHelper->sendAccountCreated($user, $activationLink)) {
            return new JsonResponse(['message' => 'Mail could not be sent'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->userDao->save($user);

        return new JsonResponse(['message' => 'Account created but waiting for activation'], Response::HTTP_CREATED);
    }

    /**
     * @OA\Post(
     *     path="/account/activate/{activation_token}",
     *     tags={"Account", "@POST"},
     *     description="Activate an account",
     *     @OA\Parameter(
     *         description="activation token",
     *         in="path",
     *         name="activation_token",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/genericToken" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Account activated",
     *          @OA\JsonContent(ref="#/components/schemas/ActivationSuccess")
     *     ),
     *     @OA\Response(response=400, ref="#/components/responses/400"),
     *     @OA\Response(response=404, ref="#/components/responses/404")
     * )
     */
    public function activate(Request $request, string $activationToken): JsonResponse
    {
        $user = $this->userDao->getByActivationToken($activationToken);
        if ($user === null) {
            return new JsonResponse([
                'message' => 'Invalid activation token'
            ], Response::HTTP_NOT_FOUND);
        }
        if ($user->getActive()) {
            return new JsonResponse([
                'message' => 'Account already activated'
            ], Response::HTTP_BAD_REQUEST);
        }
        if ($user->getPassword() === null) {
            $hydrator = FluidHydrator::new()->field('password')->string()->required();
            $data = $this->extractRequest($request);
            try {
                $hydrator->hydrateNewObject($data, User::class);
                $user->setPassword($data['password']);
            } catch (HydratingException $e) {
                return new JsonResponse(['errors' => $e->getErrorsMap()], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        $user->setActive(true);
        $this->userDao->save($user);
        return new JsonResponse([
            'message' => 'Account activated'
        ], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/account/recover",
     *     tags={"Account", "@POST"},
     *     description="Recover an account",
     *     @OA\RequestBody(
     *         description="Recovery infos",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/RecoverAccount")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Account recovered",
     *          @OA\JsonContent(ref="#/components/schemas/RecoverSuccess")
     *     ),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function recover(Request $request): JsonResponse
    {
        try {
            $data = $this->extractAll($request);

            if (!$this->validateFlatData(['username', 'recoveryUrl'], $data, $errors)) {
                throw new ApiSchemaException($errors);
            }

            $username = $data['username'];
            $recoveryUrl = $data['recoveryUrl'];
            if (!preg_match('(\{token\})', $recoveryUrl)) {
                throw new ApiSchemaException(['recoveryUrl' => 'Url should contain injection `{token}`']);
            }

            $user = $this->userDao->getByEmail($username);
            if ($user === null) {
                return new JsonResponse([
                    'message' => "Account recovery failed",
                    'errors' => ['username' => 'Unknown user']
                ], 404);
            }

            if (empty($user->getRecoveryToken())) {
                $user->setRecoveryToken(md5($username . time() . rand()));
                $changed = true;
            } else {
                $changed = false;
            }

            $recoveryToken = $user->getRecoveryToken();
            $resetLink = str_replace('{token}', $recoveryToken, $recoveryUrl);


            if (!$this->mailHelper->sendResetPassword($user, $resetLink)) {
                throw new \Exception('Mail could not be sent');
            }

            if ($changed) {
                $this->userDao->save($user);
            }

            return new JsonResponse([
                'message' => "An email has been sent to user $username",
            ]);
        } catch (ApiSchemaException $exception) {
            return new JsonResponse([
                'message' => 'Account recovery failed',
                'errors' => $exception->getErrors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @OA\Post(
     *     path="/account/reset-password/{reset_password_token}",
     *     tags={"Account", "@POST"},
     *     description="Recover an account",
     *     @OA\Parameter(
     *         description="reset password token",
     *         in="path",
     *         name="reset_password_token",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/genericToken" )
     *     ),
     *     @OA\RequestBody(
     *         description="New password",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/NewPassword")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="New password successfully set",
     *          @OA\JsonContent(ref="#/components/schemas/NewPasswordSuccess")
     *     ),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function resetPassword(Request $request, string $recoveryToken): JsonResponse
    {
        try {
            $user = $this->userDao->getByRecoveryToken($recoveryToken);
            if ($user === null) {
                throw new NotFoundHttpException('Invalid recovery token');
            }

            $params = $this->extractRequest($request);

            if (!$this->validateFlatData(['password'], $params, $errors)) {
                throw new ApiSchemaException($errors);
            }

            $password = $params['password'];

            $user->setPassword($password);

            $user->setRecoveryToken(null);

            // Invalidate current json web tokens
            $user->setApiKey(null);

            $this->userDao->save($user);

            return new JsonResponse([
                'message' => 'Password successfully reset'
            ]);
        } catch (ApiSchemaException $exception) {
            return new JsonResponse([
                'message' => 'Could not reset password',
                'errors' => $exception->getErrors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    private function validateFlatData(array $expectedFields, array $data, array &$errors = null): bool
    {
        $errors = [];
        foreach ($expectedFields as $field) {
            if (!isset($data[$field]) || empty($data[$field])) {
                $errors[$field] = 'This field is required';
            } else if (!is_scalar($data[$field])) {
                $errors[$field] = 'Invalid value';
            }
        }
        return empty($errors);
    }
}
