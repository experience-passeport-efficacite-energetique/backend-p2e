<?php

namespace Agilap\Controller\APIController;

use Agilap\Enum\Role;
use Agilap\Hydration\Hydrator\PriceRangeHydrator;
use Agilap\Model\Bean\Heating;
use Agilap\Model\Bean\PriceRange;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\HeatingDao;
use Porpaginas\Result;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

class HeatingAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;

    /** @var HeatingDao */
    private $dao;

    /** @var TokenStorage */
    private $tokenStorage;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage)
    {
        parent::__construct('heating');
        $this->tdbmService = $tdbmService;
        $this->dao = new HeatingDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param $id
     * @return \JsonSerializable
     * @throws TDBMException
     *
     * @OA\Get(
     *     path="/heatings/{id}",
     *     tags={"Heatings", "@GET"},
     *     description="Get the heating with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of heating to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the heating with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="heating",
     *                  ref="#/components/schemas/HeatingPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        return $this->dao->getById($id);
    }

    /**
     * @OA\Get(
     *     path="/heatings",
     *     tags={"Heatings", "@GET"},
     *     description="Get a list of all heatings accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         name="available",
     *         in="query",
     *         description="returns only the heatings that are available",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/genericBoolean" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of heatings returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="heatings",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/HeatingPriceRange")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        return $this->dao->search($params);
    }

    /**
     * @OA\Post(
     *     path="/heatings",
     *     tags={"Heatings", "@POST"},
     *     description="Create a heating",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="Heating to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/HeatingPriceRange")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Heating created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="heating",
     *                  ref="#/components/schemas/HeatingPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException("You are not allowed to create heatings)");
        }
        $hydrator = $this->creationHydrator();
        /** @var Heating $heating */
        $heating = $hydrator->hydrateNewObject($data, Heating::class);
        $this->dao->save($heating);
        return $heating;
    }

    /**
     * @OA\Patch(
     *     path="/heatings/{id}",
     *     tags={"Heatings", "@PATCH"},
     *     description="Update a heating",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of heating to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="Heating to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/HeatingPriceRange")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated heating",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="heating",
     *                  ref="#/components/schemas/HeatingPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($heating, array $data)
    {

        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            $id = $heating->getId();
            throw new AccessDeniedHttpException("You are not allowed to update heating ($id)");
        }

        $hydrator = $this->hydrator();
        $hydrator->hydrateObject($data, $heating);
        $this->dao->save($heating);
        return;
    }

    /**
     * @OA\Delete(
     *     path="/heatings/{id}",
     *     tags={"Heatings", "@DELETE"},
     *     description="Delete heating with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of heating to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that heating has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedHeating"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($heating): void
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException("You are not allowed to delete heatings)");
        }
        $this->dao->delete($heating);
    }

    private function hydrator()
    {
        return FluidHydrator::new()
            ->field('priceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())
            ->field('resistance')->float()->min(0.00001)->required() //not 0
            ->field('apci')->float()
            ->field('label')->string()
            ->field('emissionFactor')->float()->required()
            ->field('energyNeeds')->float()->required()
            ->field('primaryEnergy')->float()->required();
    }

    protected function creationHydrator()
    {
        return FluidHydrator::new()
            ->field('priceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())
            ->field('apci')->float()->required()
            ->field('label')->string()->required()
            ->field('available')->bool()->required()
            ->field('longevity')->float()->required()
            ->field('resistance')->float()->min(0.00001)->required() //not 0
            ->field('emissionFactor')->float()->required()
            ->field('energyNeeds')->float()->required()
            ->field('primaryEnergy')->float()->required();
    }

    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
