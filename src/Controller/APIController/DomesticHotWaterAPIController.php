<?php

namespace Agilap\Controller\APIController;

use Agilap\Enum\Role;
use Agilap\Hydration\Hydrator\PriceRangeHydrator;
use Agilap\Model\Bean\DomesticHotWater;
use Agilap\Model\Bean\PriceRange;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\DomesticHotWaterDao;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

class DomesticHotWaterAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;

    /** @var DomesticHotWaterDao */
    private $dao;

    /** @var TokenStorage */
    private $tokenStorage;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage)
    {
        parent::__construct(['domesticHotWater', 'domesticHotWaters']);
        $this->tdbmService = $tdbmService;
        $this->dao = new DomesticHotWaterDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @OA\Get(
     *     path="/domesticHotWaters/{id}",
     *     tags={"DomesticHotWaters", "@GET"},
     *     description="Get the domesticHotWater with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of domesticHotWater to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the domesticHotWater with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="domesticHotWater",
     *                  ref="#/components/schemas/DHWPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        return $this->dao->getById($id);
    }

    /**
     * @OA\Get(
     *     path="/domesticHotWaters",
     *     tags={"DomesticHotWaters", "@GET"},
     *     description="Get a list of all domesticHotWaters accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         name="available",
     *         in="query",
     *         description="returns only the domesticHotWaters that are available",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/genericBoolean" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of DHW returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="domesticHotWaters",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/DHWPriceRange")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        return $this->dao->search($params);
    }

    /**
     * @OA\Post(
     *     path="/domesticHotWaters",
     *     tags={"DomesticHotWaters", "@POST"},
     *     description="Create a DHW",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="DHW to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/DHWPriceRange")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Heating created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="domesticHotWater",
     *                  ref="#/components/schemas/DHWPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException("You are not allowed to create dhw)");
        }
        $hydrator = $this->creationHydrator();
        /** @var DomesticHotWater $dhw */
        $dhw = $hydrator->hydrateNewObject($data, DomesticHotWater::class);
        $this->dao->save($dhw);
        return $dhw;
    }

    /**
     * @OA\Patch(
     *     path="/domesticHotWaters/{id}",
     *     tags={"DomesticHotWaters", "@PATCH"},
     *     description="Update a DHW",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of DHW to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="DHW to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/DHWPriceRange")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated DHW",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="domesticHotWater",
     *                  ref="#/components/schemas/DHWPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($dhw, array $data)
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            $id = $dhw->getId();
            throw new AccessDeniedHttpException("You are not allowed to update dhw ($id)");
        }
        $hydrator = $this->hydrator();
        $hydrator->hydrateObject($data, $dhw);
        $this->dao->save($dhw);
    }

    /**
     * @OA\Delete(
     *     path="/domesticHotWaters/{id}",
     *     tags={"DomesticHotWaters", "@DELETE"},
     *     description="Delete DHW with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of DHW to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that DHW has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedDHW"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($item): void
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException("You are not allowed to delete dhw)");
        }
        $this->dao->delete($item);
    }

    protected function hydrator(): Hydrator
    {
        return FluidHydrator::new()
            ->field('priceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())
            ->field('apci')->float()
            ->field('label')->string()
            ->field('isSolar')->bool()
            ->field('resistance')->float()->min(0.00001)->required() //not 0
            ->field('emissionFactor')->float()->required()
            ->field('energyNeeds')->float()->required()
            ->field('primaryEnergy')->float()->required();
    }

    protected function creationHydrator(): Hydrator
    {
        return FluidHydrator::new()
            ->field('priceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())
            ->field('apci')->float()->required()
            ->field('label')->string()->required()
            ->field('available')->bool()->required()
            ->field('isSolar')->bool()->required()
            ->field('longevity')->float()->required()
            ->field('resistance')->float()->min(0.00001)->required() //not 0
            ->field('emissionFactor')->float()->required()
            ->field('energyNeeds')->float()->required()
            ->field('primaryEnergy')->float()->required();
    }

    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
