<?php

namespace Agilap\Controller\APIController;

use Agilap\Controller\APIController\CleanupParametersTrait;
use Agilap\Enum\RenovationPlanStatus;
use Agilap\Enum\Role;
use Agilap\Exception\DataNotFoundException;
use Agilap\Exception\ResourceLockedException;
use Agilap\Hydration\Parser\TDBMRetriever;
use Agilap\Hydration\Validator\CacheResult;
use Agilap\Hydration\Validator\CallbackValidator;
use Agilap\Hydration\Validator\FilledPassportValidator;
use Agilap\Hydration\Validator\IntegerHigherThanValidator;
use Agilap\Misc\Pointer;
use Agilap\Model\Bean\Combinatory;
use Agilap\Model\Bean\RenovationPlan;
use Agilap\Model\Bean\RenovationPlanImprovement;
use Agilap\Model\Bean\RenovationPlanTdf;
use Agilap\Model\Bean\RenovationPlanVigilancePoint;
use Agilap\Model\Bean\User;
use Agilap\Model\Bean\VigilancePointCondition;
use Agilap\Model\Dao\RenovationPlanDao;
use Agilap\Model\Dao\RenovationPlanVigilancePointDao;
use Agilap\Model\Dao\VigilancePointConditionDao;
use Agilap\Model\Dao\VigilancePointDao;
use Agilap\Service\MailHelper;
use Agilap\Service\MiddlewareService;
use Agilap\Service\RenovationService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use MetaHydrator\Exception\ValidationException;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

class RenovationPlanAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;
    /** @var RenovationPlanDao */
    private $dao;
    /** @var RenovationPlanVigilancePointDao */
    private $rvpDao;
    /** @var VigilancePointDao */
    private $vpDao;
    /** @var VigilancePointConditionDao */
    private $vpcDao;
    /** @var RenovationService */
    private $service;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var MiddlewareService */
    private $middleware;
    /** @var MailHelper */
    private $mailHelper;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage, MiddlewareService $middleware, MailHelper $mailHelper)
    {
        parent::__construct(['renovationPlan', 'renovationPlans']);
        $this->tdbmService = $tdbmService;
        $this->dao = new RenovationPlanDao($tdbmService);
        $this->rvpDao = new RenovationPlanVigilancePointDao($tdbmService);
        $this->vpDao = new VigilancePointDao($tdbmService);
        $this->vpcDao = new VigilancePointConditionDao($tdbmService);
        $this->service = new RenovationService($this->tdbmService);
        $this->tokenStorage = $tokenStorage;
        $this->middleware = $middleware;
        $this->mailHelper = $mailHelper;
    }

    use CleanupParametersTrait;

    public function connect(Application $app)
    {
        /** @var ControllerCollection $controllers */
        $controllers = parent::connect($app);
        $controllers->get('{id}/view', function ($id) {
            return $this->html($id);
        });
        $controllers->get('{id}/synthese_passeport', function ($id) {
            return $this->pdf($id);
        });
        return $controllers;
    }

    /**
     * @OA\Get(
     *     path="/renovationPlans",
     *     tags={"RenovationPlans", "@GET"},
     *     description="Get a list of all renovationPlans accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         name="auditor_id",
     *         in="query",
     *         description="returns only the renovationPlans assigned to auditor with id `id`",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of renovationPlans returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="renovationPlans",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/RenovationPlan")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        return $this->dao->superSearch($this->getCleanParameters($this->getLogged(), $params));
    }

    /**
     * @OA\Get(
     *     path="/renovationPlans/{id}",
     *     tags={"RenovationPlans", "@GET"},
     *     description="Get the renovationPlan with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of renovationPlan to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the renovationPlan with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="renovationPlan",
     *                  ref="#/components/schemas/RenovationPlan"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        $renovationPlan = $this->dao->getById($id);
        if ($renovationPlan->getVpUpToDate() === false) {
            $this->updateRpVps($renovationPlan);
        }
        if ($this->allowedAccess($renovationPlan)) {
            return $renovationPlan;
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access renovationPlan($id)");
        }
    }

    /**
     * @OA\Post(
     *     path="/renovationPlans",
     *     tags={"RenovationPlans", "@POST"},
     *     description="Create a renovationPlan",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="renovationPlan to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/RenovationPlan")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="renovationPlan created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="renovationPlan",
     *                  ref="#/components/schemas/RenovationPlan"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::AUDITOR) && !$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException('You are not allowed to create a renovation plan');
        }

        $hydrator = $this->hydrator();
        /** @var RenovationPlan $plan */
        $plan = $hydrator->hydrateNewObject($data, RenovationPlan::class);
        $plan->setCreationDate(new \DateTimeImmutable());

        $this->service->updateFromPassport($plan);
        // $this->service->computePriceRange($plan);

        $this->service->updateTdfFromPassport($plan);
        // $this->service->computeTdfPriceRange($plan);

        $this->dao->save($plan);
        return $plan;
    }

    /**
     * @param RenovationPlan $plan
     * @param array $data
     *
     * @OA\Patch(
     *     path="/renovationPlans/{id}",
     *     tags={"RenovationPlans", "@PATCH"},
     *     description="Update a renovationPlan",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of renovationPlan to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="RenovationPlans to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/RenovationPlan")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated renovationPlan",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="renovationPlan",
     *                  ref="#/components/schemas/RenovationPlan"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($plan, array $data)
    {
        if (!$this->allowedUpdate($plan)) {
            throw new AccessDeniedHttpException("You are not allowed to update this renovation plan");
        }
        if ($plan->getWlocked()) {
            throw new ResourceLockedException("Plan is currently locked, cannot update plan");
        } else {
            $plan->setWlocked(true);
            $this->dao->save($plan);
        }
        try {
            $hydrator = $this->hydrator($plan);
            $hydrator->hydrateObject($data, $plan);
            $plan->setModificationDate(new \DateTimeImmutable());

            //Manually update vigilance points since TCM Hydrator cannot handle array of customObjects
            if (isset($data['vigilancePoints']) && is_array($data['vigilancePoints'])) {
                foreach ($data['vigilancePoints'] as $vp) {
                    $vpHydrator = $this->vpHydrator();

                    //not using direct rvpDao->getById() access to make sure updated vp belongs to this plan
                    $vpObject = $plan->getVigilancePointById($vp['id']);
                    if ($vpObject == null) {
                        throw new DataNotFoundException("Vigilance point (" + $vp['id'] + ") not found");
                    }
                    $vpHydrator->hydrateObject($vp, $vpObject);
                    $vpObject->setModificationDate(new \DateTimeImmutable());
                    $this->rvpDao->save($vpObject);
                }
            }

            if (isset($data['passport'])) {
                $this->service->updateFromPassport($plan);
                $this->service->updateTdfFromPassport($plan);
            }

            $plan->setWlocked(false);
            $this->dao->save($plan);

            return $plan;
        } catch (\Throwable $th) {
            $plan->setWlocked(false);
            $this->dao->save($plan);
            throw $th;
        }
    }

    /**
     * @OA\Delete(
     *     path="/renovationPlans/{id}",
     *     tags={"RenovationPlans", "@DELETE"},
     *     description="Delete renovationPlan with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of renovationPlan to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that renovationPlan has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedRenovationPlan"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($item): void
    {
        $this->dao->delete($item);
    }


    /**
     * @OA\Get(
     *     path="/renovationPlans/{id}/princeRanges",
     *     tags={"RenovationPlans", "@GET"},
     *     description="Get price ranges of each improvement and tdf of a given renovation plan",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of renovationPlan",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="The price ranges of improvements and tdf for the renovationPlan with the id `id`",
     *          @OA\JsonContent(ref="#/components/schemas/RenovationPlanPriceRanges")
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404")
     * )
     */
    public function getPriceRanges($id): JsonResponse
    {
        $renovationPlan = $this->dao->getById($id);
        if ($this->allowedAccess($renovationPlan)) {
            return new JsonResponse($this->service->getPriceRanges($renovationPlan));
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access renovationPlan($id)");
        }
    }

    /**
     * @OA\Post(
     *     path="/renovationPlans/{id}/computeVPs",
     *     tags={"RenovationPlans", "@POST"},
     *     description="Compute vigilance points for the renovationPlan with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of renovationPlan to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the renovationPlan with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="renovationPlan",
     *                  ref="#/components/schemas/RenovationPlan"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function computeVPs(Request $request, $id): JsonResponse
    {
        $plan = $this->dao->getById($id);
        if (!$this->allowedUpdate($plan)) {
            throw new AccessDeniedHttpException("You are not allowed to update this renovation plan");
        }
        return new JsonResponse(["renovationPlan" => $this->updateRpVps($plan)]);
    }

    public function updateRpVps($plan)
    {
        if ($plan->getWlocked()) {
            throw new ResourceLockedException("Plan is currently locked, cannot compute VPs");
        } else {
            $plan->setWlocked(true);
            $this->dao->save($plan);
        }
        try {
            $selectedVps = [];
            foreach ($plan->getRenovationPlansVigilancePoints() as $currentVp) {
                $selectedVps[$currentVp->getVigilancePoint()->getId()] = $currentVp->getSelected();
                $this->rvpDao->delete($currentVp);
            }

            foreach ($this->vpDao->findTriggeredVps($plan) as $newVp) {
                $newRvp = new RenovationPlanVigilancePoint($plan, $newVp);
                if (!array_key_exists($newRvp->getVigilancePoint()->getId(), $selectedVps)) {
                    $newRvp->setSelected(true);
                } else {
                    $newRvp->setSelected($selectedVps[$newRvp->getVigilancePoint()->getId()]);
                }
                try {
                    $this->rvpDao->save($newRvp);
                } catch (UniqueConstraintViolationException $e) {
                    // Catch only unique constraint exception and mute them.
                    // This happens when "updateRpVps" is called twice on the same rp in a very short period of time.
                }
            }
            $plan->setVpUpToDate(true);
            $plan->setWlocked(false);
            $this->dao->save($plan);
            return $plan;
        } catch (\Throwable $th) {
            $plan->setWlocked(false);
            $this->dao->save($plan);
            throw $th;
        }
    }

    const SYNTHESIS_TEMPLATE = 'p2e-p2e-synthesep2ev3-1';
    const SYNTHESIS_RESULT_FILENAME = 'Synthese_Passeport';

    public function html($id): Response
    {
        $renovationPlan = $this->get($id);
        $json = [
            'renovationPlan' => $renovationPlan->jsonSerialize(),
            'priceRanges' => $this->service->getPriceRanges($renovationPlan)
        ];
        return $this->middleware->generateSynthesis($json, self::SYNTHESIS_TEMPLATE, self::SYNTHESIS_RESULT_FILENAME, ['Accept' => 'text/html']);
    }

    public function pdf($id): Response
    {
        $renovationPlan = $this->get($id);
        $json = [
            'renovationPlan' => $renovationPlan->jsonSerialize(),
            'priceRanges' => $this->service->getPriceRanges($renovationPlan)
        ];
        return $this->middleware->generateSynthesis($json, self::SYNTHESIS_TEMPLATE, self::SYNTHESIS_RESULT_FILENAME, ['Accept' => 'application/pdf']);
    }

    public function sendReport($id)
    {
        $renovationPlan = $this->dao->getById($id);
        if ($this->allowedSendReport($renovationPlan)) {
            $attachment = [];
            $attachment[] = [
                "resultFileName" => self::SYNTHESIS_RESULT_FILENAME,
                'templateName' => self::SYNTHESIS_TEMPLATE,
                'data' => [
                    'renovationPlan' => $renovationPlan->jsonSerialize(),
                    'priceRanges' => $this->service->getPriceRanges($renovationPlan)
                ]
            ];

            $this->mailHelper->sendLeadReport($renovationPlan, $attachment);

            $renovationPlan->setSendDate(new \DateTimeImmutable());
            $this->dao->save($renovationPlan);

            return new JsonResponse(['message' => "OK"]); // TODO
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access renovationPlan($id)");
        }
    }

    public function vpHydrator(): Hydrator
    {
        return FluidHydrator::new()
            ->field('selected')->bool()->required();
    }

    public function hydrator(?RenovationPlan $plan = null): Hydrator
    {
        return FluidHydrator::new()
            ->field('passport')->parser(new TDBMRetriever($this->tdbmService, 'passports'))->required()
            ->validator(new FilledPassportValidator('Le passeport doit être complété pour créer le plan de rénovation'))
            ->validator(new CacheResult($passportPointer = new Pointer))
            ->field('status')->string()->enum(RenovationPlanStatus::Enum)->default('DRAFT')
            ->field('budgetComment')->string()
            ->field('currentConsumption')->float()
            ->field('plannedConsumption')->float()
            ->field('economy')->float()
            ->field('combinatoryConsumption')->float()
            ->field('combinatoryEconomy')->float()
            ->field('log')->string()
            ->field('improvement')->subobject(RenovationPlanImprovement::class)
            ->begin()
            ->field('combinatory')->parser(new TDBMRetriever($this->tdbmService, 'combinatories'))->required()
            ->validator(new CallbackValidator(function (?Combinatory $value) use ($passportPointer, $plan) {
                $passport = $passportPointer->value;
                if (!$passport) {
                    $passport = $plan ? $plan->getPassport() : null;
                }
                if ($value !== null && $passport !== null && !$value->isCompatible($passport))
                    throw new ValidationException('Combinatoire incompatible');
            }))
            ->field('wallsComment')->string()
            ->field('wallsSelected')->bool()->default(true)->required()
            ->field('wallsDefinedPriceMin')->int()->validator(new CacheResult($wallsDefinedPriceMinPointer = new Pointer()))
            ->field('wallsDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($wallsDefinedPriceMinPointer, "au prix minimum saisi pour les murs"))
            ->field('wallsProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('wallsForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('wallsDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('floorComment')->string()
            ->field('floorSelected')->bool()->default(true)->required()
            ->field('floorDefinedPriceMin')->int()->validator(new CacheResult($floorDefinedPriceMinPointer = new Pointer()))
            ->field('floorDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($floorDefinedPriceMinPointer, "au prix minimum saisi pour le plancher"))
            ->field('floorProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('floorForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('floorDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('roofComment')->string()
            ->field('roofSelected')->bool()->default(true)->required()
            ->field('roofDefinedPriceMin')->int()->validator(new CacheResult($roofDefinedPriceMinPointer = new Pointer()))
            ->field('roofDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($roofDefinedPriceMinPointer, "au prix minimum saisi pour la toiture"))
            ->field('roofProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('roofForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('roofDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('ventsComment')->string()
            ->field('ventsSelected')->bool()->default(true)->required()
            ->field('ventsDefinedPriceMin')->int()->validator(new CacheResult($ventsDefinedPriceMinPointer = new Pointer()))
            ->field('ventsDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($ventsDefinedPriceMinPointer, "au prix minimum saisi pour les ouvrants"))
            ->field('ventsProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('ventsForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('ventsDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('ventilationComment')->string()
            ->field('ventilationSelected')->bool()->default(true)->required()
            ->field('ventilationDefinedPriceMin')->int()->validator(new CacheResult($ventilationDefinedPriceMinPointer = new Pointer()))
            ->field('ventilationDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($ventilationDefinedPriceMinPointer, "au prix minimum saisi pour la ventilation"))
            ->field('ventilationProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('ventilationForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('ventilationDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('airtightnessComment')->string()
            ->field('airtightnessDefinedPriceMin')->int()->validator(new CacheResult($airtightnessDefinedPriceMinPointer = new Pointer()))
            ->field('airtightnessDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($airtightnessDefinedPriceMinPointer, "au prix minimum saisi pour l'étanchéité"))
            ->field('airtightnessSelected')->bool()->default(true)->required()
            ->field('airtightnessProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('airtightnessForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('airtightnessDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('heatingComment')->string()
            ->field('heatingSelected')->bool()->default(true)->required()
            ->field('heatingDefinedPriceMin')->int()->validator(new CacheResult($heatingDefinedPriceMinPointer = new Pointer()))
            ->field('heatingDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($heatingDefinedPriceMinPointer, "au prix minimum saisi pour le chauffage"))
            ->field('heatingProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('heatingForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('heatingDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('thermostatComment')->string()
            ->field('thermostatSelected')->bool()->default(true)->required()
            ->field('thermostatDefinedPriceMin')->int()->validator(new CacheResult($thermostatDefinedPriceMinPointer = new Pointer()))
            ->field('thermostatDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($thermostatDefinedPriceMinPointer, "au prix minimum saisi pour la gestion dynamique du chauffage"))
            ->field('thermostatProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('thermostatForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('thermostatDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->field('dhwComment')->string()
            ->field('dhwSelected')->bool()->default(true)->required()
            ->field('dhwDefinedPriceMin')->int()->validator(new CacheResult($dhwDefinedPriceMinPointer = new Pointer()))
            ->field('dhwDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($dhwDefinedPriceMinPointer, "au prix minimum saisi pour les ECS"))
            ->field('dhwProposedInterventionYear')->int()->validator(new CacheResult($proposedInterventionYearPointer = new Pointer()))
            // ->field('dhwForceInterventionYear')->bool()->default(false)->validator(new CacheResult($forceInterventionYearPointer = new Pointer()))
            // ->field('dhwDefinedInterventionYear')->parser(new DefinedInterventionYearParser($forceInterventionYearPointer, $proposedInterventionYearPointer))->validator($this->validateInterventionYearValidator($proposedInterventionYearPointer, $forceInterventionYearPointer))
            ->end()->required()
            ->field('tdf')->subobject(RenovationPlanTdf::class)
            ->begin()
            ->field('electricalWiringSelected')->bool()->default(false)->required()
            ->field('electricalWiringDefinedPriceMin')->int()->validator(new CacheResult($electricalWiringDefinedPriceMinPointer = new Pointer()))
            ->field('electricalWiringDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($electricalWiringDefinedPriceMinPointer, "au prix minimum saisi pour l'installation électrique"))
            ->field('gasSystemSelected')->bool()->default(false)->required()
            ->field('gasSystemDefinedPriceMin')->int()->validator(new CacheResult($gasSystemDefinedPriceMinPointer = new Pointer()))
            ->field('gasSystemDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($gasSystemDefinedPriceMinPointer, "au prix minimum saisi pour le gaz"))
            ->field('plumbingSelected')->bool()->default(false)->required()
            ->field('plumbingDefinedPriceMin')->int()->validator(new CacheResult($plumbingDefinedPriceMinPointer = new Pointer()))
            ->field('plumbingDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($plumbingDefinedPriceMinPointer, "au prix minimum saisi pour la plomberie"))
            ->field('leadSelected')->bool()->default(false)->required()
            ->field('leadDefinedPriceMin')->int()->validator(new CacheResult($leadDefinedPriceMinPointer = new Pointer()))
            ->field('leadDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($leadDefinedPriceMinPointer, "au prix minimum saisi pour le traitement plomb"))
            ->field('asbestosSelected')->bool()->default(false)->required()
            ->field('asbestosDefinedPriceMin')->int()->validator(new CacheResult($asbestosDefinedPriceMinPointer = new Pointer()))
            ->field('asbestosDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($asbestosDefinedPriceMinPointer, "au prix minimum saisi pour le traitement amiante"))
            ->field('termitesSelected')->bool()->default(false)->required()
            ->field('termitesDefinedPriceMin')->int()->validator(new CacheResult($termitesDefinedPriceMinPointer = new Pointer()))
            ->field('termitesDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($termitesDefinedPriceMinPointer, "au prix minimum saisi pour le traitement termites"))
            ->field('epcsSelected')->bool()->default(false)->required()
            ->field('epcsDefinedPriceMin')->int()->validator(new CacheResult($epcsDefinedPriceMinPointer = new Pointer()))
            ->field('epcsDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($epcsDefinedPriceMinPointer, "au prix minimum saisi pour le DPE"))
            ->field('septicTankSelected')->bool()->default(false)->required()
            ->field('septicTankDefinedPriceMin')->int()->validator(new CacheResult($septicTankDefinedPriceMinPointer = new Pointer()))
            ->field('septicTankDefinedPriceMax')->int()->validator(new IntegerHigherThanValidator($septicTankDefinedPriceMinPointer, "au prix minimum saisi pour la fosse septique"))
            ->field('comment')->string()
            ->end()->default(new RenovationPlanTdf())->required();
        // ->field("thermostatData")->subobject(RenovationPlanVigilancePoint::class)
        // ->hydrator(FluidHydrator::new()
        //     ->field('selected')->bool())->array();
    }

    private function validateInterventionYearValidator(Pointer $proposedInterventionYear, Pointer $forceInterventionYear): CallbackValidator
    {
        return new CallbackValidator(function ($value, $object) use ($proposedInterventionYear, $forceInterventionYear) {
            if ($forceInterventionYear->value === true) {
                if ($value === null) {
                    throw new ValidationException('This field is required');
                }
                if ($value < date("Y") || $value > $proposedInterventionYear->value) {
                    throw new ValidationException('Invalid value');
                }
            }
        });
    }

    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function allowedAccess(RenovationPlan $renovationPlan): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR)) {
            if ($renovationPlan->getPassport()->getAuditor() === $loggedUser) return true;
        }
        if ($loggedUser->hasRole(Role::USER)) {
            if ($renovationPlan->getPassport()->getOwner() === $loggedUser) return true;
        }
        if ($loggedUser->hasRole(Role::ADMIN_TERRITORY) || $loggedUser->hasRole(Role::USER_TERRITORY)) {
            foreach ($loggedUser->getTerritories() as $t) {
                $territory_addresses = array_map(function ($a) {
                    return $a->getId();
                }, $t->getAddresses());
                if (in_array($renovationPlan->getPAssport()->getAddress()->getId(), $territory_addresses)) {
                    return true;
                }
            }
        }

        return false;
    }

    private function allowedUpdate(RenovationPlan $renovationPlan): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR)) {
            if ($renovationPlan->getPassport()->getAuditor() === $loggedUser) return true;
        }

        return false;
    }

    private function allowedSendReport($renovationPlan): bool
    {
        $loggedUser = $this->getLogged();

        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR) && $renovationPlan->getPassport()->getAuditor() === $loggedUser) return true;

        return false;
    }
}
