<?php

namespace Agilap\Controller\APIController;

use Agilap\Controller\APIController\CleanupParametersTrait;
use Agilap\Enum\Role;
use Agilap\Exception\APIException;
use Agilap\Hydration\Hydrator\AddressHydrator;
use Agilap\Hydration\Parser\TDBMRetriever;
use Agilap\Model\Bean\Address;
use Agilap\Model\Bean\Lead;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\AddressDao;
use Agilap\Model\Dao\LeadDao;
use Agilap\Model\Dao\UserDao;
use Agilap\Service\MailHelper;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

class LeadApiController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;
    /** @var LeadDao */
    private $dao;
    /** @var AddressDao */
    private $addressDao;
    /** @var UserDao */
    private $userDao;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var MailHelper */
    private $mailHelper;


    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage, MailHelper $mailHelper)
    {
        parent::__construct('lead', 10, true);
        $this->tdbmService = $tdbmService;
        $this->dao = new LeadDao($tdbmService);
        $this->addressDao = new AddressDao($tdbmService);
        $this->userDao = new UserDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
        $this->mailHelper = $mailHelper;
    }

    use CleanupParametersTrait;

    /**
     * @OA\Get(
     *     path="/leads",
     *     tags={"Leads", "@GET"},
     *     description="Get a list of all leads accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         name="distance",
     *         in="query",
     *         description="max distance of lead from the active user",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             format="int32"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="auditor",
     *         in="query",
     *         description="returns only the leads assigne to auditor with id `auditor`",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of leads returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="leads",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Lead")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        return $this->dao->superSearch($this->getCleanParameters($this->getLogged(), $params));
    }

    /**
     * @OA\Get(
     *     path="/leads/{id}",
     *     tags={"Leads", "@GET"},
     *     description="Get the lead with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of lead to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the lead with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="lead",
     *                  ref="#/components/schemas/Lead"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        $lead = $this->dao->getById($id);
        if ($this->allowedAccess($lead)) {
            return $lead;
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access lead($id)");
        }
    }

    /**
     * @OA\Post(
     *     path="/leads",
     *     tags={"Leads", "@POST"},
     *     description="Create a lead",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="Lead to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/NewLead")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Lead created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="lead",
     *                  ref="#/components/schemas/Lead"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        $hydrator = $this->hydrator();
        /** @var Lead $lead */
        $lead = $hydrator->hydrateNewObject($data, Lead::class);
        $this->addressDao->fillAddress($lead->getAddress());
        $lead->setCreationDate(new \DateTimeImmutable());
        /** @var User $user */
        $user = $this->getLogged();
        if ($lead->getOwner() === null) {
            if ($user->hasRole(Role::USER)) {
                $lead->setOwner($user);
            } else {
                throw new AccessDeniedHttpException('You are not allowed to create a lead');
            }
        }
        if ($lead->getAuditor() === null) {
            if (
                $user->hasRole(Role::AUDITOR)
                && (!$user->hasRole(Role::ADMIN) || !array_key_exists('auditor', $data))
            ) {
                $lead->setAuditor($user);
            }
        }
        $this->dao->save($lead);

        if ($lead->getAuditor() === null) {
            $auditors = $this->userDao->getWithinDistanceFromLead($lead);
            $this->mailHelper->sendNewLead($lead, $auditors);
        }

        $this->mailHelper->sendConfirmationLeadCreated($lead);

        return $lead;
    }

    /**
     * @OA\Patch(
     *     path="/leads/{id}",
     *     tags={"Leads", "@PATCH"},
     *     description="Update a lead",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of lead to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="Lead to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Lead")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated lead",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="lead",
     *                  ref="#/components/schemas/Lead"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($lead, array $data)
    {
        if (!$this->allowedUpdate($lead)) {
            throw new AccessDeniedHttpException("You are not allowed to update this lead");
        }
        $hydrator = $this->hydrator();
        $hydrator->hydrateObject($data, $lead);
        $this->addressDao->fillAddress($lead->getAddress());
        $this->dao->save($lead);
    }

    /**
     * @OA\Delete(
     *     path="/leads/{id}",
     *     tags={"Leads", "@DELETE"},
     *     description="Delete lead with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of lead to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that lead has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedLead"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($lead): void
    {
        if (count($lead->getPassports()) > 0) {
            $id = $lead->getId();
            throw new APIException([
                'message' => "lead($id) could not be deleted because passport have already been created"
            ], Response::HTTP_LOCKED);
        }
        $this->dao->delete($lead);
    }

    public function getDashboardData(Request $request)
    {
        if (!$this->getLogged()->canAccessDashboard()) {
            throw new AccessDeniedHttpException("You are not allowed to access dashboard data");
        }

        $leads = $this->find($request->query->all())->toArray();

        $dashboardData = array_map(function ($lead) {
            return $lead->dashboardDataJson();
        }, $leads);

        return  new JsonResponse($dashboardData);
    }

    private function hydrator(): Hydrator
    {
        $hydrator = FluidHydrator::new()
            ->field('address')->subobject(Address::class)->hydrator(new AddressHydrator())->required()
            ->field('leadOrigin')->string()->required()
            ->field('linkToLead')->string()->required('A link to display the lead must be provided')->regex('(\{leadId\})', 'Url should contain injection `{leadId}`')
            ->field('legals1')->bool()->required('You must accept the terms of use to continue')->enum([true], 'You must accept the terms of use to continue')
            ->field('legals2')->bool()->required('You must certify that the information given is correct')->enum([true], 'You must certify that the information given is correct')
            ->field('comments')->string();
        /** @var User $user */
        $user = $this->getLogged();
        if ($user->hasRole(Role::AUDITOR) || $user->hasRole(Role::ADMIN)) {
            $ownerField = $hydrator->field('owner')->parser(new TDBMRetriever($this->tdbmService, 'users'));
            if (!$user->hasRole(Role::USER)) {
                $ownerField->required();
            }
        }
        if ($user->hasRole(Role::ADMIN)) {
            $auditorField = $hydrator->field('auditor')->parser(new TDBMRetriever($this->tdbmService, 'users'));
        }
        return $hydrator;
    }

    private function getLogged(): ?User
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            return null;
        }
        return $token->getUser();
    }

    private function allowedAccess(Lead $lead): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR)) {
            if ($lead->getAuditor() === null) return true;
            if ($lead->getAuditor() === $loggedUser) return true;
        }
        if ($loggedUser->hasRole(Role::USER)) {
            if ($lead->getOwner() === $loggedUser) return true;
        }
        if ($loggedUser->hasRole(Role::ADMIN_TERRITORY) || $loggedUser->hasRole(Role::USER_TERRITORY)) {
            foreach ($loggedUser->getTerritories() as $t) {
                $territory_addresses = array_map(function ($a) {
                    return $a->getId();
                }, $t->getAddresses());
                if (in_array($lead->getAddress()->getId(), $territory_addresses)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function allowedUpdate(Lead $lead): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR)) {
            if ($lead->getAuditor() === null) return true;
            if ($lead->getAuditor() === $loggedUser) return true;
        }
        if ($loggedUser->hasRole(Role::USER)) {
            if ($lead->getOwner() === $loggedUser) return true;
        }
        return false;
    }
}
