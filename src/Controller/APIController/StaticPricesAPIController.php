<?php

namespace Agilap\Controller\APIController;

use Agilap\Hydration\Hydrator\PriceRangeHydrator;
use Agilap\Model\Bean\PriceRange;
use Agilap\Model\Dao\StaticPriceDao;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\NoBeanFoundException;
use TheCodingMachine\TDBM\TDBMService;

class StaticPricesAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;
    /** @var StaticPriceDao */
    private $dao;

    public function __construct(TDBMService $tdbmService)
    {
        parent::__construct(['staticPrice', 'staticPrices'], 100, false);
        $this->tdbmService = $tdbmService;
        $this->dao = new StaticPriceDao($tdbmService);
        $this->methods = [
            'get' => 'ADMIN',
            'find' => 'ADMIN',
            'update' => 'ADMIN'
        ];
    }

    public function find(array $params): Result
    {
        return $this->dao->findAll();
    }

    public function get($label): \JsonSerializable
    {
        $staticPrice = $this->dao->findByLabel($label)->first();
        if ($staticPrice) {
            return $staticPrice;
        } else {
            throw new NoBeanFoundException();
        }
    }

    public function update($item, array $data)
    {
        $hydrator = $this->hydrator();
        $hydrator->hydrateObject($data, $item);
        $this->dao->save($item);
    }

    private function hydrator(): Hydrator
    {
        return FluidHydrator::new()
            ->field('priceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())
        ;
    }
}