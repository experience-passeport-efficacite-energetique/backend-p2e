<?php

namespace Agilap\Controller\APIController;

use Agilap\Controller\AbstractController;
use Agilap\Routing\OptionsUrlMatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class OptionsAPIController extends AbstractController
{
    /** @var RouteCollection */
    private $routeCollection;
    /** @var RequestContext */
    private $requestContext;

    public function __construct(RouteCollection $routeCollection, RequestContext $requestContext)
    {
        $this->routeCollection = $routeCollection;
        $this->requestContext = $requestContext;
    }

    public function options(Request $request): Response
    {
        $urlMatcher = new OptionsUrlMatcher($this->routeCollection, $this->requestContext);

        $methods = $urlMatcher->getAllowedMethods($request->getPathInfo());
        if (empty($methods) || $methods === ['OPTIONS']) {
            throw new NotFoundHttpException('');
        }

        return new Response(null, Response::HTTP_NO_CONTENT, [
            'Allow' => implode(', ', $methods),
            'Access-Control-Allow-Methods' => implode(', ', $methods),
            'Access-Control-Allow-Headers' => 'Content-Type, Authorization',
            'Content-Type' => 'application/json'
        ]);
    }
}
