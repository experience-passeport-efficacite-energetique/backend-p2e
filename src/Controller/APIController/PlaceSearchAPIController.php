<?php
namespace Agilap\Controller\APIController;

use Agilap\Enum\Role;
use Agilap\Exception\APIException;
use Agilap\Exception\DataNotFoundException;
use Agilap\Model\Bean\Department;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\CityDao;
use Agilap\Model\Dao\DepartmentDao;
use Agilap\Model\Dao\RegionDao;
use Porpaginas\Result;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\TDBM\TDBMException;
use TheCodingMachine\TDBM\TDBMService;

class PlaceSearchAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;

    /** @var CityDao */
    private $cityDao;

    /** @var DepartmentDao */
    private $departmentDao;

    /** @var RegionDao */
    private $regionDao;

    /** @var TokenStorage */
    private $tokenStorage;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage)
    {
        parent::__construct('place');
        $this->tdbmService = $tdbmService;
        $this->cityDao = new CityDao($tdbmService);
        $this->departmentDao = new DepartmentDao($tdbmService);
        $this->regionDao = new RegionDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
    }

    public function get($id) : \JsonSerializable
    {
        throw new AccessDeniedHttpException("You are not allowed to fetch by ID individual cities, departments or regions");
    }

    /**
     * @OA\Get(
     *     path="/places",
     *     tags={"Places","Territories", "@GET"},
     *     description="Find places (cities, departments, region) using a string as search parameter",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         name="search",
     *         in="query",
     *         description="search string. Can search for cities on name, insee code and zipcode. Region and department on names only. Search string must be at least 2 characters long.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="territory",
     *         in="query",
     *         description="limit results to cities included in territory with id passed as `territory` parameter",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of places found returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="regions",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Lead")
     *              ),
     *              @OA\Property(
     *                  property="departments",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Lead")
     *              ),
     *              @OA\Property(
     *                  property="cities",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/City")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function findFromString(Request $request) : JsonResponse
    {
        if ($this->getLogged() != null) {
            $params = $request->query->all();
            if (!isset($params['search'])) {
                throw new APIException(['message' => "Missing parameter: 'search'"], Response::HTTP_BAD_REQUEST);
            }
            $search = $params['search'];
            if (strlen($search) < 2) {
                throw new APIException(['message' => "The search parameter needs to be a least 2 characters long"], Response::HTTP_BAD_REQUEST);
            }
            if (isset($params['territory'])) {
                $cities = $this->cityDao->findByNameOrInseeOrZipInTerritory($params['territory'], $search);
            } else {
                $cities = $this->cityDao->findByNameOrInseeOrZip($search);
            }
            $departments = $this->departmentDao->findByNameOrCode($search);
            $regions = $this->regionDao->findByNameContains($search);
            return new JsonResponse(['regions' => $regions, 'departments' => $departments, 'cities' => $cities], Response::HTTP_OK);
        } else {
            throw new AccessDeniedHttpException("You are not allowed to search for places");
        }
    }

    public function find(array $params) : Result
    {
        throw new AccessDeniedHttpException("You are not allowed make a paginated searches on cities, departments or regions");
    }

    public function create(array $data) : \JsonSerializable
    {
        throw new AccessDeniedHttpException("You are not allowed to create cities, departments or regions");
    }

    public function update($place, array $data)
    {
        throw new AccessDeniedHttpException("You are not allowed to update cities, departments or regions");
    }

    public function delete($place) : void
    {
        throw new AccessDeniedHttpException("You are not allowed to delete cities, departments or regions");
    }

    private function getLogged() : ? User
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}