<?php
namespace Agilap\Controller\APIController;

use Agilap\Controller\AbstractController;
use Agilap\Enum\Role;
use Agilap\Exception\APIException;
use Agilap\Hydration\Hydrator\AddressHydrator;
use Agilap\Hydration\Parser\TDBMRetriever;
use Agilap\Hydration\Validator\UnassignedLeadValidator;
use Agilap\Model\Bean\Address;
use Agilap\Model\Bean\User;
use Agilap\Model\Custom\Assignment;
use Agilap\Model\Dao\LeadDao;
use Agilap\Model\Dao\UserDao;
use Agilap\Service\MailHelper;
use MetaHydrator\Exception\HydratingException;
use Mouf\Hydrator\Hydrator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\NoBeanFoundException;
use TheCodingMachine\TDBM\TDBMService;

class ProfileAPIController extends AbstractController
{
    /** @var TDBMService */
    private $tdbmService;
    /** @var UserDao */
    private $dao;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var MailHelper */
    private $mailHelper;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage,  MailHelper $mailHelper)
    {
        $this->tdbmService = $tdbmService;
        $this->dao = new UserDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
        $this->mailHelper = $mailHelper;
    }

    /**
     * @OA\Get(
     *     path="/my/profile",
     *     tags={"Profile", "@GET"},
     *     description="Get the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="Current User",
     *          @OA\JsonContent(ref="#/components/schemas/User"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     * )
     */
    public function get(Request $request) : JsonResponse
    {
        $token = $this->tokenStorage->getToken();
        $user = $token->getUser();
        return new JsonResponse($user);
    }

    /**
     * @OA\Patch(
     *     path="/my/profile",
     *     tags={"Profile", "@PATCH"},
     *     description="Update the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="User to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Updated User",
     *          @OA\JsonContent(ref="#/components/schemas/User"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update(Request $request) : JsonResponse
    {
        $token = $this->tokenStorage->getToken();
        $user = $token->getUser();

        $data = $this->extractRequest($request);

        $hydrator = $this->profileHydrator();
        try {
            $hydrator->hydrateObject($data, $user);
        } catch (HydratingException $exception) {
            return new JsonResponse([
                'message' => 'Could not update profile',
                'errors' => $exception->getErrorsMap()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $this->dao->save($user);
        return new JsonResponse($user);
    }

    /**
     * @OA\Post(
     *     path="/my/assignments",
     *     tags={"Profile","Leads","Assignments", "@POST"},
     *     description="Assign a lead to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="Lead to assign",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/AddAssignment")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Assigned Lead",
     *          @OA\JsonContent(ref="#/components/schemas/Assignment"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function addAssignment(Request $request) : JsonResponse
    {
        $token = $this->tokenStorage->getToken();
        /** @var User $user */
        $user = $token->getUser();

        if ($user === null || !$user->hasRole(Role::AUDITOR)) {
            throw new AccessDeniedHttpException('You are not allowed to manage assignments');
        }

        $data = $this->extractRequest($request);
        $hydrator = $this->assignmentHydrator();

        /** @var Assignment $assignment */
        $assignment = $hydrator->hydrateNewObject($data, Assignment::class);

        $lead = $assignment->getLead();
        $lead->setAuditor($user);
        $leadDao = new LeadDao($this->tdbmService);
        $leadDao->save($lead);

        $this->mailHelper->sendLeadAssign($lead);

        return new JsonResponse(['lead' => $lead], Response::HTTP_CREATED);
    }

    /**
     * @OA\Get(
     *     path="/my/assignments",
     *     tags={"Profile","Leads", "Assignments", "@GET"},
     *     description="Get a list of all leads assigned to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="Get assigned leads",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="leads",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Lead")
     *              )
     *          ),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     * )
     */
    public function getAssignments(Request $request) : JsonResponse
    {
        $token = $this->tokenStorage->getToken();
        /** @var User $user */
        $user = $token->getUser();

        if ($user === null || !$user->hasRole(Role::AUDITOR)) {
            throw new AccessDeniedHttpException('You are not allowed to manage assignments');
        }

        $json = [];
        foreach ($user->getLeadsByAuditorId() as $lead) {
            $json[] = $lead->jsonSerialize();
        }

        return new JsonResponse([
            'leads' => $user->getLeadsByAuditorId()
        ], Response::HTTP_OK);
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     * @throws APIException
     * @OA\Delete(
     *     path="/my/assignments/{id}",
     *     tags={"Profile","Leads", "Assignments", "@DELETE"},
     *     description="Remove lead with id `id` from user's assigned leads",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of lead to unassign",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that lead has been unassigned",
     *          @OA\JsonContent(ref="#/components/schemas/UnassignedLead"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function deleteAssignment($id, Request $request) : JsonResponse
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $dao = new LeadDao($this->tdbmService);
        try {
            $lead = $dao->getById($id);
            if ($lead->getAuditor() !== $user) {
                throw new NoBeanFoundException();
            }
        } catch (NoBeanFoundException $e) {
            throw new APIException(['message' => "assignment($id) could not be found"], Response::HTTP_NOT_FOUND);
        }

        $dao->save($lead);
        return new JsonResponse(['message' => "lead($id) has been unassigned"]);
    }

    /**
     * @OA\Get(
     *     path="/my/leads",
     *     tags={"Profile","Leads", "@GET"},
     *     description="Get a list of all leads belonging to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="List of owned leads returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="leads",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Lead")
     *              )
     *          ),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function getLeads(Request $request) : JsonResponse
    {
        $token = $this->tokenStorage->getToken();
        /** @var User $user */
        $user = $token->getUser();
        if ($user === null || !$user->hasRole(Role::USER)) {
            throw new AccessDeniedHttpException('You cannot make requests');
        }

        return new JsonResponse([
            'leads' => $user->getLeadsByOwnerId()
        ], Response::HTTP_OK);
    }

    public function getMaxFlyingDistances(Request $request) : JsonResponse
    {
        return new JsonResponse([
            'min' => MIN_FLYING_DISTANCE,
            'max' => MAX_FLYING_DISTANCE
        ], Response::HTTP_OK);
    }

    public function profileHydrator() : Hydrator
    {
        return FluidHydrator::new()
            ->field('company')->string()->required()
            ->field('firstname')->string()->required()
            ->field('lastname')->string()->required()
            ->field('maxFlyingDistance')->int()
            ->field('address')->subobject(Address::class)->hydrator(new AddressHydrator());
    }

    public function assignmentHydrator() : Hydrator
    {
        return FluidHydrator::new()
            ->field('lead')->parser(new TDBMRetriever($this->tdbmService, 'leads'))
            ->required()->validator(new UnassignedLeadValidator());
    }
}
