<?php

namespace Agilap\Controller\APIController;

use Agilap\Controller\APIController\AbstractRESTfulAPIController;
use Agilap\Controller\APIController\CleanupParametersTrait;
use Agilap\Exception\APIException;
use Agilap\Exception\DataNotFoundException;
use Agilap\Model\Bean\User;
use Agilap\Model\Bean\VigilancePoint;
use Agilap\Model\Bean\VigilancePointCondition;
use Agilap\Model\Dao\RenovationPlanDao;
use Agilap\Model\Dao\VigilancePointConditionDao;
use Agilap\Model\Dao\VigilancePointDao;
use Porpaginas\Result;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMException;
use TheCodingMachine\TDBM\TDBMService;

class VigilancePointAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;

    /** @var VigilancePointDao */
    private $dao;
    /** @var VigilancePointConditionDao */
    private $conditionDao;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var RenovationPlanDao */
    private $rpDao;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage)
    {
        parent::__construct(['vigilancePoint', 'vigilancePoints']);
        $this->tdbmService = $tdbmService;
        $this->dao = new VigilancePointDao($tdbmService);
        $this->rpDao = new RenovationPlanDao($tdbmService);
        $this->conditionDao = new VigilancePointConditionDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
    }

    use CleanupParametersTrait;

    /**
     * @param $id
     * @return \JsonSerializable
     * @throws TDBMException
     *
     * @OA\Get(
     *     path="/vigilancePoints/{id}",
     *     tags={"VigilancePoints", "@GET"},
     *     description="Get the vigilancePoint with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of vigilancePoint to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the vigilancePoint with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="vigilancePoint",
     *                  ref="#/components/schemas/serializedVp"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        if (!$this->getLogged()->canSeeVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to access vigilance point($id)");
        }
        $vigilancePoint = $this->dao->getById($id);
        return $vigilancePoint;
    }

    /**
     * @OA\Get(
     *     path="/vigilancePoints",
     *     tags={"VigilancePoints", "@GET"},
     *     description="Get a list of all vigilancePoints accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="List of vigilancePoints returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="vigilancePoints",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/serializedVp")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        if (!$this->getLogged()->canSeeVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to access vigilance points");
        }
        return $this->dao->findAll();
    }

    /**
     * @OA\Post(
     *     path="/vigilancePoints",
     *     tags={"VigilancePoints", "@POST"},
     *     description="Create a vigilancePoint",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="vigilancePoint to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/createVp")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="vigilancePoint created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="vigilancePoint",
     *                  ref="#/components/schemas/serializedVp"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        if (!$this->getLogged()->canManageVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to create a vigilance point");
        }
        $this->rpDao->vpUpToDateToFalse();
        $hydrator = $this->hydrator();
        $vp = $hydrator->hydrateNewObject($data, VigilancePoint::class);
        $vp->setCreationDate(new \DateTimeImmutable());
        $this->dao->save($vp);
        return $vp;
    }

    /**
     * @OA\Patch(
     *     path="/vigilancePoints/{id}",
     *     tags={"VigilancePoints", "@PATCH"},
     *     description="Update a vigilancePoint",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of vigilancePoint to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="vigilancePoint to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/createVp")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated vigilancePoint",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="vigilancePoint",
     *                  ref="#/components/schemas/serializedVp"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($vp, array $data)
    {
        if (!$this->getLogged()->canManageVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to update a vigilance point");
        }

        $this->rpDao->vpUpToDateToFalse();
        $hydrator = $this->hydrator($vp);
        $hydrator->hydrateObject($data, $vp);
        $vp->setModificationDate(new \DateTimeImmutable());

        $this->dao->save($vp);

        return $vp;
    }

    /**
     * @OA\Delete(
     *     path="/vigilancePoints/{id}",
     *     tags={"VigilancePoints", "@DELETE"},
     *     description="Delete vigilancePoint with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of vigilancePoint to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that vigilancePoint has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedVigilancePoint"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($item): void
    {
        if (!$this->getLogged()->canManageVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to delete a vigilance point");
        }
        $this->rpDao->vpUpToDateToFalse();
        $this->dao->delete($item);
    }

    /**
     * @OA\Post(
     *     path="/vigilancePoints/{id}/addCondition",
     *     tags={"VigilancePoints", "@POST"},
     *     description="Add a condition to a vigilancePoint",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of vigilancePoint to which the condition will be added",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="condition to add to the vp",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/createCondition")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="vp with created condition",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="vigilancePoint",
     *                  ref="#/components/schemas/serializedVp"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function addCondition(Request $request, $vpId): JsonResponse
    {
        if (!$this->getLogged()->canManageVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to update a vigilance point");
        }

        $data = json_decode($request->getContent(), true) ?? [];
        //Exceptionaly, increase time limit for this method
        set_time_limit(180);

        try {
            $vp = $this->dao->getById($vpId);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Vigilance point ($vpId) not found");
        }
        $this->rpDao->vpUpToDateToFalse();
        $conditionHydrator = $this->conditionHydrator();
        /** @var VigilancePointCondition */
        $condition = $conditionHydrator->hydrateNewObject($data, VigilancePointCondition::class);
        $condition->setCreationDate(new \DateTimeImmutable());
        $condition->setVigilancePoint($vp);
        $this->conditionDao->save($condition);

        return new JsonResponse(['vigilancePoint' => $vp, "time" => ini_get('max_execution_time')], Response::HTTP_CREATED);
    }

    /**
     * @OA\Patch(
     *     path="/vigilancePoints/{id}/addCondition/{conditionId}",
     *     tags={"VigilancePoints", "@PATCH"},
     *     description="Update a condition on a vigilancePoint",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of vigilancePoint to which the condition will be added",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Parameter(
     *         description="ID of condition to update",
     *         in="path",
     *         name="conditionId",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="condition to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/createCondition")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="vp with updated condition",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="vigilancePoint",
     *                  ref="#/components/schemas/serializedVp"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function updateCondition(Request $request, $conditionId, $vpId): JsonResponse
    {
        if (!$this->getLogged()->canManageVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to update a vigilance point");
        }

        $data = json_decode($request->getContent(), true) ?? [];
        //Exceptionaly, increase time limit for this method
        set_time_limit(180);

        try {
            $vp = $this->dao->getById($vpId);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Vigilance point ($vpId) not found");
        }

        $conditionHydrator = $this->conditionHydrator();

        /** @var VigilancePointCondition not using direct conditionDao->getById() access to make
         * sure updated condition belongs to this VP
         */
        $condition = $vp->getVigilancePointConditionById($conditionId);
        if ($condition == null) {
            throw new DataNotFoundException("Vigilance point condition($conditionId) not found");
        }
        $this->rpDao->vpUpToDateToFalse();
        $condition = $conditionHydrator->hydrateObject($data, $condition);
        $condition->setModificationDate(new \DateTimeImmutable());
        $this->conditionDao->save($condition);

        return new JsonResponse(['vigilancePoint' => $vp, "time" => ini_get('max_execution_time')], Response::HTTP_OK);
    }

    /**
     * @OA\Delete(
     *     path="/vigilancePoints/{id}/addCondition/{conditionId}",
     *     tags={"VigilancePoints", "@DELETE"},
     *     description="delete a condition from a vigilancePoint",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of vigilancePoint from which the condition will be deleted",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Parameter(
     *         description="ID of condition to delete",
     *         in="path",
     *         name="conditionId",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that vigilancePoint has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedVPCondition"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function removeCondition($conditionId, $vpId): JsonResponse
    {
        if (!$this->getLogged()->canManageVigilancePoints()) {
            throw new AccessDeniedHttpException("You are not allowed to update a vigilance point");
        }

        try {
            $vp = $this->dao->getById($vpId);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Vigilance point ($vpId) not found");
        }

        /** @var VigilancePointCondition not using direct conditionDao->getById() access to make
         * sure updated condition belongs to this VP
         */
        $condition = $vp->getVigilancePointConditionById($conditionId);
        if ($condition == null) {
            throw new DataNotFoundException("Vigilance point condition($conditionId) not found");
        }
        $this->rpDao->vpUpToDateToFalse();
        $this->conditionDao->delete($condition);

        return new JsonResponse(['message' => "vigilancePointCondition($conditionId) has been deleted"]);
    }


    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function hydrator()
    {
        $hydrator =  FluidHydrator::new()
            ->field('priority')->int()->min(0)->required()
            ->field('complexity')->int()->min(1)->max(3)->required()
            ->field('title')->string()->maxLength(300)
            ->field('description')->string()->required()->maxLength(300);

        return $hydrator;
    }

    private function conditionHydrator()
    {
        $hydrator =  FluidHydrator::new()
            ->field('precedingLot')->string()
            ->field('precedingLotValueBefore')->string()
            ->field('precedingLotValueAfter')->string()
            ->field('followingLot')->string()
            ->field('followingLotValueBefore')->string()
            ->field('followingLotValueAfter')->string();

        return $hydrator;
    }
}
