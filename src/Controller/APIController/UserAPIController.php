<?php

namespace Agilap\Controller\APIController;

use Agilap\Enum\Role;
use Agilap\Exception\APIException;
use Agilap\Exception\DataNotFoundException;
use Agilap\Hydration\Hydrator\AddressHydrator;
use Agilap\Hydration\Validator\TDBMUniqueValidator;
use Agilap\Model\Bean\Address;
use Agilap\Model\Bean\User;
use Agilap\Model\Custom\EmptyObject;
use Agilap\Model\Dao\AddressDao;
use Agilap\Model\Dao\UserDao;
use Agilap\Service\MailHelper;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\NoBeanFoundException;
use TheCodingMachine\TDBM\TDBMService;

class UserAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;
    /** @var UserDao */
    private $dao;
    /** @var AddressDao */
    private $addressDao;
    /** @var TokenStorage */
    private $tokenStorage;

    private $mailHelper;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage, MailHelper $mailHelper)
    {
        parent::__construct('user');
        $this->tdbmService = $tdbmService;
        $this->dao = new UserDao($tdbmService);
        $this->addressDao = new AddressDao($tdbmService);
        $this->mailHelper = $mailHelper;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @OA\Get(
     *     path="/users/{id}",
     *     tags={"Users", "@GET"},
     *     description="Get the user with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of user to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the user with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="user",
     *                  ref="#/components/schemas/User"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        $user = $this->dao->getById($id);
        if ($this->allowedAccess($user)) {
            return $user;
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access user($id)");
        }
    }

    /**
     * @OA\Get(
     *     path="/users",
     *     tags={"Users", "@GET"},
     *     description="Get a list of all users accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="List of user returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="users",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN) && $loggedUser->hasRole(Role::ADMIN_TERRITORY)) {
            $params['role'] = [Role::USER, Role::USER_TERRITORY];
            return $this->dao->searchByTerritories($loggedUser->getTerritories(), $params);
        } else if (!$loggedUser->hasRole(Role::ADMIN) && $loggedUser->hasRole(Role::USER_TERRITORY)) {
            $params['role'] = Role::USER;
            return $this->dao->searchByTerritories($loggedUser->getTerritories(), $params);
        } else if (!$loggedUser->hasRole(Role::ADMIN) && $loggedUser->hasRole(Role::AUDITOR)) {
            $params['role'] = Role::USER;
        } else if (!$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException("You are not allowed to access users");
        }
        return $this->dao->search($params);
    }

    /**
     * @OA\Post(
     *     path="/users",
     *     tags={"Users", "@POST"},
     *     description="Create a user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="User to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CreateUser")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="User created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="user",
     *                  ref="#/components/schemas/User"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        if (
            !$this->isGranted(Role::ADMIN)
            && array_key_exists('roles', $data)
        ) {
            unset($data['roles']);
        }

        if (
            !$this->isGranted(Role::ADMIN)
            && $this->isGranted(Role::ADMIN_TERRITORY)
        ) {
            $data['roles'] = [Role::USER_TERRITORY];
        }

        $hydrator = $this->getHydrator();
        /** @var User $user */
        $user = $hydrator->hydrateNewObject($data, User::class);
        $user->setActive(false);
        $user->setActivationToken(sha1($user->getEmail() . time() . rand()));
        $this->addressDao->fillAddress($user->getAddress());

        $this->sendInvitationMail($user, $data['activationUrl']);

        $this->dao->save($user);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     *
     * @OA\Patch(
     *     path="/users/{id}",
     *     tags={"Users", "@PATCH"},
     *     description="Update a user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of user to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="User to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated user",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="user",
     *                  ref="#/components/schemas/User"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($user, array $data)
    {
        if (
            !$this->isGranted(Role::ADMIN)
            && array_key_exists('roles', $data)
        ) {
            unset($data['roles']);
        }
        if (
            !$this->isGranted(Role::ADMIN)
            && $this->isGranted(Role::ADMIN_TERRITORY)
        ) {
            $data['roles'] = [Role::USER_TERRITORY];
        }
        if (array_key_exists('email', $data)) {
            unset($data['email']);
        }
        $hydrator = $this->getHydrator();
        $hydrator->hydrateObject($data, $user);
        $this->addressDao->fillAddress($user->getAddress());
        $this->dao->save($user);
        return;
    }

    /**
     * @param User $user
     *
     * @OA\Delete(
     *     path="/users/{id}",
     *     tags={"Users", "@DELETE"},
     *     description="Delete user with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of user to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that user has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedUser")
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($user): void
    {
        $this->dao->delete($user);
        return;
    }

    /**
     * @OA\Post(
     *     path="/users/{id}/invite",
     *     tags={"Users", "@POST"},
     *     description="Send an invitation to a user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of user to invite",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="User to update",
     *         required=true,
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="user",
     *                  ref="#/components/schemas/User"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated user",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="activationUrl",
     *                  type="string",
     *                  description="url for account activation"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function invite(Request $request, $id): JsonResponse
    {
        try {
            /** @var User $user */
            $user = $this->get($id);
        } catch (NoBeanFoundException $exception) {
            throw new DataNotFoundException("$this->singularItem($id) not found");
        }

        if ($user->getActive()) {
            throw new APIException(['message' => "user($id) account has already been activated"], Response::HTTP_BAD_REQUEST);
        }

        $data = json_decode($request->getContent(), true) ?? [];
        FluidHydrator::new()
            ->field('activationUrl')->string()->required()->regex('(\{token\})', 'Url should contain injection `{token}`')
            ->hydrateNewObject($data, EmptyObject::class);
        $activationUrl = $data['activationUrl'];

        $this->sendInvitationMail($user, $activationUrl);

        return new JsonResponse(['message' => 'an email has been sent to ' . $user->getEmail()]);
    }

    private function sendInvitationMail(User $user, string $activationUrl)
    {
        $activationToken = $user->getActivationToken();
        $creator = $this->getLogged();
        $activationLink = str_replace('{token}', $activationToken, $activationUrl);

        $sendResult = false;
        if ($user->isHigherRoleUser()) {
            if ($creator->isHigherRoleAuditor()) {
                $sendResult = $this->mailHelper->sendInvitationFromAuditor($user, $activationLink, $creator);
            } elseif ($creator->isTerritoryBound()) {
                $sendResult = $this->mailHelper->sendInvitationFromUserTerritory($user, $activationLink, $creator);
            } else {
                $sendResult = $this->mailHelper->sendAccountCreated($user, $activationLink);
            }
        } else {
            $sendResult = $this->mailHelper->sendAccountCreated($user, $activationLink);
        }
        if (!$sendResult) {
            throw new APIException(['message' => 'Mail could not be sent'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function getHydrator(): Hydrator
    {
        $hydrator = FluidHydrator::new()
            ->field('firstname')->string()->required()
            ->field('lastname')->string()->required()
            ->field('email')->string()->email()->required()->validator(new TDBMUniqueValidator($this->tdbmService, 'users', 'email'))
            ->field('address')->subobject(Address::class)->hydrator(new AddressHydrator())
            ->field('company')->string()
            ->field('phone')->string()->phone()
            ->field('roles')->string()->enum(Role::Enum)->array()->default([Role::USER])->required()
            ->field('activationUrl')->string()->required()->regex('(\{token\})', 'Url should contain injection `{token}`');
        return $hydrator;
    }

    private function isGranted(string $role)
    {
        return ($loggedUser = $this->tokenStorage->getToken()->getUser()) !== null && $loggedUser->hasRole($role);
    }

    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function allowedAccess(User $user): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR)) {
            if ($user->hasRole(Role::USER)) return true;
        }
        if ($loggedUser->hasRole(Role::ADMIN_TERRITORY)) {
            if ($user->hasRole(Role::USER_TERRITORY)) return true;
        }
        if ($loggedUser === $user) return true;
        return false;
    }
}
