<?php

namespace Agilap\Controller\APIController;

use Agilap\Enum\AirtightnessType;
use Agilap\Enum\ClimateZone;
use Agilap\Enum\EnergyLevel;
use Agilap\Enum\InsulationType;
use Agilap\Enum\Role;
use Agilap\Hydration\Hydrator\PriceRangeHydrator;
use Agilap\Hydration\Parser\TDBMRetriever;
use Agilap\Hydration\Validator\AvailableEquipmentValidator;
use Agilap\Model\Bean\Combinatory;
use Agilap\Model\Bean\PriceRange;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\CombinatoryDao;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

class CombinatoryAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;
    /** @var CombinatoryDao */
    private $dao;
    /** @var TokenStorage */
    private $tokenStorage;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage)
    {
        parent::__construct('combinatory', 10, true);
        $this->tdbmService = $tdbmService;
        $this->dao = new CombinatoryDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @OA\Get(
     *     path="/combinatories",
     *     tags={"Combinatories", "@GET"},
     *     description="Get a list of all combinatories accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         name="ventilation_id",
     *         in="query",
     *         description="returns only the combinatories that have a specific ventilation",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Parameter(
     *         name="heating_id",
     *         in="query",
     *         description="returns only the combinatories that have a specific heating",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Parameter(
     *         name="zone",
     *         in="query",
     *         description="returns only the combinatories that have a specific zone",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/zoneEnum" )
     *     ),
     *     @OA\Parameter(
     *         name="insulation",
     *         in="query",
     *         description="returns only the combinatories that have a specific insulation",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/insulationEnum" )
     *     ),
     *     @OA\Parameter(
     *         name="airtightness",
     *         in="query",
     *         description="returns only the combinatories that have a specific airtightness",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/airtightnessEnum" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of combinatories returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="combinatories",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Combinatory")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::USER)) {
            throw new AccessDeniedHttpException('You are not allowed to access combinatories');
        }

        $zone = isset($params['zone']) ? $params['zone'] : null;
        $insulation = isset($params['insulation']) ? $params['insulation'] : null;
        $airtightness = isset($params['airtightness']) ? $params['airtightness'] : null;
        $ventilation = isset($params['ventilation']) ? $params['ventilation'] : null;
        $heating = isset($params['heating']) ? $params['heating'] : null;
        $dhw = isset($params['dhw']) ? $params['dhw'] : null;

        return $this->dao->findCompatible($zone, $insulation, $airtightness, $ventilation, $heating, $dhw);
    }

    /**
     * @OA\Get(
     *     path="/combinatories/{id}",
     *     tags={"Combinatories", "@GET"},
     *     description="Get the combinatory with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of combinatory to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the combinatory with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="combinatory",
     *                  ref="#/components/schemas/Combinatory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::USER)) {
            throw new AccessDeniedHttpException("You are not allowed to access combinatory ($id)");
        }

        return $this->dao->getById($id);
    }

    /**
     * @OA\Post(
     *     path="/combinatories",
     *     tags={"Combinatories", "@POST"},
     *     description="Create a combinatory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="Combinatory to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Combinatory")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Combinatory created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="combinatory",
     *                  ref="#/components/schemas/Combinatory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException("You are not allowed to create combinatories");
        }

        $hydrator = $this->hydrator();
        /** @var Combinatory $combinatory */
        $combinatory = $hydrator->hydrateNewObject($data, Combinatory::class);
        $combinatory->setProtected(false);
        $this->dao->save($combinatory);
        return $combinatory;
    }

    /**
     * @param Combinatory $combinatory
     * @param array $data
     *
     * @OA\Patch(
     *     path="/combinatories/{id}",
     *     tags={"Combinatories", "@PATCH"},
     *     description="Update a combinatory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of combinatory to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="Combinatory to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Combinatory")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated combinatory",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="combinatory",
     *                  ref="#/components/schemas/Combinatory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($combinatory, array $data)
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::ADMIN)) {
            $id = $combinatory->getId();
            throw new AccessDeniedHttpException("You are not allowed to access combinatory ($id)");
        }

        $hydrator = $this->hydrator(boolval($combinatory->getProtected()));
        $hydrator->hydrateObject($data, $combinatory);
        $this->dao->save($combinatory);
    }

    /**
     * @param Combinatory $combinatory
     *
     * @OA\Delete(
     *     path="/combinatories/{id}",
     *     tags={"Combinatories", "@DELETE"},
     *     description="Delete combinatory with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of combinatory to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that combinatory has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedCombinatory"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */

    public function delete($combinatory): void
    {
        if ($combinatory->getProtected()) {
            throw new AccessDeniedHttpException('This combinatory cannot be edited');
        }
        $this->dao->delete($combinatory);
    }

    private function hydrator(bool $protected = false): Hydrator
    {
        $hydrator = FluidHydrator::new();
        if (!$protected) {
            $hydrator
                ->field('number')->int()->required()
                ->field('code')->int()->required()->maxLength(45)
                ->field('level')->string()->required()->enum(EnergyLevel::Enum)
                ->field('zone')->string()->required()->enum(ClimateZone::Enum)
                ->field('insulation')->string()->required()->enum(InsulationType::PlannedEnum)
                ->field('airtightness')->string()->required()->enum(AirtightnessType::PlannedEnum)
                ->field('ventilation')->parser(new TDBMRetriever($this->tdbmService, 'ventilations'))->required()
                ->field('heating')->parser(new TDBMRetriever($this->tdbmService, 'heatings'))->required()->validator(new AvailableEquipmentValidator(true));
        }
        $hydrator
            ->field('wallsValue')->float()->required()
            ->field('wallsPriceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())->required()
            ->field('ventsValue')->float()->required()
            ->field('ventsPriceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())->required()
            ->field('floorValue')->float()->required()
            ->field('floorPriceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())->required()
            ->field('roofValue')->float()->required()
            ->field('roofPriceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())->required();

        return $hydrator;
    }

    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
