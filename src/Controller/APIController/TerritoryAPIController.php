<?php

namespace Agilap\Controller\APIController;

use Agilap\Enum\Role;
use Agilap\Exception\APIException;
use Agilap\Exception\DataNotFoundException;
use Agilap\Model\Bean\Department;
use Agilap\Model\Bean\Territory;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\AddressDao;
use Agilap\Model\Dao\CityDao;
use Agilap\Model\Dao\DepartmentDao;
use Agilap\Model\Dao\RegionDao;
use Agilap\Model\Dao\TerritoryDao;
use Agilap\Model\Dao\UserDao;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMException;
use TheCodingMachine\TDBM\TDBMService;

class TerritoryAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService */
    private $tdbmService;

    /** @var TerritoryDao */
    private $dao;

    /** @var UserDao */
    private $userDao;

    /** @var CityDao */
    private $cityDao;

    /** @var DepartmentDao */
    private $departmentDao;

    /** @var RegionDao */
    private $regionDao;

    /** @var AddressDao */
    private $addressDao;

    /** @var TokenStorage */
    private $tokenStorage;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage)
    {
        parent::__construct('territory');
        $this->tdbmService = $tdbmService;
        $this->dao = new TerritoryDao($tdbmService);
        $this->userDao = new UserDao($tdbmService);
        $this->cityDao = new CityDao($tdbmService);
        $this->departmentDao = new DepartmentDao($tdbmService);
        $this->regionDao = new RegionDao($tdbmService);
        $this->addressDao = new AddressDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param $id
     * @return \JsonSerializable
     * @throws TDBMException
     *
     * @OA\Get(
     *     path="/territories/{id}",
     *     tags={"Territories", "@GET"},
     *     description="Get the territory with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the territory with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="territory",
     *                  ref="#/components/schemas/Territory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     *
     */
    public function get($id): \JsonSerializable
    {
        $territory = $this->dao->getById($id);
        $territory->getUsers();
        if ($this->allowedAccess($territory)) {
            return $territory;
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access territory($id)");
        }
    }

    /**
     * @OA\Get(
     *     path="/territories",
     *     tags={"Territories", "@GET"},
     *     description="Get a list of all territories accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="List of territories returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="territories",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Territory")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        if ($this->isGranted(Role::ADMIN)) {
            return $this->dao->findAll();
        } elseif ($this->isGranted(Role::ADMIN_TERRITORY) || $this->isGranted(Role::USER_TERRITORY)) {
            return $this->dao->findByAssignee($this->getLogged());
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access territories");
        }
    }

    /**
     * @OA\Post(
     *     path="/territories",
     *     tags={"Territories", "@POST"},
     *     description="Create a territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="Territory to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/OnlyLabelTerritory")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Territory created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="territory",
     *                  ref="#/components/schemas/Territory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        if ($this->isGranted(Role::ADMIN)) {
            $hydrator = $this->hydrator();
            /* @var Territory $territory */
            $territory = $hydrator->hydrateNewObject($data, Territory::class);
            $this->dao->save($territory);
            return $territory;
        } else {
            throw new AccessDeniedHttpException("You are not allowed to create territories");
        }
    }

    /**
     * @OA\Patch(
     *     path="/territories/{id}",
     *     tags={"Territories", "@PATCH"},
     *     description="Update a territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="Territory to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Territory")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated territory",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="territory",
     *                  ref="#/components/schemas/Territory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($territory, array $data)
    {
        if ($this->allowedAccess($territory) && !$this->isGranted(Role::USER_TERRITORY)) {
            $hydrator = $this->hydrator();
            $hydrator->hydrateObject($data, $territory);
            $this->dao->save($territory);
        } else {
            throw new AccessDeniedHttpException("You are not allowed to update this territory");
        }
    }


    /**
     * @OA\Delete(
     *     path="/territories/{id}",
     *     tags={"Territories", "@DELETE"},
     *     description="Delete territory with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that territory has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedTerritory"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($territory): void
    {
        if ($this->isGranted(Role::ADMIN)) {
            $this->dao->delete($territory);
        } else {
            throw new AccessDeniedHttpException("You are not allowed to delete territories");
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws APIException
     * @throws DataNotFoundException
     *
     * @OA\Post(
     *     path="/territories/{id}/user-assignment",
     *     tags={"Territories", "@POST"},
     *     description="Assign user to territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory to which user is to be added",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="User to assign to territory",
     *         required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="user",
     *                  ref="#/components/schemas/OnlyIdUser"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="User assigned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="territory",
     *                  ref="#/components/schemas/Territory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function addUserAssignment(Request $request, $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true) ?? [];
        $loggedUser = $this->getLogged();

        if (!isset($data['user'])) {
            throw new APIException(['message' => "Missing parameter user"], Response::HTTP_BAD_REQUEST);
        }

        try {
            $territory = $this->dao->getById($id);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Territory ($id) not found");
        }

        try {
            $user = $this->userDao->getById($data['user']['id']);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("User (" . $data['user']['id'] . ") not found");
        }

        if (!$this->isGranted(Role::ADMIN)) {
            if (!$loggedUser->hasTerritory($territory)) {
                throw new AccessDeniedHttpException("You are not allowed to manage assignments on this territory");
            }
        } else {
            if (!$user->hasRole(Role::ADMIN_TERRITORY) && !$user->hasRole(Role::USER_TERRITORY)) {
                throw new APIException(['message' => "User (" . $user->getId() . ") could not be assigned to territories"], Response::HTTP_BAD_REQUEST);
            }
        }

        if ($user->hasRole(Role::USER_TERRITORY) && !empty($user->getTerritories()) && !$user->hasTerritory($territory)) {
            throw new APIException(['message' => "User (" . $user->getId() . ") is already assigned to a territory"], Response::HTTP_BAD_REQUEST);
        }
        if (!$territory->hasUser($user)) {
            $territory->addUser($user);
            $this->dao->save($territory);
        }

        return new JsonResponse(['territory' => $territory], Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     * @throws DataNotFoundException
     *
     * @OA\Get(
     *     path="/territories/{id}/user-assignment",
     *     tags={"Territories", "@GET"},
     *     description="Get users assigned to a territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory to which user are assigned",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of assigned users",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="users",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/MinimalGetUser")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function getUserAssignments(Request $request, $id): JsonResponse
    {
        $loggedUser = $this->getLogged();

        try {
            $territory = $this->dao->getById($id);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Territory ($id) not found");
        }

        if (!$this->isGranted(Role::ADMIN)) {
            if (!$loggedUser->hasTerritory($territory)) {
                throw new AccessDeniedHttpException("You are not allowed to manage assignments on this territory");
            }
        }

        $json = [];
        foreach ($territory->getUsers() as $user) {
            $json[] = $user->jsonSerialize(true);
        }

        return new JsonResponse([
            'users' => $json
        ], Response::HTTP_OK);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     * @throws APIException
     * @throws DataNotFoundException
     *
     * @OA\Delete(
     *     path="/territories/{id}/user-assignment",
     *     tags={"Territories", "@DELETE"},
     *     description="Unassign user from territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory from which the user is to be unassigned",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="User to assign to territory",
     *         required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="user",
     *                  ref="#/components/schemas/OnlyIdUser"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="User assigned",
     *          @OA\JsonContent(ref="#/components/schemas/UserUnassignedTerritory")
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function deleteUserAssignment($id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true) ?? [];
        $loggedUser = $this->getLogged();

        if (!isset($data['user'])) {
            throw new APIException(['message' => "Missing parameter user"], Response::HTTP_BAD_REQUEST);
        }

        try {
            $territory = $this->dao->getById($id);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Territory ($id) not found");
        }

        try {
            $user = $this->userDao->getById($data['user']['id']);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("User (" . $data['user']['id'] . ") not found");
        }

        if (!$this->isGranted(Role::ADMIN)) {
            if (!$loggedUser->hasTerritory($territory)) {
                throw new AccessDeniedHttpException("You are not allowed to manage assignments on this territory");
            }
        }

        $territory->removeUser($user);
        $this->dao->save($territory);

        return new JsonResponse(['message' => "User (" . $user->getId() . ") has been unassigned"]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws APIException
     * @throws DataNotFoundException
     *
     * @OA\Post(
     *     path="/territories/{id}/city-assignment",
     *     tags={"Territories", "@POST"},
     *     description="Assign city to territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory to which city is to be added",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="City to assign to territory",
     *         required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="city",
     *                  description="city to assign to territory",
     *                  ref="#/components/schemas/OnlyIdCity"
     *              ),
     *              @OA\Property(
     *                  property="department",
     *                  description="department to assign to territory",
     *                  ref="#/components/schemas/OnlyIdDepartment"
     *              ),
     *              @OA\Property(
     *                  property="region",
     *                  description="region to assign to territory",
     *                  ref="#/components/schemas/OnlyIdRegion"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="City assigned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="territory",
     *                  ref="#/components/schemas/Territory"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function addCityAssignment(Request $request, $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true) ?? [];
        //Exceptionaly, increase time limit for this method
        set_time_limit(180);

        try {
            $territory = $this->dao->getById($id);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Territory ($id) not found");
        }

        if (!isset($data['city']) && !isset($data['department']) && !isset($data['region'])) {
            throw new APIException(['message' => "Missing parameter city, department or region"], Response::HTTP_BAD_REQUEST);
        }

        if (isset($data['city'])) {
            try {
                $city = $this->cityDao->getById($data['city']['id']);
                if (!$territory->hasCity($city)) {
                    $territory->addCity($city);
                    $this->addressDao->addAll($city, $territory);
                    $this->dao->save($territory);
                }
            } catch (TDBMException $exception) {
                throw new DataNotFoundException("City (" . $data['city']['id'] . ") not found");
            }
        }

        if (isset($data['department'])) {
            try {
                $department = $this->departmentDao->getById($data['department']['id']);
                foreach ($department->getCities() as $city) {
                    if (!$territory->hasCity($city)) {
                        $territory->addCity($city);
                        $this->addressDao->addAll($city, $territory);
                    }
                }
                $this->dao->save($territory);
            } catch (TDBMException $exception) {
                throw new DataNotFoundException("Department (" . $data['department']['id'] . ") not found");
            }
        }

        if (isset($data['region'])) {
            try {
                $region = $this->regionDao->getById($data['region']['id']);
                foreach ($region->getDepartments() as $department) {
                    foreach ($department->getCities() as $city) {
                        if (!$territory->hasCity($city)) {
                            $territory->addCity($city);
                            $this->addressDao->addAll($city, $territory);
                        }
                    }
                }
                $this->dao->save($territory);
            } catch (TDBMException $exception) {
                throw new DataNotFoundException("Region (" . $data['region']['id'] . ") not found");
            }
        }

        return new JsonResponse(['territory' => $territory, "time" => ini_get('max_execution_time')], Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     * @throws DataNotFoundException
     *
     * @OA\Get(
     *     path="/territories/{id}/city-assignment",
     *     tags={"Territories", "@GET"},
     *     description="Get cities assigned to a territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory to which city are assigned",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of assigned cities",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="cities",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/City")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     *
     */
    public function getCityAssignments(Request $request, $id): JsonResponse
    {
        try {
            $territory = $this->dao->getById($id);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Territory ($id) not found");
        }

        if (!$this->isGranted(Role::ADMIN)) {
            if (!$this->getLogged()->hasTerritory($territory)) {
                throw new AccessDeniedHttpException("You are not allowed to manage cities on this territory");
            }
        }

        $json = [];
        foreach ($territory->getCities() as $city) {
            $json[] = $city->jsonSerialize(true);
        }

        return new JsonResponse([
            'cities' => $json,
            'time' => ini_get('max_execution_time')
        ], Response::HTTP_OK);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     * @throws APIException
     * @throws DataNotFoundException
     *
     * @OA\Delete(
     *     path="/territories/{id}/city-assignment",
     *     tags={"Territories", "@DELETE"},
     *     description="Unassign city from territory",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of territory from which the city is to be unassigned",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="City to assign to territory",
     *         required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="city",
     *                  ref="#/components/schemas/OnlyIdCity"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="City assigned",
     *          @OA\JsonContent(ref="#/components/schemas/CityUnassignedTerritory")
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function deleteCityAssignment($id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true) ?? [];
        //Exceptionaly, increase time limit for this method
        set_time_limit(180);

        if (!isset($data['city'])) {
            throw new APIException(['message' => "Missing parameter city"], Response::HTTP_BAD_REQUEST);
        }

        try {
            $territory = $this->dao->getById($id);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("Territory ($id) not found");
        }

        try {
            $city = $this->cityDao->getById($data['city']['id']);
        } catch (TDBMException $exception) {
            throw new DataNotFoundException("City (" . $data['city']['id'] . ") not found");
        }

        $territory->removeCity($city);
        $this->addressDao->removeAll($city, $territory);
        $this->dao->save($territory);

        return new JsonResponse(['message' => "City (" . $city->getId() . ") has been unassigned"]);
    }

    public function hydrator(): Hydrator
    {
        return FluidHydrator::new()
            ->field('label')->string()->required()->maxLength(300);;;
    }

    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function isGranted(string $role)
    {
        return ($loggedUser = $this->tokenStorage->getToken()->getUser()) !== null && $loggedUser->hasRole($role);
    }

    private function allowedAccess(Territory $territory): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::ADMIN)) {
            return true;
        } else {
            return $loggedUser->hasTerritory($territory);
        }
    }
}
