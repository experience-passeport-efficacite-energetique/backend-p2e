<?php

namespace Agilap\Controller\APIController;

trait CleanupParametersTrait
{
    // returns an array of parameters
    // params are check against current user's rights and a whitelist of parameters
    public function getCleanParameters($currentUser, $parameters): array
    {
        $cleanParams = [];

        // Handle distance parameter.
        // If user is bound to a max distance from lead, add parameter to query.
        if ($currentUser->isDistanceBound() || array_key_exists('distance', $parameters)) {
            $cleanParams['max_distance'] = min(
                array_key_exists('distance', $parameters) ?
                    $parameters['distance']
                    : is_null($currentUser->getMaxFlyingDistance()) ? MAX_FLYING_DISTANCE : $currentUser->getMaxFlyingDistance(),
                MAX_FLYING_DISTANCE
            );
            $cleanParams['distance_ref_user_id'] = $currentUser->getId();
        }

        // Handle territories param
        if (array_key_exists('territory_ids', $parameters) && $currentUser->canSpecifyTerritory()) {
            $cleanParams['territory_ids'] = $parameters['territory_ids'];
        }
        // If user is bound to one or many territories, add parameter to query
        if ($currentUser->isTerritoryBound()) {
            $cleanParams['territory_ids'] = array_map(function ($t) {
                return $t->getId();
            }, $currentUser->getTerritories());
        }

        // Handle auditor param
        if (array_key_exists('auditor', $parameters) && $currentUser->canSpecifyAuditor()) {
            $cleanParams['auditor_id'] = $parameters['auditor'];
        }
        // If user is auditor, auditor_id MUST be the current user's ID
        if ($currentUser->isHigherRoleAuditor()) {
            $cleanParams['auditor_id'] = $currentUser->getId();
        }

        //handler owner param
        if (array_key_exists('owner', $parameters) && $currentUser->canSpecifyOwner()) {
            $cleanParams['owner_id'] = $parameters['owner'];
        }
        // If user is only basic user, owner_id MUST be the current user's ID
        if ($currentUser->isHigherRoleUser()) {
            $cleanParams['owner_id'] = $currentUser->getId();
        }

        //handle date param
        if (array_key_exists('before', $parameters)) {
            $date_before = strtotime($parameters['before']);
            if ($date_before) {
                $cleanParams['date_before'] =  date('Y-m-d H:i:s', strtotime($parameters['before']));
            }
        }
        if (array_key_exists('after', $parameters)) {
            $date_after = strtotime($parameters['after']);
            if ($date_after) {
                $cleanParams['date_after'] =  date('Y-m-d H:i:s', strtotime($parameters['after']));
            }
        }

        return $cleanParams;
    }
}
