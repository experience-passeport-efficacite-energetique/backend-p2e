<?php

namespace Agilap\Controller\APIController;

use Agilap\Controller\APIController\CleanupParametersTrait;
use Agilap\Enum\Adjacency;
use Agilap\Enum\AirtightnessType;
use Agilap\Enum\AtticType;
use Agilap\Enum\ClimateZone;
use Agilap\Enum\EPCSControlState;
use Agilap\Enum\EquipmentState;
use Agilap\Enum\Floors;
use Agilap\Enum\FloorType;
use Agilap\Enum\GlazingType;
use Agilap\Enum\InsulationType;
use Agilap\Enum\MaintenanceFrequency;
use Agilap\Enum\PassportType;
use Agilap\Enum\PoseType;
use Agilap\Enum\RehabilitationState;
use Agilap\Enum\Role;
use Agilap\Enum\SouthExposure;
use Agilap\Enum\SurfaceType;
use Agilap\Enum\TDFControlState;
use Agilap\Enum\TDFPresenceType;
use Agilap\Enum\TDFState;
use Agilap\Hydration\Hydrator\AddressHydrator;
use Agilap\Hydration\Parser\TDBMRetriever;
use Agilap\Hydration\Validator\AvailableEquipmentValidator;
use Agilap\Hydration\Validator\CacheResult;
use Agilap\Hydration\Validator\CallbackValidator;
use Agilap\Hydration\Validator\UndoneLeadValidator;
use Agilap\Misc\Pointer;
use Agilap\Model\Bean\Address;
use Agilap\Model\Bean\Passport;
use Agilap\Model\Bean\PassportAirtightnessData;
use Agilap\Model\Bean\PassportDhwData;
use Agilap\Model\Bean\PassportFloorData;
use Agilap\Model\Bean\PassportHeatingData;
use Agilap\Model\Bean\PassportRoofData;
use Agilap\Model\Bean\PassportTdfData;
use Agilap\Model\Bean\PassportThermostatData;
use Agilap\Model\Bean\PassportVentData;
use Agilap\Model\Bean\PassportVentilationData;
use Agilap\Model\Bean\PassportWallData;
use Agilap\Model\Bean\User;
use Agilap\Model\Dao\AddressDao;
use Agilap\Model\Dao\PassportDao;
use MetaHydrator\Exception\ValidationException;
use Porpaginas\Result;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMException;
use TheCodingMachine\TDBM\TDBMService;

class PassportAPIController extends AbstractRESTfulAPIController
{
    const GES_TARGET = 6; // Target for greenhouse gas emmission in kg CO2eq/m2.an

    /** @var TDBMService */
    private $tdbmService;

    /** @var PassportDao */
    private $dao;
    /** @var AddressDao */
    private $addressDao;
    /** @var TokenStorage */
    private $tokenStorage;

    public function __construct(TDBMService $tdbmService, TokenStorage $tokenStorage)
    {
        parent::__construct('passport');
        $this->tdbmService = $tdbmService;
        $this->dao = new PassportDao($tdbmService);
        $this->addressDao = new AddressDao($tdbmService);
        $this->tokenStorage = $tokenStorage;
    }

    use CleanupParametersTrait;

    /**
     * @param $id
     * @return \JsonSerializable
     * @throws TDBMException
     *
     * @OA\Get(
     *     path="/passports/{id}",
     *     tags={"Passports", "@GET"},
     *     description="Get the passport with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of passport to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the passport with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="passport",
     *                  ref="#/components/schemas/Passport"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        $passport = $this->dao->getById($id);
        if ($this->allowedAccess($passport)) {
            return $passport;
        } else {
            throw new AccessDeniedHttpException("You are not allowed to access passport($id)");
        }
    }

    /**
     * @OA\Get(
     *     path="/passports",
     *     tags={"Passports", "@GET"},
     *     description="Get a list of all passports accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         name="auditor",
     *         in="query",
     *         description="returns only the passports assigne to auditor with id `auditor`",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Parameter(
     *         name="owner",
     *         in="query",
     *         description="returns only the passports owned by user with id `owner`",
     *         required=false,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="List of passport returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="passports",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Passport")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        return $this->dao->superSearch($this->getCleanParameters($this->getLogged(), $params));
    }

    /**
     * @OA\Post(
     *     path="/passports",
     *     tags={"Passports", "@POST"},
     *     description="Create a passport",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="Passport to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Passport")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Passport created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="passport",
     *                  ref="#/components/schemas/Passport"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        $loggedUser = $this->getLogged();
        if (!$loggedUser->hasRole(Role::AUDITOR) && !$loggedUser->hasRole(Role::ADMIN)) {
            throw new AccessDeniedHttpException('You are not allowed to create a passport');
        }

        $hydrator = $this->hydrator();
        /** @var Passport $passport */
        $passport = $hydrator->hydrateNewObject($data, Passport::class);
        $passport->setCreationDate(new \DateTimeImmutable());
        $this->addressDao->fillAddress($passport->getAddress());
        if ($passport->getAuditor() === null) {
            $passport->setAuditor($loggedUser);
        }
        $this->dao->save($passport);
        return $passport;
    }

    /**
     * @param Passport $passport
     * @param array $data
     *
     * @OA\Patch(
     *     path="/passports/{id}",
     *     tags={"Passports", "@PATCH"},
     *     description="Update a passport",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of passport to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="Passport to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Passport")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated passport",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="passport",
     *                  ref="#/components/schemas/Passport"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($passport, array $data)
    {
        if (!$this->allowedUpdate($passport)) {
            throw new AccessDeniedHttpException("You are not allowed to update this passport");
        }
        $hydrator = $this->hydrator();
        $hydrator->hydrateObject($data, $passport);
        $passport->setModificationDate(new \DateTimeImmutable());
        $this->addressDao->fillAddress($passport->getAddress());
        $this->dao->save($passport);
        return;
    }

    /**
     * @OA\Delete(
     *     path="/passports/{id}",
     *     tags={"Passports", "@DELETE"},
     *     description="Delete passport with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of passport to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that passport has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedPassport"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($item): void
    {
        $this->dao->delete($item);
    }

    /**
     * @OA\Get(
     *     path="/passports/emissionTarget",
     *     tags={"Passports", "@GET"},
     *     description="Get the emission target for SNBC. Unit is kgCO2eq/m2/year",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="emission target",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="target",
     *                  type="number"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403")
     * )
     */
    public function getEmissionTarget(): JsonResponse
    {
        return new JsonResponse([
            'target' => self::GES_TARGET
        ]);
    }


    private function hydrator()
    {
        $hydrator = FluidHydrator::new()
            ->field('lead')->parser(new TDBMRetriever($this->tdbmService, 'leads'))->required()->validator(new UndoneLeadValidator());
        if ($this->getLogged()->hasRole(Role::ADMIN)) {
            $field = $hydrator->field('auditor')->parser(new TDBMRetriever($this->tdbmService, 'users'));
            if (!$this->getLogged()->hasRole(Role::AUDITOR)) {
                $field->required();
            }
        }
        $hydrator
            ->field('address')->subobject(Address::class)->hydrator(new AddressHydrator())
            ->field('type')->string()->required()->enum(PassportType::Enum)
            ->field('project')->string()
            ->field('climateZone')->string()->required()->enum(ClimateZone::Enum)
            ->field('altitude')->int()->required()
            ->field('heatedSurface')->int()
            ->field('constructionYear')->int()->required()
            ->field('surfaceType')->string()->enum(SurfaceType::Enum)
            ->field('adjacency')->string()->enum(Adjacency::Enum)
            ->field('floors')->string()->enum(Floors::Enum)
            ->field('southExposure')->string()->enum(SouthExposure::Enum)
            ->field('generalComments')->string()
            ->field('pec')->float()
            ->field('emissions')->float()
            ->field('wallsData')->subobject(PassportWallData::class)
            ->hydrator(FluidHydrator::new()
                ->field('state')->string()->enum(EquipmentState::Enum)->required()
                ->field('rehabilitation')->string()->enum(RehabilitationState::Enum)->required()->validator(new CacheResult($p = new Pointer()))
                ->field('rehabilitationYear')->int()->validator($this->requiredRehabilitationYear($p))
                ->field('surface')->int()->required()
                ->field('currentInsulation')->string()->enum(InsulationType::CurrentEnum)->required()
                ->field('plannedInsulation')->string()->enum(InsulationType::PlannedEnum)->required()
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('floorData')->subobject(PassportFloorData::class)
            ->hydrator(FluidHydrator::new()
                ->field('state')->string()->enum(EquipmentState::Enum)->required()
                ->field('floorType')->string()->enum(FloorType::Enum)->required()
                ->field('rehabilitation')->string()->enum(RehabilitationState::Enum)->required()->validator(new CacheResult($p = new Pointer()))
                ->field('rehabilitationYear')->int()->validator($this->requiredRehabilitationYear($p))
                ->field('surface')->int()->required()
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('roofData')->subobject(PassportRoofData::class)
            ->hydrator(FluidHydrator::new()
                ->field('state')->string()->enum(EquipmentState::Enum)->required()
                ->field('atticType')->string()->enum(AtticType::Enum)->required()
                ->field('atticTypePlanned')->bool()
                ->field('rehabilitation')->string()->enum(RehabilitationState::Enum)->required()->validator(new CacheResult($p = new Pointer()))
                ->field('rehabilitationYear')->int()->validator($this->requiredRehabilitationYear($p))
                ->field('surface')->int()->required()
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('ventsData')->subobject(PassportVentData::class)
            ->hydrator(FluidHydrator::new()
                ->field('state')->string()->enum(EquipmentState::Enum)->required()
                ->field('glazingType')->string()->enum(GlazingType::Enum)->required()
                ->field('poseType')->string()->enum(PoseType::Enum)->required()
                ->field('rehabilitation')->string()->enum(RehabilitationState::RequiredEnum)->required()->validator(new CacheResult($p = new Pointer()))
                ->field('rehabilitationYear')->int()->validator($this->requiredRehabilitationYear($p))
                ->field('surface')->int()->required()
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('ventilationData')->subobject(PassportVentilationData::class)
            ->hydrator(FluidHydrator::new()
                ->field('state')->string()->enum(EquipmentState::Enum)->required()
                ->field('currentVentilation')->parser(new TDBMRetriever($this->tdbmService, 'ventilations'))->required()
                ->field('rehabilitation')->string()->enum(RehabilitationState::RequiredEnum)->required()->validator(new CacheResult($p = new Pointer()))
                ->field('rehabilitationYear')->int()->validator($this->requiredRehabilitationYear($p))
                ->field('maintenance')->string()->enum(MaintenanceFrequency::Enum)->required()
                ->field('plannedVentilation')->parser(new TDBMRetriever($this->tdbmService, 'ventilations'))->required()->validator(new AvailableEquipmentValidator(true))
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('airtightnessData')->subobject(PassportAirtightnessData::class)
            ->hydrator(FluidHydrator::new()
                ->field('currentAirtightness')->string()->enum(AirtightnessType::CurrentEnum)->required()
                ->field('plannedAirtightness')->string()->enum(AirtightnessType::PlannedEnum)->required()
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('heatingData')->subobject(PassportHeatingData::class)
            ->hydrator(FluidHydrator::new()
                ->field('state')->string()->enum(EquipmentState::Enum)->required()
                ->field('currentHeating')->parser(new TDBMRetriever($this->tdbmService, 'heatings'))->required()->validator(new AvailableEquipmentValidator(false))
                ->field('rehabilitation')->string()->enum(RehabilitationState::RequiredEnum)->required()->validator(new CacheResult($p = new Pointer()))
                ->field('rehabilitationYear')->int()->validator($this->requiredRehabilitationYear($p))
                ->field('maintenance')->string()->enum(MaintenanceFrequency::Enum)->required()
                ->field('plannedHeating')->parser(new TDBMRetriever($this->tdbmService, 'heatings'))->required()->validator(new AvailableEquipmentValidator(true))
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field("thermostatData")->subobject(PassportThermostatData::class)
            ->hydrator(FluidHydrator::new()
                ->field('presence')->bool()->required()
                ->field('state')->string()->enum(EquipmentState::EnumWithNA)->required()
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('dhwData')->subobject(PassportDhwData::class)
            ->hydrator(FluidHydrator::new()
                ->field('state')->string()->enum(EquipmentState::Enum)->required()
                ->field('currentDhw')->parser(new TDBMRetriever($this->tdbmService, 'domestic_hot_waters'))->required()->validator(new AvailableEquipmentValidator(false))
                ->field('rehabilitation')->string()->enum(RehabilitationState::DHWEnum)->required()->validator(new CacheResult($p = new Pointer()))
                ->field('rehabilitationYear')->int()->validator($this->requiredRehabilitationYear($p))
                ->field('maintenance')->string()->enum(MaintenanceFrequency::Enum)->required()
                ->field('plannedDhw')->parser(new TDBMRetriever($this->tdbmService, 'domestic_hot_waters'))->required()->validator(new AvailableEquipmentValidator(true))
                ->field('comment')->string()
                ->field('projectComment')->string())
            ->field('tdfData')->subobject(PassportTdfData::class)
            ->hydrator(FluidHydrator::new()
                ->field('electricalWiringState')->string()->enum(TDFState::Enum)->required()
                ->field('electricalWiringLastControl')->string()->enum(TDFControlState::Enum)
                ->field('gasSystemState')->string()->enum(TDFState::Gaz_Specific_Enum)->required()
                ->field('gasSystemLastControl')->string()->enum(TDFControlState::Enum)
                ->field('plumbingState')->string()->enum(TDFState::Enum)->required()
                ->field('plumbingLastControl')->string()->enum(TDFControlState::Enum)
                ->field('leadPresence')->string()->enum(TDFPresenceType::Enum)->required()
                ->field('leadLastControl')->string()->enum(TDFControlState::Enum)
                ->field('asbestosPresence')->string()->enum(TDFPresenceType::Enum)->required()
                ->field('asbestosLastControl')->string()->enum(TDFControlState::Enum)
                ->field('termitesPresence')->string()->enum(TDFPresenceType::Enum)->required()
                ->field('termitesLastControl')->string()->enum(TDFControlState::Enum)
                ->field('epcs')->string()->enum(EPCSControlState::Enum)->required()
                ->field('septicTankIndividual')->bool()->required()
                ->field('septicTankLastControl')->string()->enum(TDFControlState::Enum)
                ->field('comment')->string());
        return $hydrator;
    }

    private function requiredRehabilitationYear(Pointer $p)
    {
        return new CallbackValidator(function ($value, $object) use ($p) {
            if (($p->value ?? ($object ? $object->getRehabilitation() : null)) === RehabilitationState::REPLACED && $value === null) {
                throw new ValidationException('This field is required');
            }
        });
    }

    private function getLogged(): ?User
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function allowedAccess(Passport $passport): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::USER) && $passport->getOwner() === $loggedUser) return true;

        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR)) {
            if ($passport->getAuditor() === $loggedUser) return true;
        }
        if ($loggedUser->hasRole(Role::ADMIN_TERRITORY) || $loggedUser->hasRole(Role::USER_TERRITORY)) {
            foreach ($loggedUser->getTerritories() as $t) {
                $territory_addresses = array_map(function ($a) {
                    return $a->getId();
                }, $t->getAddresses());
                if (in_array($passport->getAddress()->getId(), $territory_addresses)) {
                    return true;
                }
            }
        }

        return false;
    }

    private function allowedUpdate(Passport $passport): bool
    {
        $loggedUser = $this->getLogged();
        if ($loggedUser->hasRole(Role::ADMIN)) return true;
        if ($loggedUser->hasRole(Role::AUDITOR)) {
            if ($passport->getAuditor() === $loggedUser) return true;
        }

        return false;
    }
}
