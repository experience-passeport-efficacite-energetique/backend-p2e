<?php

namespace Agilap\Controller\APIController;

use Agilap\Hydration\Hydrator\PriceRangeHydrator;
use Agilap\Model\Bean\PriceRange;
use Agilap\Model\Bean\Ventilation;
use Agilap\Model\Dao\VentilationDao;
use Mouf\Hydrator\Hydrator;
use Porpaginas\Result;
use TheCodingMachine\FluidHydrator\FluidHydrator;
use TheCodingMachine\TDBM\TDBMService;

class VentilationAPIController extends AbstractRESTfulAPIController
{
    /** @var TDBMService  */
    private $tdbmService;

    /** @var VentilationDao */
    private $dao;

    public function __construct(TDBMService $tdbmService)
    {
        parent::__construct('ventilation');
        $this->tdbmService = $tdbmService;
        $this->dao = new VentilationDao($tdbmService);
    }

    /**
     * @OA\Get(
     *     path="/ventilations/{id}",
     *     tags={"Ventilations", "@GET"},
     *     description="Get the ventilation with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of ventilation to get",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the ventilation with the id `id`",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="ventilation",
     *                  ref="#/components/schemas/VentilationPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function get($id): \JsonSerializable
    {
        return $this->dao->getById($id);
    }

    /**
     * @OA\Get(
     *     path="/ventilations",
     *     tags={"Ventilations", "@GET"},
     *     description="Get a list of all ventilations accessible to the current user",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Response(
     *          response="200",
     *          description="List of ventilations returned",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="ventilations",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/VentilationPriceRange")
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function find(array $params): Result
    {
        return $this->dao->findAll();
    }

    /**
     * @OA\Post(
     *     path="/ventilations",
     *     tags={"Ventilations", "@POST"},
     *     description="Create a ventilation",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\RequestBody(
     *         description="Ventilation to create",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/VentilationPriceRange")
     *     ),
     *     @OA\Response(
     *          response="201",
     *          description="Ventilation created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="ventilation",
     *                  ref="#/components/schemas/VentilationPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function create(array $data): \JsonSerializable
    {
        $hydrator = $this->hydrator();
        /** @var Ventilation $ventilation */
        $ventilation = $hydrator->hydrateNewObject($data, Ventilation::class);
        $this->dao->save($ventilation);
        return $ventilation;
    }

    /**
     * @OA\Patch(
     *     path="/ventilations/{id}",
     *     tags={"Ventilations", "@PATCH"},
     *     description="Update a ventilation",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of ventilation to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\RequestBody(
     *         description="Ventilation to update",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/VentilationPriceRange")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns the updated ventilation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="ventilation",
     *                  ref="#/components/schemas/VentilationPriceRange"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function update($ventilation, array $data)
    {
        $hydrator = $this->hydrator();
        $hydrator->hydrateObject($data, $ventilation);
        $this->dao->save($ventilation);
    }

    /**
     * @OA\Delete(
     *     path="/ventilations/{id}",
     *     tags={"Ventilations", "@DELETE"},
     *     description="Delete ventilation with id `id`",
     *     security={
     *         {"JWTtoken": {}}
     *     },
     *     @OA\Parameter(
     *         description="ID of ventilation to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(ref="#/components/schemas/idParam" )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Confirmation message that ventilation has been deleted",
     *          @OA\JsonContent(ref="#/components/schemas/DeletedVentilation"),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=422, ref="#/components/responses/422")
     * )
     */
    public function delete($ventilation): void
    {
        $this->dao->delete($ventilation);
    }

    public function hydrator(): Hydrator
    {
        return FluidHydrator::new()
            ->field('label')->string()->required()->maxLength(300)
            ->field('priceRange')->subobject(PriceRange::class)->hydrator(new PriceRangeHydrator())
            ->field('longevity')->int()->min(1)
            ->field('available')->bool()
            ->field('resistance')->float();
    }
}
