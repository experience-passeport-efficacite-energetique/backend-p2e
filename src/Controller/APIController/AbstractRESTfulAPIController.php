<?php

namespace Agilap\Controller\APIController;

use Agilap\Exception\DataNotFoundException;
use Agilap\Response\ItemResponse;
use Agilap\Response\PageResponse;
use Doctrine\Common\Inflector\Inflector;
use MetaHydrator\Exception\HydratingException;
use Porpaginas\Result;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Intl\Exception\NotImplementedException;
use TheCodingMachine\TDBM\NoBeanFoundException;

/**
 * Abstract class providing routes for standard API methods:
 * - GET /
 * - GET /{id}
 * - POST /
 * - PATCH /{id}
 * - DELETE /{id}
 * You may remove or customize rights on these.
 * Default permissions are readonly access for 'USER' and read/write access for 'ADMIN'
 */
abstract class AbstractRESTfulAPIController implements ControllerProviderInterface
{
    /** @var string */
    protected $singularItem;

    /** @var string */
    protected $pluralItem;

    /** @var int */
    protected $perPage;

    /** @var bool */
    protected $hasIntegerId;

    /** @var array */
    protected $methods = [
        'get' => 'USER',
        'find' => 'USER',
        'create' => 'ADMIN',
        'update' => 'ADMIN',
        'delete' => 'ADMIN'
    ];

    /**
     * AbstractRESTfulAPIController constructor.
     * @param string|array $itemLabel May be a string or a 2-sized array (singular and plural item name).
     * @param int $perPage
     * @param bool $hasIntegerId
     */
    public function __construct($itemLabel = 'item', int $perPage = 10, bool $hasIntegerId = true)
    {
        if (is_array($itemLabel)) {
            $this->singularItem = $itemLabel[0];
            $this->pluralItem = $types ?? Inflector::pluralize($itemLabel[1]);
        } else {
            $this->singularItem = $itemLabel;
            $this->pluralItem = Inflector::pluralize($itemLabel);
        }
        $this->perPage = $perPage;
        $this->hasIntegerId = $hasIntegerId;
    }

    public function connect(Application $app)
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];
        // TODO : check permissions! (404 if not allowed? 403?)
        if (array_key_exists('get', $this->methods)) {
            $controllers->get('{id}', function ($id) {
                return $this->_get($id);
            });
        }
        if (array_key_exists('find', $this->methods)) {
            $controllers->get('', function (Request $request) {
                return $this->_find($request);
            });
        }
        if (array_key_exists('create', $this->methods)) {
            $controllers->post('', function (Request $request) {
                return $this->_create($request);
            });
        }
        if (array_key_exists('update', $this->methods)) {
            $controllers->patch('{id}', function ($id, Request $request) {
                return $this->_update($id, $request);
            });
        }
        if (array_key_exists('delete', $this->methods)) {
            $controllers->delete('{id}', function ($id) {
                return $this->_delete($id);
            });
        }

        if ($this->hasIntegerId) {
            $controllers->assert('id', '\d+');
        }

        return $controllers;
    }

    public function get($id): \JsonSerializable
    {
        throw new NotImplementedException("method not available");
    }
    public function find(array $params): Result
    {
        throw new NotImplementedException("method not available");
    }
    public function create(array $data): \JsonSerializable
    {
        throw new NotImplementedException("method not available");
    }
    public function update($item, array $data)
    {
        throw new NotImplementedException("method not available");
    }
    public function delete($item): void
    {
        throw new NotImplementedException("method not available");
    }

    protected function _get($id): Response
    {
        try {
            return new ItemResponse($this->get($id), $this->singularItem);
        } catch (NoBeanFoundException $e) {
            throw new DataNotFoundException("$this->singularItem($id) not found", $e);
        }
    }

    protected function _find(Request $request): Response
    {
        $params = $request->query->all();
        $perpage = isset($params['perpage']) ? $params['perpage'] * 1 : $this->perPage;
        $index = isset($params['index']) ? $params['index'] * 1 : 1;
        try {
            $result = $this->find($params);
        } catch (AccessDeniedHttpException $e) {
            throw $e;
        }
        $page = $result->take(($index - 1) * $perpage, $perpage);
        return new PageResponse($page, $this->pluralItem);
    }

    protected function _create(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true) ?? [];
            return new ItemResponse($this->create($data), $this->singularItem, 201);
        } catch (HydratingException $e) {
            return new JsonResponse([
                'message' => "cannot create $this->singularItem",
                'errors' => $e->getErrorsMap()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    protected function _update($id, Request $request): Response
    {
        try {
            $item = $this->get($id);
            $data = json_decode($request->getContent(), true) ?? [];
            try {
                $this->update($item, $data);
            } catch (AccessDeniedHttpException $e) {
                throw $e;
            }
            return new ItemResponse($item, $this->singularItem);
        } catch (HydratingException $e) {
            return new JsonResponse([
                'message' => "cannot update $this->singularItem",
                'errors' => $e->getErrorsMap()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (NoBeanFoundException $e) {
            throw new DataNotFoundException("$this->singularItem($id) not found", $e);
        }
    }

    protected function _delete($id): Response
    {
        try {
            $item = $this->get($id);
            $this->delete($item);
            return new JsonResponse([
                'message' => "$this->singularItem($id) has been deleted"
            ]);
        } catch (NoBeanFoundException $e) {
            throw new DataNotFoundException("$this->singularItem($id) not found", $e);
        }
    }
}
