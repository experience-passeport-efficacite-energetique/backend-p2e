<?php
namespace Agilap\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

class AbstractController
{
    public function extractQuery(Request $request): array
    {
        return $request->query->all();
    }

    public function extractRequest(Request $request): array
    {
        $values = json_decode($request->getContent(), true);
        if (!is_array($values)) {
            $values = $request->request->all();
        }
        return $values;
    }

    public function extractAll(Request $request): array
    {
        return array_merge($this->extractRequest($request), $this->extractQuery($request));
    }
}
