<?php

namespace Agilap\Controller;

use Symfony\Component\HttpFoundation\Request;

class SwaggerUIController extends AbstractController
{

    public function getApiDoc(Request $request)
    {

        $apiDef = file_get_contents(__DIR__.'/../../docs/api/OAS-p2e-api.json', FILE_USE_INCLUDE_PATH);

        return str_replace('"{{OAS_p2e}}"', $apiDef, file_get_contents(__DIR__.'/../../docs/api/apidoc.html', FILE_USE_INCLUDE_PATH));
    }
}
