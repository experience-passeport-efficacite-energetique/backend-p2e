<?php

use Agilap\Service\JWTService;
use Agilap\Service\MailHelper;
use Agilap\Service\MiddlewareService;
use Agilap\Service\Provider\SchemaVersionControlServiceProvider;
use Agilap\Service\Provider\TdbmServiceProvider;
use Agilap\Service\StaticDataService;
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new HttpFragmentServiceProvider());

$app->register(new DoctrineServiceProvider(), [
    'db.options' => [
        'host' => DB_HOST,
        'driver' => DB_DRIVER,
        'port' => DB_PORT,
        'dbname' => DB_NAME,
        'user' => DB_USER,
        'password' => DB_PASSWORD,
        'charset' => DB_CHARSET,
        'url' => DB_URL,
    ]
]);
$app->register(new \CHH\Silex\CacheServiceProvider, [
    'cache.options' => [
        'default' => [
            'driver' => \Doctrine\Common\Cache\ApcuCache::class
        ]
    ]
]);
$app->register(new TdbmServiceProvider(), [
    'tdbm.daoNamespace' => 'Agilap\Model\Dao',
    'tdbm.beanNamespace' => 'Agilap\Model\Bean'
]);
$app->register(new SchemaVersionControlServiceProvider(), [
    'schemaversion.file' => __DIR__ . '/../etc/db_schema.yml'
]);

$app['jwt.service'] = function () use ($app) {
    return new JWTService($app['tdbmService']);
};
$app['staticData'] = function () use ($app) {
    return new StaticDataService($app['db']);
};

$app['mail.helper'] = function () use ($app) {
    return new MailHelper();
};

$app['middleware.service'] = function () {
    return new MiddlewareService();
};
return $app;
