<?php

use Agilap\Command\CreateUserCommand;
use Agilap\Command\LoadDataCommand;
use Agilap\Command\SendLeadReminders;
use BrainDiminished\SchemaVersionControl\Command\ApplySchemaCommand;
use BrainDiminished\SchemaVersionControl\Command\DumpSchemaCommand;
use BrainDiminished\SchemaVersionControl\Command\SchemaStatusCommand;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputOption;
use TheCodingMachine\TDBM\Commands\GenerateCommand;

$console = new ConsoleApplication('My Silex Application', 'n/a');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);
$console->add(new GenerateCommand($app['tdbm.configuration']));
$console->add(new SchemaStatusCommand($app['schemaversion']));
$console->add(new ApplySchemaCommand($app['schemaversion']));
$console->add(new DumpSchemaCommand($app['schemaversion']));
$console->add(new CreateUserCommand($app));
$console->add(new LoadDataCommand($app['staticData']));
$console->add(new SendLeadReminders($app));

return $console;
