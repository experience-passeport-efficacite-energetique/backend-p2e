<?php

use Agilap\Security\BasicAuthenticator;
use Agilap\Security\JWTAuthenticator;
use Agilap\Service\UserProvider;
use Symfony\Component\HttpFoundation\RequestMatcher;

$app['users'] = function () use($app) { return new UserProvider($app['tdbmService']); };

$app['security.jwt.authenticator'] = function () { return new JWTAuthenticator(); };
$authenticators = ['security.jwt.authenticator'];
if (ALLOW_BASIC_AUTH) {
    $app['security.basic.authenticator'] = function () { return new BasicAuthenticator(); };
    $authenticators[] = 'security.basic.authenticator';
}

$firewalls = [];
if (DEV) {
    $firewalls['cache'] = ['pattern' => '^/clear_cache'];
}
$firewalls['authentication'] = ['pattern' => '^/auth$'];
$firewalls['recover'] = ['pattern' => '^/account/'];
$firewalls['doc'] = ['pattern' => '^/doc'];
$firewalls['preflight'] = ['pattern' => new RequestMatcher(null, null, ['OPTIONS'])];
$firewalls['main'] = [
    'security' => true,
    'users' => function () use ($app) {
        return $app['users'];
    },
    'guard' => [
        'authenticators' => $authenticators,
        'entry_point' => 'security.jwt.authenticator'
    ]
];

$app->register(new Silex\Provider\SecurityServiceProvider(), ['security.firewalls' => $firewalls]);
