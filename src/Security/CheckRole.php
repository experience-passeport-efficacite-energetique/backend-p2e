<?php

namespace Agilap\Security;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CheckRole
{
    static function required(...$roles) {
        return function(Request $request, Application $app) use ($roles) {
            /** @var TokenStorage $storage */
            $storage = $app['security.token_storage'];
            $user = $storage->getToken()->getUser();
            foreach ($roles as $role) {
                if ($user->hasRole($role)) {
                    return;
                }
            }
            throw new AccessDeniedHttpException();
        };
    }
}