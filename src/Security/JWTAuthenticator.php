<?php

namespace Agilap\Security;

use Agilap\Model\Bean\User;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class JWTAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $authUrl = $request->getSchemeAndHttpHost().$request->getBaseUrl().'/auth';
        return new JsonResponse([
            'message' => 'Authorization header required'
        ], Response::HTTP_UNAUTHORIZED, [
            'WWW-Authenticate' => "Bearer realm=\"$authUrl\""
            ]);
        }
        
    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request){
        $jwt = $this->getCredentialsFromHeaders($request) ?? $this->getCredentialsFromQuery($request);
        return $jwt!=null;
    }
    
    /**
     * @param Request $request
     *
     * @return mixed|null
     */
    public function getCredentials(Request $request)
    {
        $jwt = $this->getCredentialsFromHeaders($request) ?? $this->getCredentialsFromQuery($request);
        $parser = new Parser();
        try {
            $token = $parser->parse($jwt);
        } catch (\Exception $exception) {
            throw new BadCredentialsException();
        }
        if (!$token->verify(new Sha512(), API_SECRET_KEY)) {
            throw new BadCredentialsException();
        }

        return [
            'token' => $token
        ];
    }

    private function getCredentialsFromHeaders(Request $request)
    {
        $auth = $request->headers->get('Authorization');
        if (empty($auth)) {
            return null;
        }
        if (!preg_match('(^Bearer *)', $auth, $matches)) {
            return null;
        }
        return substr($auth, strlen($matches[0]));
    }

    private function getCredentialsFromQuery(Request $request)
    {
        return $request->query->get('jwt', null);
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @throws AuthenticationException
     *
     * @return UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var Token $token */
        $token = $credentials['token'];
        $issuer = strval($token->getClaim('iss'));
        return $userProvider->loadUserByUsername($issuer);
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     *
     * @throws AuthenticationException
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        /** @var Token $token */
        $token = $credentials['token'];

        $validationData = new ValidationData();
        $validationData->setId($user->getApiKey());
        $validationData->setAudience(API_AUDIENCE);
        if (!$token->validate($validationData)) {
            return false;
        }

        $timeValidationData = new ValidationData();
        $timeValidationData->setCurrentTime(time());
        if (!$token->validate($timeValidationData)) {
            throw new CredentialsExpiredException();
        }

        return true;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $authUrl = $request->getSchemeAndHttpHost().$request->getBaseUrl().'/auth';
        switch (true) {
            case $exception instanceof CredentialsExpiredException:
                $error = 'Your credentials have expired';
                break;
            default:
                $error = 'Invalid credentials provided';
                break;

        }
        return new JsonResponse([
            'message' => 'Authentication failed',
            'error' => $error
        ], Response::HTTP_UNAUTHORIZED, [
            'WWW-Authenticate' => "Bearer realm=\"$authUrl\""
        ]);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // do nothing
        return null;
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
