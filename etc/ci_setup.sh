#!/bin/bash
set -e

# Install git, the php image doesn't have installed
apt-get update -yqq
apt-get install git curl unzip wget gnupg mariadb-client -yqq
docker-php-ext-install pdo_mysql
git config --global user.email "p2e@gitlab-ci.com"
git config --global user.name "P2E"

# # Install composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# APCU Install
pecl install apcu && docker-php-ext-enable apcu
echo "apc.enable_cli=On" >> /usr/local/etc/php/conf.d/docker-php-ext-apcu.ini

# Xdebug install
pecl install xdebug && docker-php-ext-enable xdebug

# # Install nodeJS and newman
curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -y nodejs build-essential
npm install newman --global

a2enmod rewrite
service apache2 restart
