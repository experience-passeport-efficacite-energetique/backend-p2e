#!/bin/bash

composer install;
sleep 5;
php bin/console schema:apply --assume-yes;
php bin/console db:load etc/db_static_data.yml;
exec apache2-foreground;
